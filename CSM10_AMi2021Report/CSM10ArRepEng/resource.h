//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by CSM10ArRepEng.rc
//
#define IDD_ABOUTBOX                    100
#define CG_IDD_CARTELLA                 102
#define CG_ID_VIEW_CARTELLA             103
#define IDR_POPUP_TREEFILEVIEW          114
#define IDR_MAINFRAME                   128
#define IDR_CSM20KTYPE                  129
#define IDR_MAINFRAME2                  129
#define IDR_TREEFILECTRL_NO_DROPMOVE    132
#define IDD_REPORT1                     132
#define IDR_TREEFILECTRL_NO_DROPCOPY    133
#define IDR_TREEFILECTRL_DROPCOPY       134
#define IDD_SEL_REPORT_TYPE             156
#define IDB_LOGOEDS                     158
#define IDD_COLOR_DIALOG                162
#define IDC_DIR_TREE                    1000
#define IDC_FILE_LIDT                   1001
#define IDC_REPORT_LABEL                1003
#define IDC_PATH1                       1004
#define IDC_PATH2                       1005
#define IDC_BROWSE1                     1006
#define IDC_BROWSE2                     1007
#define IDC_RADIO1                      1008
#define IDC_RADIO2                      1009
#define IDC_CHECK_DIFETTI_PER1          1016
#define IDC_CHECK_MAPPA1                1017
#define IDC_CHECK_DOWN_WEB              1018
#define IDC_CHECK_ALLARMI1              1019
#define IDC_CHECK_CROSS_WEB             1020
#define IDC_CHECK_DIFETTI_RAND          1021
#define IDC_CHECK_THRESHOLD             1022
#define IDC_CHECK_COMPRESSION           1023
#define IDC_DALMETRO                    1063
#define IDC_ALMETRO                     1064
#define IDC_VERSIONE                    1065
#define IDC_INTERV0_10                  1065
#define IDC_INTERV10_50                 1066
#define IDC_INTERV50_90                 1067
#define IDC_REPORT_TYPE                 1099
#define IDC_STRIP_REPORT                1100
#define IDC_ELEMENTI                    1101
#define IDC_ALZATE                      1102
#define IDC_STRIP                       1103
#define IDC_STATIC_FROM                 1140
#define IDC_POS_FROM                    1141
#define IDC_STATIC_TO                   1142
#define IDC_POS_TO                      1143
#define IDC_COLOR_A                     1150
#define IDC_COLOR_B                     1151
#define IDC_COLOR_C                     1152
#define IDC_COLOR_D                     1153
#define IDC_COLOR_BIG                   1154
#define IDC_BUTTON_COLORA               1155
#define IDC_BUTTON_COLORB               1156
#define IDC_BUTTON_COLORC               1157
#define IDC_BUTTON_COLORD               1158
#define IDC_BUTTON_COLORBIG             1159
#define ID_FILE_RENAME                  32771
#define ID_CONFIGURA_REPORT             32772
#define ID_VIEW_CSM20                   32773
#define ID_FILE_DELETE                  32774
#define ID_FILE_PROPERTIES              32775
#define ID_FILE_CONFIGURE_COLOR         32776
#define ID_VIEW_KREPORT                 32829
#define CSM_GRAPHVIEW_PRINTCAPTION0     44465
#define CSM_GRAPHVIEW_CAPTION0          44466
#define CSM_GRAPHVIEW_ROTOLO            44467
#define CSM_GRAPHVIEW_LEGA              44468
#define CSM_GRAPHVIEW_LUNGHEZZA         44469
#define CSM_GRAPHVIEW_LARGHEZZA         44470
#define CSM_GRAPHVIEW_CLIENTE           44471
#define CSM_GRAPHVIEW_DATETIMEFORMAT    44472
#define CSM_GRAPHVIEW_DATE              44473
#define CSM_GRAPHVIEW_RICETTA           44474
#define CSM_GRAPHVIEW_DETDIF            44475
#define CSM_GRAPHVIEW_TOTDIF            44476
#define CSM_GRAPHVIEW_DETTAGLIODIF      44477
#define CSM_GRAPHVIEW_DETTAGLIOSOGLIE   44478
#define CSM_GRAPHVIEW_REPORTPAGINA      44479
#define CSM_GRAPHVIEW_ALLARMISISTEMA    44504
#define CSM_GRAPHVIEW_THICK             44510
#define CSM_RESVERDLL                   44540
#define AFX_IDS_APP_TITLE2              57345
#define AFX_IDS_IDLEMESSAGE2            57346
#define ID_FILE_LIST_HEADER             61446
#define ID_CSM_REPORT_NUMFORI           61447
#define ID_CSM_REPORT_CLASSE            61448
#define ID_CSM_REPORT_BANDA             61449
#define ID_CSM_REPORT_TOTALE_FORI       61450
#define ID_CSM_REPORT_SOGLIE            61451
#define ID_CSM_REPORT_TOTALE_FORI_X_INTERVALLO 61452
#define ID_CSM_REPORT_FORI_X_INTERVALLO 61453
#define ID_CSM_REPORT_METRO             61546
#define ID_CSM_REPORT_TOP_INTESTAZIONE  61547
#define CSM_GRAPHDOC_ALCLUSTER          61548

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        11000
#define _APS_NEXT_COMMAND_VALUE         32773
#define _APS_NEXT_CONTROL_VALUE         11001
#define _APS_NEXT_SYMED_VALUE           11000
#endif
#endif
