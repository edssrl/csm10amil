// MainFrm.cpp : implementation of the CMainFrame class
//

#include "stdafx.h"

#include "../CSM10_AMi2021/Profile.h"


#include "CSM10ArReport.h"
#include "CSM10ArReportDoc.h"

#include "../CSM10_AMi2021/dbset.h"
#include "../CSM10_AMi2021/dialog.h"

#include "MainFrm.h"
#include "../CSM10_AMi2021/DClassColor.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


/////////////////////////////////////////////////////////////////////////////
// CMainFrame

IMPLEMENT_DYNCREATE(CMainFrame, CFrameWnd)

BEGIN_MESSAGE_MAP(CMainFrame, CFrameWnd)
	ON_COMMAND_EX(CG_ID_VIEW_CARTELLA, OnBarCheck)
	ON_UPDATE_COMMAND_UI(CG_ID_VIEW_CARTELLA, OnUpdateControlBarMenu)
	//{{AFX_MSG_MAP(CMainFrame)
	ON_WM_CREATE()
	ON_NOTIFY(NM_DBLCLK, IDC_FILE_LIDT, OnDblclkFileLidt)
	ON_COMMAND(ID_VIEW_CSM20, OnViewCsm20)
	ON_COMMAND(ID_FILE_CONFIGURE_COLOR, OnFileConfigureColor)
	ON_BN_CLICKED(IDC_BROWSE1, OnBrowse1)
	//}}AFX_MSG_MAP
	ON_COMMAND(ID_CONFIGURA_REPORT, &CMainFrame::OnConfigReport)
END_MESSAGE_MAP()

static UINT indicators[] =
{
	ID_SEPARATOR,           // status line indicator
	ID_INDICATOR_CAPS,
	ID_INDICATOR_NUM,
	ID_INDICATOR_SCRL,
}; 

/////////////////////////////////////////////////////////////////////////////
// CMainFrame construction/destruction
void CMainFrame::OnBrowse1()
{

m_wndDirTree.OnBrowse1();
}

CMainFrame::CMainFrame()
{
	// TODO: add member initialization code here
	
}

CMainFrame::~CMainFrame()
{
}


CProgressCtrl* CMainFrame::getProgressCtrl(void)
	{
	return (NULL);
	}
BOOL CMainFrame::progressCreate(void)
	{
	return (TRUE);
	}
void CMainFrame::progressDestroy(void)
{;}
void CMainFrame::progressSetRange(int from,int to)
{;}
void CMainFrame::progressSetStep(int s)
{;}
void CMainFrame::progressStepIt(void)
{;}


int CMainFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CFrameWnd::OnCreate(lpCreateStruct) == -1)
		return -1;
	
	if (!m_wndToolBar.CreateEx(this, TBSTYLE_FLAT, WS_CHILD | WS_VISIBLE | CBRS_TOP
		| CBRS_GRIPPER | CBRS_TOOLTIPS | CBRS_FLYBY | CBRS_SIZE_DYNAMIC) ||
		!m_wndToolBar.LoadToolBar(IDR_MAINFRAME))
	{
		TRACE0("Failed to create toolbar\n");
		return -1;      // fail to create
	}

	if (!m_wndStatusBar.Create(this) ||
		!m_wndStatusBar.SetIndicators(indicators,
		  sizeof(indicators)/sizeof(UINT)))
	{
		TRACE0("Failed to create status bar\n");
		return -1;      // fail to create
	}

	// TODO: Delete these three lines if you don't want the toolbar to
	//  be dockable
	m_wndToolBar.EnableDocking(CBRS_ALIGN_ANY);
	EnableDocking(CBRS_ALIGN_ANY);
	DockControlBar(&m_wndToolBar);

	// TODO: Add a menu item that will toggle the visibility of the
	// dialog bar named "Cartella":
	//   1. In ResourceView, open the menu resource that is used by
	//      the CMainFrame class
	//   2. Select the View submenu
	//   3. Double-click on the blank item at the bottom of the submenu
	//   4. Assign the new item an ID: CG_ID_VIEW_CARTELLA
	//   5. Assign the item a Caption: Cartella

	// TODO: Change the value of CG_ID_VIEW_CARTELLA to an appropriate value:
	//   1. Open the file resource.h
	// CG: The following block was inserted by the 'Dialog Bar' component
	{

		// Initialize dialog bar m_wndDirTreeCtrl
		if (!m_wndDirTree.Create(this, CG_IDD_CARTELLA,
			CBRS_SIZE_DYNAMIC | CBRS_LEFT | CBRS_TOOLTIPS | CBRS_FLYBY | CBRS_HIDE_INPLACE,
			CG_ID_VIEW_CARTELLA))
		{
			TRACE0("Failed to create dialog bar m_wndDirTreeCtrl\n");
			return -1;		// fail to create
		}

		m_wndDirTree.EnableDocking(CBRS_ALIGN_LEFT | CBRS_ALIGN_RIGHT);
		EnableDocking(CBRS_ALIGN_ANY);
		DockControlBar(&m_wndDirTree);
	}

// load dockState -> dialogBar
loadDockState();



// Init Server Process
CString strValue;
CProfile profile;
strValue = profile.getProfileString(_T("DataBase"), _T("Definition"), 
			_T("DbHole.def"));

// Init DataBase Object And Open It
dBase->load(strValue);
strValue = profile.getProfileString(_T("DataBase"), _T("Name"), 
			_T("HOLE.MDB"));

try{dBase->Open(strValue);}
catch (CDaoException *e)
	{
	AfxGetMainWnd()->MessageBox (e->m_pErrorInfo->m_strDescription,_T("dBase.Open"));
	return -1;
	}


return 0;
}

BOOL CMainFrame::PreCreateWindow(CREATESTRUCT& cs)
{
static LPCTSTR className = NULL;

if( !CFrameWnd::PreCreateWindow(cs) )
	return FALSE;
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs


if (className == NULL) 
	{
    // One-time class registration
    // The only purpose is to make the class name something meaningful
    // instead of "Afx:0x4d:27:32:hup1hup:hike!"
    WNDCLASS wndcls;
	::GetClassInfo(AfxGetInstanceHandle(), cs.lpszClass, &wndcls);
  	wndcls.lpszClassName = CSM_REPORT_CLASSNAME;
    wndcls.hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
    VERIFY(AfxRegisterClass(&wndcls));
    className = CSM_REPORT_CLASSNAME;
    }
cs.lpszClass = className;
// Not system menu
// cs.style &= ~WS_SYSMENU;

	return TRUE;
}

/////////////////////////////////////////////////////////////////////////////
// CMainFrame diagnostics

#ifdef _DEBUG
void CMainFrame::AssertValid() const
{
	CFrameWnd::AssertValid();
}

void CMainFrame::Dump(CDumpContext& dc) const
{
	CFrameWnd::Dump(dc);
}

#endif //_DEBUG


BOOL CMainFrame::Pump (void)
{
MSG msg;
//
// Retrieve and dispatch any waiting messages.
//
while (::PeekMessage (&msg, NULL, 0, 0, PM_NOREMOVE)) 
	{
	if (!AfxGetApp ()->PumpMessage ()) 
		{
		::PostQuitMessage (0);
		return FALSE;
		}
	}

//
// Simulate the framework's idle processing mechanism.
//
LONG lIdle = 0;
while (AfxGetApp ()->OnIdle (lIdle++));
return TRUE;
}




/////////////////////////////////////////////////////////////////////////////
// CMainFrame message handlers

void CMainFrame::OnDblclkFileLidt(NMHDR* pNMHDR, LRESULT* pResult) 
{
// TODO: Add your control notification handler code here

if (pNMHDR->code == NM_DBLCLK) 
	{
	DWORD dwPos;
	LVHITTESTINFO hti; 
	dwPos = GetMessagePos ();
	int x,y;

    hti.pt.x = (int) LOWORD (dwPos);
    x = (int) LOWORD (dwPos);
	hti.pt.y = (int) HIWORD (dwPos);
	y = (int) HIWORD (dwPos);
    m_wndDirTree.m_fileList.ScreenToClient (&hti.pt);
	CPoint p = hti.pt;
	int row = m_wndDirTree.m_fileList.HitTestEx (p,NULL);
	// get filePathName
	CString filePath = m_wndDirTree.field2filePath(row);
	

	CFileBpe cf;
	CFileException e;
	
	// open
	if( !cf.Open((LPCTSTR) filePath, CFile::modeRead | CFile::typeBinary, &e ) )
		{
		AfxGetMainWnd()->MessageBox(_T("ERROR OPEN"),_T("Error"));
		return;
		}

	CCSM10ArReportDoc* pDoc;
	pDoc = (CCSM10ArReportDoc*) GetActiveDocument();
	
	if (pDoc != NULL)
		pDoc->load(&cf);
	cf.Close();

	int pos = filePath.ReverseFind('\\');
	if (pos < 0)
		pos = filePath.ReverseFind('/');

	if (pos >= 0)
		{
		CString s;
		s = filePath.Right(filePath.GetLength() - (pos+1));
		pDoc->setTitle(s);
		}

	}
	

*pResult= NULL;
}
   

void CMainFrame::OnViewCsm20() 
{
// TODO: Add your command handler code here

CWnd *csm20 = CWnd::FindWindow(CSM_CLASSNAME,NULL);
if (csm20)
	{
	// close	
	this->ShowWindow(SW_SHOWMINIMIZED);

	// already open set zOrder TopMost	
	csm20->ShowWindow(SW_SHOWMAXIMIZED);
	csm20->SetFocus( );
	csm20->SetWindowPos(&CWnd::wndTop, 0, 0, 0, 0,
      SWP_NOMOVE|SWP_NOSIZE|SWP_SHOWWINDOW);
	}

}

BOOL CMainFrame::DestroyWindow() 
{
	
// TODO: Add your specialized code here and/or call the base class

	
if (dBase != NULL)
	{
	dBase->Close();
	delete dBase;
	dBase = NULL;
	}
	
return CFrameWnd::DestroyWindow();
}


void	CMainFrame::saveDockState(void)
{

m_wndDirTree.saveToProfile(_T("DockState"));


CDockState dockState;

// Get dock state
GetDockState (dockState);
// Save into profile
dockState.SaveState (_T("DockState"));
	
}


void	CMainFrame::loadDockState(void)
{

m_wndDirTree.loadFromProfile(_T("DockState"));

CDockState dockState;
// Load default state
GetDockState (dockState);

CProfile profile;
if (profile.getProfileBool (_T("DockState"),_T("LoadFromProfile"),FALSE))
	{
	// Try load from profile
	dockState.LoadState (_T("DockState"));
	if (dockState.GetVersion() > 1)
		SetDockState (dockState);
	}
else
	{
	dockState.Clear();
	dockState.SaveState(_T("DockState"));
	}

}



void CMainFrame::OnFileConfigureColor() 
{
// TODO: Add your command handler code here

CDClassColor dialog;

dialog.loadFromProfile();

if (dialog.DoModal() == IDOK)
	{
	dialog.saveToProfile();
	CSM20ClassColorInvalidate();
	}
}


void CMainFrame::OnConfigReport()
{
DBReport dbReport(dBase);
CDReport dialog;
// dBase already Open

if (dbReport.openSelectDefault ())
	{
	// Move Init Data from Db to Dialog
	dialog.m_Allarmi = dbReport.m_SOGLIE_ALLARME;
	// dialog.m_Cliente = dbReport.m_DATI_CLIENTE;
	// dialog.m_DataLavoraz = dbReport.m_DATA_LAVORAZIONE;
	dialog.m_DifettiPer = dbReport.m_DESCR_PERIODICI;
	dialog.m_DifettiRand = dbReport.m_DESCR_RANDOM;
	// dialog.m_Lunghezza = dbReport.m_LUNGHEZZA_BOBINA;
	dialog.m_Mappa = dbReport.m_MAPPA100;
	dialog.m_alMetro = (int )dbReport.m_ALMETRO;
	dialog.m_dalMetro = (int )dbReport.m_DALMETRO;
	dialog.m_Intervallo1 = (int )dbReport.m_INTERVALLO1;
	dialog.m_Intervallo2 = (int )dbReport.m_INTERVALLO2;
	dialog.m_Intervallo3 = (int )dbReport.m_INTERVALLO3;
	dialog.m_crossWeb = dbReport.m_DESCR_CROSSWEB;
	dialog.m_downWeb = dbReport.m_DESCR_DOWNWEB;
	dialog.m_threshold = dbReport.m_DESCR_THRESHOLD;
	dialog.m_compression = dbReport.m_DESCR_COMPRESSION;
	
	if (dialog.DoModal() == IDOK)
		{
		try
			{
			dbReport.Edit();
			}
		catch (CDaoException *e)
			{
			AfxGetMainWnd()->MessageBox (e->m_pErrorInfo->m_strDescription,_T("dbReport.Open"));
			dbReport.Close();
			return ;
			}

		dbReport.m_SOGLIE_ALLARME = dialog.m_Allarmi;
		// dbReport.m_DATI_CLIENTE = dialog.m_Cliente;
		// dbReport.m_DATA_LAVORAZIONE = dialog.m_DataLavoraz;
		dbReport.m_DESCR_PERIODICI = dialog.m_DifettiPer;
		dbReport.m_DESCR_RANDOM = dialog.m_DifettiRand;
		// dbReport.m_LUNGHEZZA_BOBINA = dialog.m_Lunghezza;
		dbReport.m_MAPPA100 = dialog.m_Mappa;
		dbReport.m_ALMETRO = (double)dialog.m_alMetro;
		dbReport.m_DALMETRO = (double)dialog.m_dalMetro;
		dbReport.m_INTERVALLO1 = (double)dialog.m_Intervallo1;
		dbReport.m_INTERVALLO2 = (double)dialog.m_Intervallo2;
		dbReport.m_INTERVALLO3 = (double)dialog.m_Intervallo3;
		dbReport.m_DESCR_CROSSWEB = dialog.m_crossWeb;
		dbReport.m_DESCR_DOWNWEB = dialog.m_downWeb;
		dbReport.m_DESCR_THRESHOLD = dialog.m_threshold;
		dbReport.m_DESCR_COMPRESSION = dialog.m_compression;

		try
			{
			dbReport.Update();
			}
		catch (CDaoException *e)
			{
			AfxGetMainWnd()->MessageBox (e->m_pErrorInfo->m_strDescription,_T("dbReport.Open"));
			dbReport.Close();
			return ;
			}

		}
	}
dbReport.Close();

}

