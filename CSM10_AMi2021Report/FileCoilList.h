#if !defined(AFX_FILECOILLIST_H__C77338A1_6662_4EB6_8DC3_14F4A0CF4B31__INCLUDED_)
#define AFX_FILECOILLIST_H__C77338A1_6662_4EB6_8DC3_14F4A0CF4B31__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// FileCoilList.h : header file
//

#include "../../..\..\..\Library\UniCodeListCtrl/listctrl.h"

/////////////////////////////////////////////////////////////////////////////
// CFileCoilList window

//-------------------------------------------------------------
// SORT !!!!
// There is a second problem: even if we set the 32-bit 
// associated value of each item 
// to be the same as the item's index this example will still not work. 
// This is because the items will shift positions after each call 
// to MyCompareProc(...) and the 32-bit associated values 
// will no longer represent the item's index. 
// Vedi uso di CArray !!


struct	ListElement
{
CString		col0;
CString		col1;
CString		col2;
CString		col3;
};

class CFileCoilList : public gxListCtrl
{
// Construction
public:
	CFileCoilList();

// Attributes
public:

CArray	<ListElement , ListElement &> c_stringFileArray;

BOOL	c_colSorted[4];

// Operations
public:

	void AddFile(CString OrigFileName,CString folder);
	void AddFileFind(CFileFind& fileSearch) ;

	BOOL DeleteAllItems(void)
	{c_stringFileArray.RemoveAll();return(gxListCtrl::DeleteAllItems());};

	void sortFile(void){onColclick(0,0,0);};


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CFileCoilList)
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CFileCoilList();
	virtual void onColclick(int col, int x,int y);

	// Generated message map functions
protected:
	//{{AFX_MSG(CFileCoilList)
		// NOTE - the ClassWizard will add and remove member functions here.
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_FILECOILLIST_H__C77338A1_6662_4EB6_8DC3_14F4A0CF4B31__INCLUDED_)
