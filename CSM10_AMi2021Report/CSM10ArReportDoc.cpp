// CSM10ArReportDoc.cpp : implementation of the CCSM10ArReportDoc class
//

#include "stdafx.h"
#include "CSM10ArReport.h"
#include "MainFrm.h"

#define MAIN


#include "CSM10ArReportDoc.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif



/////////////////////////////////////////////////////////////////////////////
// CCSM10ArReportDoc

IMPLEMENT_DYNCREATE(CCSM10ArReportDoc, CLineDoc)

BEGIN_MESSAGE_MAP(CCSM10ArReportDoc, CLineDoc)
	//{{AFX_MSG_MAP(CCSM10ArReportDoc)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CCSM10ArReportDoc construction/destruction

CCSM10ArReportDoc::CCSM10ArReportDoc() 
{
	// TODO: add one-time construction code here

}

CCSM10ArReportDoc::~CCSM10ArReportDoc()
{
}

BOOL CCSM10ArReportDoc::OnNewDocument()
{
// non usiamo CLIneDoc::OnNewDocument())
// xChe` altrimenti cerca di inizializzare CMainFrm che e` diverso
if (!CDocument::OnNewDocument())
	return FALSE;

	// TODO: add reinitialization code here
	// (SDI documents will reuse this document)
//if (!UpdateBaseDoc())
//	return FALSE;


	return TRUE;
}



/////////////////////////////////////////////////////////////////////////////
// CCSM10ArReportDoc serialization

void CCSM10ArReportDoc::Serialize(CArchive& ar)
{
	CLineDoc::Serialize(ar);

}

/////////////////////////////////////////////////////////////////////////////
// CCSM10ArReportDoc diagnostics

#ifdef _DEBUG
void CCSM10ArReportDoc::AssertValid() const
{
	CLineDoc::AssertValid();
}

void CCSM10ArReportDoc::Dump(CDumpContext& dc) const
{
	CLineDoc::Dump(dc);
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CCSM10ArReportDoc commands

/*-----------------------------------------------------------------

			SWAPLONG () 		routine swap long format
	
-----------------------------------------------------------------*/

union bytelong
	{
	unsigned char byte [4];
	unsigned long word;
	};

void swaplong (unsigned long *vett,int number)
{
union bytelong local;
int i;

unsigned long *lfrom;
unsigned char *bto;

lfrom = vett;
bto = (unsigned char *) vett;

for (i=0;i<number;i++)
	{
	local.word = *lfrom;     /* carico valore in union */
	
	bto [0] = local.byte[3];	/* swap */
	bto [1] = local.byte[2];
	bto [2] = local.byte[1];
	bto [3] = local.byte[0];
	bto += sizeof (long);	/* next word */
	lfrom ++;
	}
}	


BOOL CCSM10ArReportDoc::CanCloseFrame(CFrameWnd* pFrame) 
{
// TODO: Add your specialized code here and/or call the base class

	
// Save docking control state
CMainFrame *mainFrame = (CMainFrame *) AfxGetMainWnd();
mainFrame->saveDockState();
	
return CLineDoc::CanCloseFrame(pFrame);
}
