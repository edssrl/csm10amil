# Microsoft Developer Studio Project File - Name="CSM10ArReport" - Package Owner=<4>
# Microsoft Developer Studio Generated Build File, Format Version 6.00
# ** DO NOT EDIT **

# TARGTYPE "Win32 (x86) Application" 0x0101

CFG=CSM10ArReport - Win32 Debug
!MESSAGE This is not a valid makefile. To build this project using NMAKE,
!MESSAGE use the Export Makefile command and run
!MESSAGE 
!MESSAGE NMAKE /f "CSM10ArReport.mak".
!MESSAGE 
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "CSM10ArReport.mak" CFG="CSM10ArReport - Win32 Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "CSM10ArReport - Win32 Release" (based on "Win32 (x86) Application")
!MESSAGE "CSM10ArReport - Win32 Debug" (based on "Win32 (x86) Application")
!MESSAGE 

# Begin Project
# PROP AllowPerConfigDependencies 0
# PROP Scc_ProjName ""
# PROP Scc_LocalPath ""
CPP=cl.exe
MTL=midl.exe
RSC=rc.exe

!IF  "$(CFG)" == "CSM10ArReport - Win32 Release"

# PROP BASE Use_MFC 6
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Release"
# PROP BASE Intermediate_Dir "Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 6
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "Release"
# PROP Intermediate_Dir "Release"
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MD /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_AFXDLL" /Yu"stdafx.h" /FD /c
# ADD CPP /nologo /MD /W3 /GX /O2 /D "CSM20A_REPORT" /D "CSM10_AR_REPORT" /D "NDEBUG" /D "WIN32" /D "_WINDOWS" /D "_AFXDLL" /D "_MBCS" /Yu"stdafx.h" /FD /c
# ADD BASE MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x409 /d "NDEBUG" /d "_AFXDLL"
# ADD RSC /l 0x409 /d "NDEBUG" /d "_AFXDLL"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 /nologo /subsystem:windows /machine:I386
# ADD LINK32 listctrl.lib /nologo /subsystem:windows /machine:I386 /libpath:"C:/User/Library/ListCtrl/release"

!ELSEIF  "$(CFG)" == "CSM10ArReport - Win32 Debug"

# PROP BASE Use_MFC 6
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "Debug"
# PROP BASE Intermediate_Dir "Debug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 6
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "Debug"
# PROP Intermediate_Dir "Debug"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MDd /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_AFXDLL" /Yu"stdafx.h" /FD /GZ /c
# ADD CPP /nologo /MDd /W3 /Gm /GX /ZI /Od /D "CSM10_AR_REPORT" /D "_DEBUG" /D "WIN32" /D "_WINDOWS" /D "_AFXDLL" /D "_MBCS" /Yu"stdafx.h" /FD /GZ /c
# ADD BASE MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x409 /d "_DEBUG" /d "_AFXDLL"
# ADD RSC /l 0x409 /d "_DEBUG" /d "_AFXDLL"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 /nologo /subsystem:windows /debug /machine:I386 /pdbtype:sept
# ADD LINK32 listctrl.lib /nologo /subsystem:windows /map /debug /machine:I386 /pdbtype:sept /libpath:"C:/User/Library/ListCtrl/Debug"

!ENDIF 

# Begin Target

# Name "CSM10ArReport - Win32 Release"
# Name "CSM10ArReport - Win32 Debug"
# Begin Group "Source Files"

# PROP Default_Filter "cpp;c;cxx;rc;def;r;odl;idl;hpj;bat"
# Begin Source File

SOURCE=..\CSM10_AR\CDaoDbCreate.cpp
# End Source File
# Begin Source File

SOURCE=..\CSM10_AR\CFileBpe.cpp
# End Source File
# Begin Source File

SOURCE=..\CSM10_AR\PrintLibSrc\CPage.cpp
# End Source File
# Begin Source File

SOURCE=..\CSM10_AR\PrintLibSrc\CPrinter.cpp
# End Source File
# Begin Source File

SOURCE=.\CSM10ArReport.cpp
# End Source File
# Begin Source File

SOURCE=.\CSM10ArReport.rc
# End Source File
# Begin Source File

SOURCE=.\CSM10ArReportDoc.cpp
# End Source File
# Begin Source File

SOURCE=.\CSM10ArReportView.cpp
# End Source File
# Begin Source File

SOURCE=..\CSM10_AR\DAllarmi.cpp
# End Source File
# Begin Source File

SOURCE=..\CSM10_AR\DbFori.cpp
# End Source File
# Begin Source File

SOURCE=..\CSM10_AR\DbInfoPlant.cpp
# End Source File
# Begin Source File

SOURCE=..\CSM10_AR\dbset.cpp
# End Source File
# Begin Source File

SOURCE=..\CSM10_AR\DClassColor.cpp
# End Source File
# Begin Source File

SOURCE=..\CSM10_AR\DCluster.cpp
# End Source File
# Begin Source File

SOURCE=..\CSM10_AR\PrintLibSrc\Dib.cpp
# End Source File
# Begin Source File

SOURCE=.\DirBar.cpp
# End Source File
# Begin Source File

SOURCE=..\CSM10_AR\dproduct.cpp
# End Source File
# Begin Source File

SOURCE=..\CSM10_AR\dyntempl.cpp
# End Source File
# Begin Source File

SOURCE=..\CSM10_AR\Endian.cpp
# End Source File
# Begin Source File

SOURCE=..\CSM10_AR\EnumPrinters.cpp
# End Source File
# Begin Source File

SOURCE=.\FileCoilList.cpp
# End Source File
# Begin Source File

SOURCE=.\FileTreeCtrl.cpp
# End Source File
# Begin Source File

SOURCE=..\CSM10_AR\GraphDoc.cpp
# End Source File
# Begin Source File

SOURCE=..\CSM10_AR\graphview.cpp
# End Source File
# Begin Source File

SOURCE=..\CSM10_AR\Label.cpp
# End Source File
# Begin Source File

SOURCE=.\MainFrm.cpp
# End Source File
# Begin Source File

SOURCE=..\CSM10_AR\Profile.cpp
# End Source File
# Begin Source File

SOURCE=..\CSM10_AR\pwdlg.cpp
# End Source File
# Begin Source File

SOURCE=.\ResizableDlgBar.cpp
# End Source File
# Begin Source File

SOURCE=.\StdAfx.cpp
# ADD CPP /Yc"stdafx.h"
# End Source File
# Begin Source File

SOURCE=..\CSM10_AR\Target.cpp
# End Source File
# End Group
# Begin Group "Header Files"

# PROP Default_Filter "h;hpp;hxx;hm;inl"
# Begin Source File

SOURCE=..\CSM10_AR\CDaoDbCreate.h
# End Source File
# Begin Source File

SOURCE=..\CSM10_AR\CFileBpe.h
# End Source File
# Begin Source File

SOURCE=..\CSM10_AR\PrintLibSrc\CPage.h
# End Source File
# Begin Source File

SOURCE=..\CSM10_AR\PrintLibSrc\CPrinter.h
# End Source File
# Begin Source File

SOURCE=.\CSM10ArReport.h
# End Source File
# Begin Source File

SOURCE=.\CSM10ArReportDoc.h
# End Source File
# Begin Source File

SOURCE=.\CSM10ArReportView.h
# End Source File
# Begin Source File

SOURCE=..\CSM10_AR\DAllarmi.h
# End Source File
# Begin Source File

SOURCE=..\CSM10_AR\DbFori.h
# End Source File
# Begin Source File

SOURCE=..\CSM10_AR\DbInfoPlant.h
# End Source File
# Begin Source File

SOURCE=..\CSM10_AR\dbset.h
# End Source File
# Begin Source File

SOURCE=..\CSM10_AR\DClassColor.h
# End Source File
# Begin Source File

SOURCE=..\CSM10_AR\DCluster.h
# End Source File
# Begin Source File

SOURCE=..\CSM10_AR\PrintLibSrc\Dib.h
# End Source File
# Begin Source File

SOURCE=.\DirBar.h
# End Source File
# Begin Source File

SOURCE=..\CSM10_AR\dproduct.h
# End Source File
# Begin Source File

SOURCE=..\CSM10_AR\dyntempl.h
# End Source File
# Begin Source File

SOURCE=..\CSM10_AR\Elemento.h
# End Source File
# Begin Source File

SOURCE=..\CSM10_AR\Endian.h
# End Source File
# Begin Source File

SOURCE=..\CSM10_AR\EnumPrinters.h
# End Source File
# Begin Source File

SOURCE=.\FileCoilList.h
# End Source File
# Begin Source File

SOURCE=.\FileTreeCtrl.h
# End Source File
# Begin Source File

SOURCE=..\CSM10_AR\graphdoc.h
# End Source File
# Begin Source File

SOURCE=..\CSM10_AR\graphview.h
# End Source File
# Begin Source File

SOURCE=..\CSM10_AR\Label.h
# End Source File
# Begin Source File

SOURCE=..\CSM10_AR\Layout.h
# End Source File
# Begin Source File

SOURCE=.\MainFrm.h
# End Source File
# Begin Source File

SOURCE=..\CSM10_AR\Profile.h
# End Source File
# Begin Source File

SOURCE=..\CSM10_AR\pwdlg.h
# End Source File
# Begin Source File

SOURCE=.\ResizableDlgBar.h
# End Source File
# Begin Source File

SOURCE=.\Resource.h
# End Source File
# Begin Source File

SOURCE=..\CSM10_AR\RpcMClient.h
# End Source File
# Begin Source File

SOURCE=.\StdAfx.h
# End Source File
# Begin Source File

SOURCE=..\CSM10_AR\Target.h
# End Source File
# Begin Source File

SOURCE=.\VERSIONE.H
# End Source File
# End Group
# Begin Group "Resource Files"

# PROP Default_Filter "ico;cur;bmp;dlg;rc2;rct;bin;rgs;gif;jpg;jpeg;jpe"
# Begin Source File

SOURCE=.\res\CSM10ArReport.ico
# End Source File
# Begin Source File

SOURCE=.\res\CSM10ArReport.rc2
# End Source File
# Begin Source File

SOURCE=.\res\CSM10ArReportDoc.ico
# End Source File
# Begin Source File

SOURCE=.\res\logoEds.bmp
# End Source File
# Begin Source File

SOURCE=.\res\tfdropcopy.cur
# End Source File
# Begin Source File

SOURCE=.\res\tfnodropcopy.cur
# End Source File
# Begin Source File

SOURCE=.\res\tfnodropmove.cur
# End Source File
# Begin Source File

SOURCE=.\res\Toolbar.bmp
# End Source File
# End Group
# Begin Group "Config"

# PROP Default_Filter "ini"
# Begin Source File

SOURCE=..\..\..\..\WINNT\CSM10ArReport.INI
# End Source File
# End Group
# Begin Source File

SOURCE=.\ReadMe.txt
# End Source File
# End Target
# End Project
