// CSM10ArRepCze.h : main header file for the CSM10ArRepCze DLL
//

#if !defined(AFX_CSM10ArRepCze_H__42C48640_FE81_43DC_9AEA_F47B5853E222__INCLUDED_)
#define AFX_CSM10ArRepCze_H__42C48640_FE81_43DC_9AEA_F47B5853E222__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"		// main symbols

/////////////////////////////////////////////////////////////////////////////
// CCSM10ArRepCzeApp
// See CSM10ArRepCze.cpp for the implementation of this class
//

class CCSM10ArRepCzeApp : public CWinApp
{
public:
	CCSM10ArRepCzeApp();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CCSM10ArRepCzeApp)
	//}}AFX_VIRTUAL

	//{{AFX_MSG(CCSM10ArRepCzeApp)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CSM10ArRepCze_H__42C48640_FE81_43DC_9AEA_F47B5853E222__INCLUDED_)
