; CLW file contains information for the MFC ClassWizard

[General Info]
Version=1
LastClass=CDirBar
LastTemplate=CDialog
NewFileInclude1=#include "stdafx.h"
NewFileInclude2=#include "CSM10ArReport.h"
LastPage=0

ClassCount=6
Class1=CCSM10ArReportApp
Class2=CCSM10ArReportDoc
Class3=CCSM10ArReportView
Class4=CMainFrame

ResourceCount=8
Resource1=IDD_ABOUTBOX
Resource2=IDR_MAINFRAME
Class5=CAboutDlg
Resource3=CG_IDD_CARTELLA
Resource4=IDR_POPUP_TREEFILEVIEW (English (Ireland))
Resource5=IDD_SEL_REPORT_TYPE
Class6=CDirBar
Resource6=IDD_COLOR_DIALOG
Resource7=IDD_ABOUTBOX (Italian (Italy))
Resource8=IDR_MAINFRAME (Italian (Italy))

[CLS:CCSM10ArReportApp]
Type=0
HeaderFile=CSM10ArReport.h
ImplementationFile=CSM10ArReport.cpp
Filter=N
BaseClass=CWinApp
VirtualFilter=AC
LastObject=CCSM10ArReportApp

[CLS:CCSM10ArReportDoc]
Type=0
HeaderFile=CSM10ArReportDoc.h
ImplementationFile=CSM10ArReportDoc.cpp
Filter=N
LastObject=CCSM10ArReportDoc
BaseClass=CLineDoc
VirtualFilter=DC

[CLS:CCSM10ArReportView]
Type=0
HeaderFile=CSM10ArReportView.h
ImplementationFile=CSM10ArReportView.cpp
Filter=C
LastObject=CCSM10ArReportView


[CLS:CMainFrame]
Type=0
HeaderFile=MainFrm.h
ImplementationFile=MainFrm.cpp
Filter=T
LastObject=CG_ID_VIEW_CARTELLA
BaseClass=CFrameWnd
VirtualFilter=fWC




[CLS:CAboutDlg]
Type=0
HeaderFile=CSM10ArReport.cpp
ImplementationFile=CSM10ArReport.cpp
Filter=D
LastObject=CAboutDlg
BaseClass=CDialog
VirtualFilter=dWC

[DLG:IDD_ABOUTBOX]
Type=1
ControlCount=4
Control1=IDC_STATIC,static,1342177283
Control2=IDC_STATIC,static,1342308352
Control3=IDC_STATIC,static,1342308352
Control4=IDOK,button,1342373889
Class=CAboutDlg

[MNU:IDR_MAINFRAME]
Type=1
Class=CMainFrame
Command3=ID_FILE_SAVE
Command4=ID_FILE_SAVE_AS
Command5=ID_FILE_PRINT
Command6=ID_FILE_PRINT_PREVIEW
Command7=ID_FILE_PRINT_SETUP
Command8=ID_FILE_MRU_FILE1
Command9=ID_APP_EXIT
Command10=ID_EDIT_UNDO
Command11=ID_EDIT_CUT
Command12=ID_EDIT_COPY
Command13=ID_EDIT_PASTE
Command14=ID_VIEW_TOOLBAR
Command15=ID_VIEW_STATUS_BAR
CommandCount=16
Command1=ID_FILE_NEW
Command2=ID_FILE_OPEN
Command16=ID_APP_ABOUT

[ACL:IDR_MAINFRAME]
Type=1
Class=CMainFrame
Command1=ID_FILE_NEW
Command2=ID_FILE_OPEN
Command3=ID_FILE_SAVE
Command4=ID_FILE_PRINT
Command5=ID_EDIT_UNDO
Command6=ID_EDIT_CUT
Command7=ID_EDIT_COPY
Command8=ID_EDIT_PASTE
Command9=ID_EDIT_UNDO
Command10=ID_EDIT_CUT
Command11=ID_EDIT_COPY
Command12=ID_EDIT_PASTE
CommandCount=14
Command13=ID_NEXT_PANE
Command14=ID_PREV_PANE


[TB:IDR_MAINFRAME (Italian (Italy))]
Type=1
Class=?
Command1=ID_FILE_PRINT
Command2=ID_VIEW_CSM20
Command3=ID_APP_ABOUT
CommandCount=3

[MNU:IDR_MAINFRAME (Italian (Italy))]
Type=1
Class=CMainFrame
Command1=ID_FILE_PRINT
Command2=ID_FILE_PRINT_PREVIEW
Command3=ID_FILE_PRINT_SETUP
Command4=ID_FILE_CONFIGURE_COLOR
Command5=ID_APP_EXIT
Command6=ID_VIEW_TOOLBAR
Command7=ID_VIEW_STATUS_BAR
Command8=CG_ID_VIEW_CARTELLA
Command9=ID_VIEW_CSM20
Command10=ID_APP_ABOUT
CommandCount=10

[MNU:IDR_POPUP_TREEFILEVIEW (English (Ireland))]
Type=1
Class=?
Command1=ID_FILE_OPEN
Command2=ID_FILE_RENAME
Command3=ID_FILE_DELETE
Command4=ID_FILE_PROPERTIES
CommandCount=4

[ACL:IDR_MAINFRAME (Italian (Italy))]
Type=1
Class=?
Command1=ID_EDIT_COPY
Command2=ID_FILE_NEW
Command3=ID_FILE_OPEN
Command4=ID_FILE_PRINT
Command5=ID_FILE_SAVE
Command6=ID_EDIT_PASTE
Command7=ID_EDIT_UNDO
Command8=ID_EDIT_CUT
Command9=ID_VIEW_CSM20
Command10=ID_NEXT_PANE
Command11=ID_PREV_PANE
Command12=ID_EDIT_COPY
Command13=ID_EDIT_PASTE
Command14=ID_EDIT_CUT
Command15=ID_EDIT_UNDO
CommandCount=15

[DLG:IDD_ABOUTBOX (Italian (Italy))]
Type=1
Class=CAboutDlg
ControlCount=4
Control1=IDC_STATIC,static,1342177283
Control2=IDC_VERSIONE,static,1342308480
Control3=IDC_STATIC,static,1342308352
Control4=IDOK,button,1342373889

[DLG:CG_IDD_CARTELLA]
Type=1
Class=CDirBar
ControlCount=9
Control1=IDC_DIR_TREE,SysTreeView32,1350631459
Control2=IDC_FILE_LIDT,SysListView32,1350631461
Control3=IDC_REPORT_LABEL,static,1342308865
Control4=IDC_PATH1,edit,1350633600
Control5=IDC_PATH2,edit,1350633600
Control6=IDC_BROWSE1,button,1073807360
Control7=IDC_BROWSE2,button,1073807360
Control8=IDC_RADIO1,button,1342308361
Control9=IDC_RADIO2,button,1342177289

[CLS:CDirBar]
Type=0
HeaderFile=DirBar.h
ImplementationFile=DirBar.cpp
BaseClass=CResizableDlgBar
Filter=D
VirtualFilter=dWC
LastObject=IDC_BROWSE1

[DLG:IDD_SEL_REPORT_TYPE]
Type=1
Class=?
ControlCount=14
Control1=IDOK,button,1342242817
Control2=IDCANCEL,button,1342242816
Control3=IDC_REPORT_TYPE,button,1342308361
Control4=IDC_STRIP_REPORT,button,1342177289
Control5=IDC_ELEMENTI,combobox,1342242817
Control6=IDC_STATIC,static,1342308352
Control7=IDC_ALZATE,combobox,1342242817
Control8=IDC_STRIP,combobox,1342242817
Control9=IDC_STATIC,static,1342308352
Control10=IDC_STATIC,static,1342308352
Control11=IDC_STATIC_FROM,static,1342308352
Control12=IDC_POS_FROM,edit,1350631552
Control13=IDC_STATIC_TO,static,1342308352
Control14=IDC_POS_TO,edit,1350631552

[DLG:IDD_COLOR_DIALOG]
Type=1
Class=?
ControlCount=17
Control1=IDOK,button,1342242817
Control2=IDCANCEL,button,1342242816
Control3=IDC_STATIC,static,1342308352
Control4=IDC_STATIC,static,1342308352
Control5=IDC_STATIC,static,1342308352
Control6=IDC_STATIC,static,1342308352
Control7=IDC_STATIC,static,1073872896
Control8=IDC_COLOR_A,edit,1350631552
Control9=IDC_COLOR_B,edit,1350631552
Control10=IDC_COLOR_C,edit,1350631552
Control11=IDC_COLOR_D,edit,1350631552
Control12=IDC_COLOR_BIG,edit,1082196096
Control13=IDC_BUTTON_COLORA,button,1342242816
Control14=IDC_BUTTON_COLORB,button,1342242816
Control15=IDC_BUTTON_COLORC,button,1342242816
Control16=IDC_BUTTON_COLORD,button,1342242816
Control17=IDC_BUTTON_COLORBIG,button,1073807360

