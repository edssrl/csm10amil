#pragma once

// Including SDKDDKVer.h defines the highest available Windows platform.
#define	 WINVER 0x0A00
#define _WIN32_WINNT 0x0A00
// If you wish to build your application for a previous Windows platform, include WinSDKVer.h and
// set the _WIN32_WINNT macro to the platform you wish to support before including SDKDDKVer.h.
#include <WinSDKVer.h>



#include <SDKDDKVer.h>

// disable warnings per strcpy - strcpy_s etc 
#define _CRT_SECURE_NO_WARNINGS

// disable warnings per old CDaoRecordset class
#pragma warning(disable : 4995)
// disable argument' : conversion from 'double' to 'int', possible loss of data
#pragma warning(disable : 4244)
