// CSM10ArReportView.cpp : implementation of the CCSM10ArReportView class
//

#include "stdafx.h"
#include "CSM10ArReport.h"

#include "CSM10ArReportDoc.h"
#include "CSM10ArReportView.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CCSM10ArReportView

IMPLEMENT_DYNCREATE(CCSM10ArReportView, CInitView)

BEGIN_MESSAGE_MAP(CCSM10ArReportView, CInitView)
	//{{AFX_MSG_MAP(CCSM10ArReportView)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG_MAP
	// Standard printing commands
	ON_COMMAND(ID_FILE_PRINT, CInitView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_DIRECT, CInitView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_PREVIEW, CInitView::OnFilePrintPreview)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CCSM10ArReportView construction/destruction

CCSM10ArReportView::CCSM10ArReportView()
{
	// TODO: add construction code here

}

CCSM10ArReportView::~CCSM10ArReportView()
{
}

BOOL CCSM10ArReportView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs

	return CInitView::PreCreateWindow(cs);
}

/////////////////////////////////////////////////////////////////////////////
// CCSM10ArReportView drawing

void CCSM10ArReportView::OnDraw(CDC* pDC)
{
	CCSM10ArReportDoc* pDoc = GetDocument();
	ASSERT_VALID(pDoc);
	// TODO: add draw code for native data here
	CInitView::OnDraw(pDC);
}


/////////////////////////////////////////////////////////////////////////////
// CCSM10ArReportView diagnostics

#ifdef _DEBUG
void CCSM10ArReportView::AssertValid() const
{
	CInitView::AssertValid();
}

void CCSM10ArReportView::Dump(CDumpContext& dc) const
{
	CInitView::Dump(dc);
}

CCSM10ArReportDoc* CCSM10ArReportView::GetDocument() // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CCSM10ArReportDoc)));
	return (CCSM10ArReportDoc*)m_pDocument;
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CCSM10ArReportView message handlers
