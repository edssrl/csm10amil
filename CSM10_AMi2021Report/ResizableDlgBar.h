/* Compile options needed: Default
   */ 

   // ResizableDlgBar.h : header file
   // 

#ifndef ResizableDlgBar
#define ResizableDlgBar

   class CResizableDlgBar : public CDialogBar
   {
   // Construction
   public:
        BOOL Create( CWnd* pParentWnd, UINT nIDTemplate, UINT nStyle,
                     UINT nID, BOOL = TRUE);
        BOOL Create( CWnd* pParentWnd, LPCTSTR lpszTemplateName,
                     UINT nStyle, UINT nID, BOOL = TRUE);

   // Attributes
   public:
        CSize m_sizeDocked;
        CSize m_sizeFloating;
        BOOL m_bChangeDockedSize;   // Indicates whether to keep
                                    // a default size for docking

   // Operations
   public:
	BOOL	saveToProfile(LPCTSTR strRoot);
	BOOL	loadFromProfile(LPCTSTR strRoot);

   // Overrides
       // ClassWizard generated virtual function overrides
       //{{AFX_VIRTUAL(CResizableDlgBar)
       //}}AFX_VIRTUAL
       virtual CSize CalcDynamicLayout( int nLength, DWORD dwMode );

   // Implementation
   public:

   // Generated message map functions
   protected:
       //{{AFX_MSG(CResizableDlgBar)
       // NOTE - the ClassWizard will add and remove member functions here.
       //}}AFX_MSG
       DECLARE_MESSAGE_MAP()
   };

#endif

   ///////////////////////////////////////////////////////////////////// 
