// CSM10ArReportView.h : interface of the CCSM10ArReportView class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_CSM10ArReportVIEW_H__DD37F537_F5BE_4699_9BD0_95A95CBE8D20__INCLUDED_)
#define AFX_CSM10ArReportVIEW_H__DD37F537_F5BE_4699_9BD0_95A95CBE8D20__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "../CSM10_AMi2021/graphview.h"

class CCSM10ArReportView : public CInitView
{
protected: // create from serialization only
	CCSM10ArReportView();
	DECLARE_DYNCREATE(CCSM10ArReportView)

// Attributes
public:
	CCSM10ArReportDoc* GetDocument();

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CCSM10ArReportView)
	public:
	virtual void OnDraw(CDC* pDC);  // overridden to draw this view
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	protected:
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CCSM10ArReportView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	//{{AFX_MSG(CCSM10ArReportView)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

#ifndef _DEBUG  // debug version in CSM10ArReportView.cpp
inline CCSM10ArReportDoc* CCSM10ArReportView::GetDocument()
   { return (CCSM10ArReportDoc*)m_pDocument; }
#endif

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CSM10ArReportVIEW_H__DD37F537_F5BE_4699_9BD0_95A95CBE8D20__INCLUDED_)
