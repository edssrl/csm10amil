// stdafx.h : include file for standard system include files,
//  or project specific include files that are used frequently, but
//      are changed infrequently
//

#if !defined(AFX_STDAFX_H__A2D2B9E5_B14D_4665_BD55_D6B1CEDDD2B4__INCLUDED_)
#define AFX_STDAFX_H__A2D2B9E5_B14D_4665_BD55_D6B1CEDDD2B4__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#define VC_EXTRALEAN		// Exclude rarely-used stuff from Windows headers

#include "targetver.h"

#include <vld.h>

#include <afxwin.h>         // MFC core and standard components
#include <afxext.h>         // MFC extensions
#include <afxdisp.h>        // MFC Automation classes
#include <afxdtctl.h>		// MFC support for Internet Explorer 4 Common Controls
#ifndef _AFX_NO_AFXCMN_SUPPORT
#include <afxcmn.h>			// MFC support for Windows Common Controls
#endif // _AFX_NO_AFXCMN_SUPPORT
#include <math.h>
#ifndef _AFX_NO_DAO_SUPPORT
#include <afxdao.h>			// MFC DAO database classes
#endif // _AFX_NO_DAO_SUPPORT

// Use for print preview ClipRgn
#include <afxpriv.h>
#include <math.h>

#include <afxcview.h>
#include <afxctl.h>

// SingleLock Multithread
#include <afxmt.h>

#include "../CSM10_AMi2021/Profile.h"

#include <afxtempl.h>

#define PI 3.14159265359

#define		CSM20AM_REPORT

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_STDAFX_H__A2D2B9E5_B14D_4665_BD55_D6B1CEDDD2B4__INCLUDED_)
