// CSM10ArReport.cpp : Defines the class behaviors for the application.
//

#include "stdafx.h"


#include "CSM10ArReport.h"

#include "versione.h"
#include "MainFrm.h"
#include "CSM10ArReportDoc.h"
#include "CSM10ArReportView.h"
#include "..\CSM10_AMi2021\CrashHandler.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CCSM10ArReportApp

BEGIN_MESSAGE_MAP(CCSM10ArReportApp, CWinApp)
	//{{AFX_MSG_MAP(CCSM10ArReportApp)
	ON_COMMAND(ID_APP_ABOUT, OnAppAbout)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG_MAP
	// Standard file based document commands
	ON_COMMAND(ID_FILE_NEW, CWinApp::OnFileNew)
	ON_COMMAND(ID_FILE_OPEN, CWinApp::OnFileOpen)
	// Standard print setup command
	ON_COMMAND(ID_FILE_PRINT_SETUP, CWinApp::OnFilePrintSetup)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CCSM10ArReportApp construction

CCSM10ArReportApp::CCSM10ArReportApp()
{
	// TODO: add construction code here,
	// Place all significant initialization in InitInstance
}

/////////////////////////////////////////////////////////////////////////////
// The one and only CCSM10ArReportApp object

CCSM10ArReportApp theApp;

/////////////////////////////////////////////////////////////////////////////
// CCSM10ArReportApp initialization

BOOL CCSM10ArReportApp::InitInstance()
{
// ini file in current directory
SetIniPath();

CCrashHandler ch;
ch.SetProcessExceptionHandlers();
ch.SetThreadExceptionHandlers();

	AfxEnableControlContainer();

	// Standard initialization
	// If you are not using these features and wish to reduce the size
	//  of your final executable, you should remove from the following
	//  the specific initialization routines you do not need.



	// Change the registry key under which our settings are stored.
	// TODO: You should modify this string to be something appropriate
	// such as the name of your company or organization.
	// SetRegistryKey(_T("Local AppWizard-Generated Applications"));

	LoadStdProfileSettings();  // Load standard INI file options (including MRU)

	// Register the application's document templates.  Document templates
	//  serve as the connection between documents, frame windows and views.
	// Register the application's document templates.  Document templates
	//  serve as the connection between documents, frame windows and views.
	CString resVerIt;
	if (!resVerIt.LoadString(CSM_RESVERDLL))
		{
		AfxMessageBox (_T("Attenzione Stringa Versione non caricabile"));
		return (FALSE);		
		}
	CProfile profile;
	CString dllName;
	dllName = profile.getProfileString(_T("init"),_T("LangDll"),_T("NONE"));
	if (dllName != _T("NONE"))
		{
		if ((hModRes = AfxLoadLibrary((LPCTSTR)dllName)) != NULL)
			{
			hDefRes = AfxGetResourceHandle();
			AfxSetResourceHandle (hModRes);	
			CString s;
			if (!s.LoadString(CSM_RESVERDLL))
				{
				AfxMessageBox (_T("Attenzione DLL di localizzazione non corretta"));
				return (FALSE);		
				}
			if (s != resVerIt)
				{
				AfxMessageBox (_T("Attenzione Versione DLL di localizzazione non corretta"));
				return (FALSE);		
				}
			}
		}

	// restore default printer
	m_PrinterControl.RestorePrinterSelection(m_hDevMode, m_hDevNames) ;


	CSingleDocTemplate* pDocTemplate;
	pDocTemplate = new CSingleDocTemplate(
		IDR_MAINFRAME,
		RUNTIME_CLASS(CCSM10ArReportDoc),
		RUNTIME_CLASS(CMainFrame),       // main SDI frame window
		RUNTIME_CLASS(CCSM10ArReportView));
	AddDocTemplate(pDocTemplate);

	// Parse command line for standard shell commands, DDE, file open
	CCommandLineInfo cmdInfo;
	ParseCommandLine(cmdInfo);

dBase = new  CDaoDbCreate();
if (dBase == NULL)
	return FALSE;



	// Dispatch commands specified on the command line
	if (!ProcessShellCommand(cmdInfo))
		return FALSE;

	// The one and only window has been initialized, so show and update it.
	m_pMainWnd->ShowWindow(SW_SHOWMAXIMIZED);
	m_pMainWnd->UpdateWindow();

	return TRUE;
}

// return the name of the currently selected printer
CString CCSM10ArReportApp::GetDefaultPrinter(void)
{
	PRINTDLG	pd ;
	CString		printer("Failed") ;
	
	pd.lStructSize = (DWORD)sizeof(PRINTDLG) ;
	BOOL bRet = GetPrinterDeviceDefaults(&pd) ;
	if (bRet)
		{
		// protect memory handle with ::GlobalLock and ::GlobalUnlock
		DEVMODE *pDevMode = (DEVMODE*)::GlobalLock(m_hDevMode) ;
		printer = pDevMode->dmDeviceName ;
		::GlobalUnlock(m_hDevMode) ;
		}
return printer ;
}

/////////////////////////////////////////////////////////////////////////////
// CAboutDlg dialog used for App About

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// Dialog Data
	//{{AFX_DATA(CAboutDlg)
	enum { IDD = IDD_ABOUTBOX };
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CAboutDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	//{{AFX_MSG(CAboutDlg)
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
	//{{AFX_DATA_INIT(CAboutDlg)
	//}}AFX_DATA_INIT
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CAboutDlg)
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
	//{{AFX_MSG_MAP(CAboutDlg)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

// App command to run the dialog
void CCSM10ArReportApp::OnAppAbout()
{
	CAboutDlg aboutDlg;
	aboutDlg.DoModal();
}

/////////////////////////////////////////////////////////////////////////////
// CCSM10ArReportApp message handlers


int CCSM10ArReportApp::ExitInstance() 
{
// TODO: Add your specialized code here and/or call the base class

if (hDefRes != NULL)
	{
	AfxSetResourceHandle (hDefRes);	
	AfxFreeLibrary (hModRes);
	}
	
// save the printer selection for a next run restore
m_PrinterControl.SavePrinterSelection(m_hDevMode, m_hDevNames) ;

return CWinApp::ExitInstance();
}

BOOL CAboutDlg::OnInitDialog() 
{


		CDialog::OnInitDialog();

#ifdef _DEBUG
	TCHAR m = 'd';
#else
	TCHAR m = 'r';
#endif

CString Ver;
Ver.Format (_T("CSM10ArReport Ver. %4.03lf%c del %s"),VERSIONE,m,DATAVERSIONE);
SetDlgItemText(IDC_VERSIONE,Ver);


// TODO: Add extra initialization here
	
return TRUE;  // return TRUE unless you set the focus to a control
              // EXCEPTION: OCX Property Pages should return FALSE
}



void  CCSM10ArReportApp::SetIniPath()
{
	
	CString appName = m_pszExeName;		// Get the "MyExe" portion of "MyExe.exe". Or, "MyDll" if RunDll32 is used.
	appName.Append(_T(".exe"));			// Now has "MyExe.exe" (or "MyDll.dll").
	// open or create dbase, name = appName+Config.sdb
	HMODULE hmod = GetModuleHandle(appName);  
 	CString fullPath;
	DWORD pathLen = ::GetModuleFileName( hmod, fullPath.GetBufferSetLength(MAX_PATH+1), MAX_PATH); // hmod of zero gets the main EXE
	fullPath.ReleaseBuffer( pathLen ); // Note that ReleaseBuffer doesn't need a +1 for the null byte.
	
	CString iniFileName;

	iniFileName = fullPath.Left(fullPath.GetLength() - 4);// tolgo .exe
	// c_logFileName = fullPath.Left(fullPath.GetLength() - 4);// tolgo .exe
	// 
	iniFileName+= _T(".ini");
	// c_logFileName+= _T("Log.log");	

	free((void*)m_pszProfileName);
	m_pszProfileName = _tcsdup(iniFileName);
	
}


