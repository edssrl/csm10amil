// CSM10ArReportDoc.h : interface of the CCSM10ArReportDoc class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_CSM10ArReportDOC_H__0D284F5C_5642_46AD_882C_C0B801666AF9__INCLUDED_)
#define AFX_CSM10ArReportDOC_H__0D284F5C_5642_46AD_882C_C0B801666AF9__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


#include "../CSM10_AMi2021/graphdoc.h"
#include "../CSM10_AMi2021/Cdaodbcreate.h"


#ifdef MAIN
CDaoDbCreate *dBase = NULL;
#else
extern CDaoDbCreate *dBase;
#endif


class CCSM10ArReportDoc : public CLineDoc
{
protected: // create from serialization only
	CCSM10ArReportDoc();
	DECLARE_DYNCREATE(CCSM10ArReportDoc)

// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CCSM10ArReportDoc)
	public:
	virtual BOOL OnNewDocument();
	virtual void Serialize(CArchive& ar);
	virtual BOOL CanCloseFrame(CFrameWnd* pFrame);
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CCSM10ArReportDoc();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	//{{AFX_MSG(CCSM10ArReportDoc)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CSM10ArReportDOC_H__0D284F5C_5642_46AD_882C_C0B801666AF9__INCLUDED_)
