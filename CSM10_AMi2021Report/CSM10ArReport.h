// CSM10ArReport.h : main header file for the CSM10ArReport application
//

#if !defined(AFX_CSM10ArReport_H__FD4F7884_89F0_4AB5_A4B5_8357FE2F0B7D__INCLUDED_)
#define AFX_CSM10ArReport_H__FD4F7884_89F0_4AB5_A4B5_8357FE2F0B7D__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"       // main symbols

#include "..\CSM10_AMi2021\EnumPrinters.h"

/////////////////////////////////////////////////////////////////////////////
// CCSM10ArReportApp:
// See CSM10ArReport.cpp for the implementation of this class
//



class CCSM10ArReportApp : public CWinApp
{
HMODULE		hModRes;	// Resource handle modificato 
HMODULE		hDefRes;	// Resource handle originario	

public:
	CCSM10ArReportApp();

CEnumPrinters	m_PrinterControl ;

void  SetIniPath();
// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CCSM10ArReportApp)
	public:
	virtual BOOL InitInstance();
	virtual int ExitInstance();
	//}}AFX_VIRTUAL

	// return the name of the currently selected printer
CString GetDefaultPrinter(void);

// Implementation
	//{{AFX_MSG(CCSM10ArReportApp)
	afx_msg void OnAppAbout();
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CSM10ArReport_H__FD4F7884_89F0_4AB5_A4B5_8357FE2F0B7D__INCLUDED_)
