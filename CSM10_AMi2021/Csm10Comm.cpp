//--------------------------------------------------------------------
//
// Csm10PComm.cpp: implementation of the Csm10PComm class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "Csm10Comm.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

#define TIMER_ID_PARSER		6000
#define TIMER_ID_SERALARM	6001

//---------------------------------
// definizione tipo di evento

//---------------------------------

Csm10Comm::Csm10Comm()
{
c_rxAlarmActive = false;

c_serPort = PCOM2;
c_serSpeed= 9600;
c_serPortOk = FALSE;

c_mParseTimer = GetTickCount();
}

Csm10Comm::~Csm10Comm()
{
closePort();
}



bool	Csm10Comm::openPort(LPCTSTR portName,long speed,long parity,bool slowNormal)
{

if (!isPortFree(portName))
	return FALSE;

c_serPort = PCOM1;
if (!_tcscmp(portName,_T("COM2")))
	{
	c_serPort = PCOM2;
	}
if (!_tcscmp(portName,_T("COM3")))
	{
	c_serPort = PCOM3;
	}
if (!_tcscmp(portName,_T("COM4")))
	{
	c_serPort = PCOM4;
	}
if (!_tcscmp(portName,_T("COM5")))
	{
	c_serPort = PCOM5;
	}
if (!_tcscmp(portName,_T("COM6")))
	{
	c_serPort = PCOM6;
	}
if (!_tcscmp(portName,_T("COM7")))
	{
	c_serPort = PCOM7;
	}
if (!_tcscmp(portName,_T("COM8")))
	{
	c_serPort = PCOM8;
	} 

// serve per adattare timeout alle varie velocita` della porta
c_serSpeed = speed;

// true usRts
S432Param(8,0,1,c_serPort);

if (slowNormal)
	{
	if (S432Open(speed,SLOWNORMAL,1,c_serPort)==S432ECOMPORT)
		return FALSE;
	}
else
	{
	if (S432Open(speed,NORMAL,1,c_serPort)==S432ECOMPORT)
		return FALSE;
	}
if (AfxGetMainWnd()->GetSafeHwnd() != NULL)
	S432SetWnd(AfxGetMainWnd()->GetSafeHwnd(),ID_SERCHARAVAIL,c_serPort,1);



c_serPortOk = TRUE;

// init parser
iniParser();

// request event on every rx char
// c_serPort.setRxSerCharEventDest(this,1);

// 
// c_rxSerTimer.Start(30000,wxTIMER_CONTINUOUS);

#ifdef _DEBUG

parser (0);
parser ('!');
parser ('5');
parser ('8');
parser (';');
parser ('0');
parser ('0');
parser (';');

#endif
return true;
}

bool	Csm10Comm::reOpenPort(LPCTSTR portName,long speed,long parity,bool slowNormal)
{

closePort();

return openPort(portName,speed,parity,slowNormal);
}


// chiusura seriale
bool Csm10Comm::closePort (void)
{
c_serPortOk = FALSE;

if (S432Close(c_serPort)==S432NOERROR)
	return TRUE;

return FALSE;
}




LRESULT Csm10Comm::OnRxSerChar(void)
{
unsigned char c;

while (S432GetDataByte (&c,c_serPort,FALSE))	// no pend! c_serPort.getDataByte(&c,NOPEND))
	parser(c);
return TRUE;
}

//--------------------------------------------------------------------
//	Sezione comandi da PC a testa
//


// Comandi da PC a testa 
// VAL_COMMAND
int Csm10Comm::sendGeneralCommand(Command *cmd)
{
CStringA msg;

msg.Format("!%02d;%02d;",cmd->cc,cmd->nn);

int nch = S432FillData (msg.GetLength(),(unsigned char*)((const char*)msg),c_serPort);

if (nch != msg.GetLength())
	{
	CString s;
	s.Format(_T("Error Sent only %d char\n"),nch);
	TRACE (s);
	}

return nch;
}




//--------------------------------------------------------------------
//	Sezione del parser
//

// stringhe comandi del parser 
#define CMD_LEN				3		// lunghezza della stringa identificativa del comando
#define SIZE_VECT_CMD		26		// numero di comandi gestiti
#define FIELD_SEPARATOR		';'		// 
#define FIELD_HEADER		'!'		// 

/*
#define	VAL_AUTOTEST0		50			//	Fori rilevati prima di iniziare Autotest 
#define	VAL_AUTOTEST1		51			//  Fori rilevati al termine dell�Autotest 
#define	VAL_INPUT			52			//  trasmette gli ingressi ogni 2 sec se il sistema si trova in stato TEST_I-O in corso
#define	VAL_ALARM			53			//	il pacchetto di ALLARME � trasmesso in modo asincrono se si verifica un allarme
										//	E ripetuto ogni 10 secondi
#define	VAL_FT				54			//  il pacchetto di valore FOTOCELLULE � trasmesso ogni 10 sec. 
										//  Se il sistema non � in ispezione
#define	VAL_RISAUTOTEST		55			//	Risultato Autotest 
#define	VAL_OFFLINETEST		56			//  Fori rilevati al termine dell�Autotest 
#define	VAL_RISOFFLINETEST	57			//  Risultato Autotest 
#define	VAL_STATO			58			//  il pacchetto di STATO � trasmesso ogni 10 sec. Se c�� un cambio di stato viene trasmesso immediatamente. Il pacchetto viene usato per testare il funzionamento della seriale.
#define	VAL_HOLE			59			//	il pacchetto � trasmesso ogni volta che il sistema rileva un foro.
*/

static const CmdHeader	CmdVect[]=
			{
			"50",{50+1,VAL_AUTOTEST0},
			"51",{50+1,VAL_AUTOTEST1},
			"52",{7+1,VAL_INPUT},
			"53",{7+1,VAL_ALARM},
			"54",{5+1,VAL_FT},
			"55",{34+1,VAL_RISAUTOTEST},
			"56",{50+1,VAL_OFFLINETEST},
			"57",{34+1,VAL_RISOFFLINETEST},
			"58",{2+1,VAL_STATO},
			"59",{5+1,VAL_HOLE},
			NULL,{0,0}};

/*------------------------------------------------------------------

				iniParser ()	
					
-------------------------------------------------------------------*/	

void Csm10Comm::iniParser(void)
{
						// -1 attesa di carattere FIELD_HEADER
lenStr	 = CMD_LEN;		// id cmd lunghi 2

pCmd = NULL;

c_strCmd.Empty();

c_strCmd = _T("");
}


/*------------------------------------------------------------------

				parser ()	

chiamata ogni byte ricevuto  

-------------------------------------------------------------------*/	

int Csm10Comm::parser (int ch)
{
int retCmd;
retCmd=0;

// check wrap around
if (GetTickCount() - c_mParseTimer < 0)
	{// wrap around after 49.7 days
	c_mParseTimer = GetTickCount();
	}


// arrivato comando se e` il primo attivo timer
if ((ch == FIELD_HEADER)||
	((GetTickCount() - c_mParseTimer) > 60000))
	{//arrivato id campo
	iniParser();
	// start 30 sec parser timer
	c_mParseTimer = GetTickCount();
	return 0;
	}


// nessun comando in costruzione
if (c_strCmd.GetLength() < lenStr)
	c_strCmd += (char) ch;

// prendo anche ultimo char
if ((ch == ';')||
	(c_strCmd.GetLength() >= lenStr))
	{
	//
	if (c_strCmd.GetLength() != lenStr)
		{// abort
		iniParser();
		return 0;
		}
	if (pCmd == NULL)
		// nessun comando in costruzione
		{// fine comando
		if (c_strCmd.Right(1) != FIELD_SEPARATOR)
			{// errore campo
			iniParser();
			return 0;
			}// fine
		// sostituisco ; con 0
		c_strCmd = c_strCmd.Left(c_strCmd.GetLength() -1);
		pCmd = (struct CmdHeader*)CmdVect;
		while (pCmd->idCmdStr != NULL)
			{
			if (!strcmp(pCmd->idCmdStr,c_strCmd))
				{
				if (pCmd->parField.size == 0)
					{// fine no parametri
					serverRequest(pCmd->parField.id,0,CStringA(""));
					// WAppEvent cmd(wxEVT_PROTOCOL,pCmd->parField.id);
					// if (c_pEventDest!=NULL)
						// ::wxPostEvent(c_pEventDest, cmd);
					break;
					}
				else
					{
					// indice comando in costruzione
					lenStr = pCmd->parField.size;
					c_strCmd.Empty();					
					return 0;
					}
				}
			pCmd++;;
			}
		// fine loop non trovato codice
		iniParser();
		}
	else
		// comando in costruzione
		{// fine parametro indexSubCmd
		if (c_strCmd.Right(1) != FIELD_SEPARATOR)
			{// errore campo
			iniParser();
			return 0;
			}// fine
		// sostituisco ; con 0
		c_strCmd = c_strCmd.Left(c_strCmd.GetLength() -1);
		/*
		if (!_tcscmp(pCmd->idCmdStr,"59"))
			{// !! val_hole parametri binari !!
			// trasformo binari in decimali come gli altri 
			CString sval,sval1;
			for (int i=0;i<c_strCmd.GetLength();i++)
				{// bisogna togliere ancora 128
			
				int  ui = c_strCmd.GetAt(i);
#ifdef _DEBUG
				sval1.Format(_T("%d"),ui + 128);
#else
				sval1.Format(_T("%d"),(c_strCmd.GetAt(i)-128));
#endif
				sval += sval1;
				sval += _T(",");
				}
			c_strCmd = sval.Left(sval.GetLength() -1);// tolgo ultima ','
			}
		*/
		serverRequest(pCmd->parField.id,pCmd->parField.size,c_strCmd);
		//WAppEvent cmd(wxEVT_PROTOCOL,pCmd->parField.id);
		//cmd.SetString(c_strCmd);
		//if (c_pEventDest!=NULL)
		//	::wxPostEvent(c_pEventDest, cmd);
		iniParser();
		return 0;
		}
	}
return retCmd;
}



