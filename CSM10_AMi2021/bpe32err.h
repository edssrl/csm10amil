//-------------------------------------------------------------
//
//
//				BpeComp32err.h
//
//
//-------------------------------------------------------------

// Le funzioni in dllMain hanno codice sysFunction == 0

//----------------
// Codici Errore
// 
//----------------



#define BPE32NOERROR		1
#define BPE32ERROR			0

/*-----------------------------
		 Errori
-----------------------------*/

#define BPE32EFILEOPEN	7		/* Errore Apertura file */

#define BPE32ENULLPOINTER	-150	/* Pointer nullo */

#define BPE32EWRITEFILE		-151	/* Errore scrittura file */

#define BPE32EFILETYPE		-152	/* Errore tipo di file */

#define BPE32INTERNALERROR	-153	/* Errore interno */

#define BPE32EOF			-154	/* EOF  */

#define BPE32HASHOVERFLOW	-155	/* Hash table overflow */
