#if !defined(AFX_DBINFOPLANT_H__1BB72A5A_B739_4137_B47F_AF5B196F0658__INCLUDED_)
#define AFX_DBINFOPLANT_H__1BB72A5A_B739_4137_B47F_AF5B196F0658__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// DbInfoPlant.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CDbInfoPlant DAO recordset

class CDbInfoPlant : public CDaoRecordset
{
public:
	CDbInfoPlant(CDaoDatabase* pDatabase = NULL);
	DECLARE_DYNAMIC(CDbInfoPlant)

// Field/Param Data
	//{{AFX_FIELD(CDbInfoPlant, CDaoRecordset)
	CString	m_NOME_ASSE_TMP;
	CString	m_NOME_ASSE_DEF;
	BOOL	m_STATO;
	double	m_LARGHEZZA;
	CString	m_LEGA;
	double	m_SPESSORE;
	CString	m_CLIENTE;
	CString	m_NOME_FILE_TMP;
	//}}AFX_FIELD

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CDbInfoPlant)
	public:
	virtual CString GetDefaultDBName();		// Default database name
	virtual CString GetDefaultSQL();		// Default SQL for Recordset
	virtual void DoFieldExchange(CDaoFieldExchange* pFX);  // RFX support
	//}}AFX_VIRTUAL

// Implementation
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif
	// win10
BOOL isDbOpen(){return ((m_pDatabase != NULL) && m_pDatabase->IsOpen());};
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DBINFOPLANT_H__1BB72A5A_B739_4137_B47F_AF5B196F0658__INCLUDED_)
