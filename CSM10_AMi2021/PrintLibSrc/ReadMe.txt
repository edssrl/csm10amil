
The printer classes are defined and implemented using these files:

cprinter.cpp	cprinter.h

These are low level printer primitives.

Cpage.cpp		cpage.h

These are the higher level functions.

All that is needed to include these classes in your project 
is to copy these  files to the directory and include them in your project.
In the  files that call any of the class functions include
the file CPage.h.

Fuctions are provided that facilitate using moveable 
print areas, tables, and prinying lines and rectangles
to the output device
