// CSM10_ARCze.h : main header file for the CSM10_ARCze DLL
//

#if !defined(AFX_CSM10_ARCze_H__A0FB7A36_725A_11D2_A770_00C026A019B7__INCLUDED_)
#define AFX_CSM10_ARCze_H__A0FB7A36_725A_11D2_A770_00C026A019B7__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"		// main symbols

/////////////////////////////////////////////////////////////////////////////
// CCSM10_ARCzeApp
// See CSM10_ARCze.cpp for the implementation of this class
//

class CCSM10_ARCzeApp : public CWinApp
{
public:
	CCSM10_ARCzeApp();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CCSM10_ARCzeApp)
	//}}AFX_VIRTUAL

	//{{AFX_MSG(CCSM10_ARCzeApp)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CSM10_ARCze_H__A0FB7A36_725A_11D2_A770_00C026A019B7__INCLUDED_)
