//---------------------------------------------------------------------
//
//
//			SYSTEM.H
//
//	define di sistema protocollo CSM10_P
//


#ifndef	SYSTEM_H
#define SYSTEM_H


#define	ID_SERCHARAVAIL		65000

/*
Comandi da PC a CSM10_P

VAL_COMMAND    id = 2 	Comandi da PC
Comando:	!cc;nn;		lunghezza = 7 byte
					cc [1,2] = 02
					nn [4,5] = 01	STARTCMD			dopo F1 e tutti i dati rotolo richiesti dal PC
					nn [4,5] = 02	STOPCMD			dopo F2
					nn [4,5] = 03	START_TESTCMD
					nn [4,5] = 04	STOP_TESTCMD
					nn [4,5] = 05	START_PAUSECMD
					nn [4,5] = 06	STOP_PAUSECMD
					nn [4,5] = 07	START_CHANGECMD
					nn [4,5]  = 08	STOP_CHANGECMD
					nn [4,5] = 10	AUTOTEST_CMD
					nn [4,5] = 11	SX_OFFLINETEST_CMD	lato Connettore
					nn [4,5] = 12	DX_OFFLINETEST_CMD	lato opposto Connettore
					nn [4,5] = 13	START_TEST_IO		setta le uscite e legge gli ingressi
					nn [4,5] = 14	START ISP. SENZA  DIAFRAMMA
					nn [4,5] = 15	STOP_TEST_IO		fine test io

VAL_OUTPUT	id = 29		Prova tutte le uscite se in Test_I-O
Comando:	!cc;nn;		lunghezza = 7 byte

		cc [1,2] = 29
		nn [4,5] = x1	LED_AUTOTEST		sul pannello del cabinet
		nn [4,5] = x2	LED_INSPECTION		sul pannello del cabinet
		nn [4,5] = x3	AIR_SYSTEM			comando elettrovalvola aria
		nn [4,5] = x4	ALARM_SYSTEM		uscita allarme sistema
		nn [4,5] = x5	ALARM_HOLE			uscita allarme foro
		nn [4,5] = x6	TRANSMITTER			comando trasmettitore

		x = 1	Uscita ON	
		x = 0 	uscita OFF

VAL_ALARM_PC id = 40	Allarmi da PC
Comando:	!cc;n;		lunghezza = 6 byte

		cc [1,2] = 40
		n [4] = x		ENCODER

		x = 1	Allarme NO	
		x = 0 	Allarme SI

VAL_ALPULSE id = 30		Imposta lunghezza segnale foro	OK_cpu		parametri
Comando:	!cc;nn;		lunghezza = 7 byte	 

cc [1,2] = 30
		nn [4,5] = valore lunghezza foro in decine di msec. (esempio 1= 10 msec. 2=20 msec ecc..) 

VAL_SOGLIA 	id = 3		Soglia analogica foro		OK_cpu		ricette
Comando:	!cc;nn;		lunghezza = 7 byte			 

cc [1,2] = 03
nn [4,5] = valore soglia da 10 a 99
------------------------------------------------------------------*/

// CMD-------------------------------
#define		VAL_COMMAND 2 

#define 	STARTCMD			1		//	dopo F1 e tutti i dati rotolo richiesti dal PC
#define 	STOPCMD				2		//  dopo F2
#define 	START_TESTCMD		3
#define		STOP_TESTCMD		4
#define		START_PAUSECMD		5
#define		STOP_PAUSECMD		6	
#define		START_CHANGECMD		7
#define		STOP_CHANGECMD		8
#define		AUTOTEST_CMD		10
#define		SX_OFFLINETEST_CMD	11		// lato Connettore
#define		DX_OFFLINETEST_CMD	12		// lato opposto Connettore
#define		START_TEST_IO		13		// start le uscite e legge gli ingressi
#define		START_NO_DIAFRAMMA	14
#define		STOP_TEST_IO		15


// CMD------------------------------------
#define		VAL_OUTPUT	29				// Prova tutte le uscite se in Test_I-O
		
#define		LED_AUTOTEST_ON		11		// sul pannello del cabinet
#define		LED_INSPECTION_ON	12		// sul pannello del cabinet
#define		AIR_SYSTEM_ON		13		// comando elettrovalvola aria
#define		ALARM_SYSTEM_ON		14		// uscita allarme sistema
#define		ALARM_HOLE_ON		15		// uscita allarme foro
#define		TRANSMITTER_ON		16		// comando trasmettitore

#define		LED_AUTOTEST_OFF	01		// sul pannello del cabinet
#define		LED_INSPECTION_OFF	02		// sul pannello del cabinet
#define		AIR_SYSTEM_OFF		03		// comando elettrovalvola aria
#define		ALARM_SYSTEM_OFF	04		// uscita allarme sistema
#define		ALARM_HOLE_OFF		05		// uscita allarme foro
#define		TRANSMITTER_OFF		06		// comando trasmettitore

// CMD-----------------------------------------------------------
#define		VAL_ALARM_PC		40		//	Allarmi da PC
#define		VAL_COM_PC			41		// pacchetto inviato periodicamente a hde per verificare seriale collegata           
#define		VAL_ALPULSE			30		//	Lunghezza impulso
#define		VAL_SOGLIA			3		//	Soglia fori

#define		AL_ENCODER_ON		01		// x = 1	Allarme NO	
#define		AL_ENCODER_OFF		11		// x = 0 	Allarme SI


/*-------------------------------------------------------

da CSM10_P	a PC

VAL_AUTOTEST0	id = 50		Fori rilevati prima di iniziare Autotest 
Comando:	!cc;mm,nn,nn,��.,nn;	lunghezza = 55 byte 

		cc [1,2] = 50
		mm [4,5] = 01,02�..10	numero del modulo da 1 a 10 (1 = lato conettore)
		nn [7,8] = 00-99		num. Fori prima cella rilevati prima di iniziare Autotest
		nn [10,11] = 00-99	num. Fori seconda cella rilevati prima di iniziareAutotest
		ecc.	numero di celle per ciascun modulo = 16


VAL_AUTOTEST1	id = 51		Fori rilevati al termine dell�Autotest 
Comando:	!cc;mm,nn,nn,�..,nn;	lunghezza = 55 byte

		Cc[1,2] = 50
		mm [4,5] = 01,02�..10	numero del modulo da 1 a 10 (1 = lato conettore)
		nn [7,8] = 00-99		num. Fori prima cella rilevati al termine Autotest
		nn [10,11] = 00-99	num. Fori seconda cella rilevati al termine Autotest
		ecc.	numero di celle per ciascun modulo = 16


VAL_INPUT	id = 52		trasmette gli ingressi ogni 2 sec se il sistema si trova in stato TEST_I-O in corso
Comando:	!cc;n1,n2,n3,n4;	lunghezza = 12 byte

		cc [1,2]= 52
		n1 [4] = x	ingresso Start Inspection (Enable)
		n2 [6] = x	ingresso Pausa
		n3 [8] = x	ingresso Cambio
		n4 [10] = x	ingresso Riserva	

		x = 1	ingresso ON
		x = 0	ingresso OFF  


VAL_ALARM	id = 53		il pacchetto di ALLARME � trasmesso in modo asincrono se si verifica un allarme
				E ripetuto ogni 10 secondi
Comando:	!cc;nnn;		lunghezza = 8 byte

		cc [1,2] = 53
		nnn[10,11,12] = stato e allarmi

		bit0 = allarme FT 1 Diaf. Sx (lato connettore) sporca		1 = Allarme	0 = OK
		bit1 = allarme FT 2 Diaf. Dx sporca			1 = Allarme	0 = OK
		bit2 = allarme led TX rotto				1 = Allarme	0 = OK
		bit3 = allarme Ott 1 Sx (lato connettore) non in posizione 	1 = Allarme	0 = OK
		bit4 = allarme Ott 2 Dx non in posizione 			1 = Allarme	0 = OK 
		bit5 = diaframma fuori posizione in ispezione		1 = Allarme	0 = OK
		bit6 = diaframma abilitato in ispezione			1 = Disabilitato	0 = Abilitato
		bit7 = stato ispezione					1 = Ispezione ON


VAL_FT            id = 54	il pacchetto di valore FOTOCELLULE � trasmesso ogni 10 sec. Se il sistema non � in ispezione
Comando:	!cc;nn,nn;	lunghezza = 10 byte

		Cc[1,2] = 58
		nn [4,5] =  valore analogico ft. Inseguitore diaframma sinistro
		nn [7,8] =  valore analogico ft. Inseguitore diaframma destro
		


VAL_RISAUTOTEST	id = 55		Risultato Autotest 
Comando:	!cc;mm,n,n,�..,n;	lunghezza = 39 byte

		Cc[1,2] = 55
		mm [4,5] = 01,02�..10	numero del modulo da 1 a 10 (1 = lato conettore)
		n [7] = 0 o 1		0 = ricevitore not OK	1 = ricevitore OK
		n [9] = 0 o 1 		0 = ricevitore not OK	1 = ricevitore OK 
		ecc.	numero di celle per ciascun modulo = 16


VAL_OFFLINETEST	id = 56		Fori rilevati al termine dell�Autotest 
Comando:	!cc;mm,nn,nn,�..,nn;	lunghezza = 55 byte

		Cc[1,2] = 56
		mm [4,5] = 01,02�..10	numero del modulo da 1 a 10 (1 = lato connettore)
		nn [7,8] = 00-99		num. Fori prima cella rilevati al termine Autotest
		nn [10,11] = 00-99	num. Fori seconda cella rilevati al termine Autotest
		ecc.	numero di celle per ciascun modulo = 16


VAL_RISOFFLINETEST	id = 57		Risultato Autotest 
Comando:	!cc;mm,n,n,�..,n;	lunghezza = 39 byte

		Cc[1,2] = 57
		mm [4,5] = 01,02�..10	numero del modulo da 1 a 10 (1 = lato conettore)
		n [7] = 0 o 1		0 = ricevitore not OK	1 = ricevitore OK
		n [9] = 0 o 1 		0 = ricevitore not OK	1 = ricevitore OK 
		ecc.	numero di celle per ciascun modulo = 16


VAL_STATO	id = 58		il pacchetto di STATO � trasmesso ogni 10 sec. Se c�� un cambio di stato viene trasmesso immediatamente. Il pacchetto viene usato per testare il funzionamento della seriale.
Comando:	!cc;nn;		lunghezza = 7 byte

		cc [1,2] = 58
		nn [4,5] = 01	attende start da PC (comando VAL_COMMAND 01)
		nn [4,5] = 02	attende Abilitazione verifica da impianto
		nn [4,5] = 03	sta acquisendo (TX = ON, diaframma in posizione, ispezione abilitata)
		nn [4,5] = 04	pausa (TX = ON, diaframma in posizione, non acquisisce i fori, conta i metri)
		nn [4,5] = 05	cambio (cambio bobina)
		nn [4,5] = 09	ciclo di Autotest in corso
		nn [4,5] = 10	Test in corso (potrebbe essere encoder simulato, allarmi disabilitati) 	
		nn [4,5] = 11	Test Off Line in corso 
		nn [4,5] = 13	Test_I-O in corso (setta le uscite e legge gli ingressi)
		nn [4,5] = 14	Dopo start da PC e Abilitazione in attesa che inizi l�ispezione 
						(quando entrambi i diaframmi raggiungono il bordo del materiale)

------------------------------*/

// Stati HDE

#define HDE_STATE		0
#define HDE_WAITSTART	1
#define HDE_WAITENABLE	2
#define HDE_INSPECTION	3
#define HDE_HOLD		4
#define HDE_CHANGE		5
#define HDE_SELFTEST	9
#define HDE_TEST		10
#define HDE_TESTOFFLINE 11

#define HDE_TESTIO		13
#define HDE_READY		14

/*----------------------------
VAL_HOLE	id = 59		il pacchetto � trasmesso ogni volta che il sistema rileva un foro.
Comando:	!cc;x1x2;	lunghezza = 7 byte

		cc [1,2] = 59
		x1 = valore binario      posizione trasversale del foro valori possibili da 1 a 112 (7 moduli x 16 ch)
	al valore che ti mando devi sottrarre 128 per avere il numero giusto cosi non ti arriver� mai ! ; ,	
		x2 = valore binario      numero fori nella cella
-----------------------------------------------------------------------------------------------------------------*/	

#define	VAL_AUTOTEST0		50			//	Fori rilevati prima di iniziare Autotest 
#define	VAL_AUTOTEST1		51			//  Fori rilevati al termine dell�Autotest 
#define	VAL_INPUT			52			//  trasmette gli ingressi ogni 2 sec se il sistema si trova in stato TEST_I-O in corso
#define	VAL_ALARM			53			//	il pacchetto di ALLARME � trasmesso in modo asincrono se si verifica un allarme
										//	E ripetuto ogni 10 secondi
#define	VAL_FT				54			//  il pacchetto di valore FOTOCELLULE � trasmesso ogni 10 sec. 
										//  Se il sistema non � in ispezione
#define	VAL_RISAUTOTEST		55			//	Risultato Autotest 
#define	VAL_OFFLINETEST		56			//  Fori rilevati al termine dell�Autotest 
#define	VAL_RISOFFLINETEST	57			//  Risultato Autotest 
#define	VAL_STATO			58			//  il pacchetto di STATO � trasmesso ogni 10 sec. Se c�� un cambio di stato viene trasmesso immediatamente. Il pacchetto viene usato per testare il funzionamento della seriale.
#define	VAL_HOLE			59			//	il pacchetto � trasmesso ogni volta che il sistema rileva un foro.


#endif
