// InfoPlantDlg.h : header file
//
 
#if !defined(AFX_INFOPLANTDLG_H__9A8B7CFA_C628_41E0_AA00_B2F9E043438C__INCLUDED_)
#define AFX_INFOPLANTDLG_H__9A8B7CFA_C628_41E0_AA00_B2F9E043438C__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


#include "..\..\..\..\..\..\Library\UniCodeListCtrl\gxDaoListCtrl.h"
#include "DbInfoPlant.h"

/////////////////////////////////////////////////////////////////////////////
// CInfoPlantDlg dialog

class CInfoPlantDlg : public CDialog
{
// Construction
public:
	CInfoPlantDlg(CWnd* pParent = NULL);	// standard constructor

// Dialog Data
	//{{AFX_DATA(CInfoPlantDlg)
	enum { IDD = IDD_INFOPLANT_DIALOG };
	gxDaoListCtrl	m_infoList;
	//}}AFX_DATA

	CDbInfoPlant* c_pDbInfoPlant;
	CDaoDatabase* c_pDBase;


	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CInfoPlantDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	HICON m_hIcon;

	// Generated message map functions
	//{{AFX_MSG(CInfoPlantDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	afx_msg void OnDestroy();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_INFOPLANTDLG_H__9A8B7CFA_C628_41E0_AA00_B2F9E043438C__INCLUDED_)
