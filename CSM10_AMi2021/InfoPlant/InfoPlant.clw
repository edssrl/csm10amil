; CLW file contains information for the MFC ClassWizard

[General Info]
Version=1
LastClass=CInfoPlantDlg
LastTemplate=CDaoRecordset
NewFileInclude1=#include "stdafx.h"
NewFileInclude2=#include "InfoPlant.h"

ClassCount=4
Class1=CInfoPlantApp
Class2=CInfoPlantDlg
Class3=CAboutDlg

ResourceCount=3
Resource1=IDD_ABOUTBOX
Resource2=IDR_MAINFRAME
Class4=CDbInfoPlant
Resource3=IDD_INFOPLANT_DIALOG

[CLS:CInfoPlantApp]
Type=0
HeaderFile=InfoPlant.h
ImplementationFile=InfoPlant.cpp
Filter=N

[CLS:CInfoPlantDlg]
Type=0
HeaderFile=InfoPlantDlg.h
ImplementationFile=InfoPlantDlg.cpp
Filter=D
BaseClass=CDialog
VirtualFilter=dWC
LastObject=CInfoPlantDlg

[CLS:CAboutDlg]
Type=0
HeaderFile=InfoPlantDlg.h
ImplementationFile=InfoPlantDlg.cpp
Filter=D

[DLG:IDD_ABOUTBOX]
Type=1
Class=CAboutDlg
ControlCount=4
Control1=IDC_STATIC,static,1342177283
Control2=IDC_STATIC,static,1342308480
Control3=IDC_STATIC,static,1342308352
Control4=IDOK,button,1342373889

[DLG:IDD_INFOPLANT_DIALOG]
Type=1
Class=CInfoPlantDlg
ControlCount=2
Control1=IDOK,button,1342242817
Control2=IDC_LIST1,SysListView32,1350631941

[CLS:CDbInfoPlant]
Type=0
HeaderFile=DbInfoPlant.h
ImplementationFile=DbInfoPlant.cpp
BaseClass=CDaoRecordset
Filter=N
VirtualFilter=x

[DB:CDbInfoPlant]
DB=1
DBType=DAO
ColumnCount=8
Column1=[NOME_ASSE_TMP], 12, 50
Column2=[NOME_ASSE_DEF], 12, 50
Column3=[STATO], -7, 1
Column4=[LARGHEZZA], 8, 8
Column5=[LEGA], 12, 32
Column6=[SPESSORE], 8, 8
Column7=[CLIENTE], 12, 32
Column8=[NOME_FILE_TMP], 12, 128

