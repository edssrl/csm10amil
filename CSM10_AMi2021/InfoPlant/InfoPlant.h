// InfoPlant.h : main header file for the INFOPLANT application
//
  
#if !defined(AFX_INFOPLANT_H__10ECF3CD_6D76_483D_84E5_5FC68097DA8E__INCLUDED_)
#define AFX_INFOPLANT_H__10ECF3CD_6D76_483D_84E5_5FC68097DA8E__INCLUDED_
 
#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"		// main symbols

/////////////////////////////////////////////////////////////////////////////
// CInfoPlantApp:
// See InfoPlant.cpp for the implementation of this class
//

class CInfoPlantApp : public CWinApp
{
public:
	CInfoPlantApp();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CInfoPlantApp)
	public:
	virtual BOOL InitInstance();
	//}}AFX_VIRTUAL

// Implementation

	//{{AFX_MSG(CInfoPlantApp)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_INFOPLANT_H__10ECF3CD_6D76_483D_84E5_5FC68097DA8E__INCLUDED_)
