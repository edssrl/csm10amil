// GraphView.cpp : implementation file
//

#include "stdafx.h"
#include "MainFrm.h"
#include "resource.h"

#include <AfxTempl.h>

#include "GraphDoc.h"
#include "GraphView.h"
#include "dyntempl.h"
#include "CSM10_AR.h"
#include "DbSet.h"
#include "DClassColor.h"

#include "PrintLibSrc\CPage.h"
#include "PrintLibSrc\Dib.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


//---------------------------------------------------------
// Global function
// calcolo colori
//---------------------------------------------------------

//---------------------------------------------------------
// Global function
// calcolo colori
//---------------------------------------------------------
int ClassColor [6]={0,0,0,0,0,0};

void CSM20ClassColorInvalidate (void)
{
	ClassColor[5] = 0;
}

COLORREF CSM20GetClassColor (int cl)
{

if (ClassColor[5] == 0)
	{
	CDClassColor dialog;
	dialog.loadFromProfile();
	ClassColor[0] = dialog.getRgbColorA();
	ClassColor[1] = dialog.getRgbColorB();
	ClassColor[2] = dialog.getRgbColorC();
	ClassColor[3] = dialog.getRgbColorD();
	ClassColor[4] = dialog.getRgbColorBig();
	ClassColor[5] = 1;
	}

return (ClassColor[cl%5]); 
}



/*
// colori possibili
static int ClassColor[4][3]={{255,0,0},{0,255,0},{0,0,255},{255,255,0}}; 
// default 0->red,1->blue,2->green,3->yellow
static int ClassColorMap[4]={0,1,2,3};

void CSM10_ARInitClassColor(void)
{
CProfile profile;
CString strColor;
strColor = profile.getProfileString("init","ClassColor","RGBY");

// Get red position
int pos = strColor.Find('R');
if ((pos < 0)||
	(pos > 3))
	pos = 0;

ClassColorMap[0] = pos;

// Get green position
pos = strColor.Find('G');
if ((pos < 0)||
	(pos > 3))
	pos = 0;

ClassColorMap[1] = pos;

// Get blue position
pos = strColor.Find('B');
if ((pos < 0)||
	(pos > 3))
	pos = 0;

ClassColorMap[2] = pos;

// Get yellow position
pos = strColor.Find('Y');
if ((pos < 0)||
	(pos > 3))
	pos = 0;

ClassColorMap[3] = pos;


}

COLORREF CSM10_ARGetClassColor (int cl)
{
int r,g,b;
switch (cl%4)
	{
	case 0 : 
		// classe A
		r = ClassColor[ClassColorMap[0]][0];
		g = ClassColor[ClassColorMap[0]][1];
		b = ClassColor[ClassColorMap[0]][2];
		break;
	case 1 : 
		// classe B
		r = ClassColor[ClassColorMap[1]][0];
		g = ClassColor[ClassColorMap[1]][1];
		b = ClassColor[ClassColorMap[1]][2];
		break;
	case 2 : 
		// classe C
		r = ClassColor[ClassColorMap[2]][0];
		g = ClassColor[ClassColorMap[2]][1];
		b = ClassColor[ClassColorMap[2]][2];
		break;
	case 3 : 
		// classe D
		r = ClassColor[ClassColorMap[3]][0];
		g = ClassColor[ClassColorMap[3]][1];
		b = ClassColor[ClassColorMap[3]][2];
		break;
	}
int d = 1 + cl/4;
if (d>1)
	{
	r /= d;
	g /= d;
	b /= d;
	}

return (RGB(r,g,b)); 

}
*/


//---------------------------------------------------------------------

Layout::Layout(void)
{
sCaption = _T("SCONOSCIUTO");
fCaption.name = _T("Arial");
fLabel.name = _T("Arial");
fNormal.name = _T("Arial");

fCaption.size = 120;
fLabel.size = 100;
fNormal.size = 110;

for (int i=0;i<MAX_NUMLABEL;i++)
	{
	c_sLabel[i] = _T("SCONOSCIUTO");
	c_vLabel[i] = _T("SCONOSCIUTO");
	c_nlAfterLabel [i] = -1;
	c_vOrderLabel [i] = -1;
	c_pxSepLabel [i] = 10; // 10 pixel separazione
	}

for (int i=0;i<MAX_NUMCLASSI;i++)
	{
	c_cLabel[i] = _T("SCONOSCIUTO");
	}

/*
sPosition = "SCONOSCIUTO";
sPositionDal = "SCONOSCIUTO";
sCliente = "SCONOSCIUTO";
sDimX = "SCONOSCIUTO";
sProduct = "SCONOSCIUTO";
sFori = "SCONOSCIUTO";
sAlarm = "SCONOSCIUTO";
sRicetta = "SCONOSCIUTO";
sRotolo = "SCONOSCIUTO"; 


vPosition = "SCONOSCIUTO";
vPositionDal = "SCONOSCIUTO";
vCliente = "SCONOSCIUTO";
vDimX = "SCONOSCIUTO";
vProduct = "SCONOSCIUTO";
vFori = "SCONOSCIUTO";
vAlarm = "SCONOSCIUTO";
vRicetta = "SCONOSCIUTO"; 
vRotolo = "SCONOSCIUTO"; 
*/

maxValY = 100.;
minValY = 0.;
c_numSchede = 7;

viewType = UNKVIEW;

mode = LINEAR;
// Se True non aggiorna il background della parte interna
// della finestra di disegno
excludeUpdateBackGr = FALSE;
excludeUpdateBkgLV = FALSE;

// vettore contenente le label dopo le quali andare a capo

rgbBackColor = RGB(192,192,192);
rgbDrawBackColor = RGB(192,192,192);

c_sizeFBanda = 1;
// larghezza bande
c_sizeBanda = 1;
// bande per colonna
c_bandePerCol =1;
c_numClassi = 4;
}

//	1998
//	I campi inizializzati a SCONOSCIUTO 
//	non vengono visualizzati

BOOL Layout::Create(CWnd* pParentWnd)
{

// static
if (!sCaption.Create(pParentWnd,S_CAPTION))
	return FALSE;
sCaption.setFontInfo(fCaption);
sCaption.set3Dlook(TRUE);
sCaption.setSensDbclk(TRUE);

for (int i=0;i<MAX_NUMLABEL;i++)
	{
	if (!c_sLabel[i].Create(pParentWnd,i*2))
		return FALSE;
	c_sLabel[i].setFontInfo(fNormal);
	
	if (!c_vLabel[i].Create(pParentWnd,i*2+1))
		return FALSE;
	c_vLabel[i].setFontInfo(fNormal);
	c_vLabel[i].set3Dlook(TRUE);
	c_vLabel[i].setStringBase(_T("999999"));
	}

// label delle classi
for (int i=0;i<MAX_NUMCLASSI;i++)
	{
	if (!c_cLabel[i].Create(pParentWnd,100+i))
		return FALSE;
	c_cLabel[i].setFontInfo(fLabel);
	c_cLabel[i].set3Dlook(FALSE);
	c_cLabel[i].setStringBase(_T("9"));
	}
	
/*

if (!sAlarm.Create(pParentWnd,S_ALARM))
	return FALSE;
sAlarm.setFontInfo(fNormal);

if (!sCliente.Create(pParentWnd,S_CLIENTE))
	return FALSE;
sCliente.setFontInfo(fNormal);

if (!sProduct.Create(pParentWnd,S_DENSITA))
	return FALSE;
sProduct.setFontInfo(fNormal);

if (!sDimX.Create(pParentWnd,S_DIMX))
	return FALSE;
sDimX.setFontInfo(fNormal);

if (!sFori.Create(pParentWnd,S_FORI))
	return FALSE;
sFori.setFontInfo(fNormal);

if (!sPosition.Create(pParentWnd,S_POSITION))
	return FALSE;
sPosition.setFontInfo(fNormal);

if (!sPositionDal.Create(pParentWnd,S_POSITIONDAL))
	return FALSE;
sPositionDal.setFontInfo(fNormal);

if (!sRicetta.Create(pParentWnd,S_RICETTA))
	return FALSE;
sRicetta.setFontInfo(fNormal);

if (!sRotolo.Create(pParentWnd,S_ROTOLO))
	return FALSE;
sRotolo.setFontInfo(fNormal);

// valori

if (!vAlarm.Create(pParentWnd,V_ALARM))
	return FALSE;
vAlarm.setFontInfo(fNormal);
vAlarm.set3Dlook(TRUE);

if (!vCliente.Create(pParentWnd,V_CLIENTE))
	return FALSE;
vCliente.setFontInfo(fNormal);
vCliente.set3Dlook(TRUE);

if (!vProduct.Create(pParentWnd,V_DENSITA))
	return FALSE;
vProduct.setFontInfo(fNormal);
vProduct.set3Dlook(TRUE);
vProduct.setStringBase("999999");

if (!vDimX.Create(pParentWnd,V_DIMX))
	return FALSE;
vDimX.setFontInfo(fNormal);
vDimX.set3Dlook(TRUE);
vDimX.setStringBase("999999");

if (!vFori.Create(pParentWnd,V_FORI))
	return FALSE;
vFori.setFontInfo(fNormal);
vFori.set3Dlook(TRUE);
vFori.setStringBase("999");

if (!vPosition.Create(pParentWnd,V_POSITION))
	return FALSE;
vPosition.setFontInfo(fNormal);
vPosition.set3Dlook(TRUE);
vPosition.setStringBase("999999");

if (!vPositionDal.Create(pParentWnd,V_POSITIONDAL))
	return FALSE;
vPositionDal.setFontInfo(fNormal);
vPositionDal.set3Dlook(TRUE);
vPositionDal.setStringBase("999999");

if (!vRicetta.Create(pParentWnd,V_RICETTA))
	return FALSE;

vRicetta.setFontInfo(fNormal);
vRicetta.set3Dlook(TRUE);

if (!vRotolo.Create(pParentWnd,V_ROTOLO))
	return FALSE;
vRotolo.setFontInfo(fNormal);
vRotolo.set3Dlook(TRUE);
vRotolo.setStringBase("999999");

*/

return TRUE;
}


void Layout::draw(CDC* pdc)
{

pdc->SaveDC();

CRect rectB;
CRect yRectLabel;
int numStepY,numLabel;

TEXTMETRIC tm;

CFont fontCaption,fontLabel,fontNormal;
fontCaption.CreatePointFont (fCaption.size,fCaption.name,pdc);
fontLabel.CreatePointFont (fLabel.size,fLabel.name,pdc);
fontNormal.CreatePointFont (fNormal.size,fNormal.name,pdc);

pdc->SelectObject(&fontLabel); 

// Trovo Finestra disegno (grigia)
CalcDrawRect (rcBounds,rectB);

// Limiti Finestra Label
yRectLabel.left = rcBounds.left;
yRectLabel.right = rectB.left;
yRectLabel.top	= rcBounds.top;
yRectLabel.bottom = rcBounds.bottom;

//-------------------------------------
// 3D Look
// 
//------------------------------------------
Draw3DRect (pdc,rectB);

// Ini Text Mode
pdc->GetTextMetrics(&tm);
pdc->SetTextAlign(TA_RIGHT | TA_TOP);
pdc->SetBkMode (TRANSPARENT);

// Scala verticale
// NumStepY Parametrizzato alla dim Finestra
CRect subRectB [MAX_NUMCLASSI];
subRectB[0]=rectB;
if (mode & MSCALEV)
	{// caso multiscala verticale
	for (int i=0;i<c_numClassi;i++)
		{
		subRectB[i]=rectB;
		subRectB[i].TopLeft().y += (rectB.Size().cy/c_numClassi)*i;
		subRectB[i].BottomRight().y -= (rectB.Size().cy/c_numClassi)*(c_numClassi-(i+1));
		if ((c_numClassi > 1)&&(i>0))
			// disegno asse intermedio
			{
			CPen pen1,*oldPen;
			pen1.CreatePen(PS_SOLID,1,RGB(0,0,0));	
			oldPen = pdc->SelectObject(&pen1);
			pdc->MoveTo(subRectB[i].left,subRectB[i].TopLeft().y);
			pdc->LineTo(subRectB[i].right,subRectB[i].TopLeft().y);		
			pdc->SelectObject(oldPen);
			pen1.DeleteObject();	
			}
		}
	}

// SubRect tutti uguali
numStepY = abs(subRectB[0].top - subRectB[0].bottom) / (tm.tmHeight + 10);
ASSERT (numStepY >= 0);

numLabel = 10;
if ((numStepY <= 10) && (numStepY > 5)) numLabel = 5;
if ((numStepY <= 5) && (numStepY > 2)) numLabel = 2;
if (numStepY == 2) numLabel = 1;
if (numStepY < 2) numLabel = 0;


CString	str;
double label;
double deltaY;
int numL;
double MY,mY;

if (mode & NOLABELV)
	numL = 0;
else
	{
	if (mode & LOGARITHM)
		{
		MY = pow (10,log10(maxValY));
		mY = pow (10,log10(minValY));
		numL = (int ) ((log10(MY)) - log10(mY));
		if (numL <= 0)
			numL = 1;
		deltaY = MY-mY;
		}
	else
		{// LINEAR
		deltaY = maxValY-minValY;
		numL = numLabel;
		}
	}


if (numL > 0)
	{
	int j = 0;
	do 	{
		for (int i = 0;i<=numL;i++)
			{
			// eliminata ultima label nei grafici intermedi
			if ((j != 0)&&(i==0))
				continue;
			// ------------
			int ly,lx;
			if (mode & LOGARITHM)
				label = (pow (10,(double)numL-i)) * mY;
			else
				label = maxValY - (deltaY/numL * i); 
			if ((label < 10.)&&(label > 0.))
				str.Format(_T("%4.1lf"),label);
			else
				str.Format(_T("%4.0lf"),label);
			lx = subRectB[j].left -5;
			ly = (((subRectB[j].bottom -subRectB[j].top)/ (numL))*i) + subRectB[j].top; // - tm.tmHeight/2;	

			pdc->SetTextAlign(TA_RIGHT | TA_TOP);

			if (i==numL)
				{// modificata posiz
				CSize sz;
				sz = pdc->GetOutputTextExtent(str,str.GetLength());
				ly -= sz.cy/2;
				// pdc->SetTextAlign(TA_RIGHT | TA_BOTTOM);
				}

			pdc->ExtTextOut (lx,ly,ETO_CLIPPED,
				yRectLabel,str,str.GetLength(),NULL);
			}
		}
	while ((mode & MSCALEV) && ((j++)<c_numClassi));
	}

// Scala Orizzontale
// c_bandePerCol; 
int larghezza = rectB.Width();
// c_sizeBanda;
// c_sizeFBanda;
int numCol;
numCol = c_numSchede;
CString baseString;
baseString = _T("000 ");

CSize sz;
sz = pdc->GetOutputTextExtent(baseString,baseString.GetLength());

if (!(mode & NOLABELH))
	{
	// c_sizeBanda = c_bandePerCol * larghezza / numCol;
	c_sizeBanda = (int) ceil(1.0 * c_bandePerCol * larghezza / ( 1.0 * numCol));

	if (c_sizeBanda < sz.cx)
		{
		//bandePerCol = (int ) ((double)sz.cx*numCol/larghezza);
		c_bandePerCol = (int) ceil(1.0 * sz.cx * numCol / ( 1.0 * larghezza));
		switch(c_bandePerCol)
			{
			case 1:
			case 2:
				c_bandePerCol = 2;
				break;

			case 3:
			case 4:
				c_bandePerCol = 4;
				break;
			case 5:
				c_bandePerCol = 5;
				break;
			case 6:
			case 7:
			case 8:
				c_bandePerCol = 8;
				break;
			case 9:
			case 10:
				c_bandePerCol = 10;
				break;
			case 11:
			case 12:
			case 13:
			case 14:
			case 15:
				c_bandePerCol = 15;
				break;
			case 16:
			case 17:
			case 18:
			case 19:
				c_bandePerCol = 20;
				break;
			}
		c_sizeBanda = (int) ceil(1.0 * c_bandePerCol * larghezza / ( 1.0 * numCol));
		}
	// sizebanda deve essere divisibile per c_bandePerCol
	// es: 5 bande per colonna -> sizebanda x*5 perche` intero
	c_sizeBanda /= c_bandePerCol;
	c_sizeBanda *= c_bandePerCol;

	int numBandeToDraw = ceil(1.0 * numCol / c_bandePerCol);
	if (numBandeToDraw <= 0)
		numBandeToDraw=1;

	// FixBug 29-07-09 a seconda della risoluzione stampante
	// disegna fuori dello spazio assegnato
	while((rectB.Size().cx - c_sizeBanda*numBandeToDraw)<0)
		{
		c_sizeBanda--;
		}

	double dPosX;
	int posX;
	dPosX = rectB.TopLeft().x; 
	
	// CString label;

	// Centrato rispetto alla larghezza
	dPosX += (1. * rectB.Size().cx - c_sizeBanda*numBandeToDraw) / 2.;
	// larghezza prima banda
	c_sizeFBanda = (int)dPosX - rectB.TopLeft().x ;

	int sizeUnaBanda = c_sizeBanda/c_bandePerCol;
	for (int i=0;i<c_bandePerCol*numBandeToDraw;i++)
		{
		
		if ((i==0)||
			(i%c_bandePerCol) == (c_bandePerCol-1))
			// Sulla riga divisione
			{
			//if (mode == ROLLMAP)
			// posX = (int)(dPosX + (double)sizeUnaBanda * i);
			posX = (int)(dPosX + (double)i*c_sizeBanda/c_bandePerCol);
				{
				// pdc->MoveTo(posX + i*c_sizeBanda,rectB.TopLeft().y);
				pdc->MoveTo(posX,rectB.BottomRight().y);
				pdc->LineTo(posX,rectB.BottomRight().y+10);
				}
			int iLabel = i + 1;
			str.Format(_T("%d "),iLabel);
			pdc->SetTextAlign(TA_LEFT);
			pdc->TextOut(posX,rectB.BottomRight().y,
				str);
			}
		}

/*
	for (int i=0;i<numBandeToDraw;i++)
		{
		if (c_bandePerCol > 1)
			// Sulla riga divisione
			{
			//if (mode == ROLLMAP)
			posX = (int)(dPosX + (double)c_sizeBanda * i);
				{
				// pdc->MoveTo(posX + i*c_sizeBanda,rectB.TopLeft().y);
				pdc->MoveTo(posX,rectB.BottomRight().y);
				pdc->LineTo(posX,rectB.BottomRight().y+10);
				}
			int iLabel = c_bandePerCol*i + 1;
			str.Format("%d ",iLabel);
			pdc->SetTextAlign(TA_LEFT);
			pdc->TextOut(posX,rectB.BottomRight().y,
				str);
			}
		else
			{
			if (i<numBandeToDraw)
				{
				int iLabel = c_bandePerCol*(i+1);
				// Centrato sulla colonna
				posX  = (int)(dPosX + (double)c_sizeBanda*i);
				pdc->MoveTo(posX,rectB.BottomRight().y);
				pdc->LineTo(posX,rectB.BottomRight().y+10);
				
				str.Format("%d",iLabel);
				pdc->SetTextAlign(TA_CENTER);
				int posX2 = (int)(dPosX + (i+1)*c_sizeBanda-(c_sizeBanda/2.));
				pdc->TextOut(posX2,rectB.BottomRight().y,str);
				}
			}
		}
*/


	}

//---------------------------------------------------
// Posizionamento Label
CRect genRect;
CSize genSize;

// 
// label delle classi 
// con pallini
// 
int i;
for (i=0;(i<c_numClassi)&&(i<MAX_NUMCLASSI);i++)
	{
	CRect sbR;
	sbR=rectB;
	sbR.top		+= (rectB.Size().cy/c_numClassi)*i;
	sbR.left	 = rectB.right + 2;
	sbR.bottom  -= (rectB.Size().cy/c_numClassi)*(c_numClassi-(i+1));
	sbR.right	 = rcBounds.right;
	genSize = c_cLabel[i].getSize(pdc);
	CPoint p(sbR.left,sbR.top+(sbR.Size().cy/2)-(genSize.cy/2));
	c_cLabel[i].setVisible(TRUE);
	c_cLabel[i].setPos(p,pdc);
	}
while (i<MAX_NUMCLASSI)
	{
	c_cLabel[i].setVisible(FALSE);
	c_cLabel[i].updateLabel(pdc);
	i++;
	}

// Caption

genSize = sCaption.getSize(pdc);
genRect.left = (rcBounds.right - genSize.cx)/2;
genRect.right = genRect.left + genSize.cx;
int deltay = (abs(rcBounds.top-rectB.top)-genSize.cy)/2	;
genRect.top	= rcBounds.top + deltay ; // aggiunto spazio per label orizzontali
genRect.bottom = rectB.top - deltay; 

CPoint p(genRect.left,genRect.top);
sCaption.setPos(p,pdc);

genRect.left = rcBounds.left;
genRect.right = genRect.left;
genRect.top	= rectB.bottom + genSize.cy/2 + tm.tmHeight;
genRect.bottom = genRect.top + genSize.cy; 

int invalidLabel [MAX_NUMLABEL];
for (int k=0;k<MAX_NUMLABEL;k++)
	invalidLabel[k] = k;
for (int i=0;i<MAX_NUMLABEL;i++)
	{// posizione negativa non scrive
	if ((c_vOrderLabel[i] < MAX_NUMLABEL)&&(c_vOrderLabel[i]>=0))
		{
		genSize = c_sLabel[c_vOrderLabel[i]].getSize(pdc);
		genRect.left = genRect.right + c_pxSepLabel[i];
		genRect.right = genRect.left + genSize.cx;
		p.x = genRect.left;
		p.y = genRect.top;
		c_sLabel[c_vOrderLabel[i]].setVisible(TRUE);
		c_sLabel[c_vOrderLabel[i]].setPos(p,pdc);

		// Edit DimX
		genSize = c_vLabel[c_vOrderLabel[i]].getSize(pdc);
		genRect.left = genRect.right + 10;
		genRect.right = genRect.left + genSize.cx;

		p.x = genRect.left;
		p.y = genRect.top;
		c_vLabel[c_vOrderLabel[i]].setVisible(TRUE);
		c_vLabel[c_vOrderLabel[i]].setPos(p,pdc);
		if (c_nlAfterLabel [i] == 1)
			{// Riga Successiva
			genRect.top	+= genSize.cy + genSize.cy/2;
			genRect.bottom = genRect.top + genSize.cy; 
			genRect.left = rcBounds.left;
			genRect.right = genRect.left;
			}
		// tengo traccia di quelle visualizzate
		invalidLabel[c_vOrderLabel[i]] = -1;
		}
	}

for (int k=0;k<MAX_NUMLABEL;k++)
	{
	if (invalidLabel[k] >= 0)
		{
		c_sLabel[invalidLabel[k]].setVisible(FALSE);
		c_vLabel[invalidLabel[k]].setVisible(FALSE);
		c_sLabel[invalidLabel[k]].updateLabel();
		c_vLabel[invalidLabel[k]].updateLabel();

		}
	}


pdc->RestoreDC(-1);

fontCaption.DeleteObject();
fontLabel.DeleteObject();
fontNormal.DeleteObject();
}



void Layout::CalcDrawRect (const CRect& rcBounds,CRect& rcDraw)
{

rcDraw = rcBounds;
/*
if (mode == NOLABELV)
	rcDraw.left += abs(rcDraw.Width() / 25);
else
*/
	rcDraw.left += abs((rcDraw.Width() * c_viewLeftPerc) / 100);
	
rcDraw.right	-= abs((rcDraw.Width() * c_viewRightPerc) / 100);

rcDraw.top	+= abs((rcDraw.Height() * c_viewTopPerc)/100);

rcDraw.bottom -= abs((rcDraw.Height() * c_viewBottomPerc)/100);
}



// Disegna esternamente 
void Layout::Draw3DRect (CDC* pdc,const CRect& rectB)
{
CPen dkGrayPen,whitePen,*oPen;
CRect r;

if (rectB.top > rectB.bottom)
	return;


r = rectB;

r.left -= 1;
r.top -= 1;
r.right += 1;
r.bottom += 1;

dkGrayPen.CreatePen(PS_SOLID,0,RGB(96,96,96));
whitePen.CreatePen(PS_SOLID,0,RGB(255,255,255));


oPen = pdc->SelectObject(&dkGrayPen);
pdc->MoveTo (r.left,r.bottom);
pdc->LineTo (r.left,r.top);
pdc->MoveTo (r.left,r.top);
pdc->LineTo (r.right,r.top);

// 3D Look not for print
if (!pdc->IsPrinting())
	pdc->SelectObject(&whitePen);

pdc->MoveTo (r.right,r.top);
pdc->LineTo (r.right,r.bottom);
pdc->MoveTo (r.right,r.bottom);
pdc->LineTo (r.left,r.bottom);

pdc->SelectObject(oPen);

dkGrayPen.DeleteObject();
whitePen.DeleteObject();
}

BOOL Layout::OnEraseBkgnd(CDC* pDC,CRect &rect) 
{
// TODO: Add your message handler code here and/or call default
// ByFC
//*********************************************

CBrush brushInt,brushExt,*oBrush;
CRgn rgnExt,rgnInt,rgnAnd,rgnLabelLV,rgnLabelRV;
rgnExt.CreateRectRgnIndirect(&rect);
rgnAnd.CreateRectRgnIndirect(&rect); 
CRect rcDraw;
CalcDrawRect (rect,rcDraw);

CRect rLV (rcDraw);
rLV.left = rect.left;
rLV.right = rcDraw.left; 
rgnLabelLV.CreateRectRgnIndirect(&rLV); 

CRect rRV (rcDraw);
rRV.left = rcDraw.right;
rRV.right = rect.right; 
rgnLabelRV.CreateRectRgnIndirect(&rRV); 
 
 
rgnInt.CreateRectRgnIndirect(&rcDraw);
if ((excludeUpdateBackGr)||
	(rgbBackColor != rgbDrawBackColor))
	rgnAnd.CombineRgn(&rgnExt,&rgnInt,RGN_DIFF);
else
	rgnAnd.CopyRgn(&rgnExt); 

if (excludeUpdateBkgLV)
	{
	rgnAnd.CombineRgn(&rgnAnd,&rgnLabelLV,RGN_DIFF);
	rgnAnd.CombineRgn(&rgnAnd,&rgnLabelRV,RGN_DIFF);
	}


brushExt.CreateSolidBrush (rgbBackColor);
brushExt.UnrealizeObject();
oBrush = pDC->SelectObject (&brushExt);    
pDC->FillRgn(&rgnAnd,&brushExt);

if ((!excludeUpdateBackGr)&&
	(rgbBackColor != rgbDrawBackColor))
	{
	brushInt.CreateSolidBrush (rgbDrawBackColor);
	brushInt.UnrealizeObject();
	pDC->SelectObject (&brushInt);
	pDC->FillRect(&rcDraw,&brushInt);
	}
pDC->SelectObject (oBrush);    

rgnExt.DeleteObject();
rgnAnd.DeleteObject();
rgnInt.DeleteObject();
rgnLabelLV.DeleteObject();
rgnLabelRV.DeleteObject();
brushExt.DeleteObject();
brushInt.DeleteObject();

return(TRUE);
}

BOOL Layout::OnEraseDrawBkgnd(CDC* pDC) 
{
// TODO: Add your message handler code here and/or call default
// ByFC
//*********************************************
CBrush brushInt,*oBrush;
 
CRect rcDraw;
CalcDrawRect (rcBounds,rcDraw);

brushInt.CreateSolidBrush (rgbDrawBackColor);
brushInt.UnrealizeObject();
oBrush = pDC->SelectObject (&brushInt);
pDC->FillRect(&rcDraw,&brushInt);
pDC->SelectObject (oBrush);    

brushInt.DeleteObject();
return(TRUE);
}




// r rettangolo massimo di visualizzazione
void SubLine::setScale(CRect &rectB,LineInfo &lInfo)
{

// Save Clip region
clip = rectB;
count = lInfo.count;
numStep = lInfo.numStep;
actual = lInfo.actual;


if (gVertex != NULL)
	{
	delete [] gVertex;
	gVertex = NULL;
	}

gVertex = new POINT [numStep+1];
if (gVertex == NULL) FatalAppExit (0,_T("Error: polyline creation"));


// Trasformo da coordinate di grafico a coordinate Video
double  val;
int index;
double oldVal;
double x,y,rxr,rxl,ryb,ryt;

rxl = clip.left;
rxr = clip.right;
ryb = clip.bottom -3;
ryt = clip.top;

for (int i=0; i<= numStep;i++)
	{

	x = rxl +((rxr - rxl)/numStep*i);

	if (i <= actual)
		index = actual - i;
	else
		index = (numStep - i) + actual;


	if (mode == LINEAR)
		{
		val = lInfo.val[index];
		val -= 	lInfo.min;
		y = ryb - ((ryb - ryt)*val/lInfo.max);
		}
	else
		{
		double MY = pow (10,log10(lInfo.max));
		double mY = pow (10,log10(lInfo.min));

		int numL = (int ) (log10(MY) - log10(mY));
		if (numL <= 0)
			numL = 1;
		int deltaY = (int)(ryb - ryt)/(numL);
		val = lInfo.val[index];
		val = log10(val/mY);
		y = (int) (val * deltaY);

		y = ryb - y;
		}

	gVertex[i].x = (int ) x;
	if (y < ryt)
		y = ryt;
	gVertex[i].y = (int ) y;
	}

// set limit value
if (mode == LINEAR)
	{
	val = lInfo.limit;
	val -= 	lInfo.min;
	y = ryb - ((ryb - ryt)*val/lInfo.max);
	}
else
	{
	double MY = pow (10,log10(lInfo.max));
	double mY = pow (10,log10(lInfo.min));

	int numL = (int ) (log10(MY) - log10(mY));
	if (numL <= 0)
		numL = 1;
	int deltaY = (int)(ryb - ryt)/(numL);
	val = lInfo.limit;
	val = log10(val/mY);
	y = (int) (val * deltaY);
		y = ryb - y;
	}
// set limit value
limit = (int )y;

x = rxl +((rxr - rxl)/numStep) * actual;

oldVal = 	val = lInfo.val[numStep];

y = ryb - ((ryb - ryt)*(oldVal)/lInfo.max);
oldPoint.x = (int ) x;
oldPoint.y = (int ) y;

}

void SubLine::draw(CDC* pdc)
{
// Save Context
// pdc->SaveDC();
// Select drawing tools
CPen penLarge,pen,penLimit,*oPen;

penLarge.CreatePen(PS_SOLID,3,colorPenLarge);
pen.CreatePen(PS_SOLID,2,colorPen);
penLimit.CreatePen(PS_DOT,0,RGB(0,0,0));


// Logical to device Coord
CRect clipRect = clip;
CRgn Clip;
// ComputeClippingRectangle(pdc,&clipRect);
 
// Now if we are in print preview mode then the clipping
// rectangle needs to be adjusted before creating the
// clipping region
if (pdc->IsKindOf(RUNTIME_CLASS(CPreviewDC)))
	{
    CPreviewDC *pPrevDC = (CPreviewDC *)pdc;
 	
	pPrevDC->PrinterDPtoScreenDP(&clipRect.TopLeft());
    pPrevDC->PrinterDPtoScreenDP(&clipRect.BottomRight());
 
    // Now offset the result by the viewport origin of
    // the print preview window...
 
    CPoint ptOrg;
    ::GetViewportOrgEx(pdc->m_hDC,&ptOrg);
    clipRect += ptOrg;
    }
 
// The following two function calls are the ones that
// select the clipping region into the DC. These would be
// whatever code you already have to create/select the
// clipping region

Clip.CreateRectRgnIndirect(&clipRect);
pdc->SelectClipRgn(&Clip);

// disegno limit -------- 
// 25-05-05
int bkm = pdc->GetBkMode(); 
pdc->SetBkMode(TRANSPARENT);
oPen = pdc->SelectObject(&penLimit);
if (c_drawLimit)
	{
	pdc->MoveTo (gVertex[0].x,limit);
	pdc->LineTo (gVertex[count].x,limit);
	}
pdc->SelectObject(&pen);
// Fine disegno limit
//------------------------


// Disegna Tutta La Line del Grafico
for(int i=0;i<count;i++)
	{
	if (i == actual)
		pdc->MoveTo (oldPoint);
	else
		pdc->MoveTo (gVertex[i]);
	pdc->LineTo (gVertex[i+1]);
	}

if (!pdc->IsPrinting())
	{
	pdc->SelectObject(&penLarge);

	pdc->MoveTo(gVertex[actual].x,clip.bottom);
	pdc->LineTo (gVertex[actual].x,clip.top);
	}

// Restore context
pdc->SelectObject(oPen);
penLarge.DeleteObject();
pen.DeleteObject();
penLimit.DeleteObject();

// ByFC
// Update 01-05-06
// fix BUG!!! must select null rgn before delete
pdc->SelectClipRgn(NULL);
//----------
Clip.DeleteObject();
//pdc->RestoreDC(-1);

if (gVertex != NULL)
	{
	delete [] gVertex;
	gVertex = NULL;
	}

pdc->SetBkMode(bkm);
}


// subrect

// r rettangolo massimo di visualizzazione
// torna posizione alta 
int SubRect::setScale(CRect &r,int offset,double max,double val)
{
double perc = val/max;

int delta = (int ) (perc * r.Height()); 

// save max rect
bar = r;
// offset inserito per modo TOWER
bar.bottom -= offset;
bar.top	= bar.bottom - abs(delta);
// Clipping
if (bar.top < r.top)
	bar.top = r.top;
if (bar.top > r.bottom)
	bar.top = r.bottom;

return (abs(bar.top-bar.bottom));

}




void SubRect::draw(CDC* pDC)
{
// Select drawing tools
CBrush brush,br,*oBrush;
CPen penTB,penLR,*oPen;



// Unused brush fix bug
br.CreateSolidBrush(lBrush.lbColor);
if (!brush.CreateBrushIndirect(&lBrush))
	{
	AfxMessageBox (_T("SW fail create BRUSH f. SubRect.draw"));
	return;
	}

if (!penTB.CreatePenIndirect(&lPenTB))
	{
	AfxMessageBox (_T("SW fail create PEN f. SubRect.draw"));
	return;
	}
if (!penLR.CreatePenIndirect(&lPenLR))
	{
	AfxMessageBox (_T("SW fail create PEN f. SubRect.draw"));
	return;
	}


if ((oPen = pDC->SelectObject(&penTB)) == NULL)
	{
	AfxMessageBox (_T("SW fail Select PEN f. SubRect.draw"));
	return;
	}

if ((oBrush = pDC->SelectObject(&brush)) == NULL)
 	{
	AfxMessageBox (_T("SW fail Select BRUSH f. SubRect.draw"));
	return;
	}

// Init hatch origin
CPoint org (bar.TopLeft());
pDC->SetBrushOrg(org);

pDC->Rectangle((LPCRECT)bar);
/*---------------------------
 Testo nel rettangolo
pDC->SetTextAlign(TA_CENTER | TA_BOTTOM);
// pDC->SetBkColor(lPen.lopnColor);
pDC->SetTextColor(RGB(0,0,0));

CPoint p;
p.x = (bar.right-bar.left)/2 + bar.left;
p.y = bar.bottom - 2;
CString str;
str.Format ("%c%1d",id,pos);
pDC->ExtTextOut (p.x,p.y,ETO_CLIPPED,
		bar,str,str.GetLength(),NULL);
--------------------------------*/
  
// Draw border
// Top and bottom compresi in rectangle

// Linea Sx
if ((pDC->SelectObject(&penLR)) == NULL)
	{
	AfxMessageBox (_T("SW fail Select PEN f. SubRect.draw"));
	return;
	}
CPoint point(bar.TopLeft());
pDC->MoveTo (point);
point.y = bar.BottomRight().y; 
pDC->LineTo (point);

// Linea Dx
point = bar.TopLeft();
point.x = bar.BottomRight().x;
pDC->MoveTo (point);
point.y = bar.BottomRight().y; 
pDC->LineTo (point);


// Restore context
pDC->SelectObject(oPen);
pDC->SelectObject(oBrush);
br.DeleteObject();
brush.DeleteObject();
penTB.DeleteObject();
penLR.DeleteObject();


}
  

void ViewDensUnit::setScale(CRect &r,double max,double val[])
// 	double valB,double valC,double valD)
{
// CRect ra,rb,rc,rd;
// split avail. rect in 4 sub rect
CRect ra ;
int Xsize,Ypos;
if (vMode == FLAT)
	{
	Xsize = (r.right - r.left)/subR.GetSize();
	}
else
	{// TOWER
	Xsize = (r.right - r.left);
	}
// |a b c d|

Ypos = 0;
// Calc Rect
ra = r;
for (int i=0;i<subR.GetSize();i++)
	{
	if (vMode == FLAT)
		{
		ra.right = ra.left;
		ra.left  = ra.right + Xsize;
		Ypos = 0;
		}
	Ypos += subR[i].setScale(ra,Ypos,max,val[i]);
	}

}


/////////////////////////////////////////////////////////////////////////////
// CDensView message handlers
/////////////////////////////////////////////////////////////////////////////
// CInitView

IMPLEMENT_DYNCREATE(CInitView, CPdfPrintView)

CInitView::CInitView()
{
vType = INITVIEW;

onlyTextReport = FALSE;
directPrint = FALSE;

c_leftMarg = 20;		// margine sinistro report in mm
c_rightMarg = 10;	// margine destro report in mm
c_topMarg = 30;		// margine alto report in mm
c_bottomMarg = 20;	// margine basso report in mm

c_numTextPage=0;		// numero di pagine testo 

m_DESCR_DOWNWEB = FALSE;
m_DESCR_CROSSWEB = FALSE;
m_DESCR_THRESHOLD = FALSE;
m_DESCR_COMPRESSION = FALSE;

}

CInitView::~CInitView()
{
}


BEGIN_MESSAGE_MAP(CInitView,CPdfPrintView)
	//{{AFX_MSG_MAP(CInitView)
	ON_COMMAND(ID_FILE_PRINT, OnFilePrint)
	ON_WM_ERASEBKGND()
	ON_COMMAND(ID_FILE_PRINT_PREVIEW, OnFilePrintPreview)
	ON_UPDATE_COMMAND_UI(ID_FILE_PRINT, OnUpdateFilePrint)
	ON_UPDATE_COMMAND_UI(ID_FILE_PRINT_PREVIEW, OnUpdateFilePrintPreview)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CInitView drawing

CRect CInitView::getDrawRect(void)
{
CRect rect (0,0,0,0);
return (rect);
}

CRect CInitView::getMeterRect(void)
{
CRect rect (0,0,0,0);
return (rect);
}

void CInitView::OnDraw(CDC* pDC)
{

	DOC_CLASS* pDoc = (DOC_CLASS *) GetDocument();
	// TODO: add draw code here
CRect rcBounds;
TEXTMETRIC tm;

pDC->SaveDC();

if (pDC->IsPrinting())
	{
	rcBounds =  prLimit;
	}
else
	{
	GetClientRect (rcBounds);
	}


// Ini Text Mode
pDC->GetTextMetrics(&tm);
pDC->SetBkMode (TRANSPARENT);


CFont fontCaption,fontCompany,fontNormal;
fontCaption.CreatePointFont (300,_T("Arial"),pDC);
LOGFONT plf;
fontCaption.GetLogFont(&plf);

plf.lfQuality = 1;
plf.lfUnderline = 1;
plf.lfWeight = 10000;
plf.lfItalic = 1;
fontCaption.DeleteObject();

fontCaption.CreateFontIndirect (&plf);

//------------------------
fontCompany.CreatePointFont (200,_T("Arial"),pDC);
LOGFONT pclf;
fontCompany.GetLogFont(&pclf);

pclf.lfQuality = 1;
pclf.lfUnderline = 1;
pclf.lfWeight = 10000;
pclf.lfItalic = 1;
fontCompany.DeleteObject();

fontCompany.CreateFontIndirect (&pclf);


fontNormal.CreatePointFont (120,_T("Arial"),pDC);

pDC->SelectObject(&fontCaption); 

CString str;
if (pDC->IsPrinting())
	{
	str.LoadString (CSM_GRAPHVIEW_PRINTCAPTION0);
	// str ="Report di Controllo Qualita`";
	}
else
	{
	str.LoadString (CSM_GRAPHVIEW_CAPTION0);
	// str ="CSM 20";
	}

CPoint pt;
CSize genSize;
genSize = pDC->GetOutputTextExtent(str,str.GetLength());

pDC->SetTextAlign(TA_CENTER | TA_TOP);

pt.x = rcBounds.left + (rcBounds.right - rcBounds.left)/2;

int deltay = 50;
pt.y	= rcBounds.top + deltay;

// Logo EDS
if (!pDC->IsPrinting())
	{

	HANDLE hDib = OpenDIB ("logoEds.bmp");
	if (hDib)
		{
		BITMAPINFOHEADER bi;
		DibInfo (hDib,&bi);
		StretchDibBlt (pDC->GetSafeHdc(),rcBounds.left + 30,rcBounds.top + 10, 200,100,hDib,0,0,bi.biWidth,bi.biHeight,SRCCOPY);
		FreeDib();
		}

 }

pDC->ExtTextOut (pt.x,pt.y,0,
		NULL,str,str.GetLength(),NULL);

pDC->SelectObject(&fontCompany); 
pt.y	+= genSize.cy;
pDC->ExtTextOut (pt.x,pt.y,0,
		NULL,pDoc->getCompany(),pDoc->getCompany().GetLength(),NULL);

pDC->SetTextAlign(TA_LEFT | TA_TOP);
if (pDoc->loadedData)
	{
	pDC->SelectObject(&fontNormal); 
	pt.x = rcBounds.left + 30;
	pt.y += (genSize.cy) ;

	int alignVal = 2*(rcBounds.right - rcBounds.left)/7;
	if (pDC->IsPrinting())
		alignVal = (rcBounds.right - rcBounds.left)/2;

	//str = "COIL       : " + pDoc->getRotolo();
	str.LoadString (CSM_GRAPHVIEW_ROTOLO);
	//str += pDoc->getRotolo();
	pDC->ExtTextOut (pt.x,pt.y,0,
		NULL,str,str.GetLength(),NULL);
	str = " : ";
	// extIncluded
	str += pDoc->getRotolo();
	pDC->ExtTextOut (pt.x+alignVal,pt.y,0,
		NULL,str,str.GetLength(),NULL);

	
	genSize = pDC->GetOutputTextExtent(str,str.GetLength());
	pt.y += genSize.cy + genSize.cy/2;
	//str = "PRODUCT    : " + pDoc->lega;
	str.LoadString (CSM_GRAPHVIEW_LEGA);
	pDC->ExtTextOut (pt.x,pt.y,0,
		NULL,str,str.GetLength(),NULL);
	str = " : ";
	str += pDoc->lega;
	pDC->ExtTextOut (pt.x+alignVal,pt.y,0,
		NULL,str,str.GetLength(),NULL);

	genSize = pDC->GetOutputTextExtent(str,str.GetLength());
	pt.y += genSize.cy + genSize.cy/2;

//	str = "STATO    : " + pDoc->stato;
//	pDC->ExtTextOut (pt.x,pt.y,0,
//		NULL,str,str.GetLength(),NULL);
//	genSize = pDC->GetOutputTextExtent(str,str.GetLength());

//	pt.y += genSize.cy + genSize.cy/2;

	CString s;
	// str = "SPESSORE (mm)   : " + s;
	str.LoadString (CSM_GRAPHVIEW_SPESSORE);
	str += s;
	pDC->ExtTextOut (pt.x,pt.y,0,
		NULL,str,str.GetLength(),NULL);
	s.Format (_T(" : %5.2lf"),pDoc->spessore);
	pDC->ExtTextOut (pt.x+alignVal,pt.y,0,
		NULL,s,s.GetLength(),NULL);
	genSize = pDC->GetOutputTextExtent(str,str.GetLength());

	pt.y += genSize.cy + genSize.cy/2;
	// str = "LENGTH (m)   : " + s;
	str.LoadString (CSM_GRAPHVIEW_LUNGHEZZA);
	pDC->ExtTextOut (pt.x,pt.y,0,
		NULL,str,str.GetLength(),NULL);
	str = _T(" : ");
	s.Format (_T("%d"),(int)pDoc->getVTotalLength());
	str += s;	
	pDC->ExtTextOut (pt.x+alignVal,pt.y,0,
		NULL,str,str.GetLength(),NULL);
	genSize = pDC->GetOutputTextExtent(str,str.GetLength());

	
	pt.y += genSize.cy + genSize.cy/2;
	// str = "WIDTH (mm)   : " + s;
	str.LoadString (CSM_GRAPHVIEW_LARGHEZZA);
	pDC->ExtTextOut (pt.x,pt.y,0,
		NULL,str,str.GetLength(),NULL);
	s.Format (_T(" : %5.0lf"),pDoc->c_larghezza);
	pDC->ExtTextOut (pt.x+alignVal,pt.y,0,
		NULL,s,s.GetLength(),NULL);
	genSize = pDC->GetOutputTextExtent(str,str.GetLength());
	

	pt.y += genSize.cy + genSize.cy/2;
	// str = "CUSTOMER     : " + pDoc->bolla;
	str.LoadString (CSM_GRAPHVIEW_CLIENTE);
	pDC->ExtTextOut (pt.x,pt.y,0,
		NULL,str,str.GetLength(),NULL);
	str = _T(" : ");
	str += pDoc->GetVCliente();
	pDC->ExtTextOut (pt.x+alignVal,pt.y,0,
		NULL,str,str.GetLength(),NULL);
	genSize = pDC->GetOutputTextExtent(str,str.GetLength());

	pt.y += genSize.cy + genSize.cy/2;
	CTime t (pDoc->startTime);
	// str = "DATE          : " + t.Format("%d-%m-%Y  %H:%M:%S");
	CString formatRes;
	formatRes.LoadString (CSM_GRAPHVIEW_DATETIMEFORMAT);
	str.LoadString(CSM_GRAPHVIEW_DATE);
	pDC->ExtTextOut (pt.x,pt.y,0,
		NULL,str,str.GetLength(),NULL);
	str = _T(" : ");
	str += t.Format(formatRes);
	pDC->ExtTextOut (pt.x+alignVal,pt.y,0,
		NULL,str,str.GetLength(),NULL);

	pt.y += genSize.cy + genSize.cy/2;
//	str = "INSPECTION CODE: " + pDoc->nomeRicetta;
	str.LoadString(CSM_GRAPHVIEW_RICETTA);
	pDC->ExtTextOut (pt.x,pt.y,0,
		NULL,str,str.GetLength(),NULL);
	str = _T(" : ");
	str += pDoc->nomeRicetta;
	pDC->ExtTextOut (pt.x+alignVal,pt.y,0,
		NULL,str,str.GetLength(),NULL);
	genSize = pDC->GetOutputTextExtent(str,str.GetLength());

	pt.y += genSize.cy + genSize.cy/2;

	//-----------------------------------------------------
	// sezione difetti
	int seg1 = (int)(pDoc->getVTotalLength() * m_INTERVALLO1 / 100);
	int seg2 = (int)(pDoc->getVTotalLength() * m_INTERVALLO2 / 100);
	int seg3 = (int)(pDoc->getVTotalLength() * m_INTERVALLO3 / 100);

	for (int nc=0;nc<pDoc->c_difCoil.getNumClassi();nc++)
		{
		// st.Format ("Totale Difetti Classe %c : %d \r\n",nc+'A',
		CString strRes;
		strRes.LoadString(CSM_GRAPHVIEW_TOTDIF);
		str.Format (strRes,nc+'A');
		pt.y += genSize.cy;
		pDC->ExtTextOut (pt.x,pt.y,0,
			NULL,str,str.GetLength(),NULL);
		str.Format(_T(" : %d"),(int)pDoc->c_difCoil.getTotDifetti(nc+'A',0,(int)pDoc->getVTotalLength()));
		pDC->ExtTextOut (pt.x+alignVal,pt.y,0,
			NULL,str,str.GetLength(),NULL);
		genSize = pDC->GetOutputTextExtent(str,str.GetLength());
		}
	CString strRes;
	strRes.LoadString(CSM_GRAPHVIEW_DETDIF);

	//st.Format ("Dettaglio Difetti tra 0 e %d %% : %d\r\n",(int)m_INTERVALLO1,(int)pDoc->c_difCoil.getTotDifetti(0,seg1));
	pt.y += genSize.cy;
	str.Format (strRes,0,(int)m_INTERVALLO1);
	pDC->ExtTextOut (pt.x,pt.y,0,
		NULL,str,str.GetLength(),NULL);
	
	str.Format(_T(" : %d"),(int)pDoc->c_difCoil.getTotDifetti(0,seg1));
	pDC->ExtTextOut (pt.x+alignVal,pt.y,0,
		NULL,str,str.GetLength(),NULL);
	genSize = pDC->GetOutputTextExtent(str,str.GetLength());

	//st.Format ("Dettaglio Difetti tra %d e %d %% : %d\r\n",(int)m_INTERVALLO1,(int)m_INTERVALLO2,(int)pDoc->c_difCoil.getTotDifetti(seg1,seg2));
	pt.y += genSize.cy;
	str.Format (strRes,(int)m_INTERVALLO1,(int)m_INTERVALLO2);
	pDC->ExtTextOut (pt.x,pt.y,0,
			NULL,str,str.GetLength(),NULL);

	str.Format(_T(" : %d"),(int)pDoc->c_difCoil.getTotDifetti(seg1,seg2));
	pDC->ExtTextOut (pt.x+alignVal,pt.y,0,
			NULL,str,str.GetLength(),NULL);
	
	genSize = pDC->GetOutputTextExtent(str,str.GetLength());

	// st.Format ("Dettaglio Difetti tra %d e %d %% : %d\r\n",(int)m_INTERVALLO2,(int)m_INTERVALLO3,(int)pDoc->c_difCoil.getTotDifetti(seg2,seg3));
	pt.y += genSize.cy;
	str.Format (strRes,(int)m_INTERVALLO2,(int)m_INTERVALLO3);
	pDC->ExtTextOut (pt.x,pt.y,0,
			NULL,str,str.GetLength(),NULL);
	str.Format(_T(" : %d"),(int)pDoc->c_difCoil.getTotDifetti(seg2,seg3));
	pDC->ExtTextOut (pt.x+alignVal,pt.y,0,
			NULL,str,str.GetLength(),NULL);
	genSize = pDC->GetOutputTextExtent(str,str.GetLength());

	//st.Format ("Dettaglio Difetti tra %d e 100 %% : %d\r\n",(int)m_INTERVALLO3,(int)pDoc->c_difCoil.getTotDifetti(seg3,(int)pDoc->c_difCoil.getMeter()));
	pt.y += genSize.cy;
	str.Format (strRes,(int)m_INTERVALLO3,100);
	pDC->ExtTextOut (pt.x,pt.y,0,
			NULL,str,str.GetLength(),NULL);
	str.Format(_T(" : %d"),(int)pDoc->c_difCoil.getTotDifetti(seg3,(int)pDoc->getVTotalLength()));
	pDC->ExtTextOut (pt.x+alignVal,pt.y,0,
			NULL,str,str.GetLength(),NULL);
	genSize = pDC->GetOutputTextExtent(str,str.GetLength());

	}
pDC->RestoreDC(-1);
}

/////////////////////////////////////////////////////////////////////////////
// CInitView diagnostics

#ifdef _DEBUG
void CInitView::AssertValid() const
{
	CPdfPrintView::AssertValid();
}

void CInitView::Dump(CDumpContext& dc) const
{
	CPdfPrintView::Dump(dc);
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CInitView message handlers

BOOL CInitView::Create(LPCTSTR lpszClassName, LPCTSTR lpszWindowName, DWORD dwStyle, const RECT& rect, CWnd* pParentWnd, UINT nID, CCreateContext* pContext) 
{
	// TODO: Add your specialized code here and/or call the base class
if (!CWnd::Create(lpszClassName, lpszWindowName, dwStyle, rect, pParentWnd, nID, pContext))
	return FALSE;

return (TRUE);
}

void CInitView::OnInitialUpdate() 
{
	CPdfPrintView::OnInitialUpdate();
	
	// TODO: Add your specialized code here and/or call the base class
	
// Load report option
DBReport dbReport(dBase);

if (!dbReport.openSelectDefault ())
	{
	AfxGetMainWnd()->MessageBox (_T("Error openSelectDefault"),_T("dbReport.Open"));
	dbReport.Close();
	return;
	}

//  Move Init Data from Db to Dialog
m_SOGLIE_ALLARME = dbReport.m_SOGLIE_ALLARME;
m_DATI_CLIENTE = dbReport.m_DATI_CLIENTE;
m_DATA_LAVORAZIONE = dbReport.m_DATA_LAVORAZIONE;
m_DESCR_PERIODICI = dbReport.m_DESCR_PERIODICI;
m_DESCR_RANDOM = dbReport.m_DESCR_RANDOM;
m_LUNGHEZZA_BOBINA = dbReport.m_LUNGHEZZA_BOBINA;
m_ALLARMISISTEMA = dbReport.m_MAPPA100;
m_ALMETRO = dbReport.m_ALMETRO;
m_DALMETRO = dbReport.m_DALMETRO;
m_INTERVALLO1 = dbReport.m_INTERVALLO1;
m_INTERVALLO2 = dbReport.m_INTERVALLO2;
m_INTERVALLO3 = dbReport.m_INTERVALLO3;

// Close DbReport	
dbReport.Close();
return ;	
}

//---------------------------------------------------------
//
//			 STAMPA
//
//---------------------------------------------------------

// Sequenza
// OnPreparePrinting
// DoPreparePrinting

// Loop OnPrint
// OnEndPrinting
// Init device dependent

// prepare textPage in CPage mode
// calcola quante pagine servono
// calcolo quante righe di tabella 16 colonne 4 blocchi
int numLineDif = 0;
int numLineAl1 = 0;
int numLineAl2 = 0;
int CInitView::prepareTextPageEx(CDC* pDC,CRect r) 
{
CLineDoc* pDoc = (CLineDoc* )GetDocument();

// calcolo quante righe di tabella 16 colonne 4 blocchi
numLineDif = 0;
numLineAl1 = 0;
numLineAl2 = 0;
if (m_DESCR_RANDOM)
	{// use table
	// Intestazione: Metro,Numero dei fori,Classe difetto,Banda
	//1 riga
	numLineDif ++;
	// Aggiunta dettaglio difetti per ogni decaMetro difettoso indico
	CString sItem;
	for (int i=0;i<	pDoc->c_difCoil.GetSize();i++)
		{
		double pos;
		// pos =  (pDoc->c_difCoil.trueSizeEncod/1000.)*i;
		// Posizione a posteriori (valore 10 indica da 0 a 10)
		pos =  pDoc->c_difCoil[i].posizione; // posizione del metro dif
		// NOT trepassing total length
		if (pos >= pDoc->getVTotalLength())
			break;
		int numItem = 0;
		// nel metro ogni scheda
		for (int j=0;j<pDoc->c_difCoil[i].GetSize();j++)
			{// Schede fuori finestra ispezione NON gestite
			// Ogni classe
			for (int k=0;k<pDoc->c_difCoil[i].ElementAt(j).GetSize();k++)
				{
				int v = pDoc->c_difCoil[i].ElementAt(j).ElementAt(k);
				if (v > 0)
					{// Bande cominciano da 1 non da zero
					numLineDif ++;
					}
				}
		   	}	
		}
	}

// tre dati x ogni riga 
// Fix Bug 20-06-05 3 righe resto %3 no %4
numLineDif = (numLineDif / 3) + ((numLineDif % 3)?1:0);

if (m_SOGLIE_ALLARME)
	{// use region
	numLineAl1 = pDoc->densitaAlarm.getNumLine();
	}

if (m_ALLARMISISTEMA)
	{// use region
	numLineAl2 = pDoc->systemAlarm.getNumLine();
	}

CPage*	ps= new CPage(r,pDC,FALSE,MM_TEXT);

// calcolo dimensione 1 riga
TABLEHEADER* pTable=new TABLEHEADER;        
pTable->PointSize=10;
pTable->LineSize=1;    // default shown only for demp purposes
pTable->NumPrintLines=1;
pTable->UseInches=TRUE;
pTable->AutoSize=FALSE;
pTable->Border=TRUE;
pTable->FillFlag=FILL_LTGRAY;
pTable->NumColumns=12;
pTable->NumRows = 1;
pTable->StartRow=0.0;
pTable->StartCol=0.0;
pTable->EndCol=ps->SetRightMargin(0.);
pTable->HeaderLines=0;
ps->Table(pTable);	

double dimOneRow=pTable->EndRow;

// almeno 1 header
double sizeTable = 0.;
int numHeader = 0;
if (m_DESCR_RANDOM&&(numLineDif>0))
	{
	sizeTable += dimOneRow * (numLineDif + 4);
	numHeader ++;
	}
if (m_SOGLIE_ALLARME&&(numLineAl1 > 0))
	{
	sizeTable += dimOneRow * (numLineAl1 + 4);
	numHeader ++;
	}
if (m_ALLARMISISTEMA&&(numLineAl2>0))
	{
	sizeTable += dimOneRow * (numLineAl2 + 4);
	numHeader ++;
	}

//aggiungo 1 riga per n-1 header
if (numHeader > 0)
	sizeTable += dimOneRow * (numHeader - 1);


double bTop,bBottom;
CRect internalRect = calcPrintInternalRect(pDC,r);

bTop = ps->ConvertYToInches(internalRect.top);
bBottom = ps->ConvertYToInches(internalRect.bottom);
double sizePageY = bBottom - bTop;
double lineXPage = sizePageY / dimOneRow;

int numPage = max(1,(int)(sizeTable / sizePageY));
if ((numLineDif==0)&&
	(numLineAl1==0)&&
	(numLineAl2==0))
	numPage = 0;

// ricalcolo con 1 header per pagina
if (numPage > 1)
	{
	// almeno 1 header per pagina meno prima gia` calcolata
	int extraRow = 3 * (numPage-1);
	if (m_ALLARMISISTEMA)
		extraRow +=3;
	if (m_SOGLIE_ALLARME)
		extraRow +=3;
	// cluster
	extraRow += 3;
	sizeTable = sizeTable + (dimOneRow  *  extraRow);
	// caso in cui allarmi sistema sta` su due pagine
	}
numPage = (int)(ceil(sizeTable / sizePageY));

// caso in cui soglie allarmi sta` su due pagine
//if (floor(sizeTable / sizePageY)> 0. )
//	numPage++;

// lascio spazio per region

int nPage = numPage;
delete pTable;
delete ps;
// Set print Num Page X text	
return (nPage);
}

int numLineAl3 = 0;
int CInitView::prepareTextPageEx2(CDC* pDC,CRect r) 
{
CLineDoc* pDoc = (CLineDoc* )GetDocument();

CPage*	ps= new CPage(r,pDC,FALSE,MM_TEXT);

// calcolo dimensione 1 riga
TABLEHEADER* pTable=new TABLEHEADER;        
pTable->PointSize=10;
pTable->LineSize=1;    // default shown only for demp purposes
pTable->NumPrintLines=1;
pTable->UseInches=TRUE;
pTable->AutoSize=FALSE;
pTable->Border=TRUE;
pTable->FillFlag=FILL_LTGRAY;
pTable->NumColumns=12;
pTable->NumRows = 1;
pTable->StartRow=0.0;
pTable->StartCol=0.0;
pTable->EndCol=ps->SetRightMargin(0.);
pTable->HeaderLines=0;
ps->Table(pTable);	

double dimOneRow=pTable->EndRow;

// almeno 1 header
double sizeTable = 0.;
int numHeader = 0;



// calcolo quante righe di tabella 16 colonne 4 blocchi
numLineAl3 = 0;
if (m_SOGLIE_ALLARME)
	{// use region
	numLineAl3 = pDoc->clusterAlarm.getNumLine();
	}

// cluster
	{
	sizeTable += dimOneRow * (numLineAl3 + 2);
	numHeader ++;
	}

//aggiungo 1 riga per n-1 header
if (numHeader > 0)
	sizeTable += dimOneRow * (numHeader - 1);
	

double bTop,bBottom;
CRect internalRect = calcPrintInternalRect(pDC,r);

bTop = ps->ConvertYToInches(internalRect.top);
bBottom = ps->ConvertYToInches(internalRect.bottom);
double sizePageY = bBottom - bTop;
double lineXPage = sizePageY / dimOneRow;

int numPage = max(1,(int)(sizeTable / sizePageY));
if (numLineAl3==0)
	numPage = 0;

// ricalcolo con 1 header per pagina
if (numPage > 1)
	{
	// almeno 1 header per pagina meno prima gia` calcolata
	int extraRow = 3 * (numPage-1);
	// cluster
	extraRow += 3;
	sizeTable = sizeTable + (dimOneRow  *  extraRow);
	// caso in cui allarmi sistema sta` su due pagine
	}
if (numLineAl3 > 0)
	numPage = (int)(ceil(sizeTable / sizePageY));

// caso in cui soglie allarmi sta` su due pagine
//if (floor(sizeTable / sizePageY)> 0. )
//	numPage++;

// lascio spazio per region

int nPage = numPage;
delete pTable;
delete ps;
// Set print Num Page X text	
return (nPage);
}




// pageRelative contiene indice della pagina in stampa
// viene calcolato 
double CInitView::printTextPageEx(CPage *ps,CRect rcBounds,int pageIndex) 
{
CLineDoc* pDoc = (CLineDoc* )GetDocument();

double bTop,bLeft,bRight,bBottom;

bTop = ps->ConvertYToInches(rcBounds.top);
bLeft = ps->ConvertXToInches(rcBounds.left);
bBottom = ps->ConvertYToInches(rcBounds.bottom);
bRight = ps->ConvertXToInches(rcBounds.right);

double lastYPos = bTop;

TABLEHEADER* pTable1=new TABLEHEADER;        
pTable1->PointSize=10;
pTable1->LineSize=1;    // default shown only for demp purposes
pTable1->NumPrintLines=1;
pTable1->UseInches=TRUE;
pTable1->AutoSize=FALSE;
pTable1->Border=TRUE;
pTable1->FillFlag=FILL_NONE;
pTable1->NumColumns=12;
pTable1->NumRows = 1;
pTable1->StartRow=0.0;
pTable1->StartCol=0.0;
pTable1->EndCol=bRight;
pTable1->HeaderLines=0;

ps->setRealPrint(FALSE);
ps->Table(pTable1);	
ps->setRealPrint(TRUE);

double sizeOneRow =pTable1->EndRow;
// nessuna variazione solo valore attuale
double hSize = bBottom - bTop;
// Numero righe per pagina ( tolgo intestazione )
int numRowXPage = (int)(hSize/sizeOneRow);
// righe di pagina stampate
int pagePrintedRow = pageIndex * numRowXPage;
int difRowToPrint = 0;
if (m_DESCR_RANDOM)
	{
	// finora ho stampato
	int difPrintedRow = pageIndex * (numRowXPage-2);
	// ancora da stampare 
	difRowToPrint = min((numRowXPage-2),numLineDif-difPrintedRow);
	
	if (difRowToPrint>0)
		{// use table
		TABLEHEADER* pTable2 = NULL;        
		pTable2=new TABLEHEADER;        
		pTable2->PointSize=10;
		pTable2->LineSize=1;    // default shown only for demp purposes
		pTable2->NumPrintLines=1;
		pTable2->UseInches=TRUE;
		pTable2->AutoSize=FALSE;
		pTable2->Border=TRUE;
		pTable2->FillFlag=FILL_LTGRAY;
		pTable2->NumColumns=12;
		pTable2->NumRows = difRowToPrint;
		pTable2->StartRow=bTop;
		pTable2->StartCol=bLeft;
		pTable2->EndCol=bRight;
		pTable2->HeaderLines=3;
		// Intestazione: Metro,Numero dei fori,Classe difetto,Banda
		CString s;
		s.LoadString(ID_CSM_REPORT_METRO);
		// pTable2->ColDesc[0].Init(0.7,"Metro",FILL_LTGRAY);
		pTable2->ColDesc[0].Init(0.7,s,FILL_LTGRAY);
		s.LoadString(ID_CSM_REPORT_NUMFORI);
		// pTable2->ColDesc[1].Init(0.5,"Num. Fori",FILL_NONE);
		pTable2->ColDesc[1].Init(0.5,s,FILL_NONE);
		s.LoadString(ID_CSM_REPORT_CLASSE);
		// pTable2->ColDesc[2].Init(0.5,"Classe",FILL_NONE);
		pTable2->ColDesc[2].Init(0.5,s,FILL_NONE);
		s.LoadString(ID_CSM_REPORT_BANDA);
		//pTable2->ColDesc[3].Init(0.5,"Banda",FILL_NONE);
		pTable2->ColDesc[3].Init(0.5,s,FILL_NONE);
		
		s.LoadString(ID_CSM_REPORT_METRO);
		//pTable2->ColDesc[4].Init(0.7,"Metro",FILL_LTGRAY);
		pTable2->ColDesc[4].Init(0.7,s,FILL_LTGRAY);
		s.LoadString(ID_CSM_REPORT_NUMFORI);
		//pTable2->ColDesc[5].Init(0.5,"Num. Fori",FILL_NONE);
		pTable2->ColDesc[5].Init(0.5,s,FILL_NONE);
		s.LoadString(ID_CSM_REPORT_CLASSE);
		//pTable2->ColDesc[6].Init(0.5,"Classe",FILL_NONE);
		pTable2->ColDesc[6].Init(0.5,s,FILL_NONE);
		s.LoadString(ID_CSM_REPORT_BANDA);
		//pTable2->ColDesc[7].Init(0.5,"Banda",FILL_NONE);
		pTable2->ColDesc[7].Init(0.5,s,FILL_NONE);

		s.LoadString(ID_CSM_REPORT_METRO);
		//pTable2->ColDesc[8].Init(0.7,"Metro",FILL_LTGRAY);
		pTable2->ColDesc[8].Init(0.7,s,FILL_LTGRAY);
		s.LoadString(ID_CSM_REPORT_NUMFORI);
		//pTable2->ColDesc[9].Init(0.5,"Num. Fori",FILL_NONE);
		pTable2->ColDesc[9].Init(0.5,s,FILL_NONE);
		s.LoadString(ID_CSM_REPORT_CLASSE);
		//pTable2->ColDesc[10].Init(0.5,"Classe",FILL_NONE);
		pTable2->ColDesc[10].Init(0.5,s,FILL_NONE);
		s.LoadString(ID_CSM_REPORT_BANDA);
		//pTable2->ColDesc[11].Init(0.5,"Banda",FILL_NONE);
		pTable2->ColDesc[11].Init(0.5,s,FILL_NONE);
		ps->setRealPrint(TRUE);
		ps->Table(pTable2);	
		lastYPos = pTable2->EndRow;
		// Aggiunta dettaglio difetti per ogni decaMetro difettoso indico
		CString sItem;
		int ColPos = 0;
		int RowPos = 0;
		int curRow = 0;
		for (int i=0;i<	pDoc->c_difCoil.GetSize();i++)
			{
			double pos;
			// pos =  (pDoc->c_difCoil.trueSizeEncod/1000.)*i;
			// Posizione a posteriori (valore 10 indica da 0 a 10)
			pos =  pDoc->c_difCoil[i].posizione; // posizione del metro dif
			// NOT trepassing total length
			if (pos >= pDoc->getVTotalLength())
				break;
			// nel metro ogni scheda
			for (int j=0;j<pDoc->c_difCoil[i].GetSize();j++)
				{// Schede fuori finestra ispezione NON gestite
				// Ogni classe
				for (int k=0;k<pDoc->c_difCoil[i].ElementAt(j).GetSize();k++)
					{
					int v = pDoc->c_difCoil[i].ElementAt(j).ElementAt(k);
					if (v > 0)
						{// Bande cominciano da 1 non da zero
						if (RowPos > difRowToPrint)
							break;
						if (++curRow > (difPrintedRow*3))
							{
							sItem.Format (_T("%7.1lf"),pos);
							ps->Print(pTable2,RowPos,ColPos++,8,TEXT_BOLD|TEXT_CENTER,(LPCTSTR)sItem);
							if (ColPos >= 12)
								{
								RowPos++;
								ColPos = 0;
								}

							sItem.Format (_T("%3d"),v);
							ps->Print(pTable2,RowPos,ColPos++,8,TEXT_BOLD|TEXT_CENTER,(LPCTSTR)sItem);
							if (ColPos >= 12)
								{
								RowPos++;
								ColPos = 0;
								}
							
							sItem.Format (_T("%c"),'A'+k);
							ps->Print(pTable2,RowPos,ColPos++,8,TEXT_BOLD|TEXT_CENTER,(LPCTSTR)sItem);
							if (ColPos >= 12)
								{
								RowPos++;
								ColPos = 0;
								}							
							sItem.Format (_T("%3d"),pDoc->c_difCoil[i].ElementAt(j).banda+1);
							ps->Print(pTable2,RowPos,ColPos++,8,TEXT_BOLD|TEXT_CENTER,(LPCTSTR)sItem);
							if (ColPos >= 12)
								{
								RowPos++;
								ColPos = 0;
								}
							}
						}
					}
		   		}	
			}
		delete pTable2;
		}
	}

if(difRowToPrint < 0)
	difRowToPrint = 0;

int al1RowToPrint=0;
if (m_SOGLIE_ALLARME)
	{// use region
	//	Spazio totale fino ad ora = paginePrec + difetti ev. pagina attuale
	int totalPrintedRow = pagePrintedRow+difRowToPrint;
	int pageFreeRow = (difRowToPrint==0)?(numRowXPage):(numRowXPage-difRowToPrint-3);
	// ancora da stampare 
	int tmpRowToPrint = pDoc->densitaAlarm.getNumLine()+numLineDif+pageIndex*2;
	if ((m_DESCR_RANDOM)&&
		(numLineDif>0))
		tmpRowToPrint += 3;
	al1RowToPrint = min(pageFreeRow-2,(tmpRowToPrint)-totalPrintedRow);

	// quelle da stampare sono meno dello spazio teorico
	al1RowToPrint = min(al1RowToPrint,pDoc->densitaAlarm.getNumLine());
	
	if (difRowToPrint>0)
		{
		lastYPos += 1 * sizeOneRow;
		}
	if (al1RowToPrint > 0)
		{
		TABLEHEADER* pTable3 = NULL;        
		pTable3=new TABLEHEADER;  
		pTable3->SetSkip=TRUE;	// no fill 
		pTable3->PointSize=10;
		pTable3->LineSize=1;    // default shown only for demp purposes
		pTable3->NumPrintLines=1;
		pTable3->UseInches=TRUE;
		pTable3->AutoSize=FALSE;
		pTable3->Border=TRUE;
		pTable3->VLines = FALSE;	//	true draw vertical seperator lines
		pTable3->HLines = FALSE;    // ditto on horizontal lines
		pTable3->FillFlag=FILL_LTGRAY;
		pTable3->NumColumns=1;
		pTable3->NumRows = al1RowToPrint;
		pTable3->StartRow=lastYPos;
		pTable3->StartCol=bLeft;
		pTable3->EndCol=bRight;
		pTable3->HeaderLines=3;
		// Intestazione: Metro,Numero dei fori,Classe difetto,Banda
		CString sxs;
		sxs.LoadString(CSM_GRAPHVIEW_DETTAGLIOSOGLIE);
		//pTable3->ColDesc[0].Init(0.7,"Soglie di Allarme",FILL_NONE);
		pTable3->ColDesc[0].Init(0.7,sxs,FILL_NONE);
		ps->setRealPrint(TRUE);
		ps->Table(pTable3);	
		// tutti dif. + 2 intestaz. di * pagina + intestaz pagina mista
		int curRow=numLineDif+pageIndex*2;
		if ((m_DESCR_RANDOM)&&
			(numLineDif>0))
			curRow += 3;

		int al1PrintedRow = 0;
		for(int i=0;i<pDoc->densitaAlarm.getNumLine();i++)
			{
			if (al1PrintedRow > al1RowToPrint)
				break;
			if (++curRow > (totalPrintedRow))
				{
				CString s;
				s.Format(_T("  [%3d] "),i+1);
				s += pDoc->densitaAlarm.c_messages[i].Left(pDoc->densitaAlarm.c_messages[i].GetLength()-2);
				ps->Print(pTable3,al1PrintedRow,0,8,TEXT_BOLD|TEXT_LEFT,(LPCTSTR)s);
				al1PrintedRow++;
				}
			}
		lastYPos = pTable3->EndRow;
		delete pTable3;
		}
	}

if(al1RowToPrint < 0)
	al1RowToPrint = 0;
int al2RowToPrint = 0;
if (m_ALLARMISISTEMA)
	{// use region
	//	Spazio totale fino ad ora = paginePrec + difetti ev. pagina attuale
	int totalPrintedRow = pagePrintedRow+difRowToPrint+al1RowToPrint;
	int pageFreeRow = numRowXPage;
	if (difRowToPrint > 0)
		pageFreeRow -= (difRowToPrint+3);
	if (al1RowToPrint > 0) // tolgo una intestaz e uno spazio in +
		pageFreeRow -= (al1RowToPrint+3);

	int tmpRowToPrint = pDoc->systemAlarm.getNumLine()+numLineDif+numLineAl1+pageIndex*2;
	if ((m_DESCR_RANDOM)&&
		(numLineDif>0))
		tmpRowToPrint += 3;
	if ((m_SOGLIE_ALLARME)&&
		(numLineAl1>0))
		tmpRowToPrint += 3;

	al2RowToPrint = min(pageFreeRow-2,(tmpRowToPrint)-totalPrintedRow);
	
	// quelle da stampare sono meno dello spazio teorico
	al2RowToPrint = min(al2RowToPrint,pDoc->systemAlarm.getNumLine());
		
	if (difRowToPrint>0)
		lastYPos += 1 * sizeOneRow;
	if (al1RowToPrint>0)
		lastYPos += 1 * sizeOneRow;

	if (al2RowToPrint > 0)
		{
		TABLEHEADER* pTable4 = NULL;        
		pTable4=new TABLEHEADER;        
		pTable4->SetSkip=TRUE;
		pTable4->PointSize=10;
		pTable4->LineSize=1;    // default shown only for demp purposes
		pTable4->NumPrintLines=1;
		pTable4->UseInches=TRUE;
		pTable4->AutoSize=FALSE;
		pTable4->Border=TRUE;
		pTable4->VLines = FALSE;	//	true draw vertical seperator lines
		pTable4->HLines = FALSE;    // ditto on horizontal lines
		pTable4->FillFlag=FILL_LTGRAY;
		pTable4->NumColumns=1;
		pTable4->NumRows = al2RowToPrint;
		pTable4->StartRow=lastYPos;
		pTable4->StartCol=bLeft;
		pTable4->EndCol=bRight;
		pTable4->HeaderLines=3;
		// Intestazione: Metro,Numero dei fori,Classe difetto,Banda
		CString sxs;
		sxs.LoadString(CSM_GRAPHVIEW_ALLARMISISTEMA);
		//pTable4->ColDesc[0].Init(0.7,"Allarmi di sistema",FILL_NONE);
		pTable4->ColDesc[0].Init(0.7,sxs,FILL_NONE);
		ps->setRealPrint(TRUE);
		ps->Table(pTable4);	
		// tutti dif. + 2 intestaz. di * pagina + intestaz pagina mista
		int curRow=numLineDif+numLineAl1+pageIndex*2;
		if ((m_SOGLIE_ALLARME)&&
			(numLineAl1>0))
			curRow += 3;
		if ((m_DESCR_RANDOM)&&
			(numLineDif>0))
			curRow += 3;

		int al2PrintedRow = 0;
		for(int i=0;i<pDoc->systemAlarm.getNumLine();i++)
			{
			if (al2PrintedRow > al2RowToPrint)
				break;
			if (++curRow > (totalPrintedRow))
				{
				CString s;
				s.Format(_T("  [%3d] "),i+1);
				s += pDoc->systemAlarm.c_messages[i].Left(pDoc->systemAlarm.c_messages[i].GetLength()-2);
				ps->Print(pTable4,al2PrintedRow,0,8,TEXT_BOLD|TEXT_LEFT,(LPCTSTR)s);
				al2PrintedRow++;
				}
			}
		lastYPos = pTable4->EndRow;
		delete pTable4;
		}
	}


delete pTable1;
// Set print Num Page X text	
return (0.0);
}

// ------------------------------------------------------------
// viene calcolato
// cluster 
double CInitView::printTextPageEx2(CPage *ps,CRect rcBounds,int pageIndex) 
{
CLineDoc* pDoc = (CLineDoc* )GetDocument();

double bTop,bLeft,bRight,bBottom;

bTop = ps->ConvertYToInches(rcBounds.top);
bLeft = ps->ConvertXToInches(rcBounds.left);
bBottom = ps->ConvertYToInches(rcBounds.bottom);
bRight = ps->ConvertXToInches(rcBounds.right);

double lastYPos = bTop;

TABLEHEADER* pTable1=new TABLEHEADER;        
pTable1->PointSize=10;
pTable1->LineSize=1;    // default shown only for demp purposes
pTable1->NumPrintLines=1;
pTable1->UseInches=TRUE;
pTable1->AutoSize=FALSE;
pTable1->Border=TRUE;
pTable1->FillFlag=FILL_NONE;
pTable1->NumColumns=12;
pTable1->NumRows = 1;
pTable1->StartRow=0.0;
pTable1->StartCol=0.0;
pTable1->EndCol=bRight;
pTable1->HeaderLines=0;

ps->setRealPrint(FALSE);
ps->Table(pTable1);	
ps->setRealPrint(TRUE);

double sizeOneRow =pTable1->EndRow;
// nessuna variazione solo valore attuale
double hSize = bBottom - bTop;
// Numero righe per pagina ( tolgo intestazione )
int numRowXPage = (int)(hSize/sizeOneRow);
// righe di pagina stampate
int pagePrintedRow = pageIndex * numRowXPage;
int difRowToPrint = 0;

// cluster
// if (m_ALLARMISISTEMA)
	{// use region
	//	Spazio totale fino ad ora = paginePrec + difetti ev. pagina attuale
	int totalPrintedRow = pagePrintedRow+difRowToPrint;
	int pageFreeRow = numRowXPage;

	int tmpRowToPrint = pDoc->systemAlarm.getNumLine()+numLineDif+pageIndex*2;
	int al3RowToPrint = min(pageFreeRow-2,(tmpRowToPrint)-totalPrintedRow);
	
	// quelle da stampare sono meno dello spazio teorico
	al3RowToPrint = min(al3RowToPrint,pDoc->clusterAlarm.getNumLine());

	if (al3RowToPrint > 0)
		{
		TABLEHEADER* pTable4 = NULL;        
		pTable4=new TABLEHEADER;        
		pTable4->SetSkip=TRUE;
		pTable4->PointSize=10;
		pTable4->LineSize=1;    // default shown only for demp purposes
		pTable4->NumPrintLines=1;
		pTable4->UseInches=TRUE;
		pTable4->AutoSize=FALSE;
		pTable4->Border=TRUE;
		pTable4->VLines = FALSE;	//	true draw vertical seperator lines
		pTable4->HLines = FALSE;    // ditto on horizontal lines
		pTable4->FillFlag=FILL_LTGRAY;
		pTable4->NumColumns=1;
		pTable4->NumRows = al3RowToPrint;
		pTable4->StartRow=lastYPos;
		pTable4->StartCol=bLeft;
		pTable4->EndCol=bRight;
		pTable4->HeaderLines=3;
		// Intestazione: Metro,Numero dei fori,Classe difetto,Banda
		CString sxs;
		sxs.LoadString(CSM_GRAPHDOC_ALCLUSTER);
		//pTable4->ColDesc[0].Init(0.7,"Allarmi dei Cluster",FILL_NONE);
		pTable4->ColDesc[0].Init(0.7,sxs,FILL_NONE);
		ps->setRealPrint(TRUE);
		ps->Table(pTable4);	
		// tutti dif. + 2 intestaz. di * pagina + intestaz pagina mista
		int curRow=numLineDif+numLineAl1+numLineAl2+pageIndex*2;

		int al3PrintedRow = 0;
		for(int i=0;i<pDoc->clusterAlarm.getNumLine();i++)
			{
			if (al3PrintedRow > al3RowToPrint)
				break;
			if (++curRow > (totalPrintedRow))
				{
				CString s;
				s.Format(_T("  [%3d] "),i+1);
				s += pDoc->clusterAlarm.c_messages[i].Left(pDoc->clusterAlarm.c_messages[i].GetLength()-2);
				ps->Print(pTable4,al3PrintedRow,0,8,TEXT_BOLD|TEXT_LEFT,(LPCTSTR)s);
				al3PrintedRow++;
				}
			}
		lastYPos = pTable4->EndRow;
		delete pTable4;
		}
	}


delete pTable1;
// Set print Num Page X text	
return (0.0);
}





int CInitView::prepareTextPage(CDC* pDC) 
{
CSize printSize,trueSize;
// in stampa tolgo bordi
// pixel
printSize.cx = pDC->GetDeviceCaps(HORZRES);
printSize.cy = pDC->GetDeviceCaps(VERTRES);

// millimetri
trueSize.cx = pDC->GetDeviceCaps(HORZSIZE);
trueSize.cy = pDC->GetDeviceCaps(VERTSIZE);

// calcolo px per mm
CSize pixel;
pixel.cy = printSize.cy/trueSize.cy;
pixel.cx = printSize.cx/trueSize.cx;

/* Tolgo 2 cm a destra ed 1 a sx */
printSize.cx -= (c_leftMarg + c_rightMarg) * pixel.cx;
/* Tolgo 3 cm in alto e 2 in basso */	
printSize.cy -= (c_topMarg + c_bottomMarg) * pixel.cy;

CFont fontNormal,*pold;
fontNormal.CreatePointFont (pagePr.getFontNormal()->size,pagePr.getFontNormal()->name,pDC);
pold = pDC->SelectObject(&fontNormal); 
CString baseString ("M.00000.0,000,0,000   ");
CSize sz;
sz = pDC->GetOutputTextExtent(baseString,baseString.GetLength());
TEXTMETRIC tm;
pDC->GetTextMetrics(&tm);

// sz.cy = tm.tmHeight; 
sz.cy = tm.tmHeight + tm.tmExternalLeading;
sz.cy += (tm.tmHeight + tm.tmExternalLeading)/6;

pDC->SelectObject(pold); 

LinePerPage = printSize.cy/sz.cy;
ColumnPerPage  = printSize.cx/sz.cx;

// Bottom Border
LinePerPage -= 3;

CLineDoc* pDoc = (CLineDoc* )GetDocument();

int numPage	= 0;
int numPageEnd	= 0;




pagePr.clear();
pagePrEnd.clear();		  // Update Modify report

int nl = 0;
int nlEnd = 0;

if (m_DESCR_RANDOM)
	{
	CString str;
	if ((nl % LinePerPage) != 0)
		{// Inizio pagina nessuna linea in piu`
		str = "   ";
		pagePr.add(str,TA_LEFT | TA_TOP);
		nl += 1;
		}
	// str = "DETTAGLIO DIFETTI  ";
	str.LoadString(CSM_GRAPHVIEW_DETTAGLIODIF);
	pagePr.add(str,TA_CENTER | TA_TOP);
	nl += 1;

	str = "   ";
	pagePr.add(str,TA_LEFT | TA_TOP);
	nl += 1;

	CString st;


	// Aggiunta dettaglio difetti per ogni decaMetro difettoso indico
	// Metro,Numero dei fori,Classe difetto,Banda


	CString sItem;
	int maxItem = ColumnPerPage;
	for (int i=0;i<	pDoc->c_difCoil.GetSize();i++)
		{
		double pos;
		// pos =  (pDoc->c_difCoil.trueSizeEncod/1000.)*i;
		// Posizione a posteriori (valore 10 indica da 0 a 10)
		pos =  pDoc->c_difCoil[i].posizione; // posizione del metro dif
		// NOT trepassing total length
		if (pos >= pDoc->getVTotalLength())
			break;
		int numItem = 0;
		sItem.Empty();
		st.Empty();
		// nel metro ogni scheda
		for (int j=0;j<pDoc->c_difCoil[i].GetSize();j++)
			{// Schede fuori finestra ispezione NON gestite
		//	if (pDoc->c_difCoil[i].size == INFINITO)
		//		continue;
			// Ogni classe
			for (int k=0;k<pDoc->c_difCoil[i].ElementAt(j).GetSize();k++)
				{
				int v = pDoc->c_difCoil[i].ElementAt(j).ElementAt(k);
				if (v > 0)
					{// Bande cominciano da 1 non da zero
					sItem.Format (_T("M.%7.1lf,%3d,%c,%3d   "),pos,v,'A'+k,pDoc->c_difCoil[i].ElementAt(j).banda+1);
					numItem++;
					st+= sItem;
					if (numItem >= maxItem)
						{
						st += _T("\r\n");
						pagePr.add(st,TA_LEFT | TA_TOP);		
						nl += 1;
						numItem = 0;
						st.Empty();
						}
					}
				}
		   	}	
		if (numItem > 0)
			{
			st += _T("\r\n");
			pagePr.add(st,TA_LEFT | TA_TOP);
			nl += 1;
			numItem = 0;
			st.Empty();
			}
		}
	}

/*-------------------------------------------
if (m_DESCR_PERIODICI)
	{
	CString str;
	if ((nl % LinePerPage) != 0)
		{// Inizio pagina nessuna linea in piu`
		str = "   ";
		pagePr.add(str,TA_LEFT | TA_TOP);
		nl += 1;
		}

	str = "DETTAGLIO PERIODICI  ";
	pagePr.add(str,TA_CENTER | TA_TOP);
	nl += 1;

	str = "   ";
	pagePr.add(str,TA_LEFT | TA_TOP);
	nl += 1;

	for (int i=0;i<	pDoc->periodAlarm.getNumLine();i++)
		{
		pagePr.add(pDoc->periodAlarm.c_messages[i],TA_LEFT | TA_TOP);
		}
	nl += pDoc->periodAlarm.getNumLine();
	}

-------------------------------------------*/

if (m_SOGLIE_ALLARME)
	{
	CString str;
	if ((nl % LinePerPage) != 0)
		{// Inizio pagina nessuna linea in piu`
		str = _T("   ");
		pagePr.add(str,TA_LEFT | TA_TOP);
		nl += 1;
		}

	//str = "DETTAGLIO SOGLIE ALLARME  ";
	str.LoadString(CSM_GRAPHVIEW_DETTAGLIOSOGLIE);
	pagePr.add(str,TA_CENTER | TA_TOP);
	nl += 1;

	str = _T("   ");
	pagePr.add(str,TA_LEFT | TA_TOP);
	nl += 1;

	for (int i=0;i<	pDoc->densitaAlarm.getNumLine();i++)
		{
		pagePr.add(pDoc->densitaAlarm.c_messages[i],TA_LEFT | TA_TOP);
		}

	nl += pDoc->densitaAlarm.getNumLine();
	}

if (m_ALLARMISISTEMA)
	{
	CString str;
	if ((nl % LinePerPage) != 0)
		{// Inizio pagina nessuna linea in piu`
		str = _T("   ");
		pagePr.add(str,TA_LEFT | TA_TOP);
		nl += 1;
		}

	str.LoadString(CSM_GRAPHVIEW_ALLARMISISTEMA);
	//str = "ALLARMI DI SISTEMA ";
	pagePr.add(str,TA_CENTER | TA_TOP);
	nl += 1;

	str = _T("   ");
	pagePr.add(str,TA_LEFT | TA_TOP);
	nl += 1;

	for (int i=0;i<	pDoc->systemAlarm.getNumLine();i++)
		{
		pagePr.add(pDoc->systemAlarm.c_messages[i],TA_LEFT | TA_TOP);
		}
	nl += pDoc->systemAlarm.getNumLine();
	}

// cluster
// if (m_ALLARMISISTEMA)
	{
	CString str;
	if ((nl % LinePerPage) != 0)
		{// Inizio pagina nessuna linea in piu`
		str = _T("   ");
		pagePr.add(str,TA_LEFT | TA_TOP);
		nl += 1;
		}

	str.LoadString(CSM_GRAPHDOC_ALCLUSTER);
	// str = "ALLARMI DEI CLUSTER ";
	pagePr.add(str,TA_CENTER | TA_TOP);
	nl += 1;

	str = _T("   ");
	pagePr.add(str,TA_LEFT | TA_TOP);
	nl += 1;

	for (int i=0;i<	pDoc->clusterAlarm.getNumLine();i++)
		{
		pagePr.add(pDoc->clusterAlarm.c_messages[i],TA_LEFT | TA_TOP);
		}
	nl += pDoc->clusterAlarm.getNumLine();
	}

/*
if (m_MAPPA100)
	{
	CString str1,str2,s1,s2;
	if ((nlEnd % LinePerPage) != 0)
		{// Inizio pagina nessuna linea in piu`
		str1 = "   ";
		pagePrEnd.add(str1,TA_LEFT | TA_TOP);
		nlEnd += 1;
		}

	str1 = "DETTAGLIO ULTIMI 100 METRI  ";
	pagePrEnd.add(str1,TA_CENTER | TA_TOP);
	nlEnd += 1;

	str1 = "   ";
	pagePrEnd.add(str1,TA_LEFT | TA_TOP);
	nlEnd += 1;

	int lRes = (int)pDoc->c_lunResStop;
	int metri = (int) pDoc->c_difCoil.getMeter();
	if (metri > 100) metri = 100;
	//int end = metri - lRes;
	//if ((end > metri)||(end < 0)) end = metri;

	int end = metri;
	int start = (lRes>=0)?0:abs(lRes);
	for (int i=start;i<	end;i++)
		{
		//int m = (lRes < 0)?i+lRes:i;

		double m = pDoc->c_fattCorrDimLin*i + lRes;
		str1.Format ("M. %4.1lf : ",m);
		str2.Format ("              ");

//		pDoc->dif100.format(i,s1,s2);
//		str1 += s1;
//		str2 += s2;

		pagePrEnd.add(str1,TA_LEFT | TA_TOP);
		pagePrEnd.add(str2,TA_LEFT | TA_TOP);
		nlEnd += 2;
		}
   
	}
*/


if (nl > 0)
	{
	numPage =  nl / LinePerPage + 1;
	pagePr.setNumPage(numPage);
	}

if (nlEnd > 0)
	{
	numPageEnd =  nlEnd / LinePerPage + 1;
	pagePrEnd.setNumPage(numPageEnd);
	}


// Set print Num Page X text	
return (numPage+numPageEnd);

}

void CInitView::OnBeginPrinting(CDC* pDC, CPrintInfo* pInfo) 
{
	// TODO: Add your specialized code here and/or call the base class
	
CPdfPrintView::OnBeginPrinting(pDC, pInfo);
}


BOOL CInitView::OnPreparePrinting(CPrintInfo* pInfo) 
{
// dBase already Open
CLineDoc* pDoc = (CLineDoc* )GetDocument();

// Create the CProgressCtrl as a child of the status bar positioned
// over the first pane.
 
CMainFrame *frame = (CMainFrame *)AfxGetMainWnd();
frame->progressCreate();
frame->progressSetRange(0, 10);
frame->progressSetStep(2);


// Load report option
DBReport dbReport(dBase);

if (!dbReport.openSelectDefault ())
	{
	AfxGetMainWnd()->MessageBox (_T("Error openSelectDefault"),_T("dbReport.Open"));
	dbReport.Close();
	return FALSE;
	}

//  Move Init Data from Db to Dialog
m_SOGLIE_ALLARME = dbReport.m_SOGLIE_ALLARME;
m_DATI_CLIENTE = dbReport.m_DATI_CLIENTE;
m_DATA_LAVORAZIONE = dbReport.m_DATA_LAVORAZIONE;
m_DESCR_PERIODICI = dbReport.m_DESCR_PERIODICI;
m_DESCR_RANDOM = dbReport.m_DESCR_RANDOM;
m_LUNGHEZZA_BOBINA = dbReport.m_LUNGHEZZA_BOBINA;
m_ALLARMISISTEMA = dbReport.m_MAPPA100;
m_ALMETRO = dbReport.m_ALMETRO;
m_DALMETRO = dbReport.m_DALMETRO;
m_INTERVALLO1 = dbReport.m_INTERVALLO1;
m_INTERVALLO2 = dbReport.m_INTERVALLO2;
m_INTERVALLO3 = dbReport.m_INTERVALLO3;
m_DESCR_DOWNWEB = dbReport.m_DESCR_DOWNWEB;
m_DESCR_CROSSWEB = dbReport.m_DESCR_CROSSWEB;
m_DESCR_THRESHOLD = dbReport.m_DESCR_THRESHOLD;
m_DESCR_COMPRESSION = dbReport.m_DESCR_COMPRESSION;

// Close DbReport	
dbReport.Close();


CProfile profile;

c_leftMarg = profile.getProfileInt(_T("Report"),_T("LeftMarg"),20);		// margine sinistro report in mm
c_rightMarg = profile.getProfileInt(_T("Report"),_T("RightMarg"),10);	// margine destro report in mm
c_topMarg = profile.getProfileInt(_T("Report"),_T("TopMarg"),30);		// margine alto report in mm
c_bottomMarg = profile.getProfileInt(_T("Report"),_T("BottomMarg"),20);	// margine basso report in mm


// Visualizza Dialog di stampa
// If not printPreview NO Print DialogBox
pInfo->m_bDirect = directPrint; 	

// Show Progress
frame->progressStepIt();

// Abilita multicopie (Da gestire manualmente)
// pInfo->m_pPD->m_pd.Flags &= ~PD_USEDEVMODECOPIES;


// --------------------------------

// Prepare Device Context
BOOL retVal = DoPreparePrinting (pInfo);
if (!retVal)
	{
	frame->progressDestroy();
	return(retVal);
	}
// Set max Number page	3 + testo
// 
CDC dc;
dc.Attach (pInfo->m_pPD->m_pd.hDC);
// save
CRect r;
r.SetRect(0, 0, 
             dc.GetDeviceCaps(HORZRES), 
             dc.GetDeviceCaps(VERTRES)) ;

c_numTextPage = prepareTextPageEx(&dc,r);

c_numTextPage2 = prepareTextPageEx2(&dc,r);


dc.Detach();


// Modifica per stampa solo report Testo
int numPage ;
int numGraphPage = 0;
// grafico trend longit
if (m_DESCR_DOWNWEB)
	{
	c_graphMap [numGraphPage++] = 1;	// pagina downWeb
	}
// grafico  crossweb
if (m_DESCR_CROSSWEB)
	{
	c_graphMap [numGraphPage++] = 2;	// pagina crossWeb
	}
// grafico soglie
if (m_DESCR_THRESHOLD)
	{
	c_graphMap [numGraphPage++] = 3;	// pagina threshold
	}
// compressione
if (m_DESCR_COMPRESSION)
	{
	c_graphMap [numGraphPage++] = 4;	// pagina compressione
	}

if (onlyTextReport)
	{// stampa automatica 
	//numPage = 1 + pagePr.getNumPage();
	// numPage = 1 + c_numTextPage + c_numTextPage2;
	numPage = 1 + c_numTextPage + numGraphPage + c_numTextPage2; // grafici + testo
	}
else
	//numPage = 1 + pagePr.getNumPage() + 3 + pagePrEnd.getNumPage(); // grafici + testo
	numPage = 1 + c_numTextPage + numGraphPage + c_numTextPage2; // grafici + testo

pInfo->SetMaxPage((UINT) numPage);

// Insert Wait Dialog Box

if (retVal)
	{
	// TODO: Add your specialized code here and/or call the base class
	
	// int code;
	CString rotolo;
	// extIncluded
	rotolo = pDoc->getRotolo();

	// Show Progress
	frame->progressStepIt();

	// Trend longitudinale * 4
	// Init layoutL Fix Attributes
	vType = MULTILINEVIEW;
	layoutL.setViewType(MULTILINEVIEW);

	// Init layoutL Fix Attributes
	CProfile profile;
	CString s;
	

	layoutL.fCaption.size =  profile.getProfileInt(_T("MLineReport"),_T("CaptionSize"),120);
	layoutL.fLabel.size = profile.getProfileInt(_T("MLineReport"),_T("LabelSize"),100);
	layoutL.fNormal.size = profile.getProfileInt(_T("MLineReport"),_T("NormalSize"),110);
	

	s = profile.getProfileString(_T("MLineReport"),_T("NewLine"),_T("3,6"));
	while (s.GetLength() >0)
		{
		CString subS;
		subS = s.SpanExcluding(_T(",;"));
		s = s.Right(s.GetLength()-subS.GetLength()-1);
		int v;
		_stscanf((LPCTSTR)subS,_T("%d"),&v);
		if ((v < 10)&&(v>=0))
			layoutL.c_nlAfterLabel [v] = 1;
		}

	s = profile.getProfileString(_T("MLineReport"),_T("LabelOrder"),_T("0,1,2,3,4,5,6,7,8,9"));
	int k = 0;
	while (s.GetLength() >0)
		{
		CString subS;
		subS = s.SpanExcluding(_T(",;"));
		s = s.Right(s.GetLength()-subS.GetLength()-1);
		int v;
		_stscanf((LPCTSTR)subS,_T("%d"),&v);
		if ((v < 10)&&(v>=0))
			layoutL.c_vOrderLabel [k++] = v;
		if (k>9)
			break;
		}

	s = profile.getProfileString(_T("MLineReport"),_T("PixelSepLabel"),
							 _T("10,10,10,10,10,10,10,10,10,10"));
	k = 0;
	while (s.GetLength() >0)
		{
		CString subS;
		subS = s.SpanExcluding(_T(",;"));
			s = s.Right(s.GetLength()-subS.GetLength()-1);
		int v;
		_stscanf((LPCTSTR)subS,_T("%d"),&v);
		layoutL.c_pxSepLabel [k++] = v;
		if (k>9)
			break;
		}

	s = profile.getProfileString(_T("MLineReport"),_T("SCaption"),
							 _T("TREND"));
	layoutL.setCaption(s);
	layoutL.sCaption.setFontInfo(layoutL.fCaption);
	layoutL.sCaption.set3Dlook(FALSE);

	for (int i=0;i<MAX_NUMLABEL;i++)
		{
		layoutL.c_sLabel[i].setFontInfo(layoutL.fNormal);
		layoutL.c_vLabel[i].setFontInfo(layoutL.fNormal);
		layoutL.c_vLabel[i].set3Dlook(TRUE);
		layoutL.c_vLabel[i].setStringBase(_T("999999"));
		}


	for (int i=0;i<10;i++)
		{
		CString sIndex;
		sIndex.Format (_T("SLabel%d"),i);
		s = profile.getProfileString(_T("MLineReport"),sIndex,sIndex);
		if (s != sIndex)
			{
			layoutL.c_sLabel[i] = s;
			}
		}

	for (int i=0;(i<pDoc->c_difCoil.getNumClassi());i++)
		{
		CString s;
		s = TCHAR('A'+i);
		layoutL.c_cLabel[i] = s;
		layoutL.c_cLabel[i].setTColor(CSM20GetClassColor(i));
		layoutL.c_cLabel[i].setTipoIcon(IPALLINO);
		}

	// CalcDrawRect
	layoutL.c_viewTopPerc = profile.getProfileInt(_T("MLineReport"),_T("ViewTop%"),10);
	layoutL.c_viewBottomPerc = profile.getProfileInt(_T("MLineReport"),_T("ViewBottom%"),25);
	layoutL.c_viewLeftPerc = profile.getProfileInt(_T("MLineReport"),_T("ViewLeft%"),8);
	layoutL.c_viewRightPerc = profile.getProfileInt(_T("MLineReport"),_T("ViewRight%"),5);


	layoutL.setMode (LINEAR | NOLABELH | MSCALEV);
	// fori codice
	// code = 'A'+j;
	// s.Format("%c",code); 
	
	// Test di congruenza valori visualizzazione
	if (m_ALMETRO > pDoc->getVTotalLength())
		m_ALMETRO = pDoc->getVTotalLength();

	if (m_DALMETRO > pDoc->getVTotalLength())
		m_DALMETRO = 0;

	if (m_DALMETRO > m_ALMETRO)
		m_DALMETRO = 0;

	// Set Values in string
	// scala Verticale la massima delle quattro classi
	double dMY = 0.;
	for (int i=0;i<pDoc->c_difCoil.getNumClassi();i++)
		{
		dMY = max(pDoc->c_difCoil.getMaxTrendLFromTo('A'+i,
			(int) m_DALMETRO,(int)m_ALMETRO),dMY);
		}
	layoutL.setMaxY(dMY*1.25);
	layoutL.setMinY(0.);
	layoutL.c_vLabel[0]  = (int )m_ALMETRO;
	layoutL.c_vLabel [1] = (int )m_DALMETRO;
	layoutL.c_vLabel [2] = pDoc->GetVCliente();
	int delta = (int) (m_ALMETRO-m_DALMETRO);
	layoutL.c_vLabel [3] = delta;		// (int)maxValX;		// maxValX 
	layoutL.c_vLabel [4] = pDoc->getRotolo();
	layoutL.c_vLabel [5] = pDoc->lega;
	CTime d(pDoc->startTime);
	layoutL.c_vLabel [6] = d.Format( _T("%A, %B %d, %Y") );

	// int numClassi = ;
	layoutL.setNumClassi(pDoc->c_difCoil.getNumClassi());

	// Inserire i quattro codici per quattro pagine di stampa
	for (int j=0;j<pDoc->c_difCoil.getNumClassi();j++)
		{
		// fori codice
		int code = 'A'+j;

		trendLine[j].setPen (CSM20GetClassColor(j));		// era RGB (0,0,0) black
		trendLine[j].setPenLarge (CSM20GetClassColor(j));	// Black
		trendLine[j].setMode(LINEAR);	

		lineInfo[j].max = dMY * 1.25;
		lineInfo[j].min = 0.;
		lineInfo[j].limit = pDoc->c_difCoil.c_sogliaAllarme[j];

		// Determinazione dimensione asse X
		//int stepSize;
		double stepSize;
		stepSize = pDoc->GetTrendStep();	
		
		double deltaEnd		= (pDoc->getVTotalLength() - m_ALMETRO)>0?(pDoc->getVTotalLength()- m_ALMETRO):0.;
		double deltaStart	= m_DALMETRO;

		int deltaEndStep	= (int)(deltaEnd/stepSize);
		int deltaStartStep	= (int)(deltaStart/stepSize);
		
		int numStepX		= (int)(pDoc->getVTotalLength()/stepSize);
			numStepX -= deltaEndStep;
		numStepX -= deltaStartStep;
		if (numStepX <= 0)
			numStepX = 1;

		// Numero step da disegnare
		lineInfo[j].count	= (int)(((int)pDoc->getVTotalLength()/(int)stepSize < numStepX)?
					(pDoc->getVTotalLength()/(int)stepSize) : numStepX); 

//		lineInfo[j].count = (pDoc->c_difCoil.getSize(3,code) < numStep)?
//			pDoc->c_difCoil.getSize(3,code) : numStep; 
	
		lineInfo[j].numStep = numStepX;
		lineInfo[j].actual	= numStepX ;  // pDoc->c_difCoil.getSize(0,code)% numStep;
		
		// Aggiornato 27-19-1997
		lineInfo[j].newVal (lineInfo[j].numStep+1);

		// |-------|++++++++++++++|---------------|
		// 0	  Dal			  Al             meter
		//               delta		  deltaEnd
		// getValTrendL torna valori riferiti all'ultimo,lavora in metri
		// bisogna passare indici a partire da Al in poi
		// offset dal fondo
		// sostituire stepSize a 10

		for (int i=0;i<= lineInfo[j].numStep;i++)
			{
			double val = 0.;
			lineInfo[j].val[i] = pDoc->c_difCoil.getValNumLStep(code,i*(int)stepSize);
			if (i > 200)
				int a = lineInfo[j].val[i];
			}
		}	
	// Fine preparazione lineInfo
	
	// Show Progress
	frame->progressStepIt();

	/*------------------------------------------*/
	// Preparazione Densita' init colors pen and brush

	layoutD.fCaption.size = profile.getProfileInt(_T("TowerReport"),_T("CaptionSize"),120);
	layoutD.fLabel.size =  profile.getProfileInt(_T("TowerReport"),_T("LabelSize"),100);
	layoutD.fNormal.size =  profile.getProfileInt(_T("TowerReport"),_T("NormalSize"),110);


	s = profile.getProfileString(_T("TowerReport"),_T("NewLine"),_T("3,6"));
	while (s.GetLength() >0)
		{
		CString subS;
		subS = s.SpanExcluding(_T(",;"));
		s = s.Right(s.GetLength()-subS.GetLength()-1);
		int v;
		_stscanf((LPCTSTR)subS,_T("%d"),&v);
		if ((v < 10)&&(v>=0))
			layoutD.c_nlAfterLabel [v] = 1;
		}

	s = profile.getProfileString(_T("TowerReport"),_T("LabelOrder"),_T("0,1,2,3,4,5,6,7,8,9"));
	k = 0;
	while (s.GetLength() >0)
		{
		CString subS;
		subS = s.SpanExcluding(_T(",;"));
		s = s.Right(s.GetLength()-subS.GetLength()-1);
		int v;
		_stscanf((LPCTSTR)subS,_T("%d"),&v);
		if ((v < 10)&&(v>=0))
			layoutD.c_vOrderLabel [k++] = v;
		if (k>9)
			break;
		}

	s = profile.getProfileString(_T("TowerReport"),_T("PixelSepLabel"),
							 _T("10,10,10,10,10,10,10,10,10,10"));
	k = 0;
	while (s.GetLength() >0)
		{
		CString subS;
		subS = s.SpanExcluding(_T(",;"));
		s = s.Right(s.GetLength()-subS.GetLength()-1);
		int v;
		_stscanf((LPCTSTR)subS,_T("%d"),&v);
		layoutD.c_pxSepLabel [k++] = v;
		if (k>9)
			break;
		}

	// layoutD.setLabel ("label 1");
	layoutD.setViewType(TOWERVIEW);
	layoutD.setMode (LINEAR);
	s = profile.getProfileString(_T("TowerReport"),_T("SCaption"),
							 _T("CROSS WEB DISTRIBUTION"));
	layoutD.setCaption(s);
	
	for (int i=0;i<MAX_NUMLABEL;i++)
		{
		layoutD.c_sLabel[i].setFontInfo(layoutD.fNormal);
		layoutD.c_vLabel[i].setFontInfo(layoutD.fNormal);
		layoutD.c_vLabel[i].set3Dlook(TRUE);
		layoutD.c_vLabel[i].setStringBase(_T("999999"));
		}

	for (int i=0;i<MAX_NUMLABEL;i++)
		{
		CString sIndex;
		sIndex.Format (_T("SLabel%d"),i);
		s = profile.getProfileString(_T("TowerReport"),sIndex,sIndex);
		if (s != sIndex)
			layoutD.c_sLabel[i] = s;
		}

	for (int i=0;(i<pDoc->c_difCoil.getNumClassi());i++)
		{
		CString s;
		s = TCHAR('A'+i);
		layoutD.c_cLabel[i] = s;
		layoutD.c_cLabel[i].setTColor(CSM20GetClassColor(i));
		layoutD.c_cLabel[i].setTipoIcon(IPALLINO);
		}

	// CalcDrawRect
	layoutD.c_viewTopPerc = profile.getProfileInt(_T("TowerReport"),_T("ViewTop%"),10);
	layoutD.c_viewBottomPerc = profile.getProfileInt(_T("TowerReport"),_T("ViewBottom%"),25);
	layoutD.c_viewLeftPerc = profile.getProfileInt(_T("TowerReport"),_T("ViewLeft%"),8);
	layoutD.c_viewRightPerc = profile.getProfileInt(_T("TowerReport"),_T("ViewRight%"),5);

	densView.SetSize (pDoc->c_difCoil.getNumSchede()); 
	for (int i=0;i<pDoc->c_difCoil.getNumSchede();i++)
		{
		densView[i].init (pDoc->c_difCoil.getNumClassi());
		densView[i].setMode (TOWER);
		for (int j=0;j<pDoc->c_difCoil.getNumClassi();j++)
			{
			densView[i].setPenTB (j,CSM20GetClassColor(j));	  // Top and Bottom color
			densView[i].setPenLR (j,layoutD.getRgbBackColor());	  // Red
			switch(j%4)
				{
				case 0 :
					densView[i].setBrush (j,CSM20GetClassColor(j),BS_HATCHED,HS_BDIAGONAL);
					break;
				case 1 :
					densView[i].setBrush (j,CSM20GetClassColor(j),BS_HATCHED,HS_DIAGCROSS);
					break;
				case 2 :
					densView[i].setBrush (j,CSM20GetClassColor(j),BS_HATCHED,HS_FDIAGONAL);
					break;
				case 3 :
					densView[i].setBrush (j,CSM20GetClassColor(j),BS_HATCHED,HS_DIAGCROSS);
					break;

				}
			}
		}

	// Set Values in string
	int maxy = max(1,pDoc->c_difCoil.getMaxDensFromTo((int )m_DALMETRO,
			(int )m_ALMETRO)*1.25);
	layoutD.setMaxY(maxy);
	layoutD.setMinY(0.);
	layoutD.c_vLabel[0] = pDoc->GetVPosition();
	layoutD.c_vLabel [1] = pDoc->GetTrendDVPositionDal();
	layoutD.c_vLabel [2] = pDoc->GetVCliente();
	layoutD.c_vLabel [3] = 0;		// maxValX 
	layoutD.c_vLabel [4] = pDoc->getRotolo();
	layoutD.c_vLabel [5] = pDoc->lega;
	// d contiene starttime settato da trend long piu` sopra
	layoutD.c_vLabel [6] = d.Format( _T("%A, %B %d, %Y") );

	layoutD.setNumSchede(pDoc->c_difCoil.getNumSchede());
	int numClassi = pDoc->c_difCoil.getNumClassi();
	layoutD.setNumClassi(numClassi);


	// Fine Preparazione densita'
//-----------------------------------------------------------------------
	// Rapporto allarme densita`
	
	// Trend longitudinale * 4
	// Init layoutD Fix Attributes
	vType = MULTILINEVIEW;
	layoutAD.setViewType(MULTILINEVIEW);

	// Init layoutAD Fix Attributes

	layoutAD.fCaption.size =  profile.getProfileInt(_T("AlarmReport"),_T("CaptionSize"),120);
	layoutAD.fLabel.size = profile.getProfileInt(_T("AlarmReport"),_T("LabelSize"),100);
	layoutAD.fNormal.size = profile.getProfileInt(_T("AlarmReport"),_T("NormalSize"),110);
	

	s = profile.getProfileString(_T("AlarmReport"),_T("NewLine"),_T("3,6"));
	while (s.GetLength() >0)
		{
		CString subS;
		subS = s.SpanExcluding(_T(",;"));
		s = s.Right(s.GetLength()-subS.GetLength()-1);
		int v;
		_stscanf((LPCTSTR)subS,_T("%d"),&v);
		if ((v < 10)&&(v>=0))
			layoutAD.c_nlAfterLabel [v] = 1;
		}

	s = profile.getProfileString(_T("AlarmReport"),_T("LabelOrder"),_T("0,1,2,3,4,5,6,7,8,9"));
	k = 0;
	while (s.GetLength() >0)
		{
		CString subS;
		subS = s.SpanExcluding(_T(",;"));
		s = s.Right(s.GetLength()-subS.GetLength()-1);
		int v;
		_stscanf((LPCTSTR)subS,_T("%d"),&v);
		if ((v < 10)&&(v>=0))
			layoutAD.c_vOrderLabel [k++] = v;
		if (k>9)
			break;
		}

	s = profile.getProfileString(_T("AlarmReport"),_T("PixelSepLabel"),
							 _T("10,10,10,10,10,10,10,10,10,10"));
	k = 0;
	while (s.GetLength() >0)
		{
		CString subS;
		subS = s.SpanExcluding(_T(",;"));
			s = s.Right(s.GetLength()-subS.GetLength()-1);
		int v;
		_stscanf((LPCTSTR)subS,_T("%d"),&v);
		layoutAD.c_pxSepLabel [k++] = v;
		if (k>9)
			break;
		}

	s = profile.getProfileString(_T("AlarmReport"),_T("SCaption"),
							 _T("ALARM"));
	layoutAD.setCaption(s);
	layoutAD.sCaption.setFontInfo(layoutAD.fCaption);
	layoutAD.sCaption.set3Dlook(FALSE);

	for (int i=0;i<MAX_NUMLABEL;i++)
		{
		layoutAD.c_sLabel[i].setFontInfo(layoutAD.fNormal);
		layoutAD.c_vLabel[i].setFontInfo(layoutAD.fNormal);
		layoutAD.c_vLabel[i].set3Dlook(TRUE);
		layoutAD.c_vLabel[i].setStringBase(_T("999999"));
		}


	for (int i=0;i<MAX_NUMLABEL;i++)
		{
		CString sIndex;
		sIndex.Format (_T("SLabel%d"),i);
		s = profile.getProfileString(_T("AlarmReport"),sIndex,sIndex);
		if (s != sIndex)
			{
			layoutAD.c_sLabel[i] = s;
			}
		}

	for (int i=0;(i<pDoc->c_difCoil.getNumClassi());i++)
		{
		CString s;
		s = TCHAR('A'+i);
		layoutAD.c_cLabel[i] = s;
		layoutAD.c_cLabel[i].setTColor(CSM20GetClassColor(i));
		layoutAD.c_cLabel[i].setTipoIcon(IPALLINO);
		}

	// CalcDrawRect
	layoutAD.c_viewTopPerc = profile.getProfileInt(_T("AlarmReport"),_T("ViewTop%"),10);
	layoutAD.c_viewBottomPerc = profile.getProfileInt(_T("AlarmReport"),_T("ViewBottom%"),25);
	layoutAD.c_viewLeftPerc = profile.getProfileInt(_T("AlarmReport"),_T("ViewLeft%"),8);
	layoutAD.c_viewRightPerc = profile.getProfileInt(_T("AlarmReport"),_T("ViewRight%"),5);


	layoutAD.setMode (LINEAR | NOLABELH | MSCALEV);
	// fori codice
	// code = 'A'+j;
	// s.Format("%c",code); 
	
	// Test di congruenza valori visualizzazione
	if (m_ALMETRO > pDoc->getVTotalLength())
		m_ALMETRO = pDoc->getVTotalLength();

	if (m_DALMETRO > pDoc->getVTotalLength())
		m_DALMETRO = 0;

	if (m_DALMETRO > m_ALMETRO)
		m_DALMETRO = 0;

	// Set Values in string
	// scala Verticale la massima delle quattro classi
	dMY = 0.;
	/*--------------
	calcolo valore max
	for (i=0;i<pDoc->c_difCoil.getNumClassi();i++)
		{
		dMY = max(pDoc->c_difCoil.getMaxTrendLFromTo('A'+i,
			(int) m_DALMETRO,(int)m_ALMETRO),dMY);
		}
	*/
	dMY = 1.0; // per allarmi
	layoutAD.setMaxY(dMY*1.25);
	layoutAD.setMinY(0.);
	layoutAD.c_vLabel[0]  = (int )m_ALMETRO;
	layoutAD.c_vLabel [1] = (int )m_DALMETRO;
	layoutAD.c_vLabel [2] = pDoc->GetVCliente();
	
	delta = (int) (m_ALMETRO-m_DALMETRO);
	layoutAD.c_vLabel [3] = delta;		// (int)maxValX;		// maxValX 
	layoutAD.c_vLabel [4] = pDoc->getRotolo();
	layoutAD.c_vLabel [5] = pDoc->lega;
	//CTime d(pDoc->startTime);
	// d tiene gia` i valori della data da trend Long
	layoutAD.c_vLabel [6] = d.Format( _T("%A, %B %d, %Y") );

	// int numClassi = ;
	layoutAD.setNumClassi(pDoc->c_difCoil.getNumClassi());

	// Inserire i quattro codici per quattro pagine di stampa
	for (int j=0;j<pDoc->c_difCoil.getNumClassi();j++)
		{
		// fori codice
		int code = 'A'+j;

		trendAD[j].setPen (CSM20GetClassColor(j));		// era RGB (0,0,0) black
		trendAD[j].setPenLarge (CSM20GetClassColor(j));	// Black
		trendAD[j].setMode(LINEAR);	

		lineInfoAD[j].max = dMY*1.25;
		lineInfoAD[j].min = 0.;

		// Determinazione dimensione asse X
		//int stepSize;
		double stepSize;
		stepSize = pDoc->GetTrendStep();	
		
		double deltaEnd = (pDoc->getVTotalLength() - m_ALMETRO)>0?(pDoc->getVTotalLength() - m_ALMETRO):0.;
		double deltaStart = m_DALMETRO;

		int deltaEndStep = (int)(deltaEnd/stepSize);
		int deltaStartStep = (int)(deltaStart/stepSize);
		
		int numStepX = (int)(pDoc->getVTotalLength()/stepSize);
			numStepX -= deltaEndStep;
		numStepX -= deltaStartStep;
		if (numStepX <= 0)
			numStepX = 1;

		// Numero step da disegnare
		lineInfoAD[j].count = (int)((((int)pDoc->getVTotalLength()/(int)stepSize) < numStepX)?
					(pDoc->getVTotalLength()/(int)stepSize) : numStepX); 

//		lineInfoAD[j].count = (pDoc->c_difCoil.getSize(3,code) < numStep)?
//			pDoc->c_difCoil.getSize(3,code) : numStep; 
	
		lineInfoAD[j].numStep = numStepX;
		lineInfoAD[j].actual	= numStepX ;  // pDoc->c_difCoil.getSize(0,code)% numStep;
		
		// Aggiornato 27-19-1997
		lineInfoAD[j].newVal (lineInfoAD[j].numStep+1);

		// |-------|++++++++++++++|---------------|
		// 0	  Dal			  Al             meter
		//               delta		  deltaEnd
		// 23-05-05
		// getDensityLevel torna valori riferiti alla posizione pos assoluta
		// Il sistema di disegno inverte rispetto a posizione
		// bisogna partire dal fondo e tornare indietro 

		for (int i=0;i<= lineInfoAD[j].numStep;i++)
			{
			double val = 0.;
			
			double pos = pDoc->getVTotalLength()-(double) i*stepSize;
			val = pDoc->c_difCoil.getDensityLevel(code,pDoc->c_densityCalcLength,pos);
			double al = pDoc->c_difCoil.getAllarme(code);
			if (val > al)
				val = 1.;
			else
				val = 0.;


			lineInfoAD[j].val[i] = val;

			//lineInfoAD[j].val[i] = pDoc->c_difCoil.getValAlarmNumLStep(code,i*(int)stepSize);
			}
// Rapporto Compressione Asse

  // Show Progress
	frame->progressStepIt();

	c_layoutCompressABBig.setViewType(TOWERVIEW);
//	c_layoutCompressABBig.setMode (LINEAR| USE_STRING_LABELV);
	c_layoutCompressABBig.setMode (LINEAR);

	c_layoutCompressABBig.fCaption.size =  profile.getProfileInt(_T("CompressReport"),_T("CaptionSize"),120);
	c_layoutCompressABBig.fLabel.size = profile.getProfileInt(_T("CompressReport"),_T("LabelSize"),100);
	c_layoutCompressABBig.fNormal.size = profile.getProfileInt(_T("CompressReport"),_T("NormalSize"),110);
	c_layoutCompressABBig.setNumSchede(pDoc->c_difCoil.getNumSchede());	

	s = profile.getProfileString(_T("CompressReport"),_T("NewLine"),_T("3,6"));
	while (s.GetLength() >0)
		{
		CString subS;
		subS = s.SpanExcluding(_T(",;"));
		s = s.Right(s.GetLength()-subS.GetLength()-1);
		int v;
		_stscanf((LPCTSTR)subS,_T("%d"),&v);
		if ((v < MAX_NUMLABEL)&&(v>=0))
			c_layoutCompressABBig.c_nlAfterLabel [v] = 1;
		}

	s = profile.getProfileString(_T("CompressReport"),_T("LabelOrder"),_T("0,1,2,3,4,5,6,7,8,9"));
	k = 0;
	while (s.GetLength() >0)
		{
		CString subS;
		subS = s.SpanExcluding(_T(",;"));
		s = s.Right(s.GetLength()-subS.GetLength()-1);
		int v;
		_stscanf((LPCTSTR)subS,_T("%d"),&v);
		if ((v < MAX_NUMLABEL)&&(v>=0))
			c_layoutCompressABBig.c_vOrderLabel [k++] = v;
		if (k>(MAX_NUMLABEL-1))
			break;
		}

	s = profile.getProfileString(_T("CompressReport"),_T("PixelSepLabel"),
							 _T("10,10,10,10,10,10,10,10,10,10"));

	k = 0;
	while (s.GetLength() >0)
		{
		CString subS;
		subS = s.SpanExcluding(_T(",;"));
			s = s.Right(s.GetLength()-subS.GetLength()-1);
		int v;
		_stscanf((LPCTSTR)subS,_T("%d"),&v);
		c_layoutCompressABBig.c_pxSepLabel [k++] = v;
		if (k>(MAX_NUMLABEL-1))
			break;
		}

	s = profile.getProfileString(_T("CompressReport"),_T("SCaption"),
							 _T("Compress ABCD"));
	c_layoutCompressABBig.setCaption(s);
	c_layoutCompressABBig.sCaption.setFontInfo(c_layoutCompressABBig.fCaption);
	c_layoutCompressABBig.sCaption.set3Dlook(FALSE);

	for (int i=0;i<MAX_NUMLABEL;i++)
		{
		c_layoutCompressABBig.c_sLabel[i].setFontInfo(c_layoutCompressABBig.fNormal);
		c_layoutCompressABBig.c_vLabel[i].setFontInfo(c_layoutCompressABBig.fNormal);
		c_layoutCompressABBig.c_vLabel[i].set3Dlook(TRUE);
		c_layoutCompressABBig.c_vLabel[i].setStringBase(_T("999999"));
		}


	for (int i=0;i<MAX_NUMLABEL;i++)
		{
		CString sIndex;
		sIndex.Format (_T("SLabel%d"),i);
		s = profile.getProfileString(_T("CompressReport"),sIndex,sIndex);
		if (s != sIndex)
			{
			c_layoutCompressABBig.c_sLabel[i] = s;
			}
		}

	for (int i=0;(i<(pDoc->c_difCoil.getNumClassi()));i++)
		{
		CString s;
		s = TCHAR('A'+i);
		c_layoutCompressABBig.c_cLabel[i] = s;
		c_layoutCompressABBig.c_cLabel[i].setTColor(CSM20GetClassColor(i));
		c_layoutCompressABBig.c_cLabel[i].setTipoIcon(IPALLINO);
		}

	// aggiunte label alzate
//	StringLabelH strLV;
	// 28-11-08 fixBug clear previous rising
//	c_layoutCompressABBig.clearStringLabelV();
	int cnt = 0;
/*-------------------------------
Niente alzate!
	{
	for (int i=0;i<pDoc->c_rotoloMapping.size();i++)
		{
		for (int j=0;j<pDoc->c_rotoloMapping[i].c_alzate.size();j++)
			{		
			if (cnt >= 20)
				break;
			if (j == 0)
				continue;
			if ((i == pDoc->c_rotoloMapping.size()-1)&&
				(j == pDoc->c_rotoloMapping[i].c_alzate.size()-1))
				continue;
			double dPos = pDoc->c_rotoloMapping[i].c_alzate[j].c_yPos/(m_ALMETRO-m_DALMETRO);
			// dPos espresso in % dello spazio video
			strLV.pos = dPos;
			strLV.label.Format("Ris%d",cnt+1);
			c_layoutCompressABBig.c_stringLabelV[cnt++] = strLV;
			}
		}
	}
-----------------------*/

	// CalcDrawRect
	c_layoutCompressABBig.c_viewTopPerc = profile.getProfileInt(_T("CompressReport"),_T("ViewTop%"),10);
	c_layoutCompressABBig.c_viewBottomPerc = profile.getProfileInt(_T("CompressReport"),_T("ViewBottom%"),25);
	c_layoutCompressABBig.c_viewLeftPerc = profile.getProfileInt(_T("CompressReport"),_T("ViewLeft%"),8);
	c_layoutCompressABBig.c_viewRightPerc = profile.getProfileInt(_T("CompressReport"),_T("ViewRight%"),5);

	
	// Test di congruenza valori visualizzazione
	if (m_ALMETRO > pDoc->c_difCoil.getMeter())
		m_ALMETRO = pDoc->c_difCoil.getMeter();

	if (m_DALMETRO > pDoc->c_difCoil.getMeter())
		m_DALMETRO = 0;

	if (m_DALMETRO > m_ALMETRO)
		m_DALMETRO = 0;

	// Set Values in string
	// Tarato su max value comunque
	if (m_ALMETRO == m_DALMETRO)
		m_ALMETRO = m_DALMETRO+1;

	c_layoutCompressABBig.c_vLabel [0] = (int )m_ALMETRO;
	c_layoutCompressABBig.c_vLabel [1] = (int )m_DALMETRO;
	c_layoutCompressABBig.c_vLabel [2] = pDoc->GetVCliente();
	
	delta = (int) (m_ALMETRO-m_DALMETRO);
	c_layoutCompressABBig.c_vLabel [3] = delta;		// (int)maxValX;		// maxValX 
	c_layoutCompressABBig.c_vLabel [4] = pDoc->getRotolo();
	c_layoutCompressABBig.c_vLabel [5] = pDoc->lega;
	//CTime d(pDoc->startTime);
	// d tiene gia` i valori della data da trend Long
	c_layoutCompressABBig.c_vLabel [6] = d.Format( _T("%A, %B %d, %Y") );

	int numClassi;
	// aggiungo anche classe Big
	numClassi = pDoc->c_difCoil.getNumClassi();
	c_layoutCompressABBig.setNumClassi(numClassi);
//	c_layoutCompressABBig.setNumLabelClassi(numClassi);
//	c_layoutCompressABBig.c_indexClassi = 0;
	c_layoutCompressABBig.setMaxY (m_DALMETRO);
	c_layoutCompressABBig.setMinY (m_ALMETRO);

	// Preparazione TargetBoard
	c_targetBoardABBig.setEmpty();

	int c_pxBRoll = profile.getProfileInt(_T("CompressReport"),_T("PxRollSize"),10);

	for (int index=0;index <pDoc->c_difCoil.GetSize();index ++)
		{
		// cerco schede
		int mSize = pDoc->c_difCoil.ElementAt(index).GetSize();
		for (int scIndex=0;scIndex<mSize;scIndex ++)
			{
			// add BigHole
/* 
Niente big hole
			if (pDoc->c_difCoil.ElementAt(index).ElementAt(scIndex).BigHoleAt() > 0)
				{// found 
				double pos = pDoc->c_difCoil.ElementAt(index).posizione;
				if((pos >= m_DALMETRO)&&
					(pos <= m_ALMETRO))
					{					
					CTarget t;
					t.setBanda (pDoc->c_difCoil.ElementAt(index).ElementAt(scIndex).banda);
					t.setSize (CSize(3*c_pxBRoll,5*c_pxBRoll));
					t.setColor (CSM10_ARGetClassColor(4));
					// TRUE exclude left text
					c_targetBoardABBig.add(pDoc->c_difCoil.ElementAt(index).posizione*1000,t,TRUE);
					}
				}
*/
		int clSize = pDoc->c_difCoil.ElementAt(index).ElementAt(scIndex).GetSize();
			for (int clIndex=(clSize-1);clIndex>=0;clIndex --)
				{// invertita priorita` di visualizzazione fori D + prioritari degli A
				if (pDoc->c_difCoil.ElementAt(index).ElementAt(scIndex).ElementAt(clIndex) > 0)
					{
					// found 
					double pos = pDoc->c_difCoil.ElementAt(index).posizione;
					if((pos >= m_DALMETRO)&&
						(pos <= m_ALMETRO))
						{
						CTarget t;
						t.setBanda (pDoc->c_difCoil.ElementAt(index).ElementAt(scIndex).banda);
						t.setSize (CSize(2*c_pxBRoll,3*c_pxBRoll));
						t.setColor (CSM20GetClassColor(clIndex));
						// TRUE exclude left text
						c_targetBoardABBig.add(pDoc->c_difCoil.ElementAt(index).posizione*1000,t,TRUE);
						}
					}
				}	
			}
		}
	 // Fine Rapporto Allarmi
									
	// Show Progress
	frame->progressStepIt();
	// Fine Rapporto qualita`
	}
}
frame->progressDestroy();

return (retVal);
}

// Routine di stampa equivalente di OnDraw
// Chiamata per ogni pagina di stampa
// pInfo.m_nCurPage [1 .. n] indica attuale pagina di stampa
void CInitView::OnPrint(CDC* pdc, CPrintInfo* pInfo) 
{

CRect rectBorder;
// GetClientRect
CRect rcBounds = calcPrintInternalRect(pdc,pInfo->m_rectDraw,&rectBorder);

// draw border
CLineDoc* pDoc = (CLineDoc* )GetDocument();
CPen p,*op;
p.CreatePen (PS_SOLID,2,RGB(0,0,0));
op = pdc->SelectObject(&p);
pdc->MoveTo(rectBorder.left,rectBorder.top);
pdc->LineTo(rectBorder.right,rectBorder.top);
pdc->LineTo(rectBorder.right,rectBorder.bottom);
pdc->LineTo(rectBorder.left,rectBorder.bottom);
pdc->LineTo(rectBorder.left,rectBorder.top);
pdc->SelectObject (op);


prLimit = rcBounds;

int page = pInfo->m_nCurPage;



CFont fontNormal,*pold;
fontNormal.CreatePointFont (pagePr.getFontNormal()->size,pagePr.getFontNormal()->name,pdc);
pold = pdc->SelectObject(&fontNormal); 
CString s,strRes;
//s.Format ("Page %d",page);
pdc->SetTextAlign(TA_CENTER | TA_TOP);
strRes.LoadString (CSM_GRAPHVIEW_REPORTPAGINA);
s.Format (strRes,page);
CSize pagSize;
pagSize = pdc->GetTextExtent(s);
CPoint point;
// allineato al centro
point.x = rectBorder.left+(rectBorder.right-rectBorder.left)/2;;
point.y = rectBorder.bottom + pagSize.cy;
pdc->TextOut (point.x,point.y,s,s.GetLength()); // Skip Cr/Lf

// data ora allineata a dx
CString formatRes;
formatRes.LoadString (CSM_GRAPHVIEW_DATETIMEFORMAT);
CTime t (pDoc->startTime);
s = t.Format(formatRes);
pdc->SetTextAlign(TA_RIGHT | TA_TOP);
point.x = rectBorder.right;
point.y = rectBorder.top - 2 * pagSize.cy;
pdc->TextOut (point.x,point.y,s,s.GetLength()); // Skip Cr/Lf

// allineato al centro
s = "File : ";
s += pDoc->getFileName();
pdc->SetTextAlign(TA_CENTER | TA_TOP);
point.x = rectBorder.left+(rectBorder.right-rectBorder.left)/2;
point.y = rectBorder.top - 2 * pagSize.cy;
pdc->TextOut (point.x,point.y,s,s.GetLength()); // Skip Cr/Lf

// Cercafori allineato a sx
//s = "Cercafori - EDS Srl";
s.LoadString(ID_CSM_REPORT_TOP_INTESTAZIONE);
pdc->SetTextAlign(TA_LEFT | TA_TOP);
point.x = rectBorder.left;
point.y = rectBorder.top - 2*pagSize.cy;
pdc->TextOut (point.x,point.y,s,s.GetLength()); // Skip Cr/Lf

pdc->SelectObject(pold); 

// resto del report
pdc->SetTextAlign(TA_LEFT | TA_TOP);

// stampa Prima pagina
if (page == 1)
	{
	// OnDraw(pdc);
	reportRotolo2(pdc,rcBounds);
	return;
	}

if (page <= (1+c_numTextPage))
	{
	CPage*	ps= new CPage(pInfo->m_rectDraw,pdc,TRUE, MM_TEXT);
	printTextPageEx(ps,rcBounds,page-2);
	delete ps;	
	return;
	}
if (page <= (1+c_numTextPage+c_numTextPage2))
	{
	CPage*	ps= new CPage(pInfo->m_rectDraw,pdc,TRUE, MM_TEXT);
	printTextPageEx2(ps,rcBounds,page-(1+c_numTextPage+c_numTextPage2));
	delete ps;	
	return;
	}

// Stampa pagine grafiche
if (page > (1+c_numTextPage+c_numTextPage2))
	{
	int pg = page-(1+c_numTextPage+c_numTextPage2);
	// mappo su pagine effettivamente configurate
	pg = c_graphMap [pg-1];
	pageGraphicPrint(pdc,pg,rcBounds);
	return;
	}

if (pagePrEnd.getNumPage() > 0)
	{
	CLineDoc* pDoc = (CLineDoc* )GetDocument();
		
	// indice prima riga
	int startLine = (page - 15 - pagePr.getNumPage()) * LinePerPage;
	pagePrEnd.print (startLine,LinePerPage,rcBounds,pdc);
	return;
	}
}


void CInitView::OnFilePrint() 
{
// TODO: Add your command handler code here
// directPrint = TRUE;
CPdfPrintView::OnFilePrint();
}

BOOL CInitView::OnEraseBkgnd(CDC* pDC) 
{
	// TODO: Add your message handler code here and/or call default

// Call True base class (white not gray)	
return CPdfPrintView::OnEraseBkgnd(pDC);
}


void CInitView::OnFilePrintPreview() 
{
	// TODO: Add your command handler code here
directPrint = FALSE;
CPdfPrintView::OnFilePrintPreview();
}



void CInitView::OnEndPrinting(CDC* pDC, CPrintInfo* pInfo) 
{
// TODO: Add your specialized code here and/or call the base class

// Aggiornato 27-19-1997

// Trend Longitudinale
if (lineInfo[0].val != NULL)
	{
	delete [] lineInfo[0].val;
	lineInfo[0].val=NULL;
	}

if (lineInfo[1].val != NULL)
	{
	delete [] lineInfo[1].val;
	lineInfo[1].val=NULL;
	}

if (lineInfo[2].val != NULL)
	{
	delete [] lineInfo[2].val;
	lineInfo[2].val=NULL;
	}

if (lineInfo[3].val != NULL)
	{
	delete [] lineInfo[3].val;
	lineInfo[3].val=NULL;
	}

// Allarme Densita` 
if (lineInfoAD[0].val != NULL)
	{
	delete [] lineInfoAD[0].val;
	lineInfoAD[0].val=NULL;
	}

if (lineInfoAD[1].val != NULL)
	{
	delete [] lineInfoAD[1].val;
	lineInfoAD[1].val=NULL;
	}

if (lineInfoAD[2].val != NULL)
	{
	delete [] lineInfoAD[2].val;
	lineInfoAD[2].val=NULL;
	}

if (lineInfoAD[3].val != NULL)
	{
	delete [] lineInfoAD[3].val;
	lineInfoAD[3].val=NULL;
	}

// Qualita`  
/*-------------------------------
if (lineInfoQ[0].val != NULL)
	{
	delete [] lineInfoQ[0].val;
	lineInfoQ[0].val=NULL;
	}

if (lineInfoQ[1].val != NULL)
	{
	delete [] lineInfoQ[1].val;
	lineInfoQ[1].val=NULL;
	}

if (lineInfoQ[2].val != NULL)
	{
	delete [] lineInfoQ[2].val;
	lineInfoQ[2].val=NULL;
	}

if (lineInfoQ[3].val != NULL)
	{
	delete [] lineInfoQ[3].val;
	lineInfoQ[3].val=NULL;
	}
---------------------------------*/

}

void CInitView::OnUpdateFilePrint(CCmdUI* pCmdUI) 
{
// TODO: Add your command update UI handler code here
CLineDoc* pDoc = (CLineDoc* )GetDocument();
pCmdUI->Enable(pDoc->loadedData);
	
}

void CInitView::OnUpdateFilePrintPreview(CCmdUI* pCmdUI) 
{
// TODO: Add your command update UI handler code here

CLineDoc* pDoc = (CLineDoc* )GetDocument();
pCmdUI->Enable(pDoc->loadedData);
	
}



void CInitView::OnPrepareDC(CDC* pDC, CPrintInfo* pInfo) 
{
	// TODO: Add your specialized code here and/or call the base class
if (pDC->IsPrinting())
	{
	CSize printSize;
	printSize.cx = pDC->GetDeviceCaps(HORZRES);
	printSize.cy = pDC->GetDeviceCaps(VERTRES);
	pDC->SetViewportExt(printSize.cx,printSize.cy);
	}

CPdfPrintView::OnPrepareDC(pDC, pInfo);
}


int  PagePrinter::print(int index,int numLine,CRect &rectB,CDC* pdc)
{
TEXTMETRIC tm;
CPoint p;

int lineCaption = 0;

if (index < 0)
	return 0;

CFont fontNormal,*pold;
CFont fontCaption;

fontNormal.CreatePointFont (fNormal.size,fNormal.name,pdc);
pold = pdc->SelectObject(&fontNormal); 
pdc->GetTextMetrics(&tm);
pdc->SetBkMode (TRANSPARENT);


p.x = rectB.left + tm.tmAveCharWidth;
p.y = rectB.top + tm.tmHeight;

if (index >= page.GetSize())
	return 0;
int i;
for (i=index;i<index+numLine;i++)
	{

	if (i >= page.GetSize())
		break;

	pdc->SetTextAlign(page[i].flags);
	p.y += tm.tmHeight + tm.tmExternalLeading;
	p.y += (tm.tmHeight + tm.tmExternalLeading)/6;

	int x;
	if (page[i].flags & TA_CENTER)
		{
		x = rectB.left + (rectB.right - rectB.left)/2;
 		}
	else
		x = p.x;

	pdc->ExtTextOut (x,p.y,0,
		NULL,page[i].str,page[i].str.GetLength()-2,NULL); // Skip Cr/Lf

	if (p.y > (rectB.bottom - 4*tm.tmHeight))
		break;
	
	}

// Clip rectB for other printing
rectB.top = p.y;
pdc->SelectObject(pold); 

return(i);
}


void CInitView::reportRotolo2(CDC* pDC,CRect rcBounds)
{
DOC_CLASS* pDoc = (DOC_CLASS *) GetDocument();
// TODO: add draw code here
pDC->SaveDC();

// intestazione report
// provo con CPage
CPage*	ps= new CPage(rcBounds,pDC,FALSE,MM_TEXT);

double bTop,bLeft,bRight,bBottom;

bTop = ps->ConvertYToInches(rcBounds.top);
bLeft = ps->ConvertXToInches(rcBounds.left);
bBottom = ps->ConvertYToInches(rcBounds.bottom);
bRight = ps->ConvertXToInches(rcBounds.right);

CString str;

// Intestazione
//--------------
	TABLEHEADER* pTable1 = NULL;        
	pTable1=new TABLEHEADER;  
	pTable1->SetSkip=TRUE;	// no fill 
	pTable1->PointSize=28;
	pTable1->LineSize=1;    // default shown only for demp purposes
	pTable1->NumPrintLines=1;
	pTable1->UseInches=TRUE;
	pTable1->AutoSize=FALSE;
	pTable1->Border=TRUE;
	pTable1->FillFlag=FILL_LTGRAY;
	pTable1->NumColumns=1;
	pTable1->NumRows = 0;
	pTable1->StartRow=bTop;
	pTable1->StartCol=bLeft;
	pTable1->EndCol=bRight;
	pTable1->HeaderLines=1;
	// Intestazione: 
	str.LoadString (CSM_GRAPHVIEW_PRINTCAPTION0);
	//pTable1->ColDesc[0].Init((bRight-bLeft),"QUALITY CONTROL REPORT COIL",FILL_NONE);
	pTable1->ColDesc[0].Init((bRight-bLeft),(LPCTSTR)str,FILL_NONE);
	ps->setRealPrint(TRUE);
	ps->Table(pTable1);	

	// Nome rotolo
	TABLEHEADER* pTable2 = NULL;        
	pTable2=new TABLEHEADER;  
	pTable2->SetSkip=TRUE;	// no fill 
	pTable2->PointSize=16;
	pTable2->LineSize=1;    // default shown only for demp purposes
	pTable2->NumPrintLines=1;
	pTable2->UseInches=TRUE;
	pTable2->AutoSize=TRUE;
	pTable2->Border=TRUE;
	pTable2->VLines = FALSE;	//	true draw vertical seperator lines
	pTable2->HLines = TRUE;    // ditto on horizontal lines
	pTable2->FillFlag=FILL_NONE;
	pTable2->NumColumns=2;
	pTable2->NumRows = 1;
	pTable2->StartRow=pTable1->EndRow+0.25;
	pTable2->StartCol=bLeft;
	pTable2->EndCol=bRight;
	pTable2->NoHeader=TRUE;
	ps->setRealPrint(TRUE);
	ps->Table(pTable2);
	// righe
	// strip
	str.LoadString (CSM_GRAPHVIEW_ROTOLO);
	ps->Print(pTable2,0,0,16,TEXT_LEFT,(LPCTSTR)str);
	str = ": ";
	str += pDoc->getRotolo();
	ps->Print(pTable2,0,1,16,TEXT_LEFT,(LPCTSTR)str);

	// Alloy Type 
	TABLEHEADER* pTable3 = NULL;        
	pTable3=new TABLEHEADER;  
	pTable3->SetSkip=TRUE;	// no fill 
	pTable3->PointSize=16;
	pTable3->LineSize=1;    // default shown only for demp purposes
	pTable3->NumPrintLines=1;
	pTable3->UseInches=TRUE;
	pTable3->AutoSize=TRUE;
	pTable3->Border=TRUE;
	pTable3->VLines = FALSE;	//	true draw vertical seperator lines
	pTable3->HLines = TRUE;    // ditto on horizontal lines
	pTable3->FillFlag=FILL_NONE;
	pTable3->NumColumns=2;
	pTable3->NumRows = 1;
	pTable3->StartRow= pTable2->EndRow+0.1;
	pTable3->StartCol=bLeft;
	pTable3->EndCol=bRight;
	pTable3->NoHeader = TRUE;
	ps->setRealPrint(TRUE);
	ps->Table(pTable3);	

	// Materiale
	str.LoadString (CSM_GRAPHVIEW_LEGA);
	ps->Print(pTable3,0,0,16,TEXT_LEFT,(LPCTSTR)str);
	// ??? Posizioni relative a strip sottratto subNastro.top
	str.Format (_T(": %s"),pDoc->lega);
	ps->Print(pTable3,0,1,16,TEXT_LEFT,(LPCTSTR)str);
		
	// Thickness
	TABLEHEADER* pTable4 = NULL;        
	pTable4=new TABLEHEADER;  
	pTable4->SetSkip=TRUE;	// no fill 
	pTable4->PointSize=16;
	pTable4->LineSize=1;    // default shown only for demp purposes
	pTable4->NumPrintLines=1;
	pTable4->UseInches=TRUE;
	pTable4->AutoSize=TRUE;
	pTable4->Border=TRUE;
	pTable4->VLines = FALSE;	//	true draw vertical seperator lines
	pTable4->HLines = TRUE;    // ditto on horizontal lines
	pTable4->FillFlag=FILL_NONE;
	pTable4->NumColumns=2;
	pTable4->NumRows = 1;
	pTable4->StartRow= pTable3->EndRow+0.1;
	pTable4->StartCol=bLeft;
	pTable4->EndCol=bRight;
	pTable4->NoHeader = TRUE;
	ps->setRealPrint(TRUE);
	ps->Table(pTable4);	

	// Spessore
	// str.LoadString (CSM_GRAPHVIEW_THICK);
	str.LoadString (CSM_GRAPHVIEW_SPESSORE);
	//str = "Spessore";
	ps->Print(pTable4,0,0,16,TEXT_LEFT,(LPCTSTR)str);
	// ??? Posizioni relative a strip sottratto subNastro.top
	str.Format (_T(": %4.2lf"),pDoc->spessore);
	ps->Print(pTable4,0,1,16,TEXT_LEFT,(LPCTSTR)str);

	// Lunghezza 
	TABLEHEADER* pTable5 = NULL;        
	pTable5=new TABLEHEADER;  
	pTable5->SetSkip=TRUE;	// no fill 
	pTable5->PointSize=16;
	pTable5->LineSize=1;    // default shown only for demp purposes
	pTable5->NumPrintLines=1;
	pTable5->UseInches=TRUE;
	pTable5->AutoSize=TRUE;
	pTable5->Border=TRUE;
	pTable5->VLines = FALSE;	//	true draw vertical seperator lines
	pTable5->HLines = TRUE;    // ditto on horizontal lines
	pTable5->FillFlag=FILL_NONE;
	pTable5->NumColumns=2;
	pTable5->NumRows = 1;
	pTable5->StartRow= pTable4->EndRow+0.1;
	pTable5->StartCol=bLeft;
	pTable5->EndCol=bRight;
	pTable5->NoHeader = TRUE;
	ps->setRealPrint(TRUE);
	ps->Table(pTable5);	

	// lunghezza
	str.LoadString (CSM_GRAPHVIEW_LUNGHEZZA);
	ps->Print(pTable5,0,0,16,TEXT_LEFT,(LPCTSTR)str);
	// ??? Posizioni relative a strip sottratto subNastro.top
	str.Format (_T(": %d"),(int)pDoc->getVTotalLength());
	ps->Print(pTable5,0,1,16,TEXT_LEFT,(LPCTSTR)str);

	// Larghezza 
	TABLEHEADER* pTable6 = NULL;        
	pTable6=new TABLEHEADER;  
	pTable6->SetSkip=TRUE;	// no fill 
	pTable6->PointSize=16;
	pTable6->LineSize=1;    // default shown only for demp purposes
	pTable6->NumPrintLines=1;
	pTable6->UseInches=TRUE;
	pTable6->AutoSize=TRUE;
	pTable6->Border=TRUE;
	pTable6->VLines = FALSE;	//	true draw vertical seperator lines
	pTable6->HLines = TRUE;    // ditto on horizontal lines
	pTable6->FillFlag=FILL_NONE;
	pTable6->NumColumns=2;
	pTable6->NumRows = 1;
	pTable6->StartRow= pTable5->EndRow+0.1;
	pTable6->StartCol=bLeft;
	pTable6->EndCol=bRight;
	pTable6->NoHeader = TRUE;
	ps->setRealPrint(TRUE);
	ps->Table(pTable6);	

	// larghezza
	str.LoadString (CSM_GRAPHVIEW_LARGHEZZA);
	ps->Print(pTable6,0,0,16,TEXT_LEFT,(LPCTSTR)str);
	// ??? Posizioni relative a strip sottratto subNastro.top
	str.Format (_T(": %4.0lf"),pDoc->c_larghezza);
	ps->Print(pTable6,0,1,16,TEXT_LEFT,(LPCTSTR)str);

	// Cliente 
	TABLEHEADER* pTable7 = NULL;        
	pTable7=new TABLEHEADER;  
	pTable7->SetSkip=TRUE;	// no fill 
	pTable7->PointSize=16;
	pTable7->LineSize=1;    // default shown only for demp purposes
	pTable7->NumPrintLines=1;
	pTable7->UseInches=TRUE;
	pTable7->AutoSize=TRUE;
	pTable7->Border=TRUE;
	pTable7->VLines = FALSE;	//	true draw vertical seperator lines
	pTable7->HLines = TRUE;    // ditto on horizontal lines
	pTable7->FillFlag=FILL_NONE;
	pTable7->NumColumns=2;
	pTable7->NumRows = 1;
	pTable7->StartRow= pTable6->EndRow+0.1;
	pTable7->StartCol=bLeft;
	pTable7->EndCol=bRight;
	pTable7->NoHeader = TRUE;
	ps->setRealPrint(TRUE);
	ps->Table(pTable7);	

	// cliente
	str.LoadString (CSM_GRAPHVIEW_CLIENTE);
	ps->Print(pTable7,0,0,16,TEXT_LEFT,(LPCTSTR)str);
	str.Format (_T(": %s"),pDoc->c_cliente);
	ps->Print(pTable7,0,1,16,TEXT_LEFT,(LPCTSTR)str);


	// data ora 
	TABLEHEADER* pTable8 = NULL;        
	pTable8=new TABLEHEADER;  
	pTable8->SetSkip=TRUE;	// no fill 
	pTable8->PointSize=16;
	pTable8->LineSize=1;    // default shown only for demp purposes
	pTable8->NumPrintLines=1;
	pTable8->UseInches=TRUE;
	pTable8->AutoSize=TRUE;
	pTable8->Border=TRUE;
	pTable8->VLines = FALSE;	//	true draw vertical seperator lines
	pTable8->HLines = TRUE;    // ditto on horizontal lines
	pTable8->FillFlag=FILL_NONE;
	pTable8->NumColumns=2;
	pTable8->NumRows = 1;
	pTable8->StartRow=pTable7->EndRow+0.1;
	pTable8->StartCol=bLeft;
	pTable8->EndCol=bRight;
	pTable8->NoHeader=TRUE;
	ps->setRealPrint(TRUE);
	ps->Table(pTable8);
	// Intestazione: Metro,Numero dei fori,Classe difetto,Banda
	// data
	CTime t (pDoc->startTime);
	// str = "DATE          : " + t.Format("%d-%m-%Y  %H:%M:%S");
	str.LoadString(CSM_GRAPHVIEW_DATE);
	ps->Print(pTable8,0,0,16,TEXT_LEFT,(LPCTSTR)str);
	// format data
	CString formatRes;
	formatRes.LoadString (CSM_GRAPHVIEW_DATETIMEFORMAT);
	str = ": ";
	str += t.Format(formatRes);
	ps->Print(pTable8,0,1,16,TEXT_LEFT,(LPCTSTR)str);

	// Tabella 
	TABLEHEADER* pTable9 = NULL;        
	pTable9=new TABLEHEADER;  
	pTable9->SetSkip=TRUE;	// no fill 
	pTable9->PointSize=16;
	pTable9->LineSize=1;    // default shown only for demp purposes
	pTable9->NumPrintLines=1;
	pTable9->UseInches=TRUE;
	pTable9->AutoSize=TRUE;
	pTable9->Border=TRUE;
	pTable9->VLines = FALSE;	//	true draw vertical seperator lines
	pTable9->HLines = TRUE;    // ditto on horizontal lines
	pTable9->FillFlag=FILL_NONE;
	pTable9->NumColumns=2;
	pTable9->NumRows = 1;
	pTable9->StartRow= pTable8->EndRow+0.1;
	pTable9->StartCol=bLeft;
	pTable9->EndCol=bRight;
	pTable9->NoHeader = TRUE;
	ps->setRealPrint(TRUE);
	ps->Table(pTable9);	

	// ricetta
	str.LoadString (CSM_GRAPHVIEW_RICETTA);
	ps->Print(pTable9,0,0,16,TEXT_LEFT,(LPCTSTR)str);
	str.Format (_T(": %s"),pDoc->nomeRicetta);
	ps->Print(pTable9,0,1,16,TEXT_LEFT,(LPCTSTR)str);
///---------------	
	



//-----------------------------------------------------
// sezione dettaglio difetti 

double seg[3];
int interv[3];
int numSeg = 3;


interv[0] = (int) m_INTERVALLO1;
seg[0] = (pDoc->getVTotalLength() * m_INTERVALLO1 / 100);
interv[1] = (int) m_INTERVALLO2;
seg[1] = (pDoc->getVTotalLength() * m_INTERVALLO2 / 100);
interv[2] = (int) m_INTERVALLO3;
seg[2] = (pDoc->getVTotalLength() * m_INTERVALLO3 / 100);


double lastYPos = bTop;
TABLEHEADER* pTable10 = NULL;        
pTable10=new TABLEHEADER;  
pTable10->SetSkip=TRUE;	// no fill 
pTable10->PointSize=12;
pTable10->LineSize=1;    // default shown only for demp purposes
pTable10->NumPrintLines=1;
pTable10->UseInches=TRUE;
pTable10->AutoSize=TRUE;
pTable10->Border=TRUE;
pTable10->VLines = TRUE;	//	true draw vertical seperator lines
pTable10->HLines = TRUE;    // ditto on horizontal lines
pTable10->FillFlag=FILL_NONE;
pTable10->NumColumns=2;
pTable10->NumRows = 4;
pTable10->StartRow=pTable9->EndRow+0.15;
pTable10->StartCol=bLeft;
pTable10->EndCol=bRight;
pTable10->NoHeader=TRUE;

ps->setRealPrint(TRUE);
ps->Table(pTable10);	

// recalc totali
// pDoc->c_difCoil.calcTotalCode ();

CString s;
int rowTableIndex = 0;

/* skip density 
s = "   ";
s += "Density  [holes/m2]";
ps->Print(pTable10,rowTableIndex,0,10,TEXT_LEFT,(LPCTSTR)s);
//da = pDoc->c_difCoil.getTotalDensity('A');
//db = pDoc->c_difCoil.getTotalDensity('B');
s.Format(" A=%6.04lf  B=%6.04lf  A+B=%6.04lf",da,db,da+db);
ps->Print(pTable10,rowTableIndex,1,10,TEXT_LEFT,(LPCTSTR)s);

rowTableIndex++;

*/

s = "   ";
//s += "Total Holes ";
CString sx;
sx.LoadString(ID_CSM_REPORT_TOTALE_FORI);
// s += "Total Holes ";
s += sx;
ps->Print(pTable10,rowTableIndex,0,14,TEXT_LEFT,(LPCTSTR)s);
s = "";
double totDifetti = 0.;
for (int nc=0;nc<pDoc->c_difCoil.getNumClassi();nc++)
	{
	// st.Format ("Totale Difetti Classe %c : %d \r\n",nc+'A',
	double dv = pDoc->c_difCoil.getTotDifetti(nc+'A',0,(int)pDoc->getVTotalLength());
	str.Format(_T(" %c=%2.0lf"),nc+'A',dv);
	s += str;
	totDifetti += dv;
	}

str.Format(_T(" Tot.=%2.0lf"),totDifetti);
s += str;
//da = pDoc->c_difCoil.getTotDifetti('A',0,(int)pDoc->getVTotalLength());
//db = pDoc->c_difCoil.getTotDifetti('B',0,(int)pDoc->getVTotalLength());
//s.Format(" A=%3.0lf  B=%3.0lf  A+B=%3.0lf ",da,db,da+db);
ps->Print(pTable10,rowTableIndex,1,12,TEXT_LEFT,(LPCTSTR)s);

rowTableIndex++;

s = _T("   ");
sx.LoadString(ID_CSM_REPORT_SOGLIE);
//s += "Threshold  [um] ";
s += sx;
ps->Print(pTable10,rowTableIndex,0,12,TEXT_LEFT,(LPCTSTR)s);
s = pDoc->formatStringThreshold();

ps->Print(pTable10,rowTableIndex,1,12,TEXT_LEFT,(LPCTSTR)s);

// totale fori classe B
//rowTableIndex++;
//s = "   ";
//s += "Total Holes Class B";
//ps->Print(pTable10,rowTableIndex,0,12,TEXT_LEFT,(LPCTSTR)s);
//s.Format("  %4.0lf",pDoc->c_difCoil.getTotDifetti('B',0,(int)pDoc->getVTotalLength()));
//ps->Print(pTable10,rowTableIndex,1,12,TEXT_LEFT,(LPCTSTR)s);

// totale metri forati
rowTableIndex++;
s.LoadString(CSM_GRAPHVIEW_METRIFORATI);
//s = _T("Totale metri forati");
ps->Print(pTable10,rowTableIndex,0,12,TEXT_LEFT,(LPCTSTR)s);
s.Format(_T("  %4.0lf"),(double)pDoc->c_difCoil.GetCount());
ps->Print(pTable10,rowTableIndex,1,12,TEXT_LEFT,(LPCTSTR)s);
// percentuale metri forati
rowTableIndex++;
s.LoadString(CSM_GRAPHVIEW_PERCMETRIFORATI);
//s = _T("Percentuale metri forati (%%)");
ps->Print(pTable10,rowTableIndex,0,12,TEXT_LEFT,(LPCTSTR)s);
double dv = pDoc->c_difCoil.GetCount()/(pDoc->c_difCoil.getMeter()>0.?pDoc->c_difCoil.getMeter():1);
if (pDoc->c_difCoil.getMeter() == 0.)
	dv = 0;
s.Format(_T("  %4.01lf"),dv*100.);
s += _T(" (%%)");
ps->Print(pTable10,rowTableIndex,1,12,TEXT_LEFT,(LPCTSTR)s);


//--------------------------------------------------------------------------------------


TABLEHEADER* pTable11 = NULL;        
pTable11=new TABLEHEADER;  
pTable11->SetSkip=TRUE;	// no fill 
pTable11->PointSize=12;
pTable11->LineSize=1;    // default shown only for demp purposes
pTable11->NumPrintLines=1;
pTable11->UseInches=TRUE;
pTable11->AutoSize=FALSE;
pTable11->Border=TRUE;
pTable11->VLines = TRUE;	//	true draw vertical seperator lines
pTable11->HLines = TRUE;    // ditto on horizontal lines
pTable11->FillFlag=FILL_NONE;
pTable11->NumColumns=1;
pTable11->NumRows = 0;
pTable11->StartRow=pTable10->EndRow+0.3;
pTable11->StartCol=bLeft;
pTable11->EndCol=bRight;
pTable11->HeaderLines=1;
// Intestazione: Metro,Numero dei fori,Classe difetto,Banda
// s = "Total number of holes for every length interval";
s.LoadString(ID_CSM_REPORT_TOTALE_FORI_X_INTERVALLO);
pTable11->ColDesc[0].Init((bRight-bLeft),s,FILL_NONE);
ps->setRealPrint(TRUE);
ps->Table(pTable11);	

TABLEHEADER* pTable12 = NULL;        
pTable12=new TABLEHEADER;  
pTable12->SetSkip=TRUE;	// no fill 
pTable12->PointSize=12;
pTable12->LineSize=1;    // default shown only for demp purposes
pTable12->NumPrintLines=1;
pTable12->UseInches=TRUE;
pTable12->AutoSize=TRUE;
pTable12->Border=TRUE;
pTable12->VLines = TRUE;	//	true draw vertical seperator lines
pTable12->HLines = TRUE;    // ditto on horizontal lines
pTable12->FillFlag=FILL_NONE;
pTable12->NumColumns=2;
pTable12->NumRows = numSeg+1;
pTable12->StartRow= pTable11->EndRow;
pTable12->StartCol=bLeft;
pTable12->EndCol=bRight;
pTable12->NoHeader = TRUE;

ps->setRealPrint(TRUE);
ps->Table(pTable12);	


int v;

s.LoadString(ID_CSM_REPORT_FORI_X_INTERVALLO);
for (int i=0;i<=numSeg;i++)
	{
	// "Sezione Da Metri A Metri Classe A ClasseB Classe C Classe D Totale
	CString sItem;
	if(i == 0)
		{
		v = (int)pDoc->c_difCoil.getTotDifetti(0,seg[i]);
		// sItem.Format("Number Holes interval  %6.0lf - %6.0lf [m]",0.,seg[i]); 
		sItem.Format(_T("%s  %6.0lf - %6.0lf [m]"),(LPCTSTR)s,0.,seg[i]); 
		}
	else
		{
		if (i == numSeg)
			{
			v = (int)pDoc->c_difCoil.getTotDifetti(seg[i-1],pDoc->getVTotalLength());
			//sItem.Format("Number Holes interval  %6.0lf - %6.0lf [m]",
			sItem.Format(_T("%s  %6.0lf - %6.0lf [m]"),(LPCTSTR)s,
				seg[i-1],(double)pDoc->getVTotalLength()); 
			}
		else
			{
			v = (int)pDoc->c_difCoil.getTotDifetti(seg[i-1],seg[i]);
			//sItem.Format("Number Holes interval  %6.0lf - %6.0lf [m]",seg[i-1],seg[i]); 
			sItem.Format(_T("%s  %6.0lf - %6.0lf [m]"),(LPCTSTR)s,seg[i-1],seg[i]); 
			}
		}
	int rowPos = i;
	CString s;
	s = "   ";
	s += sItem;
	ps->Print(pTable12,rowPos,0,12,TEXT_LEFT,(LPCTSTR)s);
	sItem.Format(_T("%d"),v);
	s = "   ";
	s += sItem;
	ps->Print(pTable12,rowPos,1,12,TEXT_LEFT,(LPCTSTR)s);
	}

delete pTable1;
delete pTable2;
delete pTable3;
delete pTable4;
delete pTable5;
delete pTable6;
delete pTable7;
delete pTable8;
delete pTable9;
delete pTable10;
delete pTable11;
delete pTable12;
delete ps;

pDC->RestoreDC(-1);
}



void CInitView::pageGraphicPrint(CDC* pdc,int curPage,CRect &rcBounds)
{
CRect rectB;

CLineDoc* pDoc = (CLineDoc* )GetDocument();

switch (curPage)
	{
   	case 1:
		{
		// Disegno
		// Set base Rect
		layoutL.setDrawRect(rcBounds);

		layoutL.draw(pdc);
		// Trovo Finestra disegno (grigia)
		layoutL.CalcDrawRect (rcBounds,rectB);
		// Disegno	
		CRect subRectB[4];
		int numClassi = layoutL.c_numClassi;
		for (int i=0;i<numClassi;i++)
			{
			trendLine[i].setMode (LINEAR);
			subRectB[i]=rectB;
			subRectB[i].TopLeft().y += (rectB.Size().cy/numClassi)*i;
			subRectB[i].BottomRight().y -= (rectB.Size().cy/numClassi)*(numClassi-(i+1));
			trendLine[i].setScale(subRectB[i],lineInfo[i]);
			trendLine[i].draw(pdc);
			}
		}
		break;
	case 2:
		{
		// Disegno
		// Set base Rect
		
		layoutD.setDrawRect(rcBounds);

		layoutD.draw(pdc);
		// Trovo Finestra disegno (grigia)
		layoutD.CalcDrawRect (rcBounds,rectB);


		// Disegno
		int Xsize = (rectB.right - rectB.left-layoutD.c_sizeFBanda);
		//	/pDoc->c_difCoil.getNumSchede();
		CRect clientRect;
		clientRect = rectB;
		clientRect.right = clientRect.left + Xsize;

		drawPosDiaframma(pdc,clientRect,&layoutD);

		// 18-04-05 Fix Bug Non si teneva conto sizeFBanda
		Xsize = (rectB.right - rectB.left-layoutD.c_sizeFBanda)/pDoc->c_difCoil.getNumSchede();
		clientRect = rectB;
		
		Xsize = layoutD.c_sizeBanda/layoutD.c_bandePerCol;

		clientRect.right = clientRect.left + Xsize;


		for (int i=0;i<pDoc->c_difCoil.getNumSchede();i++)
			{
			double *dVal = new double [pDoc->c_difCoil.getNumClassi()];
			for (int j=0;j<pDoc->c_difCoil.getNumClassi();j++)
				{
				// Sostituita densita` con totali per classi
				// dVal[j] = pDoc->c_difCoil.getValDensita(i,'A'+j,pDoc->GetTrendDScalaX());
				dVal[j] = pDoc->c_difCoil.getTotDifetti(i,'A'+j,(int )m_DALMETRO,
					(int )m_ALMETRO);
				}

			clientRect.left  = rectB.left + (int)(layoutD.c_sizeFBanda + Xsize * i);
			clientRect.right = rectB.left + (int)(layoutD.c_sizeFBanda + Xsize * (i+1));	
			int scale = max(1, pDoc->c_difCoil.getMaxDensFromTo((int )m_DALMETRO,(int )m_ALMETRO)*1.25);
				densView[i].setScale(clientRect,
				scale,
				dVal);
			densView[i].setPos(i+1);
	 		densView[i].draw(pdc);
			delete [] dVal;
			}
		}
		break;
	// Allarme Densita 3
	case 3:
		{
		// Disegno
		// Set base Rect
		layoutAD.setDrawRect(rcBounds);

		layoutAD.draw(pdc);
		// Trovo Finestra disegno (grigia)
		layoutAD.CalcDrawRect (rcBounds,rectB);
		// Disegno	
		CRect subRectB[4];
		int numClassi = layoutAD.c_numClassi;
		for (int i=0;i<numClassi;i++)
			{
			trendAD[i].setMode (LINEAR);
			subRectB[i]=rectB;
			subRectB[i].TopLeft().y += (rectB.Size().cy/numClassi)*i;
			subRectB[i].BottomRight().y -= (rectB.Size().cy/numClassi)*(numClassi-(i+1));
			trendAD[i].setScale(subRectB[i],lineInfoAD[i]);
			trendAD[i].draw(pdc);
			}

		 }
		break;
	// Compressione A,B,C,B,Big
	case 4:
		{
		// Disegno
		int c_pxBRoll = 5;
		// Set base Rect
		c_layoutCompressABBig.setDrawRect(rcBounds);

		c_layoutCompressABBig.draw(pdc);
		// Trovo Finestra disegno (grigia)
		c_layoutCompressABBig.CalcDrawRect (rcBounds,rectB);
		// Disegno	??
		c_targetBoardABBig.setDrawAttrib((int)c_layoutCompressABBig.c_sizeFBanda,
								(int)c_layoutCompressABBig.c_sizeBanda,
								c_pxBRoll,c_layoutCompressABBig.c_bandePerCol);
		CRect clipRect(rectB);
		clipRect.TopLeft().x = rcBounds.TopLeft().x;
		clipRect.BottomRight().x = rcBounds.BottomRight().x;
		pdc->IntersectClipRect (clipRect);

		// 18-04-05 Fix Bug Non si teneva conto sizeFBanda
		int Xsize = (rectB.right - rectB.left-c_layoutCompressABBig.c_sizeFBanda);
		//	/pDoc->c_difCoil.getNumSchede();
		CRect clientRect;
		clientRect = rectB;
		clientRect.right = clientRect.left + Xsize;

		drawPosDiaframma(pdc,clientRect,&c_layoutCompressABBig);
		c_targetBoardABBig.plot(clientRect,pdc,c_layoutCompressABBig.getMaxY(),
						c_layoutCompressABBig.getMinY());


//-------------------------------------------------------------
// Disegno Posizione bobine allineate a sx
/*
No posizione bobine
		{
		// valori per disegnare asse mediano
		int leftPos,rightPos;
		CPen pen0,pen1,*oldPen;
		// light grey
		//pen0.CreatePen(PS_DASH,1,RGB(196,196,196));
		pen0.CreatePen(PS_SOLID,1,RGB(0,0,0));
		// red
		// pen1.CreatePen(PS_DASH,1,RGB(128,0,0));
		pen1.CreatePen(PS_SOLID,1,RGB(0,0,0));
		oldPen = pdc->SelectObject(&pen0);
		int lastElem;
		lastElem = pDoc->c_rotoloMapping.c_lastElemento;
		double posLeft;
		posLeft = pDoc->getPosColtello(lastElem);
		// 15-09-2004 corretto centrato +layout.c_sizeFBanda 
		int scPosLeft = rectB.left +c_layoutCompressABBig.c_sizeFBanda +
			(1.  * posLeft * (double)c_layoutCompressABBig.c_sizeBanda / (c_layoutCompressABBig.c_bandePerCol*SIZE_BANDE));
		// sinistra per disegnare asse mediano
		leftPos = scPosLeft;

		pdc->MoveTo(scPosLeft,rectB.top);
		pdc->LineTo(scPosLeft,rectB.bottom);
		// disegno solo inizio strip 
		// fine strip solo per ultimo strip
		double endPosLastStrip = 0.;
		for (int i=0;i<pDoc->c_rotoloMapping[lastElem].size();i++)
			{
			// prima riga sx
			pdc->SelectObject(&pen0);	
			posLeft = (pDoc->c_rotoloMapping[lastElem])[i].c_xPos;

			// check distanza con fine strip precedente
			if (i > 0)
				{
				posLeft -= (fabs(posLeft - endPosLastStrip)/2);
				}

			// memo  last end position
			endPosLastStrip = (pDoc->c_rotoloMapping[lastElem])[i].c_xPos
						+ (pDoc->c_rotoloMapping[lastElem])[i].c_larghezza;
		// 15-09-2004 corretto centrato +layout.c_sizeFBanda 
			scPosLeft = rectB.left+c_layoutCompressABBig.c_sizeFBanda  + 
				(1.  * posLeft * (double)c_layoutCompressABBig.c_sizeBanda / (c_layoutCompressABBig.c_bandePerCol*SIZE_BANDE));

			pdc->MoveTo(scPosLeft,rectB.top);
			pdc->LineTo(scPosLeft,rectB.bottom);

			if((i == (pDoc->c_rotoloMapping[lastElem].size()-1))||
				(pDoc->c_viewStripBorder))
				{
				// seconda riga
				pdc->SelectObject(&pen1);	
				posLeft = (pDoc->c_rotoloMapping[lastElem])[i].c_xPos;
				posLeft += (pDoc->c_rotoloMapping[lastElem])[i].c_larghezza;

			// 15-09-2004 corretto centrato +layout.c_sizeFBanda 
				scPosLeft = rectB.left +c_layoutCompressABBig.c_sizeFBanda + 
					(1.  * posLeft * (double)c_layoutCompressABBig.c_sizeBanda 
					/ (c_layoutCompressABBig.c_bandePerCol*SIZE_BANDE));

				pdc->MoveTo(scPosLeft,rectB.top);
				pdc->LineTo(scPosLeft,rectB.bottom);
				}
			}

		// destra per disegnare asse mediano
		rightPos = scPosLeft;
		pdc->SelectObject(oldPen);
		pen0.DeleteObject();
		pen1.DeleteObject();
		// end clear
		pen1.CreatePen(PS_DASH,1,RGB(0,0,0));
		oldPen = pdc->SelectObject(&pen1);
		int mediumPos = leftPos+(rightPos-leftPos)/2;
		pdc->MoveTo(mediumPos,rectB.top);
		pdc->LineTo(mediumPos,rectB.bottom);
		pdc->SelectObject(oldPen);
		pen1.DeleteObject();
		}
*/
		// Fine disegno pos. bobine
		//---------------------------------------------------------
		}
	}

}


CRect CInitView::calcPrintInternalRect(CDC* pdc,CRect rcBounds,CRect* border )
{

CRect rectB;
// GetClientRect

CSize printSize,trueSize;
// in stampa tolgo bordi
// pixel
printSize.cx = pdc->GetDeviceCaps(HORZRES);
printSize.cy = pdc->GetDeviceCaps(VERTRES);

// millimetri
trueSize.cx = pdc->GetDeviceCaps(HORZSIZE);
trueSize.cy = pdc->GetDeviceCaps(VERTSIZE);

// calcolo px per mm
CSize pixel;
pixel.cy = printSize.cy/trueSize.cy;
pixel.cx = printSize.cx/trueSize.cx;

/* Tolgo 2 cm a destra ed 1 a sx */
rcBounds.left += c_leftMarg * pixel.cx;
rcBounds.right -= c_rightMarg * pixel.cx;
/* Tolgo 3 cm in alto e 2 in basso */	
rcBounds.top += c_topMarg * pixel.cy;
rcBounds.bottom -= c_bottomMarg * pixel.cy;

CRect rect (rcBounds);

if (border != NULL)
	*border = rcBounds;

// determino interno per scrittura e grafica
// 5 millimetri in meno
rect.DeflateRect (5 * pixel.cx,5 * pixel.cy);

return(rect);
}

void CInitView::drawPosDiaframma(CDC *pDC, CRect rectB,Layout* layout)
{
DOC_CLASS* pDoc = (DOC_CLASS* )GetDocument();
CPen pen1,pen2,*oldPen;


if (!pDoc->c_doUseDiaframmi)
	return;

// red
	
	pen1.CreatePen(PS_SOLID,2,RGB(196,0,0));
	oldPen = pDC->SelectObject(&pen1);
	double posLeft = pDoc->c_posDiaframmaSx;
	
	// 15-09-2004 corretto centrato +layout.c_sizeFBanda 
	int scPosLeft = rectB.left +layout->c_sizeFBanda + (1.  * posLeft * (double)layout->c_sizeBanda / (layout->c_bandePerCol*pDoc->c_sizeBande));
	if ((scPosLeft >= rectB.left)&&
		(scPosLeft <= rectB.right))
		{
		pDC->MoveTo(scPosLeft,rectB.top);
		pDC->LineTo(scPosLeft,rectB.bottom);
		}
	pen2.CreatePen(PS_SOLID,2,RGB(196,0,0));
	pDC->SelectObject(&pen2);
	
	double posRight = pDoc->c_posDiaframmaDx;
	// 15-09-2004 corretto centrato +layout.c_sizeFBanda 
	int scPosRight = rectB.left +layout->c_sizeFBanda + (1.  * posRight * (double)layout->c_sizeBanda / (layout->c_bandePerCol*pDoc->c_sizeBande));

	if ((scPosRight >= rectB.left)&&
		(scPosRight <= rectB.right))
		{
		pDC->MoveTo(scPosRight,rectB.top);
		pDC->LineTo(scPosRight,rectB.bottom);
		}	
	pDC->SelectObject(oldPen);
	pen1.DeleteObject();
	pen2.DeleteObject();
	// Fine disegno pos. Diaframma
	//---------------------------------------------------------

}