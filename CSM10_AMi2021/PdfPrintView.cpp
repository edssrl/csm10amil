// PdfPrintView.cpp : file di implementazione
//

#include "stdafx.h"
#include "C:\Program Files (x86)\Microsoft Visual Studio 10.0\VC\atlmfc\src\mfc\AfxImpl.h"
#include <WinSpool.h>
#include "afxprint.rc"
#include "PdfPrintView.h"


/////////////////////////////////////////////////////////////////////////////
// Printing Dialog

static BOOL m_bUserAbort; 

class CPrintingDialog : public CDialog
{
public:

//{{AFX_DATA(CPrintingDialog)
	enum { IDD = AFX_IDD_PRINTDLG };
	//}}AFX_DATA
	CPrintingDialog::CPrintingDialog(CWnd* pParent)
		{
			Create(CPrintingDialog::IDD, pParent);      // modeless !
			m_bUserAbort = FALSE;
		}
	virtual ~CPrintingDialog() { }

	virtual BOOL OnInitDialog();
	virtual void OnCancel();
};

BOOL CALLBACK PdfWinAbortProc(HDC, int)
{
	MSG msg;
	while (!m_bUserAbort &&
		::PeekMessage(&msg, NULL, NULL, NULL, PM_NOREMOVE))
	{
		if( !AfxPumpMessage() )
			return FALSE;   // terminate if WM_QUIT received
	}
	return !m_bUserAbort;
}

BOOL CPrintingDialog::OnInitDialog()
{
	SetWindowText(AfxGetAppName());
	CenterWindow();
	return CDialog::OnInitDialog();
}

void CPrintingDialog::OnCancel()
{
	m_bUserAbort = TRUE;  // flag that user aborted print
	CDialog::OnCancel();
}


// CPdfPrintView

IMPLEMENT_DYNCREATE(CPdfPrintView, CView)

CPdfPrintView::CPdfPrintView()
{

c_docName = _T("");
c_docPath = _T("");

}

CPdfPrintView::~CPdfPrintView()
{
}

BEGIN_MESSAGE_MAP(CPdfPrintView, CView)
END_MESSAGE_MAP()


// disegno di CPdfPrintView

void CPdfPrintView::OnDraw(CDC* pDC)
{
	CDocument* pDoc = GetDocument();
	// TODO: aggiungere qui il codice di disegno
}


// diagnostica di CPdfPrintView

#ifdef _DEBUG
void CPdfPrintView::AssertValid() const
{
	CView::AssertValid();
}

#ifndef _WIN32_WCE
void CPdfPrintView::Dump(CDumpContext& dc) const
{
	CView::Dump(dc);
}
#endif
#endif //_DEBUG

BOOL CPdfPrintView::DoPreparePrinting(CPrintInfo* pInfo)
	{
// check if direct and pdf printer
	if (c_docPath != _T(""))
		pInfo->m_pPD->m_pd.Flags |= PD_PRINTTOFILE;
	
	return CView::DoPreparePrinting(pInfo);
	}

// gestori di messaggi CPdfPrintView
void CPdfPrintView::OnFilePrint()
{
	// get default print info
	CPrintInfo printInfo;
	ASSERT(printInfo.m_pPD != NULL);    // must be set

	if (LOWORD(GetCurrentMessage()->wParam) == ID_FILE_PRINT_DIRECT)
	{
		CCommandLineInfo* pCmdInfo = AfxGetApp()->m_pCmdInfo;

		if (pCmdInfo != NULL)
		{
			if (pCmdInfo->m_nShellCommand == CCommandLineInfo::FilePrintTo)
			{
				printInfo.m_pPD->m_pd.hDC = ::CreateDC(pCmdInfo->m_strDriverName,
					pCmdInfo->m_strPrinterName, pCmdInfo->m_strPortName, NULL);
				if (printInfo.m_pPD->m_pd.hDC == NULL)
				{
					AfxMessageBox(AFX_IDP_FAILED_TO_START_PRINT);
					return;
				}
			}
		}

		printInfo.m_bDirect = TRUE;
	}

	if (OnPreparePrinting(&printInfo))
	{
		// hDC must be set (did you remember to call DoPreparePrinting?)
		ASSERT(printInfo.m_pPD->m_pd.hDC != NULL);

		// gather file to print to if print-to-file selected
		CString strOutput;
		if (printInfo.m_pPD->m_pd.Flags & PD_PRINTTOFILE && !printInfo.m_bDocObject)
		{
			if (c_docPath == _T(""))
				{
				// construct CFileDialog for browsing
				CString strDef(MAKEINTRESOURCE(AFX_IDS_PRINTDEFAULTEXT));
				CString strPrintDef(MAKEINTRESOURCE(AFX_IDS_PRINTDEFAULT));
				CString strFilter(MAKEINTRESOURCE(AFX_IDS_PRINTFILTER));
				CString strCaption(MAKEINTRESOURCE(AFX_IDS_PRINTCAPTION));
				CFileDialog dlg(FALSE, strDef, strPrintDef,
					OFN_HIDEREADONLY|OFN_OVERWRITEPROMPT, strFilter, NULL, 0);
				dlg.m_ofn.lpstrTitle = strCaption;

				if (dlg.DoModal() != IDOK)
					return;

				// set output device to resulting path name
				strOutput = dlg.GetPathName();
				}
			else
				// set output device to resulting path name
				strOutput = c_docPath;
		}

		// set up document info and start the document printing process
		CString strTitle;

		CDocument* pDoc = GetDocument();
		if (c_docName == _T(""))
			{
			if (pDoc != NULL)
				strTitle = pDoc->GetTitle();
			else
				EnsureParentFrame()->GetWindowText(strTitle);
			}
		else
			strTitle = c_docName;

		DOCINFO docInfo;
		memset(&docInfo, 0, sizeof(DOCINFO));
		docInfo.cbSize = sizeof(DOCINFO);
		docInfo.lpszDocName = strTitle;
		CString strPortName;
		if (strOutput.IsEmpty())
		{
			docInfo.lpszOutput = NULL;
			strPortName = printInfo.m_pPD->GetPortName();
		}
		else
		{
			docInfo.lpszOutput = strOutput;
			AfxGetFileTitle(strOutput,
				strPortName.GetBuffer(_MAX_PATH), _MAX_PATH);
		}

		// setup the printing DC
		CDC dcPrint;
		if (!printInfo.m_bDocObject)
		{
			dcPrint.Attach(printInfo.m_pPD->m_pd.hDC);  // attach printer dc
			dcPrint.m_bPrinting = TRUE;
		}
		OnBeginPrinting(&dcPrint, &printInfo);

		if (!printInfo.m_bDocObject)
			dcPrint.SetAbortProc(PdfWinAbortProc);

		// disable main window while printing & init printing status dialog
		// Store the Handle of the Window in a temp so that it can be enabled 
		// once the printing is finished
		CWnd * hwndTemp = AfxGetMainWnd();
		hwndTemp->EnableWindow(FALSE);
		CPrintingDialog dlgPrintStatus(this);

		CString strTemp;
		dlgPrintStatus.SetDlgItemText(AFX_IDC_PRINT_DOCNAME, strTitle);
		dlgPrintStatus.SetDlgItemText(AFX_IDC_PRINT_PRINTERNAME,
			printInfo.m_pPD->GetDeviceName());
		dlgPrintStatus.SetDlgItemText(AFX_IDC_PRINT_PORTNAME, strPortName);
		dlgPrintStatus.ShowWindow(SW_SHOW);
		dlgPrintStatus.UpdateWindow();

		// start document printing process
		if (!printInfo.m_bDocObject)
		{
			printInfo.m_nJobNumber = dcPrint.StartDoc(&docInfo);
			if (printInfo.m_nJobNumber == SP_ERROR)
			{
				// enable main window before proceeding
				hwndTemp->EnableWindow(TRUE);

				// cleanup and show error message
				OnEndPrinting(&dcPrint, &printInfo);
				dlgPrintStatus.DestroyWindow();
				dcPrint.Detach();   // will be cleaned up by CPrintInfo destructor
				AfxMessageBox(AFX_IDP_FAILED_TO_START_PRINT);
				return;
			}
		}

		// Guarantee values are in the valid range
		UINT nEndPage = printInfo.GetToPage();
		UINT nStartPage = printInfo.GetFromPage();

		if (nEndPage < printInfo.GetMinPage())
			nEndPage = printInfo.GetMinPage();
		if (nEndPage > printInfo.GetMaxPage())
			nEndPage = printInfo.GetMaxPage();

		if (nStartPage < printInfo.GetMinPage())
			nStartPage = printInfo.GetMinPage();
		if (nStartPage > printInfo.GetMaxPage())
			nStartPage = printInfo.GetMaxPage();

		int nStep = (nEndPage >= nStartPage) ? 1 : -1;
		nEndPage = (nEndPage == 0xffff) ? 0xffff : nEndPage + nStep;

		VERIFY(strTemp.LoadString(AFX_IDS_PRINTPAGENUM));

		// If it's a doc object, we don't loop page-by-page
		// because doc objects don't support that kind of levity.

		BOOL bError = FALSE;
		if (printInfo.m_bDocObject)
		{
			OnPrepareDC(&dcPrint, &printInfo);
			OnPrint(&dcPrint, &printInfo);
		}
		else
		{
			// begin page printing loop
			for (printInfo.m_nCurPage = nStartPage;
				printInfo.m_nCurPage != nEndPage; printInfo.m_nCurPage += nStep)
			{
				OnPrepareDC(&dcPrint, &printInfo);

				// check for end of print
				if (!printInfo.m_bContinuePrinting)
					break;

				// write current page
				TCHAR szBuf[80];
				ATL_CRT_ERRORCHECK_SPRINTF(_sntprintf_s(szBuf, _countof(szBuf), _countof(szBuf) - 1, strTemp, printInfo.m_nCurPage));
				
				dlgPrintStatus.SetDlgItemText(AFX_IDC_PRINT_PAGENUM, szBuf);

				// set up drawing rect to entire page (in logical coordinates)
				printInfo.m_rectDraw.SetRect(0, 0,
					dcPrint.GetDeviceCaps(HORZRES),
					dcPrint.GetDeviceCaps(VERTRES));
				dcPrint.DPtoLP(&printInfo.m_rectDraw);

				// attempt to start the current page
				if (dcPrint.StartPage() < 0)
				{
					bError = TRUE;
					break;
				}

				// must call OnPrepareDC on newer versions of Windows because
				// StartPage now resets the device attributes.
				OnPrepareDC(&dcPrint, &printInfo);

				ASSERT(printInfo.m_bContinuePrinting);

				// page successfully started, so now render the page
				OnPrint(&dcPrint, &printInfo);
				if ((nStep > 0) && // pages are printed in ascending order
					(nEndPage > printInfo.GetMaxPage() + nStep)) // out off pages
				{
					// OnPrint may have set the last page
					// because the end of the document was reached.
					// The loop must not continue with the next iteration.
					nEndPage = printInfo.GetMaxPage() + nStep; 
				}

				// If the user restarts the job when it's spooling, all 
				// subsequent calls to EndPage returns < 0. The first time
				// GetLastError returns ERROR_PRINT_CANCELLED
				if (dcPrint.EndPage() < 0 && (GetLastError()!= ERROR_SUCCESS))
				{
					HANDLE hPrinter;
					if (!OpenPrinter(LPTSTR(printInfo.m_pPD->GetDeviceName().GetBuffer()), &hPrinter, NULL))
					{
						bError = TRUE;
						break;
					}

					DWORD cBytesNeeded;
					if(!GetJob(hPrinter,printInfo.m_nJobNumber,1,NULL,0,&cBytesNeeded))
					{
						if (GetLastError() != ERROR_INSUFFICIENT_BUFFER)
						{
						   bError = TRUE;
						   break;
						}
					}

					JOB_INFO_1 *pJobInfo; 					
					if((pJobInfo = (JOB_INFO_1 *)malloc(cBytesNeeded))== NULL)
					{
						bError = TRUE;
						break;
					}

					DWORD cBytesUsed;

					BOOL bRet = GetJob(hPrinter,printInfo.m_nJobNumber,1,LPBYTE(pJobInfo),cBytesNeeded,&cBytesUsed);

					DWORD dwJobStatus = pJobInfo->Status;

					free(pJobInfo);
					pJobInfo = NULL;

					// if job status is restart, just continue
					if(!bRet || !(dwJobStatus & JOB_STATUS_RESTART) )
					{
						bError = TRUE;
						break;
					}
				}

				if(!PdfWinAbortProc(dcPrint.m_hDC, 0))
				{		
					bError = TRUE;
					break;
				}
			}
		}

		// cleanup document printing process
		if (!printInfo.m_bDocObject)
		{
			if (!bError)
				dcPrint.EndDoc();
			else
				dcPrint.AbortDoc();
		}

		hwndTemp->EnableWindow();    // enable main window

		OnEndPrinting(&dcPrint, &printInfo);    // clean up after printing
		dlgPrintStatus.DestroyWindow();

		dcPrint.Detach();   // will be cleaned up by CPrintInfo destructor
	}
}
