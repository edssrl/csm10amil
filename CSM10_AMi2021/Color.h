//--------------------------------------------------------------
//
//			 Color
//
//
//--------------------------------------------------------------

#ifndef FONTCOLOR
#define FONTCOLOR

#define BLACK RGB(0,0,0)
#define GREEN RGB(0,255,0)
#define RED	  RGB(255,0,0)
#define WHITE RGB(255,255,255)
#define GREY  RGB(191,191,191)

#endif
