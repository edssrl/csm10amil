#if !defined(AFX_LABEL_H__0B173E71_14B3_11D2_A707_00C026A019B7__INCLUDED_)
#define AFX_LABEL_H__0B173E71_14B3_11D2_A707_00C026A019B7__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// Label.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CLabel window
enum TIPOICON	{NOICON,IPALLINO,IQUADRATO};

class CLabel : public CStatic
{
// Construction
public:
	CLabel();
	BOOL Create(CWnd* pParentWnd,UINT id);

// Attributes
public:
CString c_sValue;	// testo visualizzato
CString c_sBase;	// testo di riferimento per calcolo finestra
// CRect c_rect;
CPoint c_point;
UINT c_id;
FontInfo c_fontInfo;	
COLORREF c_tColor;
COLORREF c_bColor;
BOOL c_doNotify;		// notify parent wnd
BOOL c_doSensDbclk;		// Sense double click
BOOL c_do3Dlook;		// 3D look 
BOOL c_visible;			// visibile
TIPOICON  c_tipoIcon;		// Inserisce  disegno prima drl testo
CLabel &operator=(CString &s)
{
if (GetSafeHwnd() != NULL)
	updateLabel(s);
else
	setString(s);
return(*this);
};

CLabel &operator=(int v)
{
CString s;
s.Format(_T("%04d"),v);
if (GetSafeHwnd() != NULL)
	updateLabel(s);
else
	setString(s);
return(*this);
};


CLabel &operator=(LPCTSTR s)
{
if (GetSafeHwnd() != NULL)
	updateLabel(CString(s));
else
	setString(CString(s));
return(*this);
};

// Gestione UpdateView selettivi
CRect eraseRect;	
// Operations
public:
	void setString(LPCTSTR s){c_sValue=s;};
	void setStringBase(LPCTSTR s){c_sBase=s;};

	void setFontInfo (FontInfo font){c_fontInfo = font;}; 
	CRect calcRect(CDC *ePdc=NULL);
	CRect calcRect(CString &s,CDC *ePdc=NULL);
	void updateLabel (CString &str);
	void updateLabel (CDC *ePdc=NULL);
	void setTColor (COLORREF c){c_tColor = c;};
	void setTipoIcon (TIPOICON c){c_tipoIcon = c;};
	void setBColor (COLORREF c){c_bColor = c;};
	void setNotify (BOOL v){c_doNotify = v;};
	void setSensDbclk (BOOL v){c_doSensDbclk = v;};
	void set3Dlook (BOOL v){c_do3Dlook = v;};
	void setPos (CPoint p,CDC *ePdc=NULL);
	void setVisible (BOOL v){c_visible = v;};
	void draw (CDC *pdc);

	CString& getString(void ){return(c_sValue);};
	CSize getSize(void ){if(c_sValue == "SCONOSCIUTO")return(CSize(0,0));
		return(calcRect().Size());};
	CSize getSize(CDC *pdc ){if(c_sValue == "SCONOSCIUTO")return(CSize(0,0));
		return(calcRect(pdc).Size());};

			// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CLabel)
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CLabel();
	void Draw3DRect (CDC* pdc,const CRect& rectB);

	// Generated message map functions
protected:
	//{{AFX_MSG(CLabel)
	afx_msg void OnLButtonDblClk(UINT nFlags, CPoint point);
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	afx_msg void OnPaint();
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_LABEL_H__0B173E71_14B3_11D2_A707_00C026A019B7__INCLUDED_)

