// FInputOutput.cpp : implementation file
//

#include "stdafx.h"
#include "MainFrm.h"
#include "resource.h"
#include "GraphDoc.h"


#include <AfxTempl.h>


#include "GraphView.h"
#include "dyntempl.h"

#include "CSM10_AR.h"
#include "FInputOutput.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CFInputOutput

IMPLEMENT_DYNCREATE(CFInputOutput, CFormView)

CFInputOutput::CFInputOutput()
	: CFormView(CFInputOutput::IDD)
{
	//{{AFX_DATA_INIT(CFInputOutput)
	m_out0 = FALSE;
	m_out1 = FALSE;
	m_out2 = FALSE;
	m_out3 = FALSE;
	m_out4 = FALSE;
	m_out5 = FALSE;
	m_out6 = FALSE;
	m_out7 = FALSE;
	m_in0 = FALSE;
	m_in1 = FALSE;
	m_in2 = FALSE;
	m_in3 = FALSE;
	m_posDiafDx = 0;
	m_posDiafSx = 0;
	m_speedEncoder = 0;
	//}}AFX_DATA_INIT
}

CFInputOutput::~CFInputOutput()
{
}

void CFInputOutput::DoDataExchange(CDataExchange* pDX)
{
	CFormView::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CFInputOutput)
	DDX_Control(pDX, IDC_START_RIGHT_DIAPH, m_startRightButton);
	DDX_Control(pDX, IDC_START_LEFT_DIAPH, m_startLeftButton);
	DDX_Control(pDX, IDC_LABEL_LR_TEST, m_ctrlLabelLRTest);
	DDX_Control(pDX, IDC_POSIZ_DIAPH_SX, m_ctrlPosDiaphSx);
	DDX_Control(pDX, IDC_POSIZ_DIAPH_DX, m_ctrlPosDiaphDx);
	DDX_Control(pDX, IDC_SPEED_ENCODER, m_ctrlSpeedEncoder);
	DDX_Control(pDX, IDC_LABEL_SPEED_ENCODER, m_ctrlLabelSpeedEncoder);
	DDX_Control(pDX, IDC_LABEL_POSIZ_DIAPH_DX, m_ctrlLabelPosDiaphDx);
	DDX_Control(pDX, IDC_LABEL_POSIZ_DIAPH_SX, m_ctrlLabelPosDiaphSx);
	DDX_Control(pDX, IDC_OUT7, m_ctrlOut7);
	DDX_Control(pDX, IDC_OUT6, m_ctrlOut6);
	DDX_Control(pDX, IDC_OUT5, m_ctrlOut5);
	DDX_Control(pDX, IDC_OUT4, m_ctrlOut4);
	DDX_Control(pDX, IDC_OUT3, m_ctrlOut3);
	DDX_Control(pDX, IDC_OUT2, m_ctrlOut2);
	DDX_Control(pDX, IDC_OUT1, m_ctrlOut1);
	DDX_Control(pDX, IDC_OUT0, m_ctrlOut0);
	DDX_Control(pDX, IDC_IN3, m_ctrlIn3);
	DDX_Control(pDX, IDC_IN2, m_ctrlIn2);
	DDX_Control(pDX, IDC_IN1, m_ctrlIn1);
	DDX_Control(pDX, IDC_IN0, m_ctrlIn0);

	DDX_Check(pDX, IDC_OUT0, m_out0);
	DDX_Check(pDX, IDC_OUT1, m_out1);
	DDX_Check(pDX, IDC_OUT2, m_out2);
	DDX_Check(pDX, IDC_OUT3, m_out3);
	DDX_Check(pDX, IDC_OUT4, m_out4);
	DDX_Check(pDX, IDC_OUT5, m_out5);
	DDX_Check(pDX, IDC_OUT6, m_out6);
	DDX_Check(pDX, IDC_OUT7, m_out7);
	DDX_Check(pDX, IDC_IN0, m_in0);
	DDX_Check(pDX, IDC_IN1, m_in1);
	DDX_Check(pDX, IDC_IN2, m_in2);
	DDX_Check(pDX, IDC_IN3, m_in3);
	DDX_Text(pDX, IDC_POSIZ_DIAPH_DX, m_posDiafDx);
	DDX_Text(pDX, IDC_POSIZ_DIAPH_SX, m_posDiafSx);
	DDX_Text(pDX, IDC_SPEED_ENCODER, m_speedEncoder);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CFInputOutput, CFormView)
	//{{AFX_MSG_MAP(CFInputOutput)
	ON_BN_CLICKED(IDC_UPDATE, OnUpdate)
	ON_WM_DESTROY()
	ON_WM_TIMER()
	ON_BN_CLICKED(IDC_START_LEFT_DIAPH, OnStartLeftDiaph)
	ON_BN_CLICKED(IDC_START_RIGHT_DIAPH, OnStartRightDiaph)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CFInputOutput diagnostics

#ifdef _DEBUG
void CFInputOutput::AssertValid() const
{
	CFormView::AssertValid();
}

void CFInputOutput::Dump(CDumpContext& dc) const
{
	CFormView::Dump(dc);
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CFInputOutput message handlers

void CFInputOutput::OnUpdate() 
{
UpdateData(TRUE);
	
// TODO: Add your control notification handler code here
CLineDoc* pDoc = (CLineDoc*)GetDocument();

unsigned short v;

v = ((m_out7 & 0x01)<<7) | ((m_out6 & 0x01)<<6) | ((m_out5 & 0x01)<<5) | 
	((m_out4 & 0x01)<<4) | ((m_out3 & 0x01)<<3) | ((m_out2 & 0x01)<<2) | 
	((m_out1 & 0x01)<<1) | (m_out0 & 0x01);  

pDoc->sendValOutput(v); 

}

void CFInputOutput::OnInitialUpdate() 
{
CFormView::OnInitialUpdate();
CProfile profile;	
// TODO: Add your specialized code here and/or call the base class
// send switch mode to hde

CLineDoc* pDoc = (CLineDoc*)GetDocument();
CString s;
//  Ingressi
s = profile.getProfileString(_T("FormIO"),_T("In0"),_T("Enable"));
if (s == "")
	{
	m_ctrlIn0.ShowWindow(SW_HIDE);
	}
m_ctrlIn0.SetWindowText((LPCTSTR)s);
// 
//  Ingressi
s = profile.getProfileString(_T("FormIO"),_T("In1"),_T("Autotest"));
if (s == "")
	{
	m_ctrlIn1.ShowWindow(SW_HIDE);
	}
m_ctrlIn1.SetWindowText((LPCTSTR)s);
// 
s = profile.getProfileString(_T("FormIO"),_T("In2"),_T("Change"));
if (s == "")
	{
	m_ctrlIn2.ShowWindow(SW_HIDE);
	}
m_ctrlIn2.SetWindowText((LPCTSTR)s);
// 
s = profile.getProfileString(_T("FormIO"),_T("In3"),_T("Pause"));
if (s == "")
	{
	m_ctrlIn3.ShowWindow(SW_HIDE);
	}
m_ctrlIn3.SetWindowText((LPCTSTR)s);

//  Uscite
s = profile.getProfileString(_T("FormIO"),_T("Out0"),_T("Hole Alarm"));
if (s == "")
	{
	m_ctrlOut0.ShowWindow(SW_HIDE);
	}
m_ctrlOut0.SetWindowText((LPCTSTR)s);
// 
s = profile.getProfileString(_T("FormIO"),_T("Out1"),_T("System Alarm"));
if (s == "")
	{
	m_ctrlOut1.ShowWindow(SW_HIDE);
	}
m_ctrlOut1.SetWindowText((LPCTSTR)s);
// 
s = profile.getProfileString(_T("FormIO"),_T("Out2"),_T("Laser"));
if (s == "")
	{
	m_ctrlOut2.ShowWindow(SW_HIDE);
	}
m_ctrlOut2.SetWindowText((LPCTSTR)s);
// 
s = profile.getProfileString(_T("FormIO"),_T("Out3"),_T("Inspection Led"));
if (s == "")
	{
	m_ctrlOut3.ShowWindow(SW_HIDE);
	}
m_ctrlOut3.SetWindowText((LPCTSTR)s);
// 
s = profile.getProfileString(_T("FormIO"),_T("Out4"),_T("Out 4"));
if (s == "")
	{
	m_ctrlOut4.ShowWindow(SW_HIDE);
	}
m_ctrlOut4.SetWindowText((LPCTSTR)s);
// 
s = profile.getProfileString(_T("FormIO"),_T("Out5"),_T("Out 5"));
if (s == "")
	{
	m_ctrlOut5.ShowWindow(SW_HIDE);
	}
m_ctrlOut5.SetWindowText((LPCTSTR)s);
// 
s = profile.getProfileString(_T("FormIO"),_T("Out6"),_T("Out 6"));
if (s == "")
	{
	m_ctrlOut6.ShowWindow(SW_HIDE);
	}
m_ctrlOut6.SetWindowText((LPCTSTR)s);
// 
s = profile.getProfileString(_T("FormIO"),_T("Out7"),_T("Air Cleaning"));
if (s == "")
	{
	m_ctrlOut7.ShowWindow(SW_HIDE);
	}
m_ctrlOut7.SetWindowText((LPCTSTR)s);
// 

// Diaframmi
s = profile.getProfileString(_T("FormIO"),_T("PosDiafSx"),_T("Diaphragm Left Position"));
if (s == "")
	{
	m_ctrlLabelPosDiaphSx.ShowWindow(SW_HIDE);
	m_ctrlPosDiaphSx.ShowWindow(SW_HIDE);
	}
m_ctrlLabelPosDiaphSx.SetWindowText((LPCTSTR)s);
// 
s = profile.getProfileString(_T("FormIO"),_T("PosDiafDx"),_T("Diaphragm Right Position"));
if (s == "")
	{
	m_ctrlLabelPosDiaphDx.ShowWindow(SW_HIDE);
	m_ctrlPosDiaphDx.ShowWindow(SW_HIDE);
	}
m_ctrlLabelPosDiaphDx.SetWindowText((LPCTSTR)s);
 
// SpeedEncoder 
s = profile.getProfileString(_T("FormIO"),_T("SpeedEncoder"),_T("Encoder Speed"));
if (s == _T(""))
	{
	m_ctrlLabelSpeedEncoder.ShowWindow(SW_HIDE);
	m_ctrlSpeedEncoder.ShowWindow(SW_HIDE);
	}
m_ctrlLabelSpeedEncoder.SetWindowText((LPCTSTR)s);
// 
// SpeedEncoder 
s = profile.getProfileString(_T("FormIO"),_T("LeftRightTest"),_T("Diaphragm Test"));
if (s == _T(""))
	{
	m_ctrlLabelLRTest.ShowWindow(SW_HIDE);
	m_startLeftButton.ShowWindow(SW_HIDE);
	m_startRightButton.ShowWindow(SW_HIDE);
	}
m_ctrlLabelLRTest.SetWindowText((LPCTSTR)s);
// 

// Start Thread


pDoc->c_thEncoder.setPulseMetro(pDoc->c_fattCorrDimLin*1000);
pDoc->c_thEncoder.setPDoc(pDoc);
pDoc->c_thEncoder.setPWnd(AfxGetMainWnd());
//pDoc->c_thEncoder.launch(FALSE);
//--------------------------------

if (pDoc->c_statoHde != HDE_WAITSTART)
	{
	CString str;
	str.LoadString(CSM_FIO_NO_WAIT_START);
	// AfxMessageBox (" Stato sistema HDE diverso da WAIT_START");
	AfxMessageBox (str);
	}
else
	{
	// send start IO mode cmd
	pDoc->sendIOStartStop (TRUE);
	}

// timer 1 sec di aggiornamento stato ingressi
if ((c_timerId=SetTimer(rand(),1000,NULL))== 0)
	{
	AfxGetMainWnd()->MessageBox (_T("Cannot Create AlarmTimer"),_T("SetTimer"));
	}


}

void CFInputOutput::shutdown(void)
{

CLineDoc* pDoc = (CLineDoc*)GetDocument();

// send start IO mode cmd
pDoc->sendIOStartStop (FALSE);

// destroy timer	
KillTimer(c_timerId);

// stop thread encoder
pDoc->c_thEncoder.waitTerminate();

	
}

void CFInputOutput::destroyCtrl(void)
{
	m_startRightButton.DestroyWindow();
		m_startLeftButton.DestroyWindow();
		m_ctrlLabelLRTest.DestroyWindow();
		m_ctrlPosDiaphSx.DestroyWindow();
		m_ctrlPosDiaphDx.DestroyWindow();
		m_ctrlSpeedEncoder.DestroyWindow();
		m_ctrlLabelSpeedEncoder.DestroyWindow();
		m_ctrlLabelPosDiaphDx.DestroyWindow();
		m_ctrlLabelPosDiaphSx.DestroyWindow();
		m_ctrlOut7.DestroyWindow();
		m_ctrlOut6.DestroyWindow();
		m_ctrlOut5.DestroyWindow();
		m_ctrlOut4.DestroyWindow();
		m_ctrlOut3.DestroyWindow();
		m_ctrlOut2.DestroyWindow();
		m_ctrlOut1.DestroyWindow();
		m_ctrlOut0.DestroyWindow();
		m_ctrlIn3.DestroyWindow();
		m_ctrlIn2.DestroyWindow();
		m_ctrlIn1.DestroyWindow();
		m_ctrlIn0.DestroyWindow();
}

void CFInputOutput::OnDestroy() 
{

shutdown();

destroyCtrl();

// wait ctrl.destroyCtrl
Sleep(10);

CFormView::OnDestroy();
// TODO: Add your message handler code here
	
}

void CFInputOutput::OnTimer(UINT nIDEvent) 
{
// TODO: Add your message handler code here and/or call default
CFormView::OnTimer(nIDEvent);
CLineDoc* pDoc = (CLineDoc*)GetDocument();

UpdateData(TRUE);

m_in0 = pDoc->c_valInput & 0x01;
m_in1 = (pDoc->c_valInput & 0x02)>>1;
m_in2 = (pDoc->c_valInput & 0x04)>>2;
m_in3 = (pDoc->c_valInput & 0x08)>>3;

CMainFrame *frame = (CMainFrame *)AfxGetMainWnd();


m_speedEncoder = (int)frame->getValLineSpeed();

m_posDiafDx = pDoc->c_rawPosDiaframmaDx;
m_posDiafSx = pDoc->c_rawPosDiaframmaSx;

UpdateData(FALSE);

}

void CFInputOutput::OnStartLeftDiaph() 
{
// TODO: Add your control notification handler code here

CLineDoc* pDoc = (CLineDoc*)GetDocument();
// true = left
pDoc->sendStartTestDiaf(TRUE);	

}

void CFInputOutput::OnStartRightDiaph() 
{
// TODO: Add your control notification handler code here

CLineDoc* pDoc = (CLineDoc*)GetDocument();
// true = left
pDoc->sendStartTestDiaf(FALSE);	

}
