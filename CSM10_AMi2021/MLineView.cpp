// Multi Line View : implementation file
//

#include "stdafx.h"
#include "MainFrm.h"
#include "resource.h"
#include "Profile.h"

#include <AfxTempl.h>

#include "GraphDoc.h"
#include "GraphView.h"
#include "dyntempl.h"
#include "CSM10_AR.h"

#include "MLineView.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CLineView

IMPLEMENT_DYNCREATE(CMLineView, CView)

CMLineView::CMLineView()
{
vType = MULTILINEVIEW;
layout.setViewType(MULTILINEVIEW);

// Init layout Fix Attributes
CProfile profile;
CString s;

layout.fCaption.size =  profile.getProfileInt(_T("MLineLayout"),_T("CaptionSize"),120);
layout.fLabel.size = profile.getProfileInt(_T("MLineLayout"),_T("LabelSize"),100);
layout.fNormal.size = profile.getProfileInt(_T("MLineLayout"),_T("NormalSize"),110);

s = profile.getProfileString(_T("MLineLayout"),_T("NewLine"),_T("3,6"));
while (s.GetLength() >0)
	{
	CString subS;
	subS = s.SpanExcluding(_T(",;"));
	s = s.Right(s.GetLength()-subS.GetLength()-1);
	int v;
	_stscanf((LPCTSTR)subS,_T("%d"),&v);
	if ((v < MAX_NUMLABEL)&&(v>=0))
		layout.c_nlAfterLabel [v] = 1;
	}

s = profile.getProfileString(_T("MLineLayout"),_T("LabelOrder"),_T("0,1,2,3,4,5,6,7,8,9"));
int k = 0;
while (s.GetLength() >0)
	{
	CString subS;
	subS = s.SpanExcluding(_T(",;"));
	s = s.Right(s.GetLength()-subS.GetLength()-1);
	int v;
	_stscanf((LPCTSTR)subS,_T("%d"),&v);
	if ((v < MAX_NUMLABEL)&&(v>=0))
		layout.c_vOrderLabel [k++] = v;
	if (k>=MAX_NUMLABEL)
		break;
	}

s = profile.getProfileString(_T("MLineLayout"),_T("PixelSepLabel"),
							 _T("10,10,10,10,10,10,10,10,10,10"));
k = 0;
while (s.GetLength() >0)
	{
	CString subS;
	subS = s.SpanExcluding(_T(",;"));
	s = s.Right(s.GetLength()-subS.GetLength()-1);
	int v;
	_stscanf((LPCTSTR)subS,_T("%d"),&v);
	layout.c_pxSepLabel [k++] = v;
	if (k>9)
		break;
	}

s = profile.getProfileString(_T("MLineLayout"),_T("SCaption"),
							 _T("TREND"));
layout.setCaption(s);
for (int i=0;i<MAX_NUMLABEL;i++)
	{
	CString sIndex;
	sIndex.Format (_T("SLabel%d"),i);
	s = profile.getProfileString(_T("MLineLayout"),sIndex,sIndex);
	if (s != sIndex)
		{
		layout.c_sLabel[i] = s;
		}
	}

for (int i=0;(i<4);i++)
	{
	CString s;
	s = TCHAR('A'+i);
	layout.c_cLabel[i] = s;
	layout.c_cLabel[i].setTColor(CSM20GetClassColor(i));
	layout.c_cLabel[i].setTipoIcon(IPALLINO);
	}

// CalcDrawRect
layout.c_viewTopPerc = profile.getProfileInt(_T("MLineLayout"),_T("ViewTop%"),10);
layout.c_viewBottomPerc = profile.getProfileInt(_T("MLineLayout"),_T("ViewBottom%"),25);
layout.c_viewLeftPerc = profile.getProfileInt(_T("MLineLayout"),_T("ViewLeft%"),8);
layout.c_viewRightPerc = profile.getProfileInt(_T("MLineLayout"),_T("ViewRight%"),5);


layout.setMode (LINEAR | NOLABELH | MSCALEV);
}

CMLineView::~CMLineView()
{
}


BEGIN_MESSAGE_MAP(CMLineView, CView)
	//{{AFX_MSG_MAP(CMLineView)
	ON_WM_ERASEBKGND()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CMLineView drawing


// area disegno interna
CRect CMLineView::getDrawRect(void)
{
CRect rcBounds,rect;
GetClientRect (rcBounds);
layout.CalcDrawRect(rcBounds,rect);
return (rect);
}




/////////////////////////////////////////////////////////////////////////////
// CMLineView diagnostics

#ifdef _DEBUG
void CMLineView::AssertValid() const
{
	CView::AssertValid();
}

void CMLineView::Dump(CDumpContext& dc) const
{
	CView::Dump(dc);
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CMLineView message handlers

BOOL CMLineView::OnEraseBkgnd(CDC* pDC) 
{

// TODO: Add your message handler code here and/or call default
CRect r;
GetClientRect(&r);
layout.OnEraseBkgnd(pDC,r);

return TRUE;	
}

BOOL CMLineView::Create(LPCTSTR lpszClassName, LPCTSTR lpszWindowName, DWORD dwStyle, const RECT& rect, CWnd* pParentWnd, UINT nID, CCreateContext* pContext) 
{

// TODO: Add your specialized code here and/or call the base class
if (!CView::Create(lpszClassName, lpszWindowName, 
	dwStyle, rect, pParentWnd, nID, pContext))
	return (FALSE);

DOC_CLASS* pDoc = (DOC_CLASS* )GetDocument();

layout.Create(this);

for (int j=0;j<pDoc->c_difCoil.getNumClassi();j++)
	{
	trendLine[j].setPen(CSM20GetClassColor(j));
	}
return TRUE;
}



BOOL CMLineView::OnNotify(WPARAM wParam, LPARAM lParam, LRESULT* pResult) 
{
	// TODO: Add your specialized code here and/or call the base class

	CString s;
	s.Format(_T("Got Notify Message from %d"),(int)wParam);
	AfxMessageBox (s);
	return CView::OnNotify(wParam, lParam, pResult);
}

void CMLineView::OnDraw(CDC* pdc)
{
DOC_CLASS* pDoc = (DOC_CLASS* )GetDocument();

double maxValY = pDoc->GetTrendLScalaY();
double maxValX = pDoc->GetTrendLScalaX();

// numero Passi divisione Disegno
int numStepX = (int)(maxValX / pDoc->GetTrendStep());	
if (numStepX <= 0)
	numStepX = 1;
ASSERT(numStepX > 0);


CRect rectB;
CRect rcBounds;
GetClientRect (rcBounds);
// Set base Rect
layout.setDrawRect(rcBounds);
// Call Base Class Draw
layout.setMaxY(pDoc->GetTrendLScalaY());
layout.setMinY(0.);
layout.c_vLabel[0] = pDoc->GetVPosition();
layout.c_vLabel [1] = pDoc->GetTrendLVPositionDal();
layout.c_vLabel [2] = pDoc->GetVCliente();
layout.c_vLabel [3] = (int)maxValX;		// maxValX 
layout.c_vLabel [4] = pDoc->getRotolo();
layout.c_vLabel [5] = pDoc->lega;
CTime d(pDoc->startTime);
layout.c_vLabel [6] = d.Format( _T("%A, %B %d, %Y") );
// visualizzazione threshold
layout.c_vLabel [7] = pDoc->formatStringThreshold();

// contatori a,b c,d,
CString s;
for(int i=0;i<pDoc->c_difCoil.getNumClassi();i++)
	{
	if (i>0)
		s += ",";
	CString s1;
	s1.Format(_T(" %c=%2.0lf"),'A'+i,pDoc->c_difCoil.getTotDifetti('A'+i,0,(int)pDoc->GetVPosition()));
	s += s1;
	}
layout.c_vLabel [8] = s;
//------------------------
// Densita`
for(int i=0;i<pDoc->c_difCoil.getNumClassi();i++)
	{
	CString s1;
	// densita`
	double densita=pDoc->c_difCoil.getDensityLevel('A'+i,pDoc->c_densityCalcLength);
	s1.Format(_T(" %c=%3.0lf"),'A'+i,densita);
	layout.c_vLabel [9+i] = s1;
		if (densita > pDoc->c_difCoil.getAllarme ('A'+i))
		{
		layout.c_vLabel [9+i].setTColor(RGB(255,0,0));
		}
	else
		{
		layout.c_vLabel [9+i].setTColor(RGB(0,0,0));
		}

	}
//------------------------
// lunghezza di base per calcolo densita`
s.Format(_T("%4.0lf"),pDoc->c_densityCalcLength);
layout.c_vLabel [13] = s;

int numClassi = pDoc->c_difCoil.getNumClassi();

// cluster  label 14,15,16,17
for (int i=0;i<numClassi;i++)
	{	
	s.Format(_T("%d"),pDoc->c_clusterCounter[i]);
	layout.c_vLabel [14+i] = s;
	}

layout.setNumClassi(numClassi);

layout.draw(pdc);

// Trovo Finestra disegno (grigia)
layout.CalcDrawRect (rcBounds,rectB);

int code = pDoc->GetForiCod();

LineInfo lineInfo[4];
for (int i=0;i<numClassi;i++)
	{
	lineInfo[i].max = pDoc->GetTrendLScalaY();
	lineInfo[i].min = 0.;
	lineInfo[i].limit = pDoc->c_difCoil.c_sogliaAllarme[i];
	//lineInfo[i].count = (pDoc->c_difCoil.getSize(3,code) < numStepX)?
	//					pDoc->c_difCoil.getSize(3,code) : numStepX; 
	lineInfo[i].count = (int)((((int)pDoc->c_difCoil.getMeter()/(int)pDoc->GetTrendStep()) < numStepX)?
						(pDoc->c_difCoil.getMeter()/(int)pDoc->GetTrendStep()) : numStepX); 
	lineInfo[i].numStep = numStepX;
	lineInfo[i].actual	= (int) (pDoc->c_difCoil.getMeter()/(int)pDoc->GetTrendStep())% numStepX;
	// Aggiornato 1997 alloca memoria
	lineInfo[i].newVal (lineInfo[i].numStep+1);
	for (int j=0;j<= lineInfo[i].numStep;j++)
		lineInfo[i].val[j] = pDoc->c_difCoil.getValNumLStep('A'+i,j*(int)pDoc->GetTrendStep());
	}


CRect subRectB[4];
for (int i=0;i<numClassi;i++)
	{
	trendLine[i].setMode (LINEAR);
	subRectB[i]=rectB;
	subRectB[i].TopLeft().y += (rectB.Size().cy/numClassi)*i;
	subRectB[i].BottomRight().y -= (rectB.Size().cy/numClassi)*(numClassi-(i+1));
	trendLine[i].setScale(subRectB[i],lineInfo[i]);
	trendLine[i].draw(pdc);
	}
}



void CMLineView::OnInitialUpdate() 
{
	CView::OnInitialUpdate();
	
	// TODO: Add your specialized code here and/or call the base class
Invalidate(TRUE);	
}

void CMLineView::OnUpdate(CView* pSender, LPARAM lHint, CObject* pHint) 
{
	// TODO: Add your specialized code here and/or call the base class

if ((lHint==0) && (pHint == NULL))
	CView::OnUpdate(pSender,lHint,pHint);	
else
	{
	DOC_CLASS* pDoc = (DOC_CLASS* )GetDocument();
	static CRect rcInvalid;
	CRect rcBounds,rectB;
	int oldStepX,actualStepX;

	int maxValX = pDoc->GetTrendLScalaX();
	int numStepX = (int)((double)maxValX / pDoc->GetTrendStep());	// numero Passi divisione Disegno
	if (numStepX <= 0)
		numStepX = 1;
	ASSERT(numStepX > 0);
	actualStepX = ((int)pDoc->c_difCoil.getMeter()/(int)pDoc->GetTrendStep()) % (numStepX) ; // Uno qualunque va bene

	oldStepX = actualStepX - 1;

	// Spazio Di Disegno
	// Trovo Finestra disegno (grigia)
	GetClientRect (rcBounds);
	layout.CalcDrawRect (rcBounds,rectB);
	// estremi del quadro
	if ((actualStepX <= 1)||(actualStepX == numStepX))
		{
		rcInvalid = rectB;
		rcInvalid.left -= 1;
		rcInvalid.right += 1;
		}
	else
		{
		double rxr,rxl;
		rxl = rectB.left;
		rxr = rectB.right;

		rcInvalid.left = (int ) (rxl +((rxr - rxl)/numStepX *oldStepX))-10;
		rcInvalid.right = (int )(rxl +((rxr - rxl)/numStepX *actualStepX))+10;

		rcInvalid.top	= rectB.top;
		rcInvalid.bottom = rectB.bottom;
		}
	InvalidateRect(rcInvalid);		
	}
}
