// Csm10Comm.h: interface for the Csm10Comm class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_CSM10PCOMM_H__35826BEB_7213_4905_AAB5_441B6193B25F__INCLUDED_)
#define AFX_CSM10PCOMM_H__35826BEB_7213_4905_AAB5_441B6193B25F__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include <memory.h>
#include "../../../../../Library/ser432/SER432.h"
#include "../../../../../Library/ser432/ser432err.h"
#include "System.h"

struct ParField
	{
	int	size;					// lunghezza della stringa attesa
	int id	;					// id event da chiamare
	};								

struct CmdHeader
	{
	char*  idCmdStr;					// stringa del comando	
	struct ParField	parField;			// lunghezza e pter a funzione di ogni parametro
	};

struct Command
{
int cc;				// cc => comando come descritto nel doc Protocollo CSM10_p
int nn;				// nn => valore successivo come descritto nel doc Protocollo CSM10_p
};

class Csm10Comm 
{
protected:
int c_serPort;
int c_serSpeed;
int c_serPortOk;

UINT wmSerCharAvail;

BOOL	c_serMsgReceived;

//-----------------------------------------
// Parser internal data
DWORD		c_mParseTimer;	// timer per timeout
DWORD		c_rxSerTimer;	// timer per allarme seriale interrotta	

BOOL		c_rxAlarmActive;
CStringA		c_strCmd;			// stringa in costruzione
int lenStr;					// lunghezza attesa del comando in costruzione
struct CmdHeader* pCmd;		// pointer comando in costruzione NULL nessun comando
// int lockParser;			// semaforo di blocco del parser, attivato in seguito ad ID non riconosciuto
// Parser function
int		parser (int ch);
void	iniParser(void);
//
LRESULT OnRxSerChar(void);

virtual HWND getSafeHwnd(void ){return NULL;};

public:
	Csm10Comm();
	virtual ~Csm10Comm();
	virtual BOOL serverRequest(int cmd,int size,CStringA data){return FALSE;};
	bool openPort(LPCTSTR portName,long speed,long parity=0,bool slowNormal=false);
	bool reOpenPort(LPCTSTR portName,long speed,long parity=0,bool slowNormal=false);
	bool closePort(void);
	void setEventDest (void *wHandle,int cmd)
		{S432SetWnd(wHandle,cmd,c_serPort,1);};			// 1 ogni byte
	BOOL txIsEmpty(void){return(S432TxIsEmpty(c_serPort));};
	// bool isConnected(void){return(c_serPort.isConnected());};
	// Comandi da PC a testa 
	int sendGeneralCommand(Command *cmd );
	
	BOOL  isPortFree(LPCTSTR portName){return TRUE;};
	    
};

#endif // !defined(AFX_CSM10PCOMM_H__35826BEB_7213_4905_AAB5_441B6193B25F__INCLUDED_)
