#include "stdafx.h"
#include "ThEncoder.h"
#include "MainFrm.h"
#include "GraphDoc.h"


// porte output per allarme encoder
#define PIN_OUT0	2
#define	PIN_OUT1	3

CThEncoder::CThEncoder(void)
{
c_pDoc = NULL;
c_pWndClient = NULL;
c_card_num = 0;
c_pulseMetro = 1000;	// numero impulsi per metro 


if((c_card=Register_Card (PCI_9221, c_card_num))<0)
	{
    // printf("Register_Card error=%d", c_card);
	c_err = 1;
	c_card = -1;
    return;
    }

safeValue();
return;
}


CThEncoder::~CThEncoder(void)
{

if (c_card >= 0)
	c_err = Release_Card(c_card);

}

//----------------------------------------------------
// <<METHOD>>: CThEncoder::config
//----------------------------------------------------
BOOL CThEncoder::config(void)
{

c_err = 0;
c_func_sel = 0;
c_gptc_Ctr = 0;
c_gptc_Mode = 0;
c_gptc_SrcCtrl = 0;
c_gptc_PolCtrl = GPTC_GATE_LACTIVE | GPTC_UPDOWN_LACTIVE;
c_gptc_Reg1Val = c_pulseMetro; //sets counter initial value
c_gptc_Reg2Val = 0;
c_gptc_Status = 0;
c_gptc_CtrVal = 0;

c_gptc_Mode = SimpleGatedEventCNT;
c_gptc_SrcCtrl = GPTC_CLK_SRC_Ext|GPTC_GATE_SRC_Int|GPTC_UPDOWN_Int;

if (c_card >= 0)
	{
	GPTC_Clear(c_card, c_gptc_Ctr);
	GPTC_Setup(c_card, c_gptc_Ctr, c_gptc_Mode, c_gptc_SrcCtrl, c_gptc_PolCtrl, c_gptc_Reg1Val, c_gptc_Reg2Val);
	GPTC_Control(c_card, c_gptc_Ctr, IntUpDnCTR, 0);	//lets counter count down
	GPTC_Control(c_card, c_gptc_Ctr, IntGate, 1);		//lets sw gate high
	GPTC_Control(c_card, c_gptc_Ctr, IntENABLE, 1);		//enable counter

	safeValue();
	}

return TRUE;
}

//----------------------------------------------------
// <<METHOD>>: CThEncoder::safeValue
//----------------------------------------------------

int CThEncoder::safeValue(void)
//#UBLK-BEG-METHOD 1-59417
{
// output allarme encoder default
if (c_card >= 0)
	{
	DO_WriteLine (c_card, (U16) 0,(U16) PIN_OUT0, (U16) 0);
	DO_WriteLine (c_card, (U16) 0,(U16) PIN_OUT1, (U16) 1);
	}
return 1;
}


//----------------------------------------------------
// <<METHOD>>: CThEncoder::run
//----------------------------------------------------

int CThEncoder::run(void* pData)
//#UBLK-BEG-METHOD 1-59417
{
int retCode=0;

HANDLE hEvents[2]; 
hEvents[0] = closeHandle();
hEvents[1] = signalHandle();

if (c_pDoc == NULL)
	return -2;

if (c_pWndClient == NULL)
	return -4;

int numEvents = 0;
if (hEvents[0] > 0)
	numEvents ++;
if (hEvents[1] > 0)
	numEvents ++;

// 
config();

// 1)
// int deltaT = c_pDoc->c_glueIo.getSampleTime();
int fixDeltaT = 100;
int deltaT = fixDeltaT; // msec
int deltaEncoder;
int cycleTime = 0;
int lastEncoder = c_pulseMetro;
// contatore intervalli con encoder sotto soglia
// per allarmi
int cntDeltaEncoderLow = 0;
// contatore di cicli, mi serve i %10 per inviare wndMsg ogni sec
int cycleCount = 0;
clock_t	startCycle,endCycle;
// filtraggio velocita` 
double lastVel = 0.;
double accVel = 0.;

// CSingleLock singleLock(&c_pDoc->c_glueIo.c_critSect);

while (((CThEncoder*)pData)->c_runGoOn)
	{
	cycleCount ++;
	startCycle = clock();
	// close signaled ?
	DWORD dwEvent;
	if((dwEvent = WaitForMultipleObjects(numEvents,hEvents,FALSE,deltaT)) == WAIT_TIMEOUT)
		{
		// 28-05-13
		// inizializzato, altrimenti torna se non legge scheda valore 0, invariato = massima velocita`
		c_gptc_CtrVal = c_pulseMetro;
		if (c_card >= 0)
			GPTC_Read(c_card, c_gptc_Ctr, &c_gptc_CtrVal);
 		endCycle = clock();

		int vcnt = c_gptc_CtrVal;
		// contatore a decremento 
		deltaEncoder = lastEncoder - vcnt;
		if (deltaEncoder == 0)
			cntDeltaEncoderLow ++;
		else
			cntDeltaEncoderLow = 0;

		lastEncoder = vcnt;

		// in c_gptc_CtrVal abbiamo valore del contatore
		// gptc_CtrVal
		if (vcnt <= 0)
			{// ok fine conteggio
			if (c_card >= 0)
				{
				GPTC_Control(c_card, c_gptc_Ctr, IntENABLE, 0);		//disable counter
				GPTC_Clear(c_card, c_gptc_Ctr);
				}
				vcnt += c_pulseMetro;
			if (vcnt <= 0)
				vcnt = c_pulseMetro/10;
			c_gptc_Reg1Val = vcnt;					//re-sets counter initial value
			lastEncoder = vcnt;
			if (c_card >= 0)
				{
				GPTC_Setup(c_card, c_gptc_Ctr, c_gptc_Mode, c_gptc_SrcCtrl, c_gptc_PolCtrl, c_gptc_Reg1Val, c_gptc_Reg2Val);
				GPTC_Control(c_card, c_gptc_Ctr, IntUpDnCTR, 0);	//lets counter count down
				GPTC_Control(c_card, c_gptc_Ctr, IntGate, 1);		//lets sw gate high
				GPTC_Control(c_card, c_gptc_Ctr, IntENABLE, 1);		//enable counter
				}
			// aggiorniamo metraggio
			// aggiunta sezione critica
			c_pDoc->c_difCoil.setMeter(c_pDoc->c_difCoil.getMeter()+c_pDoc->c_difCoil.getIncMeter());
			c_pWndClient->PostMessage(WM_COMMAND,(UINT)ID_ENCODER_UPDATED);
			}
		}
	else
		{
		
		((CThEncoder*)pData)->c_runGoOn = FALSE;
		break;
		// altrimenti acquisisci e` signalObject
		}
	// 
	// aggiorniamo velocita` 
	// c_pulseMetro : questo e` numero di impulsi corrispondenti a c_pDoc->c_difCoil.getIncMeter()
	// per fare il calcolo della velocita` devo ricavare lo spazio in metri
	// update linespeed
	

	// update log view

	cycleTime = endCycle - startCycle;

	double t = max (1.*cycleTime/CLOCKS_PER_SEC,(double)deltaT/1000.);
	// double t = (double)deltaT/1000.;
	double v = 60. * deltaEncoder * c_pDoc->c_difCoil.getIncMeter();
	v /= c_pulseMetro;
	v /= t;

	accVel += v;

	if ((cycleCount % 5) == 0)
		{// 500 msec	
		// filtraggio velocita` 
		// 20% del nuovo valore piu` 80% del vecchio
		// media di 5 campioni
		v = accVel /5.0;

		accVel = 0.;

		lastVel = 0.2 * v + 0.8 * lastVel;

		// aggiunta sezione critica
		((CMainFrame*)c_pWndClient)->setValLineSpeed(lastVel);
		}

	// test su 20 sec quindi 20sec*1000 / fixDeltaT
	if (cntDeltaEncoderLow >= ((20 * 1000) / fixDeltaT))
		{// allarme encoder
		c_pDoc->setValAlarmEncoder(TRUE); 
		}
	else
		{// NO allarme encoder
		c_pDoc->setValAlarmEncoder(FALSE);
		}

	if ((cycleCount % 10) == 0)
		{
		c_pWndClient->PostMessage(WM_COMMAND,(UINT)ID_ENCODER_ALARM);
		cycleCount = 0;
		// set output 0 alarm Cluster
		if (c_card >= 0)
			{
			if (c_pDoc->c_signalCluster)
				{
				DO_WriteLine (c_card, (U16) 0,(U16) PIN_OUT0, (U16) 1);
				DO_WriteLine (c_card, (U16) 0,(U16) PIN_OUT1, (U16) 0);
				}
			else
				{
				DO_WriteLine (c_card, (U16) 0,(U16) PIN_OUT0, (U16) 0);
				DO_WriteLine (c_card, (U16) 0,(U16) PIN_OUT1, (U16) 1);
				}
			}
		}


	}


((CMainFrame*)c_pWndClient)->setValLineSpeed(0.);

safeValue();

// update log view
// c_pWndClient = CDeviceView

return retCode;
}
//#UBLK-END-METHOD