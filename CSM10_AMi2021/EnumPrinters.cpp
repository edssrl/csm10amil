// EnumPrinters.cpp: implementation of the CEnumPrinters class.
//
// Written By : R.I.Allen
// 3rd May 2002
// Roger.Allen@sirius-analytical.com
// You can use this source as you like, but without any warranties of any kind!
//
// 19-09-08 Aggiunto save PaperSize
// versione adattata Unicode
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "EnumPrinters.h"
#include <Winspool.h>

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CEnumPrinters::CEnumPrinters()
{
	// initialise ourselves
	m_NumPrinters = 0 ;
	m_PrinterName.RemoveAll() ;
	m_PrinterLocation.RemoveAll() ;
	m_PrinterShareName.RemoveAll() ;

	ReadLocalPrinters() ;				// get the local printers
}

CEnumPrinters::~CEnumPrinters()
{

}

int CEnumPrinters::GetPrinterCount()
{
	return m_NumPrinters ;
}

CString CEnumPrinters::GetPrinterName(int index)
{
	ASSERT(index >= 0 && index < m_NumPrinters) ;
	POSITION	pos ;

	pos = m_PrinterName.GetHeadPosition() ;
	while (pos && index > 0)
		{
		// traverse the list
		m_PrinterName.GetNext(pos) ;
		index-- ;
		}
	if (pos)
		return m_PrinterName.GetAt(pos) ;
	return CString("Error") ;
}

CString CEnumPrinters::GetPrinterLocation(int index)
{
	ASSERT(index >= 0 && index < m_NumPrinters) ;
	POSITION	pos ;
	 
	pos = m_PrinterLocation.GetHeadPosition() ;
	while (pos && index > 0)
		{
		// traverse the list
		m_PrinterLocation.GetNext(pos) ;
		index-- ;
		}
	if (pos)
		return m_PrinterLocation.GetAt(pos) ;
	return CString("Error") ;
}

CString CEnumPrinters::GetPrinterShareName(int index)
{
	ASSERT(index >= 0 && index < m_NumPrinters) ;
	POSITION	pos ;

	pos = m_PrinterShareName.GetHeadPosition() ;
	while (pos && index > 0)
		{
		// traverse the list
		m_PrinterShareName.GetNext(pos) ;
		index-- ;
		}
	if (pos)
		return m_PrinterShareName.GetAt(pos) ;
	return CString("Error") ;
}

CString CEnumPrinters::GetPrinterPortName(int index)
{
	ASSERT(index >= 0 && index < m_NumPrinters) ;
	POSITION	pos ;

	pos = m_PrinterPort.GetHeadPosition() ;
	while (pos && index > 0)
		{
		// traverse the list
		m_PrinterPort.GetNext(pos) ;
		index-- ;
		}
	if (pos)
		return m_PrinterPort.GetAt(pos) ;
	return CString("Error") ;
}


void CEnumPrinters::ReadLocalPrinters()
{
	DWORD		Flags = PRINTER_ENUM_FAVORITE | PRINTER_ENUM_LOCAL; //local printers
	DWORD		cbBuf;
	DWORD		pcReturned ;
	DWORD		index;
	DWORD		Level = 2;
	TCHAR		Name[500] ;
	LPPRINTER_INFO_2 pPrinterEnum = NULL ;

	memset(Name, 0, sizeof(TCHAR) * 500) ;
	::EnumPrinters(Flags, Name, Level, NULL, 0, &cbBuf, &pcReturned) ;
	pPrinterEnum = (LPPRINTER_INFO_2)LocalAlloc(LPTR, cbBuf + 4) ;

	if (!pPrinterEnum)
		{
		TRACE(_T("Error %1d\n"), GetLastError()) ;
		goto clean_up;
		}

	if (!EnumPrinters(
			Flags,							// DWORD Flags, printer object types 
			Name,							// LPTSTR Name, name of printer object 
			Level,							// DWORD Level, information level 
			(LPBYTE)pPrinterEnum,			// LPBYTE pPrinterEnum, printer information buffer 
			cbBuf,							// DWORD cbBuf, size of printer information buffer
			&cbBuf,							// LPDWORD pcbNeeded, bytes received or required 
			&pcReturned)					// LPDWORD pcReturned number of printers enumerated 
			)
			{
			TRACE(_T("Error %1d\n"), GetLastError()) ;
			goto clean_up;
			}

	if (pcReturned > 0)
		{
		TRACE(_T("Found %1d printers\n"), pcReturned) ;
		for (index = 0; index < pcReturned; index++)
			{
			m_PrinterName.AddTail((pPrinterEnum + index)->pPrinterName) ;
			m_PrinterShareName.AddTail((pPrinterEnum + index)->pShareName) ;
			m_PrinterLocation.AddTail((pPrinterEnum + index)->pLocation) ;
			m_PrinterPort.AddTail((pPrinterEnum + index)->pPortName) ;
			m_NumPrinters++ ;
			TRACE(_T("Printer          : %1d\n"), index + 1) ;
			TRACE(_T("Share name       : %s\n"), (pPrinterEnum + index)->pShareName) ;
			TRACE(_T("Printer name     : %s\n"), (pPrinterEnum + index)->pPrinterName) ;
			TRACE(_T("Printer location : %s\n"), (pPrinterEnum + index)->pLocation) ;
			TRACE(_T("Printer port     : %s\n"), (pPrinterEnum + index)->pPortName) ;
			}
		}
clean_up:
	LocalFree(LocalHandle(pPrinterEnum)) ;
}

bool CEnumPrinters::SetNewPrinter(HANDLE& hDevMode, HANDLE& hDevNames, const CString& PrinterName, 
			const CString& PrinterSpooler, const CString& PrinterPort)
// hDeMode - Handle to the current DEVMODE structure
// hDevNames - Handle to the current DEVNAMES structure
// PrinterName - E.g. HP LaserJet 4L
// PrinterSpooler - e.g. "winspool"
// PrinterPort - e.g. "LPT1:"
{
	// we only update the existing hDevMode and hDevNames objects if we can successfgully setup the
	// new hDevMode and hDevNames objects
	HANDLE	local_hDevMode = INVALID_HANDLE_VALUE ;
	HANDLE	local_hDevNames = INVALID_HANDLE_VALUE ;
#ifdef _DEBUG
	DumpHandles(hDevMode, hDevNames) ;
#endif

	// To setup the new local_hDevMode object we need to open the printer name to get the information
	HANDLE	hPrinter;
	TCHAR	*pPrinter = new TCHAR[PrinterName.GetLength() + 1] ;
	ASSERT(pPrinter) ;
	_tcscpy(pPrinter, PrinterName) ;
	if (!OpenPrinter(pPrinter, &hPrinter, NULL))
		{
		delete []pPrinter ;
		return false ;
		}

	// A zero for last param returns the size of buffer needed for the information to be returned
	int nSize = DocumentProperties(NULL, hPrinter, pPrinter, NULL, NULL, 0);
	ASSERT(nSize >= 0);
	local_hDevMode = ::GlobalAlloc(GHND, nSize) ;							// allocate on heap
	LPDEVMODE lpDevMode = (LPDEVMODE)::GlobalLock(local_hDevMode);		// lock it

	// Fill in the rest of the structure.
	if (DocumentProperties(NULL, hPrinter, pPrinter, lpDevMode, NULL, DM_OUT_BUFFER) != IDOK)
		{
		// failed to read printer properties, abort
		ASSERT(::GlobalFlags(local_hDevMode) != GMEM_INVALID_HANDLE);
		UINT nCount = ::GlobalFlags(local_hDevMode) & GMEM_LOCKCOUNT;
		while (nCount--)
			::GlobalUnlock(local_hDevMode);

		// finally, really free the handle
		::GlobalFree(local_hDevMode);
		local_hDevMode = NULL;
		ClosePrinter(hPrinter);
		delete []pPrinter ;
		return false ;
		}
	// finsihed interrogating for DEVMODE structure
	::GlobalUnlock(local_hDevMode) ;
	ClosePrinter(hPrinter);
	delete []pPrinter ;

	// we need to allocate a new DEVNAMES object on the global heap
	// we also need the size to include the strings PrinterName, PrinterSpooler and PrinterPort
	// Layout is:
	// DEVNAMES structure
	// PrinterSpooler\0
	// PrinterName\0
	// PrinterPort\0

	int	size = sizeof(DEVNAMES);
	size += (PrinterName.GetLength() + 1 + 
				PrinterSpooler.GetLength() + 1 + 
				PrinterPort.GetLength() + 1) * sizeof(TCHAR);
	
	local_hDevNames = ::GlobalAlloc(GHND, size) ;								// allocate on heap
	LPDEVNAMES	pNewDevNames = (LPDEVNAMES)::GlobalLock(local_hDevNames) ;	// lock it
	memset(pNewDevNames, 0, size) ;											// init to 0
	
	// UNICODE ?
	// add the 3 strings to the end of the structure
	_tcscpy((TCHAR*) ((char*)pNewDevNames + sizeof(DEVNAMES)), (LPCTSTR)PrinterSpooler) ;
	// offset in caratteri!
	pNewDevNames->wDriverOffset =  sizeof(DEVNAMES)/sizeof(TCHAR) ;
	_tcscpy((TCHAR*)( (char*)pNewDevNames + sizeof(DEVNAMES) + (PrinterSpooler.GetLength() + 1)*sizeof(TCHAR)),  (LPCTSTR)PrinterName) ;
	pNewDevNames->wDeviceOffset =  sizeof(DEVNAMES)/sizeof(TCHAR) + PrinterSpooler.GetLength() + 1;
	_tcscpy((TCHAR*)((char*)pNewDevNames + sizeof(DEVNAMES) + (PrinterSpooler.GetLength() + 1 + PrinterName.GetLength() + 1)*sizeof(TCHAR)),  (LPCTSTR)PrinterPort) ;
	pNewDevNames->wOutputOffset =  sizeof(DEVNAMES)/sizeof(TCHAR) + (PrinterSpooler.GetLength() + 1 + PrinterName.GetLength() + 1);
	// pNewDevNames->wDefault = lpDevNames->wDefault ;
	
	::GlobalUnlock(local_hDevNames) ;											// free it
#ifdef _DEBUG
	DumpHandles(local_hDevMode, local_hDevNames) ;
#endif
	// now update the handles that were passed in
	// free the existing handles if they exist first
	if (hDevMode != NULL && hDevMode != INVALID_HANDLE_VALUE)
		{
		ASSERT(::GlobalFlags(hDevMode) != GMEM_INVALID_HANDLE);
		UINT nCount = ::GlobalFlags(hDevMode) & GMEM_LOCKCOUNT;
		while (nCount--)
			::GlobalUnlock(hDevMode);

		// finally, really free the handle
		::GlobalFree(hDevMode);
		hDevMode = INVALID_HANDLE_VALUE ;
		}
	if (hDevNames != NULL && hDevNames != INVALID_HANDLE_VALUE)
		{
		ASSERT(::GlobalFlags(hDevNames) != GMEM_INVALID_HANDLE);
		UINT nCount = ::GlobalFlags(hDevNames) & GMEM_LOCKCOUNT;
		while (nCount--)
			::GlobalUnlock(hDevNames);

		// finally, really free the handle
		::GlobalFree(hDevNames);
		hDevNames = INVALID_HANDLE_VALUE ;
		}
	hDevMode = local_hDevMode ;
	hDevNames = local_hDevNames ;

#ifdef _DEBUG
	DumpHandles(hDevMode, hDevNames) ;
#endif
	return true ;			// success!
}


bool CEnumPrinters::SetNewPrinter(HANDLE& hDevMode, HANDLE& hDevNames, int index)
{
	CString	printer = GetPrinterName(index) ;
	CString spooler(_T("winspool")) ;
	CString	port = GetPrinterPortName(index) ;

	return SetNewPrinter(hDevMode, hDevNames, printer, spooler, port) ;
}

bool CEnumPrinters::SavePrinterSelection(HANDLE &hDevMode, HANDLE& hDevNames)
{
	CWinApp* pApp = AfxGetApp() ;
	ASSERT(pApp) ;

	// save the current printer name, spooler and port to the registry
	CString	printer ;
	CString	spooler ;
	CString	port ;
	int		landscape = DMORIENT_PORTRAIT ;
	// 19-09-08 Aggiunto save paperSize
	int		paperSize = DMPAPER_LETTER ;

	// FixBug se nessuna stampante config. crash on exit
	// test se hDevName NULL
	if (hDevNames &&
		(hDevNames != INVALID_HANDLE_VALUE))
		{
		LPDEVNAMES lpDevNames = (LPDEVNAMES)::GlobalLock(hDevNames);
		
		printer =  (TCHAR*)((TCHAR*)lpDevNames + lpDevNames->wDeviceOffset) ;
		spooler =  (TCHAR*)((TCHAR*)lpDevNames + lpDevNames->wDriverOffset) ;
		port = (TCHAR*)((TCHAR*)lpDevNames + lpDevNames->wOutputOffset) ;

	
		::GlobalUnlock(hDevNames) ;
		}
	else
		return false ;				// not setup!

	// get the landscape/portrait mode of the printer
	if (hDevMode != INVALID_HANDLE_VALUE)
		{
		LPDEVMODE lpDevMode = (LPDEVMODE)::GlobalLock(hDevMode) ;
		// get orientation
		landscape = lpDevMode->dmOrientation ;
		::GlobalUnlock(hDevMode) ;
		}
	
	// get the paperSize mode of the printer
	if (hDevMode != INVALID_HANDLE_VALUE)
		{
		LPDEVMODE lpDevMode = (LPDEVMODE)::GlobalLock(hDevMode) ;
		// get paperSize
		paperSize = lpDevMode->dmPaperSize ;
		::GlobalUnlock(hDevMode) ;
		}

	VERIFY(pApp->WriteProfileString(_T("PrinterConfig"), _T("PrinterName"), CString(printer))) ;
	VERIFY(pApp->WriteProfileString(_T("PrinterConfig"), _T("Spooler"), CString(spooler))) ;
	VERIFY(pApp->WriteProfileString(_T("PrinterConfig"), _T("Port"),CString(port))) ;
	VERIFY(pApp->WriteProfileInt(_T("PrinterConfig"), _T("Landscape"), landscape)) ;
	VERIFY(pApp->WriteProfileInt(_T("PrinterConfig"), _T("PaperSize"), paperSize)) ;
	return true ;
}

bool CEnumPrinters::RestorePrinterSelection(HANDLE &hDevMode, HANDLE& hDevNames)
{
	// read the settings back from the registry
	// abort if not present
	CString	printer ;
	CString	spooler ;
	CString	port ;
	int		landscape = DMORIENT_PORTRAIT ;
	// 19-09-08 Aggiunto load paperSize
	int		paperSize = DMPAPER_LETTER ;
	CWinApp* pApp = AfxGetApp() ;
	ASSERT(pApp) ;

	printer = pApp->GetProfileString(_T("PrinterConfig"), _T("PrinterName"), _T("")) ;
	spooler = pApp->GetProfileString(_T("PrinterConfig"), _T("Spooler"), _T("")) ;
	port = pApp->GetProfileString(_T("PrinterConfig"), _T("Port"), _T("")) ;
	landscape = pApp->GetProfileInt(_T("PrinterConfig"), _T("Landscape"), DMORIENT_PORTRAIT) ;
	paperSize= pApp->GetProfileInt(_T("PrinterConfig"), _T("PaperSize"), DMPAPER_LETTER ) ;

	if (printer == _T("") || spooler == _T("") || port == _T(""))
		return false ;								// not setup

	// make sure the selected printer is in the list available
	int i=0;
	for (i = 0 ; i < m_NumPrinters ; i++)
		{
		if (printer == GetPrinterName(i))
			break ;				// found!
		}
	if (i >= m_NumPrinters)
		{
		// the selected printer is no longer available
		AfxMessageBox(_T("Warning : Unable to re-select your preffered printer\n")
						_T("as it is no longer available. The system default printer\n")
						_T("will be used.")) ;
		return false ;
		}
	VERIFY(SetNewPrinter(hDevMode, hDevNames, printer, spooler, port)) ;
 	VERIFY(SetPrintOrientation(hDevMode, landscape)) ;
	VERIFY(SetPrintPaperSize(hDevMode, paperSize)) ;
	return true ;
}

bool CEnumPrinters::SetPrintPaperSize(HANDLE &hDevMode, int mode)
{
	if (hDevMode == INVALID_HANDLE_VALUE)
		return false ;

// portrait mode
LPDEVMODE pDevMode = (LPDEVMODE)::GlobalLock(hDevMode) ;
// set paper Size
pDevMode->dmPaperSize = mode;
::GlobalUnlock(hDevMode) ;

return true ;
}


bool CEnumPrinters::SetPrintOrientation(HANDLE &hDevMode, int mode)
{
	if (hDevMode == INVALID_HANDLE_VALUE)
		return false ;

	switch (mode)
		{
		case DMORIENT_PORTRAIT :
				{
				// portrait mode
				LPDEVMODE pDevMode = (LPDEVMODE)::GlobalLock(hDevMode) ;
				// set orientation to portrait
				pDevMode->dmOrientation = DMORIENT_PORTRAIT ;
				::GlobalUnlock(hDevMode) ;
				}
				break ;
		case DMORIENT_LANDSCAPE :
				{
				// landscape mode
				LPDEVMODE pDevMode = (LPDEVMODE)::GlobalLock(hDevMode) ;
				// set orientation to landscape
				pDevMode->dmOrientation = DMORIENT_LANDSCAPE ;
				::GlobalUnlock(hDevMode) ;
				}
				break ;
		default :	
				ASSERT(FALSE) ;		// invalid parameter
				return false ;
		}
	return true ;
}

#ifdef _DEBUG

void CEnumPrinters::DumpHandles(HANDLE& hDevMode, HANDLE& hDevNames)
{
	// dump the content of the handles to the debug output
	TRACE(_T("===================Dumping Print Object handles==============\n")) ;
	if (hDevMode != INVALID_HANDLE_VALUE && hDevMode != NULL)
		{
		LPDEVMODE lpDevMode = (LPDEVMODE)::GlobalLock(hDevMode);		// lock it
		TRACE(_T("------hDevMode---------------------------\n")) ;
		TRACE(_T("Device name          : %s\n"), lpDevMode->dmDeviceName) ;
		TRACE(_T("dmSpecVersion        : %d\n"), lpDevMode->dmSpecVersion) ;
		TRACE(_T("dmDriverVersion      : %d\n"), lpDevMode->dmDriverVersion) ;
		TRACE(_T("dmSize               : %d\n"), lpDevMode->dmSize) ;
		TRACE(_T("dmDriverExtra        : %d\n"), lpDevMode->dmDriverExtra) ;
		TRACE(_T("dmFIelds             : %x\n"), lpDevMode->dmFields) ;
		TRACE(_T("dmScale              : %d\n"), lpDevMode->dmScale) ;
		TRACE(_T("dmCopies             : %d\n"), lpDevMode->dmCopies) ;
		TRACE(_T("dmDefaultSource      : %d\n"), lpDevMode->dmDefaultSource) ;
		TRACE(_T("dmPrintQuality       : %d\n"), lpDevMode->dmPrintQuality) ;
		TRACE(_T("dmColor              : %d\n"), lpDevMode->dmColor) ;
		TRACE(_T("dmDuplex             : %d\n"), lpDevMode->dmDuplex) ;
		TRACE(_T("dmYResolution        : %d\n"), lpDevMode->dmYResolution) ;
		TRACE(_T("dmTTOption           : %d\n"), lpDevMode->dmTTOption) ;
		TRACE(_T("dmCollate            : %d\n"), lpDevMode->dmCollate) ;
		TRACE(_T("dmFormName           : %s\n"), lpDevMode->dmFormName) ;
		TRACE(_T("dmLogPixels          : %d\n"), lpDevMode->dmLogPixels) ;
		TRACE(_T("dmBitsPerPel         : %d\n"), lpDevMode->dmBitsPerPel) ;
		TRACE(_T("dmPelsWidth          : %d\n"), lpDevMode->dmPelsWidth) ;
		TRACE(_T("dmPelsHeight         : %d\n"), lpDevMode->dmPelsHeight) ;
		TRACE(_T("dmNup/dmDisplayFlags : %d\n"), lpDevMode->dmDisplayFlags) ;
		TRACE(_T("dmDisplayFrequency   : %d\n"), lpDevMode->dmDisplayFrequency) ;
		::GlobalUnlock(hDevMode) ;
		}
	else
		TRACE(_T("hDevMode             : INVALID_HANDLE_VALUE\n")) ;

	if (hDevNames != INVALID_HANDLE_VALUE && hDevNames != NULL)
		{
		LPDEVNAMES lpDevNames = (LPDEVNAMES)::GlobalLock(hDevNames) ;
		TRACE(_T("------hDevNames--------------------------\n")) ;
		TRACE(_T("wDriverOffset   : %d\n"), lpDevNames->wDriverOffset) ;
		TRACE(_T("wDeviceOffset   : %d\n"), lpDevNames->wDeviceOffset) ;
		TRACE(_T("wOutputOffset   : %d\n"), lpDevNames->wOutputOffset) ;
		TRACE(_T("wDefault        : %x\n"), lpDevNames->wDefault) ;
		TRACE(_T("DriverName      : %s\n"), (TCHAR*)((TCHAR*)lpDevNames + lpDevNames->wDriverOffset)) ;
		TRACE(_T("DeviceName      : %s\n"), (TCHAR*)((TCHAR*)lpDevNames + lpDevNames->wDeviceOffset)) ;
		TRACE(_T("OutputName      : %s\n"), (TCHAR*)((TCHAR*)lpDevNames + lpDevNames->wOutputOffset)) ;
		::GlobalUnlock(hDevNames) ;
		}
	else
		TRACE(_T("hDevNames            : INVALID_HANDLE_VALUE\n")) ;
	TRACE(_T("===================Dump Complete=============================\n")) ;
}
#endif
