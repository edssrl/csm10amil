// CSM10_ARView.cpp : implementation of the CSM10_ARView class
//

#include "stdafx.h"

#include <AfxTempl.h>

#include "GraphDoc.h"
#include "GraphView.h"
#include "dyntempl.h"
#include "CSM10_AR.h"

#include "CSM10_ARDoc.h"
#include "CSM10_ARView.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CSM10_ARView

IMPLEMENT_DYNCREATE(CSM10_ARView, CView)

BEGIN_MESSAGE_MAP(CSM10_ARView, CView)
	//{{AFX_MSG_MAP(CSM10_ARView)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CSM10_ARView construction/destruction

CSM10_ARView::CSM10_ARView()
{
	// TODO: add construction code here

}

CSM10_ARView::~CSM10_ARView()
{
}

BOOL CSM10_ARView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs

	return CView::PreCreateWindow(cs);
}

/////////////////////////////////////////////////////////////////////////////
// CSM10_ARView drawing

void CSM10_ARView::OnDraw(CDC* pDC)
{
	CSM10_ARDoc* pDoc = GetDocument();
	ASSERT_VALID(pDoc);

	// TODO: add draw code for native data here
}

/////////////////////////////////////////////////////////////////////////////
// CSM10_ARView diagnostics

#ifdef _DEBUG
void CSM10_ARView::AssertValid() const
{
	CView::AssertValid();
}

void CSM10_ARView::Dump(CDumpContext& dc) const
{
	CView::Dump(dc);
}

CSM10_ARDoc* CSM10_ARView::GetDocument() // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CSM10_ARDoc)));
	return (CSM10_ARDoc*)m_pDocument;
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CSM10_ARView message handlers
