#if !defined(AFX_FINPUTOUTPUT_H__E8672156_0E42_49F1_8E6E_46B95224F199__INCLUDED_)
#define AFX_FINPUTOUTPUT_H__E8672156_0E42_49F1_8E6E_46B95224F199__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// FInputOutput.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CFInputOutput form view

#ifndef __AFXEXT_H__
#include <afxext.h>
#endif

class CFInputOutput : public CFormView
{
protected:
	CFInputOutput();           // protected constructor used by dynamic creation
	DECLARE_DYNCREATE(CFInputOutput)

UINT	c_timerId;

	void shutdown(void);
	void destroyCtrl(void);

// Form Data
public:
	//{{AFX_DATA(CFInputOutput)
	enum { IDD = IDD_IO_FORM };
	CButton	m_startRightButton;
	CButton	m_startLeftButton;
	CButton	m_ctrlLabelLRTest;
	CEdit	m_ctrlPosDiaphSx;
	CEdit	m_ctrlPosDiaphDx;
	CEdit	m_ctrlSpeedEncoder;
	CStatic	m_ctrlLabelSpeedEncoder;
	CStatic	m_ctrlLabelPosDiaphDx;
	CStatic	m_ctrlLabelPosDiaphSx;
	CButton	m_ctrlOut7;
	CButton	m_ctrlOut6;
	CButton	m_ctrlOut5;
	CButton	m_ctrlOut4;
	CButton	m_ctrlOut3;
	CButton	m_ctrlOut2;
	CButton	m_ctrlOut1;
	CButton	m_ctrlOut0;
	CButton	m_ctrlIn3;
	CButton	m_ctrlIn2;
	CButton	m_ctrlIn1;
	CButton	m_ctrlIn0;

	BOOL	m_out0;
	BOOL	m_out1;
	BOOL	m_out2;
	BOOL	m_out3;
	BOOL	m_out4;
	BOOL	m_out5;
	BOOL	m_out6;
	BOOL	m_out7;
	BOOL	m_in0;
	BOOL	m_in1;
	BOOL	m_in2;
	BOOL	m_in3;
	int		m_posDiafDx;
	int		m_posDiafSx;
	int		m_speedEncoder;
	//}}AFX_DATA

// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CFInputOutput)
	public:
	virtual void OnInitialUpdate();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	virtual ~CFInputOutput();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

	// Generated message map functions
	//{{AFX_MSG(CFInputOutput)
	afx_msg void OnUpdate();
	afx_msg void OnDestroy();
	afx_msg void OnTimer(UINT nIDEvent);
	afx_msg void OnStartLeftDiaph();
	afx_msg void OnStartRightDiaph();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_FINPUTOUTPUT_H__E8672156_0E42_49F1_8E6E_46B95224F199__INCLUDED_)
