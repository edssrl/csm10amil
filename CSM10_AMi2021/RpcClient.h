// RpcClient.h : header file
//
//
// USO:
// Bisogna derivare CMainFrame da CRcpClient
// Implementare virtual serverRequest in CMainFrame
// RpcClient e` sviluppato per singola connessione
// Per connessioni multiple implementare CMultiRpcClient (vedi Rey)
// Inserito c_abortNotify
/////////////////////////////////////////////////////////////////////////////
// CRpcClient window

#include "..\..\..\..\RpcServer2011\RpcNet.h"
#include "System.h"

struct Command
{
int cmd;
int size;
void *data;
};


class CRpcClient 
{
// Construction
public:
	virtual BOOL serverRequest (int cmd,int size,void *pter);
	CRpcClient();

// Attributes
public:
// Stato sistema
BOOL c_rpcOffLine; // Funzionamento senza connessione

BOOL c_serMsgReceived;	 // MainFrame override this var 
// Operations
public:
void rpcRegister(CString& wndName,int thisId,int remoteId,RPCTYPE type=TYPE_DMCS);
BOOL RegisterClient (CString &nome,RPCID id,int frameSize=4,BOOL abortNotify=FALSE);
LRESULT OnServerRequest(WPARAM wParam, LPARAM lParam);
BOOL SendGeneralCommand(Command *command);
void setProcServer(CString s){c_procServer=s;};
void setDirServer(CString s){c_dirServer=s;};


// Implementation
public:
	virtual ~CRpcClient();

private:
	CString c_procServer;
	CString c_dirServer;
	CString c_thisWnd;
	int c_thisRpcId;
	int c_remoteRpcId;
	
	RPCTYPE c_type;
};

/////////////////////////////////////////////////////////////////////////////
