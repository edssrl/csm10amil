// MainFrm.h : interface of the CMainFrame class
//
/////////////////////////////////////////////////////////////////////////////


#include "Csm10Comm.h"
#include "CustStatusBar.h"
#include "StatoAllarmi.h"


class CMainFrame : public CFrameWnd, public Csm10Comm
{

private:	
// Velocita` di linea
double c_lineSpeed; // Velocita` in metri/min

CCriticalSection c_cs;
public:
	void setValLineSpeed(double v){CSingleLock csl (&c_cs); 
	csl.Lock();c_lineSpeed=v;csl.Unlock();};	
	double getValLineSpeed(void){double v; CSingleLock csl (&c_cs); 
	csl.Lock();v = c_lineSpeed;csl.Unlock();return v;};	


protected: // create from serialization only

	afx_msg void OnUpdateDate(CCmdUI* pCmdUI);
	afx_msg void OnUpdateTime(CCmdUI* pCmdUI);
	afx_msg void OnUpdateStatus(CCmdUI* pCmdUI);
	afx_msg void OnUpdateMeter(CCmdUI* pCmdUI);

	afx_msg void OnUpdateAlLaser(CCmdUI* pCmdUI);
	afx_msg void OnUpdateAlEncoder(CCmdUI* pCmdUI);
	afx_msg void OnUpdateAlOtturatore1(CCmdUI* pCmdUI);
	afx_msg void OnUpdateAlOtturatore2(CCmdUI* pCmdUI);
	afx_msg void OnUpdateAlCpuReceiver(CCmdUI* pCmdUI);



	DECLARE_DYNCREATE(CMainFrame)
	CMainFrame();
// Attributes
public:
	int m_nDatePaneNo;
	int m_nTimePaneNo;

	virtual HWND getSafeHwnd(void ){return(GetSafeHwnd());};
// Timer alarm
BOOL c_doSendSoglie;	// invia o no valori soglie insieme allo stato
UINT alarmTimerId;
UINT statusTimerId;
UINT simulEncoderTimerId;
UINT oneSecTimerId;
UINT c_serialTimerId;

StatoAllarmi c_stAllarmi;


// Operations
public:
// Invia stato ad HC16
void sendStatus (BOOL stato);
virtual BOOL serverRequest(int cmd,int size,CStringA data);

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CMainFrame)
	public:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	virtual BOOL DestroyWindow();
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CMainFrame();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

public:  // control bar embedded members
	CCustStatusBar  m_wndStatusBar;
	CToolBar    m_wndToolBar;
	CProgressCtrl wndProgress;

	// comandi simulati
	BOOL c_remoteCambio;
	BOOL c_remotePausa;
	BOOL c_remoteAbilitazione;
	BOOL c_remoteControl;
	CToolBar    m_wndRemControlBar;
	//------------------

	BOOL progressBusy;

CProgressCtrl* getProgressCtrl(void);
BOOL progressCreate(void);
void progressDestroy(void);
void progressSetRange(int from,int to);
void progressSetStep(int s);
void progressStepIt(void);


// Gestisce arrivo VAL_TESTSPEED
void gestValLineSpeed (PVOID p,DWORD size);

// Generated message map functions
protected:
	CDialogBar m_wndCAlarmBar;
	//{{AFX_MSG(CMainFrame)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnConfiguraOpzioni();
	afx_msg void OnRicetteCopia();
	afx_msg void OnRicetteElimina();
	afx_msg void OnRicetteModifica();
	afx_msg void OnRicetteNuova();
	afx_msg void OnRicetteVisualizza();
	afx_msg void OnConfiguraParametri();
	afx_msg void OnFileModificapassword();
	afx_msg void OnRxSerChar();
	afx_msg void OnEncoderUpdated();
	afx_msg void OnEncoderAlarm();
	afx_msg void OnFileComport();
	afx_msg void OnTimer(UINT nIDEvent);
	afx_msg void OnConfiguraSoglie();
	afx_msg void OnViewRepaint();
	afx_msg void OnViewSystem();
	afx_msg void OnUpdateViewSystem(CCmdUI* pCmdUI);
	afx_msg void OnViewTrend();
	afx_msg void OnUpdateViewTrend(CCmdUI* pCmdUI);
	afx_msg void OnFileOpen();
	afx_msg void OnUpdateFileOpen(CCmdUI* pCmdUI);
	afx_msg void OnUpdateConfiguraParametri(CCmdUI* pCmdUI);
	afx_msg void OnRemoteControl();
	afx_msg void OnRemotePausa();
	afx_msg void OnRemoteAutotest();
	afx_msg void OnRemoteAbilitazione();
	afx_msg void OnRemoteUpdate();
	afx_msg void OnUpdateRemoteAbilitazione(CCmdUI* pCmdUI);
	afx_msg void OnUpdateRemoteAutotest(CCmdUI* pCmdUI);
	afx_msg void OnRemoteCambio();
	afx_msg void OnUpdateRemoteCambio(CCmdUI* pCmdUI);
	afx_msg void OnUpdateRemoteControl(CCmdUI* pCmdUI);
	afx_msg void OnUpdateRemotePausa(CCmdUI* pCmdUI);
	afx_msg void OnUpdateRemoteUpdate(CCmdUI* pCmdUI);
	afx_msg void OnConfiguraReport();
	afx_msg void OnConfiguraLocalizzazione();
	afx_msg void OnConfiguraCluster();
	afx_msg void OnViewCluster();
	afx_msg void OnUpdateViewCluster(CCmdUI* pCmdUI);
	afx_msg void OnConfiguraColori();
	afx_msg void OnViewF4();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
private:
	BOOL InitStatusBar(UINT *pIndicators, int nSize, int nSeconds);
public:
	// start encoder simulation
	int StartSimulEncoder(int millisec);
	int endSimulEncoder(void);
	afx_msg void OnUpdateOtturatoreDx(CCmdUI *pCmdUI);
	afx_msg void OnUpdateOtturatoreSx(CCmdUI *pCmdUI);
//	afx_msg void OnUpdateFtBar(CCmdUI *pCmdUI);
	afx_msg void OnUpdateFtBar(CCmdUI *pCmdUI);
};

/////////////////////////////////////////////////////////////////////////////
