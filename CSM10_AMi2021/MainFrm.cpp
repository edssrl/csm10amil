// MainFrm.cpp : implementation of the CMainFrame class
//

#include "stdafx.h"
#include "MainFrm.h"
#include "resource.h"

#include "Dialog.h"			// Dialog
#include "DCluster.h"
#include "DClassColor.h"

#include "GraphDoc.h"
#include "GraphView.h"
#include "dyntempl.h"

#include "logOff.h"

#include "CSM10_AR.h"

#include "DBSet.h"			// DB
#include "pwdlg.h"			// Password

// #include "RTextFile.h"

#include <dos.h>
#include <direct.h>


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CMainFrame

IMPLEMENT_DYNCREATE(CMainFrame, CFrameWnd)

BEGIN_MESSAGE_MAP(CMainFrame, CFrameWnd)
	ON_COMMAND_EX(CG_ID_VIEW_CALARMBAR, OnBarCheck)
	ON_UPDATE_COMMAND_UI(CG_ID_VIEW_CALARMBAR, OnUpdateControlBarMenu)
	ON_UPDATE_COMMAND_UI(IDC_LASER,       OnUpdateAlLaser)
	ON_UPDATE_COMMAND_UI(IDC_ENCODER,     OnUpdateAlEncoder)
	ON_UPDATE_COMMAND_UI(IDC_OTTURATORE1, OnUpdateAlOtturatore1)
	ON_UPDATE_COMMAND_UI(IDC_OTTURATORE2, OnUpdateAlOtturatore2)
	ON_UPDATE_COMMAND_UI(IDC_CPURECEIVER, OnUpdateAlCpuReceiver)

	ON_UPDATE_COMMAND_UI(ID_INDICATOR_DATE, OnUpdateDate)
	ON_UPDATE_COMMAND_UI(ID_INDICATOR_TIME, OnUpdateTime)
	ON_UPDATE_COMMAND_UI(ID_INDICATOR_STATUS, OnUpdateStatus)
	ON_UPDATE_COMMAND_UI(ID_INDICATOR_METER, OnUpdateMeter)

	//{{AFX_MSG_MAP(CMainFrame)
	ON_WM_CREATE()
	ON_COMMAND(ID_CONFIGURA_OPZIONI, OnConfiguraOpzioni)
	ON_COMMAND(ID_RICETTE_COPIA, OnRicetteCopia)
	ON_COMMAND(ID_RICETTE_ELIMINA, OnRicetteElimina)
	ON_COMMAND(ID_RICETTE_MODIFICA, OnRicetteModifica)
	ON_COMMAND(ID_RICETTE_NUOVA, OnRicetteNuova)
	ON_COMMAND(ID_RICETTE_VISUALIZZA, OnRicetteVisualizza)
	ON_COMMAND(ID_CONFIGURA_PARAMETRI, OnConfiguraParametri)
	ON_COMMAND(ID_FILE_MODIFICAPASSWORD, OnFileModificapassword)
	ON_COMMAND(ID_FILE_COMPORT, OnFileComport)
	ON_WM_TIMER()
	ON_COMMAND(ID_CONFIGURA_SOGLIE, OnConfiguraSoglie)
	ON_COMMAND(ID_VIEW_REPAINT, OnViewRepaint)
	ON_COMMAND(ID_VIEW_SYSTEM, OnViewSystem)
	ON_UPDATE_COMMAND_UI(ID_VIEW_SYSTEM, OnUpdateViewSystem)
	ON_COMMAND(ID_VIEW_TREND, OnViewTrend)
	ON_UPDATE_COMMAND_UI(ID_VIEW_TREND, OnUpdateViewTrend)
	ON_COMMAND(ID_FILE_OPEN, OnFileOpen)
	ON_UPDATE_COMMAND_UI(ID_FILE_OPEN, OnUpdateFileOpen)
	ON_UPDATE_COMMAND_UI(ID_CONFIGURA_PARAMETRI, OnUpdateConfiguraParametri)
	ON_COMMAND(ID_REMOTE_CONTROL, OnRemoteControl)
	ON_COMMAND(ID_REMOTE_PAUSA, OnRemotePausa)
	ON_COMMAND(ID_REMOTE_AUTOTEST, OnRemoteAutotest)
	ON_COMMAND(ID_REMOTE_ABILITAZIONE, OnRemoteAbilitazione)
	ON_COMMAND(ID_REMOTE_UPDATE, OnRemoteUpdate)
	ON_UPDATE_COMMAND_UI(ID_REMOTE_ABILITAZIONE, OnUpdateRemoteAbilitazione)
	ON_UPDATE_COMMAND_UI(ID_REMOTE_AUTOTEST, OnUpdateRemoteAutotest)
	ON_COMMAND(ID_REMOTE_CAMBIO, OnRemoteCambio)
	ON_UPDATE_COMMAND_UI(ID_REMOTE_CAMBIO, OnUpdateRemoteCambio)
	ON_UPDATE_COMMAND_UI(ID_REMOTE_CONTROL, OnUpdateRemoteControl)
	ON_UPDATE_COMMAND_UI(ID_REMOTE_PAUSA, OnUpdateRemotePausa)
	ON_UPDATE_COMMAND_UI(ID_REMOTE_UPDATE, OnUpdateRemoteUpdate)
	ON_COMMAND(ID_CONFIGURA_REPORT, OnConfiguraReport)
	ON_COMMAND(ID_CONFIGURA_LOCALIZZAZIONE, OnConfiguraLocalizzazione)
	ON_COMMAND(ID_CONFIGURA_CLUSTER, OnConfiguraCluster)
	ON_COMMAND(ID_VIEW_CLUSTER, OnViewCluster)
	ON_UPDATE_COMMAND_UI(ID_VIEW_CLUSTER, OnUpdateViewCluster)
	ON_COMMAND(ID_CONFIGURA_COLORI, OnConfiguraColori)
	ON_COMMAND(ID_VIEW_F4, OnViewF4)
	ON_COMMAND(ID_SERCHARAVAIL, OnRxSerChar)
	ON_COMMAND(ID_ENCODER_UPDATED, OnEncoderUpdated)
	ON_COMMAND(ID_ENCODER_ALARM, OnEncoderAlarm)
	//}}AFX_MSG_MAP  
	ON_UPDATE_COMMAND_UI(IDC_OTTURATORE_DX, &CMainFrame::OnUpdateOtturatoreDx)
	ON_UPDATE_COMMAND_UI(IDC_OTTURATORE_SX, &CMainFrame::OnUpdateOtturatoreSx)
//	ON_UPDATE_COMMAND_UI(IDC_FT_BAR, &CMainFrame::OnUpdateFtBar)
ON_UPDATE_COMMAND_UI(IDC_FT_BAR, &CMainFrame::OnUpdateFtBar)
END_MESSAGE_MAP()


static UINT indicators[] =
{
	ID_SEPARATOR,           // status line indicator
	ID_INDICATOR_PROGRESS,
	ID_INDICATOR_STATUS,	// visualizzazione stato hc16
	ID_INDICATOR_METER,		// Visualizzazione posizione attuale
//	ID_INDICATOR_NUM,
//	ID_INDICATOR_SCRL,
};

/////////////////////////////////////////////////////////////////////////////
// CMainFrame construction/destruction

CMainFrame::CMainFrame()
{
	// TODO: add member initialization code here
progressBusy = FALSE;
alarmTimerId = 0;
statusTimerId = 0;	
oneSecTimerId = 0;

c_lineSpeed = 0.;
c_doSendSoglie = FALSE;	// invia o no valori soglie insieme allo stato

c_remoteControl = FALSE;
c_remoteAbilitazione = FALSE;
c_remoteCambio = FALSE;
c_remotePausa = FALSE;


simulEncoderTimerId = 0;
}

CMainFrame::~CMainFrame()
{

}

CProgressCtrl* CMainFrame::getProgressCtrl(void)
	{
	if (progressBusy)
		return (&wndProgress);
	return (NULL);
	};
BOOL CMainFrame::progressCreate(void)
	{CRect rc;
	if (progressBusy)
		{
		progressDestroy();
		}
	m_wndStatusBar.GetItemRect (1, &rc);
	rc.top += 1;
	rc.bottom -=1;
	rc.left += 1;
	rc.right -=1;
	VERIFY (wndProgress.Create(WS_CHILD | WS_VISIBLE, rc,
    	     &m_wndStatusBar, 1));
	progressBusy = TRUE;
	return (TRUE);
	}
void CMainFrame::progressDestroy(void)
	{
	if (progressBusy)
		wndProgress.DestroyWindow();
	progressBusy = FALSE;
	}
void CMainFrame::progressSetRange(int from,int to)
	{if (progressBusy)
		wndProgress.SetRange(from, to);};
void CMainFrame::progressSetStep(int s)
	{if (progressBusy)
		wndProgress.SetStep(s);};
void CMainFrame::progressStepIt(void)
 	{if (progressBusy)
		wndProgress.StepIt();};






int CMainFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
CProfile profile;


	if (CFrameWnd::OnCreate(lpCreateStruct) == -1)
		return -1;
	
	if (!m_wndToolBar.Create(this) ||
		!m_wndToolBar.LoadToolBar(IDR_MAINFRAME))
	{
		TRACE0("Failed to create toolbar\n");
		return -1;      // fail to create
	}

	if (!m_wndStatusBar.Create(this) ||
		!m_wndStatusBar.SetIndicators(indicators,
		  sizeof(indicators)/sizeof(UINT)))
	{
		TRACE0("Failed to create status bar\n");
		return -1;      // fail to create
	}

/*
	m_wndStatusBar.Init();
	UINT nID, nStyle;
    int cxWidth;

    m_wndStatusBar.GetPaneInfo(2, nID, nStyle, cxWidth);
    m_wndStatusBar.SetPaneInfo(2, nID, nStyle | SBT_OWNERDRAW | SBPS_POPOUT,
           cxWidth);
*/


	// TODO: Remove this if you don't want tool tips or a resizeable toolbar
	m_wndToolBar.SetBarStyle(m_wndToolBar.GetBarStyle() |
				CBRS_FLYBY | CBRS_SIZE_DYNAMIC);
		//		CBRS_TOOLTIPS | CBRS_FLYBY | CBRS_SIZE_DYNAMIC);

	// TODO: Delete these three lines if you don't want the toolbar to
	//  be dockable
	m_wndToolBar.EnableDocking(CBRS_ALIGN_ANY);
	EnableDocking(CBRS_ALIGN_ANY);
	DockControlBar(&m_wndToolBar);


// Init Server Process
CString strValue;
	strValue = profile.getProfileString(_T("DataBase"), _T("Definition"), 
				_T("DbHole.def"));

	// Init DataBase Object And Open It
	if (dBase != NULL)
		{
		dBase->load(strValue);
		strValue = profile.getProfileString(_T("DataBase"), _T("Name"), 
					_T("HOLE.MDB"));

		try{dBase->Open(strValue);}
		catch (CDaoException *e)
			{
			AfxGetMainWnd()->MessageBox (e->m_pErrorInfo->m_strDescription,_T("dBase.Open"));
			return -1;
			}
		}

// invia o no valori soglie insieme allo stato
	 c_doSendSoglie = profile.getProfileBool(_T("Timer"), _T("SendSoglie"),FALSE);

	// CG: The following block was inserted by 'Status Bar' component.
	{
		// Find out the size of the static variable 'indicators' defined
		// by AppWizard and copy it
		int nOrigSize = sizeof(indicators) / sizeof(UINT);

		UINT* pIndicators = new UINT[nOrigSize + 2];
		memcpy(pIndicators, indicators, sizeof(indicators));

		// Call the Status Bar Component's status bar creation function
		if (!InitStatusBar(pIndicators, nOrigSize, 60))
		{
			TRACE0("Failed to initialize Status Bar\n");
			return -1;
		}
		delete[] pIndicators;
	}




// Init addressId in rpcClient
// char name [81];
// ::GetClassName(GetSafeHwnd(),name,30);
// strValue = name;
// rpcRegister(strValue,RPCID_NTCSM,RPCID_HC16CSM);


// Create alarm timer
int intervalTimer;
// default 30 sec
intervalTimer = profile.getProfileInt(_T("Timer"), _T("AlarmSec"),30); 
// Disable Timer if interval == 0
if (intervalTimer > 0)
	{
	intervalTimer *= 1000; // Valore da secondi in millisec
	if ((alarmTimerId=SetTimer(rand(),intervalTimer,NULL))== 0)
		{
		AfxGetMainWnd()->MessageBox (_T("Cannot Create AlarmTimer"),_T("SetTimer"));
		return(-1);
		}
	}

// periodo di invio msg a hde per test seriale connessa
intervalTimer = profile.getProfileInt(_T("Timer"), _T("ValComPcMsec"),5000); 
if ((c_serialTimerId=SetTimer(rand(),intervalTimer,NULL))== 0)
	{
	AfxGetMainWnd()->MessageBox (_T("Cannot Create serialTimer"),_T("SetTimer"));
	return(-1);
	}

// default 20 sec.
intervalTimer = profile.getProfileInt(_T("Timer"), _T("StatusSec"),20); 
// Disable Timer if interval == 0
if (intervalTimer > 0)
	{
	intervalTimer *= 1000; // Valore da secondi in millisec
	if ((statusTimerId=SetTimer(rand(),intervalTimer,NULL))== 0)
		{
		AfxGetMainWnd()->MessageBox (_T("Cannot Create StatusTimer"),_T("SetTimer"));
		return(-1);
		}
	}

intervalTimer = 1000; // Valore da secondi in millisec
if ((oneSecTimerId=SetTimer(rand(),intervalTimer,NULL))== 0)
	{
	AfxGetMainWnd()->MessageBox (_T("Cannot Create oneSecTimer"),_T("SetTimer"));
	return(-1);
	}


	// TODO: Add a menu item that will toggle the visibility of the
	// dialog bar named "CAlarmBar":
	//   1. In ResourceView, open the menu resource that is used by
	//      the CMainFrame class
	//   2. Select the View submenu
	//   3. Double-click on the blank item at the bottom of the submenu
	//   4. Assign the new item an ID: CG_ID_VIEW_CALARMBAR
	//   5. Assign the item a Caption: CAlarmBar

	// TODO: Change the value of CG_ID_VIEW_CALARMBAR to an appropriate value:
	//   1. Open the file resource.h
	// CG: The following block was inserted by the 'Dialog Bar' component
	{
		// Initialize dialog bar m_wndCAlarmBar
		if (!m_wndCAlarmBar.Create(this, CG_IDD_CALARMBAR,
			CBRS_TOP | CBRS_TOOLTIPS | CBRS_FLYBY | CBRS_HIDE_INPLACE,
			CG_ID_VIEW_CALARMBAR))
		{
			TRACE(_T("Failed to create dialog bar m_wndCAlarmBar\n"));
			return -1;		// fail to create
		}

		m_wndCAlarmBar.EnableDocking(CBRS_ALIGN_TOP | CBRS_ALIGN_BOTTOM);
		EnableDocking(CBRS_ALIGN_ANY);
		DockControlBar(&m_wndCAlarmBar);
	}

// apertura porta seriale
CDSelComPort dialog;
dialog.loadFromProfile(); 

if (openPort(dialog.m_SelComPort,dialog.getSpeed(),0,dialog.slowMode()))
	{
	if (getSafeHwnd() != NULL)
		setEventDest(getSafeHwnd(),ID_SERCHARAVAIL);
	}
else
	AfxMessageBox(_T("ERROR: can't open COM port")); 

//


return 0;
}

BOOL CMainFrame::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs
static LPCTSTR className = NULL;

if( !CFrameWnd::PreCreateWindow(cs))
	return FALSE;
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs


if (className == NULL) 
	{
    // One-time class registration
    // The only purpose is to make the class name something meaningful
    // instead of "Afx:0x4d:27:32:hup1hup:hike!"
    WNDCLASS wndcls;
	::GetClassInfo(AfxGetInstanceHandle(), cs.lpszClass, &wndcls);
  	wndcls.lpszClassName = CSM_CLASSNAME;
    wndcls.hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
    VERIFY(AfxRegisterClass(&wndcls));
    className= CSM_CLASSNAME;
    }
cs.lpszClass = className;
return TRUE;

 // return CFrameWnd::PreCreateWindow(cs);
}

/////////////////////////////////////////////////////////////////////////////
// CMainFrame diagnostics

#ifdef _DEBUG
void CMainFrame::AssertValid() const
{
	CFrameWnd::AssertValid();
}

void CMainFrame::Dump(CDumpContext& dc) const
{
	CFrameWnd::Dump(dc);
}

#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CMainFrame message handlers

void CMainFrame::OnConfiguraOpzioni() 
{
DBOpzioni dbOpzioni(dBase);
CDOpzioni1 dialog;
// dBase already Open
if (dbOpzioni.openSelectDefault ())
	{
	// Move Init Data from Db to Dialog
	//dialog.m_Allarme = dbOpzioni.m_ALLARME_ACU;
	//dialog.m_AllarmeDurata = (int )dbOpzioni.m_ALLARME_DURATA;
	dialog.m_StampaAutomatica = dbOpzioni.m_STAMPA_AUTO;
	// dialog.m_Mappa = dbOpzioni.m_MAPPA100;

	// dialog.m_Classe = dbOpzioni.m_TL_CLASSE;

	dialog.m_Lunghezza1 = dbOpzioni.m_TL_LUNGHEZZA;
	dialog.m_Scala1 = dbOpzioni.m_TL_SCALA;


// 	dialog.m_Densita = dbOpzioni.m_TD_DENSITA;
//	dialog.m_Lunghezza2 = dbOpzioni.m_TD_LUNGHEZZA;
	dialog.m_Scala2 = dbOpzioni.m_TD_SCALA;
	
	if (dialog.DoModal() == IDOK)
		{
		try
			{
			dbOpzioni.Edit();
			}
		catch (CDaoException *e)
			{
			AfxGetMainWnd()->MessageBox (e->m_pErrorInfo->m_strDescription,_T("dbOpzioni.Edit"));
			dbOpzioni.Close();
			return ;
			}
		
	 	//dbOpzioni.m_ALLARME_ACU = dialog.m_Allarme;
		//dbOpzioni.m_ALLARME_DURATA = (double) dialog.m_AllarmeDurata;
		dbOpzioni.m_STAMPA_AUTO = dialog.m_StampaAutomatica;
		//dbOpzioni.m_MAPPA100 = dialog.m_Mappa;
	
		//dbOpzioni.m_TL_CLASSE = dialog.m_Classe;
		dbOpzioni.m_TL_LUNGHEZZA = dialog.m_Lunghezza1;
		dbOpzioni.m_TL_SCALA = dialog.m_Scala1;

		// dbOpzioni.m_TD_DENSITA = dialog.m_Densita;

		// dbOpzioni.m_TD_LUNGHEZZA = dialog.m_Lunghezza2;
		dbOpzioni.m_TD_SCALA = dialog.m_Scala2;
		try
			{
			dbOpzioni.Update();
			}
		catch (CDaoException *e)
			{
			AfxGetMainWnd()->MessageBox (e->m_pErrorInfo->m_strDescription,_T("dbOpzioni.Update"));
			dbOpzioni.Close();
			return ;
			}
		CLineDoc *pDoc;
		pDoc = (CLineDoc *) GetActiveDocument();
		pDoc->UpdateBaseDoc();
		}
	}
dbOpzioni.Close();


// Aggiorno View
//if (!PostMessage(WM_COMMAND,ID_VIEW_AGGIORNA))
//	AfxGetMainWnd()->MessageBox ("Fail ","Post Message");

}

void CMainFrame::OnConfiguraReport() 
{
	// TODO: Add your command handler code here
DBReport dbReport(dBase);
CDReport dialog;
// dBase already Open

if (dbReport.openSelectDefault ())
	{
	// Move Init Data from Db to Dialog
	dialog.m_Allarmi = dbReport.m_SOGLIE_ALLARME;
	// dialog.m_Cliente = dbReport.m_DATI_CLIENTE;
	// dialog.m_DataLavoraz = dbReport.m_DATA_LAVORAZIONE;
	dialog.m_DifettiPer = dbReport.m_DESCR_PERIODICI;
	dialog.m_DifettiRand = dbReport.m_DESCR_RANDOM;
	// dialog.m_Lunghezza = dbReport.m_LUNGHEZZA_BOBINA;
	dialog.m_Mappa = dbReport.m_MAPPA100;
	dialog.m_alMetro = (int )dbReport.m_ALMETRO;
	dialog.m_dalMetro = (int )dbReport.m_DALMETRO;
	dialog.m_Intervallo1 = (int )dbReport.m_INTERVALLO1;
	dialog.m_Intervallo2 = (int )dbReport.m_INTERVALLO2;
	dialog.m_Intervallo3 = (int )dbReport.m_INTERVALLO3;
	dialog.m_crossWeb = dbReport.m_DESCR_CROSSWEB;
	dialog.m_downWeb = dbReport.m_DESCR_DOWNWEB;
	dialog.m_threshold = dbReport.m_DESCR_THRESHOLD;
	dialog.m_compression = dbReport.m_DESCR_COMPRESSION;
	
	if (dialog.DoModal() == IDOK)
		{
		try
			{
			dbReport.Edit();
			}
		catch (CDaoException *e)
			{
			AfxGetMainWnd()->MessageBox (e->m_pErrorInfo->m_strDescription,_T("dbReport.Open"));
			dbReport.Close();
			return ;
			}

		dbReport.m_SOGLIE_ALLARME = dialog.m_Allarmi;
		// dbReport.m_DATI_CLIENTE = dialog.m_Cliente;
		// dbReport.m_DATA_LAVORAZIONE = dialog.m_DataLavoraz;
		dbReport.m_DESCR_PERIODICI = dialog.m_DifettiPer;
		dbReport.m_DESCR_RANDOM = dialog.m_DifettiRand;
		// dbReport.m_LUNGHEZZA_BOBINA = dialog.m_Lunghezza;
		dbReport.m_MAPPA100 = dialog.m_Mappa;
		dbReport.m_ALMETRO = (double)dialog.m_alMetro;
		dbReport.m_DALMETRO = (double)dialog.m_dalMetro;
		dbReport.m_INTERVALLO1 = (double)dialog.m_Intervallo1;
		dbReport.m_INTERVALLO2 = (double)dialog.m_Intervallo2;
		dbReport.m_INTERVALLO3 = (double)dialog.m_Intervallo3;
		dbReport.m_DESCR_CROSSWEB = dialog.m_crossWeb;
		dbReport.m_DESCR_DOWNWEB = dialog.m_downWeb;
		dbReport.m_DESCR_THRESHOLD = dialog.m_threshold;
		dbReport.m_DESCR_COMPRESSION = dialog.m_compression;

		try
			{
			dbReport.Update();
			}
		catch (CDaoException *e)
			{
			AfxGetMainWnd()->MessageBox (e->m_pErrorInfo->m_strDescription,_T("dbReport.Open"));
			dbReport.Close();
			return ;
			}

		}
	}
dbReport.Close();

}

void CMainFrame::OnUpdateConfiguraParametri(CCmdUI* pCmdUI) 
{
	// TODO: Add your command update UI handler code here
CLineDoc *pDoc;

pDoc = (CLineDoc *) GetActiveDocument();

// Enable only in mode !OnLine
pCmdUI->Enable(!pDoc->systemOnLine);
}

void CMainFrame::OnConfiguraParametri() 
{
	// TODO: Add your command handler code here
DBParametri dbParametri (dBase);
CDParametri dialog;


// CG: This code was inserted by the Password Dialog Component
if (!CPasswordDlg::CheckPassword())
	return;

 if (dbParametri.openSelectDefault ())
	{
	// Move Init Data from Db to Dialog
	dialog.m_FCDL = dbParametri.m_FCDL;
	dialog.m_LrStop = dbParametri.m_L_RESIDUA_STOP;
	dialog.m_alarmPulse = (long)dbParametri.m_ALARM_PULSE;
	

	if (dialog.DoModal() == IDOK)
		{
		try
			{
			dbParametri.Edit();
			}
		catch (CDaoException *e)
			{
			AfxGetMainWnd()->MessageBox (e->m_pErrorInfo->m_strDescription,_T("dbParametri.Open"));
			dbParametri.Close();
			return ;
			}

		dbParametri.m_FCDL = dialog.m_FCDL;
		dbParametri.m_L_RESIDUA_STOP = dialog.m_LrStop;
		dbParametri.m_ALARM_PULSE = dialog.m_alarmPulse;

		try
			{
			dbParametri.Update();
			}
		catch (CDaoException *e)
			{
			AfxGetMainWnd()->MessageBox (e->m_pErrorInfo->m_strDescription,_T("dbParametri.Open"));
			dbParametri.Close();
			return ;
			}

		}
	}
dbParametri.Close();

	
}

//----------------------------------------------------------------------------------------
//
//	  Handler RICETTE
//
//----------------------------------------------------------------------------------------

void CMainFrame::OnRicetteCopia() 
{
// TODO: Add your command handler code here
// Seleziona Ricetta
CDCopiaRicette dialog;

// CG: This code was inserted by the Password Dialog Component
if (!CPasswordDlg::CheckPassword())
	return;


if (dialog.DoModal() == IDOK)
	{
	// m_ComboString contiene selezione
	DBRicette dbRicette(dBase);
	DBRicette dbRicetteNew(dBase);

	// TODO: Add extra initialization here
	// dBase already Open
	// FALSE -> NotClose if EOF
	if (!dbRicette.openSelectID (dialog.m_ComboString,FALSE))
		{// Messaggi interni dbRicette
		return;
		}	
	if (!dbRicetteNew.openSelectID (dialog.m_ComboString,FALSE))
		{// Messaggi interni dbRicette
		dbRicette.Close();
		return;
		}	


	// Perform Copy Record
	if (dbRicetteNew.CanAppend())
		{
		try	{dbRicetteNew.AddNew();}
		catch (CDaoException *e)
			{
			AfxGetMainWnd()->MessageBox (e->m_pErrorInfo->m_strDescription,_T("dbRicette.AddNew"));
			dbRicette.Close();
			dbRicetteNew.Close();
			return;
			}
   		// Modify the Name of Ricetta
		dbRicetteNew.m_NOME = dialog.m_NewRicetta;
		dbRicetteNew.m_LEGA = dbRicette.m_LEGA;
		dbRicetteNew.m_ALLARME_A = dbRicette.m_ALLARME_A;
		dbRicetteNew.m_ALLARME_B = dbRicette.m_ALLARME_B;
		dbRicetteNew.m_ALLARME_C = dbRicette.m_ALLARME_C;
		dbRicetteNew.m_ALLARME_D = dbRicette.m_ALLARME_D;
		dbRicetteNew.m_SPESSORE_MAX = dbRicette.m_SPESSORE_MAX;
		dbRicetteNew.m_SPESSORE_MIN = dbRicette.m_SPESSORE_MIN;

		try	{dbRicetteNew.Update();}
		catch (CDaoException *e)
			{
			AfxGetMainWnd()->MessageBox (e->m_pErrorInfo->m_strDescription,_T("dbRicette.Update"));
			dbRicetteNew.Close();
			dbRicette.Close();
			return;
			}
		}
	else
		AfxGetMainWnd()->MessageBox (_T("Can't Append to dbRicette"),_T("dbRicette.CanAppend"));

	dbRicette.Close();
	dbRicetteNew.Close();

	CString strRes1,strRes2;
	strRes1.LoadString(CSM_MAINFRM_RICETTACOPIA);
	strRes2.LoadString(CSM_MAINFRM_HRICETTACOPIA);
	//AfxGetMainWnd()->MessageBox ("Creata Nuova Ricetta","Copia Ricetta");
	AfxGetMainWnd()->MessageBox (strRes1,strRes2);

	return;
	}
return;	
}

void CMainFrame::OnRicetteElimina() 
{
CDSelRicette dialog;

// CG: This code was inserted by the Password Dialog Component
if (!CPasswordDlg::CheckPassword())
	return;


if (dialog.DoModal() == IDOK)
	{
	CString str1,str2;

	//str.Format ("Cancellazione ricetta %s\nPremi OK per Cancellare\nPremi ANNULLA per NON Cancellare",dialog.m_EditSel); 
	str1.LoadString(CSM_MAINFRM_OKRICETTACANCELLA);
	str2.Format (str1,dialog.m_EditSel);
	if (AfxMessageBox(str2,MB_OKCANCEL) == IDCANCEL)
		return;
		
	// m_ComboString contiene selezione
	DBRicette dbRicette(dBase);
	
	// TODO: Add extra initialization here
	// dBase already Open
	if (!dbRicette.openSelectID (dialog.m_EditSel,FALSE))
		{// Messaggi interni dbRicette
		return;
		}

	// Perform Delete Record
	if (dbRicette.CanUpdate())
		{
		try	{dbRicette.Delete();}
		catch (CDaoException *e)
			{
			AfxGetMainWnd()->MessageBox (e->m_pErrorInfo->m_strDescription,_T("dbRicette.Delete"));
			dbRicette.Close();
			return;
			}
		}
	else
		AfxGetMainWnd()->MessageBox (_T("Can't Delete record in dbRicette"),_T("dbRicette.CanUpdate"));

	dbRicette.Close();

	CString strRes1,strRes2;
	strRes1.LoadString(CSM_MAINFRM_RICETTAELIMINA);
	strRes2.LoadString(CSM_MAINFRM_HRICETTAELIMINA);

	AfxGetMainWnd()->MessageBox (strRes1,strRes2);

	return;
	}
return;	
	
}

void CMainFrame::OnRicetteModifica() 
{
// TODO: Add your command handler code here
CDSelRicette dialog;

// CG: This code was inserted by the Password Dialog Component
if (!CPasswordDlg::CheckPassword())
	return;


if (dialog.DoModal() == IDOK)
	{
	// m_ComboString contiene selezione
	DBRicette dbRicette(dBase);
	
	if (!dbRicette.openSelectID (dialog.m_EditSel,FALSE))
		{// Messaggi interni dbRicette
		return;
		}


	CDRicette dialogR;
	// Init Dialog Member Variable
	dialogR.m_Nome = dbRicette.m_NOME;

	// dialogR.m_Lega = dbRicette.m_LEGA;
	dialogR.m_SogliaA2 = (int )dbRicette.m_ALLARME_A;
	dialogR.m_SogliaB2 = (int )dbRicette.m_ALLARME_B;
	dialogR.m_SogliaC2 = (int )dbRicette.m_ALLARME_C;
	dialogR.m_SogliaD2 = (int )dbRicette.m_ALLARME_D;
	dialogR.m_SpessoreMax = dbRicette.m_SPESSORE_MAX;
	dialogR.m_SpessoreMin = dbRicette.m_SPESSORE_MIN;
	dialogR.m_lenCalcDens = dbRicette.m_LENGTH_ALLARME;

	dialogR.SetNameReadOnly();
	CLineDoc *pDoc;
	pDoc = (CLineDoc *) GetActiveDocument();

	dialogR.setNumClassi(pDoc->c_difCoil.getNumClassi());

	if (dialogR.DoModal() == IDOK)
		{
		// Perform Copy Record
		if (dbRicette.CanUpdate())
			{
			try	{dbRicette.Edit();}
			catch (CDaoException *e)
				{
				AfxGetMainWnd()->MessageBox (e->m_pErrorInfo->m_strDescription,_T("dbRicette.Edit"));
				dbRicette.Close();
				return;
				}
   			// Modify the Name of Ricetta
			// dbRicette.m_LEGA = dialogR.m_Lega;
			dbRicette.m_ALLARME_A = (double )dialogR.m_SogliaA2;
			dbRicette.m_ALLARME_B = (double )dialogR.m_SogliaB2;
			dbRicette.m_ALLARME_C = (double )dialogR.m_SogliaC2;
			dbRicette.m_ALLARME_D = (double )dialogR.m_SogliaD2;
			dbRicette.m_SPESSORE_MAX = (double )dialogR.m_SpessoreMax;
			dbRicette.m_SPESSORE_MIN = (double )dialogR.m_SpessoreMin;
			dbRicette.m_LENGTH_ALLARME = (double)dialogR.m_lenCalcDens;

			try	{dbRicette.Update();}
			catch (CDaoException *e)
				{
				AfxGetMainWnd()->MessageBox (e->m_pErrorInfo->m_strDescription,_T("dbRicette.Update"));
				dbRicette.Close();
				return;
				}
			}
		else
			AfxGetMainWnd()->MessageBox (_T("Can't Update dbRicette"),_T("dbRicette.CanUpdate"));

		dbRicette.Close();

		CString strRes1,strRes2;
		strRes1.LoadString(CSM_MAINFRM_RICETTAMODIFICA);
		strRes2.LoadString(CSM_MAINFRM_HRICETTAMODIFICA);
		//AfxGetMainWnd()->MessageBox ("Modificata Ricetta","Modifica Ricetta");
		AfxGetMainWnd()->MessageBox (strRes1,strRes2);
		}
	}	
}



void CMainFrame::OnRicetteNuova() 
{
// TODO: Add your command handler code here
DBRicette dbRicette(dBase);

// CG: This code was inserted by the Password Dialog Component
if (!CPasswordDlg::CheckPassword())
	return;

if (!dbRicette.openSelectDefault ())
		{// Messaggi interni dbRicette
		return;
		}
 

CDRicette dialogR;
// Init Dialog Member Variable
dialogR.m_Nome = "";

// dialogR.m_Lega = dbRicette.m_LEGA;
dialogR.m_SogliaA2 = (int )dbRicette.m_ALLARME_A;
dialogR.m_SogliaB2 = (int )dbRicette.m_ALLARME_B;
dialogR.m_SogliaC2 = (int )dbRicette.m_ALLARME_C;
dialogR.m_SogliaD2 = (int )dbRicette.m_ALLARME_D;
dialogR.m_SpessoreMax = dbRicette.m_SPESSORE_MAX;
dialogR.m_SpessoreMin = dbRicette.m_SPESSORE_MIN;
dialogR.m_lenCalcDens = dbRicette.m_LENGTH_ALLARME;

CLineDoc *pDoc;
pDoc = (CLineDoc *) GetActiveDocument();

dialogR.setNumClassi(pDoc->c_difCoil.getNumClassi());

if (dialogR.DoModal() == IDOK)
	{
	// Perform Copy Record
	if (dbRicette.CanAppend())
		{
		try	{dbRicette.AddNew();}
		catch (CDaoException *e)
			{
			AfxGetMainWnd()->MessageBox (e->m_pErrorInfo->m_strDescription,_T("dbRicette.AddNew"));
			dbRicette.Close();
			return;
			}
 
		// Modify the Name of Ricetta
		dbRicette.m_NOME = dialogR.m_Nome;

		// dbRicette.m_LEGA = dialogR.m_Lega;
		dbRicette.m_ALLARME_A = (double )dialogR.m_SogliaA2;
		dbRicette.m_ALLARME_B = (double )dialogR.m_SogliaB2;
		dbRicette.m_ALLARME_C = (double )dialogR.m_SogliaC2;
		dbRicette.m_ALLARME_D = (double )dialogR.m_SogliaD2;
		dbRicette.m_SPESSORE_MAX = (double )dialogR.m_SpessoreMax;
		dbRicette.m_SPESSORE_MIN = (double )dialogR.m_SpessoreMin;
		dbRicette.m_LENGTH_ALLARME = (double)dialogR.m_lenCalcDens;

		try	{dbRicette.Update();}
		catch (CDaoException *e)
			{
			AfxGetMainWnd()->MessageBox (e->m_pErrorInfo->m_strDescription,_T("dbRicette.Update"));
			dbRicette.Close();
			return;
			}
		}
	else
		AfxGetMainWnd()->MessageBox (_T("Can't Append to dbRicette"),_T("dbRicette.CanAppend"));

	CString strRes1,strRes2;
	strRes1.LoadString(CSM_MAINFRM_RICETTANUOVA);
	strRes2.LoadString(CSM_MAINFRM_HRICETTANUOVA);
	//xGetMainWnd()->MessageBox ("Inserita Ricetta","Nuova Ricetta");

	AfxGetMainWnd()->MessageBox (strRes1,strRes2);
	}
dbRicette.Close();
}	

	


void CMainFrame::OnRicetteVisualizza() 
{
// TODO: Add your command handler code here
CDSelRicette dialog;

if (dialog.DoModal() == IDOK)
	{
	// m_ComboString contiene selezione
	DBRicette dbRicette(dBase);

	if (!dbRicette.openSelectID (dialog.m_EditSel,FALSE))
		{// Messaggi interni dbRicette
		return;
		}


	CDRicette dialogR;
	// Init Dialog Member Variable
	dialogR.m_Nome = dbRicette.m_NOME;

	// dialogR.m_Lega = dbRicette.m_LEGA;

	dialogR.m_SogliaA2 = (int )dbRicette.m_ALLARME_A;
	dialogR.m_SogliaB2 = (int )dbRicette.m_ALLARME_B;
	dialogR.m_SogliaC2 = (int )dbRicette.m_ALLARME_C;
	dialogR.m_SogliaD2 = (int )dbRicette.m_ALLARME_D;
	dialogR.m_SpessoreMax = dbRicette.m_SPESSORE_MAX;
	dialogR.m_SpessoreMin = dbRicette.m_SPESSORE_MIN;
	dialogR.m_lenCalcDens = dbRicette.m_LENGTH_ALLARME;

	dialogR.SetAllReadOnly();
	dialogR.SetNameReadOnly();
	
	CLineDoc *pDoc;
	pDoc = (CLineDoc *) GetActiveDocument();

	dialogR.setNumClassi(pDoc->c_difCoil.getNumClassi());

	if (dialogR.DoModal() == IDOK)
		{
		}
	dbRicette.Close();
	}		
}

//----------------------------------------------------------------------------------------
//
//	  Handler SOGLIE
//
//----------------------------------------------------------------------------------------


void CMainFrame::OnConfiguraSoglie() 
{
	// TODO: Add your command handler code here
	// TODO: Add your command handler code here
DBParametri dbParametri (dBase);
CDSoglie dialog;


// CG: This code was inserted by the Password Dialog Component
if (!CPasswordDlg::CheckPassword())
	return;

 if (dbParametri.openSelectDefault ())
	{
	// Move Init Data from Db to Dialog
	dialog.m_SogliaA = (int )dbParametri.m_SOGLIA_A;
	dialog.m_SogliaB = (int )dbParametri.m_SOGLIA_B;
	dialog.m_SogliaC = (int )dbParametri.m_SOGLIA_C;
	dialog.m_SogliaD = (int )dbParametri.m_SOGLIA_D;

	// 12-05-2005; deprecated 31-10-2012
	// dialog.m_digitalOutput = dbParametri.m_ALARM_CLASS;

	if (dialog.DoModal() == IDOK)
		{
		try
			{
			dbParametri.Edit();
			}
		catch (CDaoException *e)
			{
			AfxGetMainWnd()->MessageBox (e->m_pErrorInfo->m_strDescription,_T("dbParametri.Open"));
			dbParametri.Close();
			return ;
			}
  		
		dbParametri.m_SOGLIA_A = dialog.m_SogliaA;
		dbParametri.m_SOGLIA_B = dialog.m_SogliaB;
		dbParametri.m_SOGLIA_C = dialog.m_SogliaC;
		dbParametri.m_SOGLIA_D = dialog.m_SogliaD;

		// 12-05-2005; deprecated 31-10-2012
		// dbParametri.m_ALARM_CLASS = dialog.m_digitalOutput;

		try
			{
			dbParametri.Update();
			}
		catch (CDaoException *e)
			{
			AfxGetMainWnd()->MessageBox (e->m_pErrorInfo->m_strDescription,_T("dbParametri.Update"));
			dbParametri.Close();
			return ;
			}

		}
	}
dbParametri.Close();
}


void CMainFrame::OnFileModificapassword() 
{
// TODO: Add your command handler code here
// CG: This code was inserted by the Password Dialog Component
if (!CPasswordDlg::ClearPassword())
	return;
	
}

void CMainFrame::OnUpdateDate(CCmdUI* pCmdUI)
{
	// CG: This function was inserted by 'Status Bar' component.

	// Get current date and format it
	CTime time = CTime::GetCurrentTime();
	CString tFormatRes;
	tFormatRes.LoadString (CSM_MAINFRM_DATEFORMAT);
	//CString strDate = time.Format(_T("%a, %d, %b %Y "));
	CString strDate = time.Format(tFormatRes);

	// BLOCK: compute the width of the date string
	CSize size;
	{
		HGDIOBJ hOldFont = NULL;
		HFONT hFont = (HFONT)m_wndStatusBar.SendMessage(WM_GETFONT);
		CClientDC dc(NULL);
		if (hFont != NULL) 
			hOldFont = dc.SelectObject(hFont);
		size = dc.GetTextExtent(strDate);
		if (hOldFont != NULL) 
			dc.SelectObject(hOldFont);
	}

	// Update the pane to reflect the current date
	UINT nID, nStyle;
	int nWidth;
	m_wndStatusBar.GetPaneInfo(m_nDatePaneNo, nID, nStyle, nWidth);
	m_wndStatusBar.SetPaneInfo(m_nDatePaneNo, nID, nStyle, size.cx);

	pCmdUI->SetText(strDate);
	pCmdUI->Enable(TRUE);

}

void CMainFrame::OnUpdateTime(CCmdUI* pCmdUI)
{
	// CG: This function was inserted by 'Status Bar' component.

	// Get current date and format it
	CString strTime; 
	CTime time = CTime::GetCurrentTime();
	if (time.GetTime() >= 0)
		{	
		CString tFormatRes;
		tFormatRes.LoadString (CSM_MAINFRM_TIMEFORMAT);
		//strTime = time.Format(_T("%I:%M %S "));
		strTime = time.Format(tFormatRes);
		strTime = (time.GetHour() < 12 ? _T("") : _T(""))+ strTime +(time.GetHour() < 12 ? _T("AM "):_T("PM "));
		

		// BLOCK: compute the width of the date string
		CSize size;
		{
		HGDIOBJ hOldFont = NULL;
		HFONT hFont = (HFONT)m_wndStatusBar.SendMessage(WM_GETFONT);
		CClientDC dc(NULL);
		if (hFont != NULL) 
			hOldFont = dc.SelectObject(hFont);
		size = dc.GetTextExtent(strTime);
		if (hOldFont != NULL) 
			dc.SelectObject(hOldFont);
		}

	// Update the pane to reflect the current time
	UINT nID, nStyle;
	int nWidth;
	m_wndStatusBar.GetPaneInfo(m_nTimePaneNo, nID, nStyle, nWidth);
	m_wndStatusBar.SetPaneInfo(m_nTimePaneNo, nID, nStyle, size.cx);
	pCmdUI->SetText(strTime);
	pCmdUI->Enable(TRUE);
	}
}

BOOL CMainFrame::InitStatusBar(UINT *pIndicators, int nSize, int nSeconds)
{
	// CG: This function was inserted by 'Status Bar' component.
return TRUE;
	// Create an index for the DATE pane
	m_nDatePaneNo = nSize++;
	pIndicators[m_nDatePaneNo] = ID_INDICATOR_DATE;
	// Create an index for the TIME pane
	m_nTimePaneNo = nSize++;
	nSeconds = 1;
	pIndicators[m_nTimePaneNo] = ID_INDICATOR_TIME;

	// TODO: Select an appropriate time interval for updating
	// the status bar when idling.
	m_wndStatusBar.SetTimer(0x1000, nSeconds * 1000, NULL);

	return m_wndStatusBar.SetIndicators(pIndicators, nSize);

}

void CMainFrame::OnRxSerChar(void )
{

Csm10Comm::OnRxSerChar();
}

void CMainFrame::OnEncoderUpdated(void )
{

CLineDoc *pDoc;

pDoc = (CLineDoc *) GetActiveDocument();

pDoc->UpdateAllViews(NULL,2);

}

void CMainFrame::OnEncoderAlarm(void )
{

CLineDoc *pDoc;

pDoc = (CLineDoc *) GetActiveDocument();

// si usa variabile globale pDoc->c_valAlarmEncoder sttata da threadEncoder
pDoc->gestValAlEncoder();

}
// Modify Com Port Settings

void CMainFrame::OnFileComport() 
{
// TODO: Add your command handler code here

// CG: This code was inserted by the Password Dialog Component
if (!CPasswordDlg::CheckPassword())
	return;
CDSelComPort dialog;
dialog.loadFromProfile(); 

if (dialog.DoModal() == IDOK)
	{
	dialog.saveToProfile();
	reOpenPort(dialog.m_SelComPort,dialog.getSpeed(),0);
	}
	
}


void CMainFrame::OnFileOpen() 
{
	// TODO: Add your command handler code here
CLineDoc *pDoc;

pDoc = (CLineDoc *) GetActiveDocument();

CString szFilter (_T("CSM Files (*.csm)|*.csm|All Files (*.*)|*.*||"));
TCHAR dir [256];

GetCurrentDirectory(127,dir);  
  											

CFileDialog dialog (TRUE,(LPCTSTR) "*.csm",
	(LPCTSTR) "*.csm",OFN_HIDEREADONLY,(LPCTSTR) szFilter);

if (dialog.DoModal() == IDOK)
	{
	// Load Data 
	CFileBpe cf;
	CFileException e;
	CString title;
	title =  dialog.GetPathName();
	if( !cf.Open((LPCTSTR) title, CFile::modeRead | CFile::typeBinary, &e ) )
		{
		AfxGetMainWnd()->MessageBox(_T("ERROR OPEN"),_T("Error"));
		return;
		}
	// create progress control
	progressCreate();
	progressSetRange(0, 8);
	progressSetStep(2);
	CProgressCtrl *progress;
	progress = getProgressCtrl();

	pDoc->load(&cf,progress);
	cf.Close();  
	// Set Title of new data
	pDoc->setTitle ((LPCTSTR)dialog.GetFileTitle());
	progressDestroy();
	}
SetCurrentDirectory((LPCTSTR) dir);  

}


void CMainFrame::OnUpdateFileOpen(CCmdUI* pCmdUI) 
{
return;
CLineDoc *pDoc;

pDoc = (CLineDoc *) GetActiveDocument();

// Enable only in mode !OnLine
pCmdUI->Enable(!pDoc->systemOnLine);

}


void CMainFrame::OnTimer(UINT nIDEvent) 
{
// TODO: Add your message handler code here and/or call default

// Test if Doc Variable serMsgReceived True otherways Alarm
CLineDoc *pDoc;
pDoc = (CLineDoc *) GetActiveDocument();

if (!pDoc)
	return;

if ((nIDEvent == simulEncoderTimerId)&&
		(pDoc->systemOnLine))
	{
	int pos = pDoc->c_difCoil.getMeter()+pDoc->c_difCoil.getIncMeter();
	pDoc->c_difCoil.setMeter(pos);
	
	pDoc->UpdateAllViews (NULL,0);
	}

// Test allarmi seriale e disco pieno
// solo a sistema online altrimenti sporca file 
// caricato per visualizzazione
// check e rinomina file in modalita` 
// PlantInfoMode=3

if ((nIDEvent == alarmTimerId)
	// &&(pDoc->systemOnLine)
	)
	{
	CString strFreeDiskSpace;
	BOOL alActive = FALSE;
	// Fill disk free information
	ULARGE_INTEGER bytesFreeAvail;
	ULARGE_INTEGER bytesTotal;
	ULARGE_INTEGER bytesFreeTotal;
	long dFree;
	if (GetDiskFreeSpaceEx(_T("C:\\"),           // directory name
		&bytesFreeAvail,    // bytes available to caller
		&bytesTotal,        // bytes on disk
		&bytesFreeTotal     // free bytes on disk
		))
		{
		dFree = (long) ((__int64)bytesFreeAvail.QuadPart/(__int64)(1024*1024));	// Valore in Mbytes
		CWinApp* pApp = AfxGetApp();
		int diskMin;
		diskMin = pApp->GetProfileInt(_T("init"), _T("MinFreeDisk"),500); 
		if ((dFree < diskMin)&&
			(!pDoc->diskAlarmActive))
			{
			CString allarmeSistema;
			CString strRes;
			strRes.LoadString(CSM_MAINFRM_SPDISK);
			// allarmeSistema.Format("Spazio su disco = %d(Mbytes)\r\nProvvedere alla Cancellazione\r\n",dFree);
			allarmeSistema.Format(strRes,dFree);

			pDoc->systemAlarm.appendMsg(allarmeSistema);
			pDoc->diskAlarmActive = TRUE;
			alActive = TRUE;
			}
		}
	else
		strFreeDiskSpace.LoadString(CG_IDS_DISK_SPACE_UNAVAIL);
	
	if ((!pDoc->serMsgReceived)&&
		// (pDoc->systemOnLine)&&
		(!pDoc->serAlarmActive))
		{
		// Alarm
		// Allarmi di sistema
		CString allarmeSistema;
		// allarmeSistema.Format("Collegamento seriale interrotto\r\n");
		allarmeSistema.LoadString(CSM_MAINFRM_SERIALNOK);
		pDoc->systemAlarm.appendMsg(allarmeSistema);
		pDoc->serAlarmActive = TRUE;
		alActive = TRUE;
		// uscita da ispezione
		if (pDoc->systemOnLine)
			{
			pDoc->sendStartStop (FALSE,FALSE);
			pDoc->FileSaveStop(TRUE);
			}
		}
	else
		{// 
		if ( // (pDoc->systemOnLine)&&
			(pDoc->serMsgReceived))
			// reset serial alarm
			pDoc->serAlarmActive = FALSE;
		}
	if (alActive)
		pDoc->systemAlarm.visualizza(this,pDoc->secAlarmSound);
	pDoc->serMsgReceived = FALSE;		
	}

if (nIDEvent == alarmTimerId)
	{
	// ? PlantInfoMode=3
	if (pDoc->c_plantInfoMode == PLANTINFO_MDB)
		{
		KillTimer(nIDEvent);
		pDoc->loadPlantInfoFromDb();
		// Create alarm timer
		int intervalTimer;
		// default 30 sec
		CProfile profile;
		intervalTimer = profile.getProfileInt(_T("Timer"), _T("AlarmSec"),30); 

		// Disable Timer if interval == 0
		intervalTimer *= 1000; // Valore da secondi in millisec
		alarmTimerId=SetTimer(rand(),intervalTimer,NULL);
		}
	}
// Invio messaggio programma attivo
if (nIDEvent == statusTimerId)
	{
	sendStatus (pDoc->systemOnLine);
	// invio valori delle soglie
	if (c_doSendSoglie)
		{
		Sleep (50);
		pDoc->sendValSoglie();
		}
	}

if (nIDEvent == oneSecTimerId)
	{
	pDoc->terminateSignalCluster();
	}

if (nIDEvent == c_serialTimerId)
	{// invio pacchetto periodico per refresh watchdog seriale
	pDoc->sendValComPc();
	}

CFrameWnd::OnTimer(nIDEvent);
}

static BOOL onDestroyFlag = FALSE;

BOOL CMainFrame::DestroyWindow() 
{
	// TODO: Add your specialized code here and/or call the base class
if (alarmTimerId != 0)
	KillTimer(alarmTimerId);
if (statusTimerId != 0)
	KillTimer(statusTimerId);
if (oneSecTimerId != 0)
	KillTimer(oneSecTimerId);
	
if (dBase != NULL)
	{
	// win10
	if (dBase->IsOpen())
		dBase->Close();
	delete dBase;
	dBase = NULL;
	// win10
	AfxDaoTerm( );
	}

CCSMApp *pApp;
pApp = (CCSMApp *) AfxGetApp();

// save the printer selection for a next run restore
pApp->savePrinterSettings() ;


CProfile profile;
BOOL logOff;
logOff = profile.getProfileBool(_T("init"),_T("LogOffOnExit"),FALSE);

if ((!onDestroyFlag)&&
	(logOff))
	{
	// logOff no shutdown, no reboot
	onDestroyFlag = TRUE;
	LogOff(FALSE,FALSE);
	return FALSE;
	}
else
	return CFrameWnd::DestroyWindow();
}


//------------------------------------
//
//	Invia status message
//				   
//------------------------------------


void CMainFrame::sendStatus (BOOL stato)
{


/*	comando di stato non presente da pc a csm10_p
struct Command command;
command.cmd = VAL_TESTSER;

if (stato)
	tipo = ONCMD;
else
	tipo = OFFCMD;


// RpcClient e' base class mainframe
sendGeneralCommand(&command);
*/

}

BOOL CMainFrame::serverRequest(int cmd,int size,CStringA dataStr)
{
// Notify alarm
c_serMsgReceived = TRUE;

CLineDoc *doc;
doc = (CLineDoc *)((CCSMApp *)AfxGetApp())->GetDocument();

int data[100];
int dataIndex = 0;
CString dataStrTmp(dataStr);

// per ogni valore in dataStr estraggo valore da mettere in data
while (dataStrTmp.GetLength() > 0)
	{
	int commaPos = dataStrTmp.Find(_T(","));
	CString v;
	if (commaPos > 0)
		v = dataStrTmp.Left(commaPos);
	else
		v = dataStrTmp;
	data[dataIndex ++ ] = _tstoi((LPCTSTR)v);
	// tolgo parte analizzata
	dataStrTmp = dataStrTmp.Right(dataStrTmp.GetLength()-(v.GetLength()+1));
	}

size = dataIndex;
switch (cmd)
	{
	case 0 :
		break;
/* comandi ricevuti */
	case VAL_STATO :
		doc->gestValStatoHde(data,size);
		break;
	case VAL_ALARM :
		doc->gestValAlarm(data,size);
		break;
	case VAL_INPUT :
		doc->gestValInput(data,size);
		break;
	case VAL_HOLE :
		doc->gestValHole(data,size);
		break;
	case VAL_RISAUTOTEST :
		doc->gestValRisAutotest(data,size);
		break;
	case VAL_FT :
		doc->gestValFt(data,size);
		break;

/*
	case VAL_EXTCOUNTER :
		doc->gestValExtCounter(data,size);
		break;
	case VAL_PERIOD :
		doc->gestValPeriod(data,size);
		break;
	case VAL_STOP :
		doc->gestValStop(data,size);
		break;
	case VAL_OPENFILE :
		doc->gestValOpen(data,size);
		break;
	case VAL_CLOSEFILE :
		doc->gestValClose(data,size);
		break;
	case PACKET_ABORTED :
		{
		CString allarmeSistema;
		allarmeSistema.LoadString(CSM_MAINFRM_EGCOMUNICAZ);
		// allarmeSistema.Format("ERRORE GENERALE Comunicazioni\r\n");
		doc->systemAlarm.appendMsg(allarmeSistema);
		}
		break;
	case VAL_SPEED :
		gestValLineSpeed(data,size);
		break;
	case VAL_DIAFRAMMA :
		doc->gestValDiaframma(data,size);
		break;
	case VAL_EXTDIAFRAMMA :
		doc->gestValExtDiaframma(data,size);
		break;
*/
	default :
		{
		CString s;
		s.Format(_T("DEFAULT COMMAND = %d ( 0x%x )\n"),cmd,cmd);
		doc->systemAlarm.appendMsg(s);
		}
		break;
/* --------------------------------------*/	
	
	}
return TRUE;
}



void CMainFrame::OnViewRepaint() 
{
// TODO: Add your command handler code here
CLineDoc *pDoc;
pDoc = (CLineDoc *) GetActiveDocument();
pDoc->UpdateAllViews(NULL);	
}

//----------------------------------------------------------------------
//		Visualizzazione CDialogBar Allarmi
//

void CMainFrame::OnUpdateAlLaser(CCmdUI* pCmdUI)
{
// CG: This function was inserted by 'Status Bar' component.
if (::IsWindow (m_wndCAlarmBar.m_hWnd)) 
	{

	CStatic* pCtrl = (CStatic*) m_wndCAlarmBar.GetDlgItem (IDC_LASER);
	ASSERT(pCtrl->GetStyle() & SS_BITMAP);
	CBitmap bm;
	switch (c_stAllarmi.laser)
		{
		default:
		case AL_OFF :
			if (!bm.LoadBitmap (IDB_LASER_OFF))
				AfxMessageBox(_T("Can't Load bitmap!"));
			break;
		case AL_OK :
			bm.LoadBitmap(IDB_LASER_ON);
			break;
		case AL_ON :
			bm.LoadBitmap(IDB_LASER_FAULT);
			break;
		}
	pCtrl->SetBitmap ((HBITMAP)bm);
	bm.DeleteObject();
	}
}



void CMainFrame::OnUpdateAlEncoder(CCmdUI* pCmdUI)
{
// CG: This function was inserted by 'Status Bar' component.
if (::IsWindow(m_wndCAlarmBar.m_hWnd)) 
	{

	CStatic* pCtrl = (CStatic*) m_wndCAlarmBar.GetDlgItem (IDC_ENCODER);
	CBitmap bm;
		switch (c_stAllarmi.encoder)
		{
		default:
		case AL_OFF :
			bm.LoadBitmap (IDB_ENCODER_OFF);
			break;
		case AL_OK :
			bm.LoadBitmap (IDB_ENCODER_ON);
			break;
		case AL_ON :
			bm.LoadBitmap (IDB_ENCODER_FAULT);
			break;
		}
	pCtrl->SetBitmap ((HBITMAP)bm);
	bm.DeleteObject();
	}
}


void CMainFrame::OnUpdateAlOtturatore1(CCmdUI* pCmdUI)
{
// CG: This function was inserted by 'Status Bar' component.
if (::IsWindow (m_wndCAlarmBar.m_hWnd)) 
	{
	
	CStatic* pCtrl = (CStatic*) m_wndCAlarmBar.GetDlgItem (IDC_OTTURATORE1);
	CBitmap bm;
		switch (c_stAllarmi.otturatore1)
		{
		default:
		case AL_OFF :
			if(!bm.LoadBitmap (IDB_OTTURATORE_OFF))
				AfxMessageBox(_T("Can't Load bitmap!"));
				
			break;
		case AL_OK :
			bm.LoadBitmap (IDB_OTTURATORE_ON);
			break;
		case AL_ON :
			bm.LoadBitmap (IDB_OTTURATORE_FAULT);
			break;
		}
	pCtrl->SetBitmap ((HBITMAP)bm);
	bm.DeleteObject();
	}

}

void CMainFrame::OnUpdateAlOtturatore2(CCmdUI* pCmdUI)
{
// CG: This function was inserted by 'Status Bar' component.
if (::IsWindow (m_wndCAlarmBar.m_hWnd)) 
	{

	CStatic* pCtrl = (CStatic*) m_wndCAlarmBar.GetDlgItem (IDC_OTTURATORE2);
	CBitmap bm;
		switch (c_stAllarmi.otturatore2)
		{
		default:
		case AL_OFF :
			bm.LoadBitmap (IDB_OTTURATORE_OFF);
			break;
		case AL_OK :
			bm.LoadBitmap (IDB_OTTURATORE_ON);
			break;
		case AL_ON :
			bm.LoadBitmap (IDB_OTTURATORE_FAULT);
			break;
		}
	pCtrl->SetBitmap (bm);
	bm.DeleteObject();
	}

}



void CMainFrame::OnUpdateOtturatoreSx(CCmdUI *pCmdUI)
{
if (::IsWindow (m_wndCAlarmBar.m_hWnd)) 
	{
	
	CStatic* pCtrl = (CStatic*) m_wndCAlarmBar.GetDlgItem (IDC_OTTURATORE_SX);
	CBitmap bm;
	// 11-05-19 assegnato ftSporco1 ( era 2 )
		switch (c_stAllarmi.ftSporco1)
		{
		default:
		case AL_OFF :
			bm.LoadBitmap (IDB_OTTURATORE_OFF_LEFT);
			break;
		case AL_OK :
			bm.LoadBitmap (IDB_OTTURATORE_ON_LEFT);
			break;
		case AL_ON :
			bm.LoadBitmap (IDB_OTTURATORE_FAULT_LEFT);
			break;
		}
	pCtrl->SetBitmap (bm);
	bm.DeleteObject();
	
	}

}



void CMainFrame::OnUpdateOtturatoreDx(CCmdUI *pCmdUI)
{
// CG: This function was inserted by 'Status Bar' component.
if (::IsWindow (m_wndCAlarmBar.m_hWnd)) 
	{

	CStatic* pCtrl = (CStatic*) m_wndCAlarmBar.GetDlgItem (IDC_OTTURATORE_DX);
	CBitmap bm;
	// 11-05-19 assegnato ftSporco2 ( era 1 )
		switch (c_stAllarmi.ftSporco2)
		{
		default:
		case AL_OFF :
			bm.LoadBitmap (IDB_OTTURATORE_OFF_RIGHT);
			break;
		case AL_OK :
			bm.LoadBitmap (IDB_OTTURATORE_ON_RIGHT);
			break;
		case AL_ON :
			bm.LoadBitmap (IDB_OTTURATORE_FAULT_RIGHT);
			break;
		}
	pCtrl->SetBitmap (bm);
	bm.DeleteObject();
	}

}


void CMainFrame::OnUpdateAlCpuReceiver(CCmdUI* pCmdUI)
{
if (::IsWindow (m_wndCAlarmBar.m_hWnd)) 
	{

	CStatic* pCtrl = (CStatic*) m_wndCAlarmBar.GetDlgItem (IDC_CPURECEIVER);
	CDC *pdc; // select current bitmap into a compatible CDC
    pdc = pCtrl->GetDC ();
	ASSERT (pdc != NULL);
	if (pdc != NULL)
		{
		CRect rect;
		pCtrl->GetClientRect(&rect);
		c_stAllarmi.draw(pdc,rect);
		pCtrl->ReleaseDC(pdc);
		}
	}

}



void CMainFrame::OnUpdateFtBar(CCmdUI *pCmdUI)
{

CLineDoc *pDoc;
pDoc = (CLineDoc *) GetActiveDocument();

CStatic* pCtrl1 = (CStatic*) m_wndCAlarmBar.GetDlgItem (IDC_FT_VAL1);
if (pCtrl1 != NULL)
	{
	CString s;
	s.Format(_T("Ph  L  = %d"),pDoc->c_valFt1);
	pCtrl1->SetWindowText(s);
	}	
CStatic* pCtrl2 = (CStatic*) m_wndCAlarmBar.GetDlgItem (IDC_FT_VAL2);
if (pCtrl2 != NULL)
	{
	CString s;
	s.Format(_T("Ph  R = %d"),pDoc->c_valFt2);
	pCtrl2->SetWindowText(s);
	}	

}


//------------------------------------------------
//
//	Visualizzazione dello statoHDE in status Bar
//
void CMainFrame::OnUpdateStatus(CCmdUI* pCmdUI)
{
CString s;

CLineDoc *pDoc;
pDoc = (CLineDoc *) GetActiveDocument();

switch (pDoc->c_statoHde)
	{
	default :
	case HDE_STATE:
		// s = " HDE STATE ";
		s.LoadString(CSM_MAINFRM_HDESTDEFAULT);
		break;
	case HDE_WAITSTART:
		//s = " WAIT START ";
		s.LoadString(CSM_MAINFRM_HDESTWAITSTART);
		break;
	case HDE_WAITENABLE:
		//s = " WAIT ENABLE ";
		s.LoadString(CSM_MAINFRM_HDESTWAITENABLE);
		break;
	case HDE_INSPECTION:
		//s = " INSPECTION ";
		s.LoadString(CSM_MAINFRM_HDESTINSPECTION);
		break;
	case HDE_HOLD:
		//s = " HOLD ";
		s.LoadString(CSM_MAINFRM_HDESTHOLD);
		break;
	case HDE_CHANGE:
		//s = " CHANGE ";
		s.LoadString(CSM_MAINFRM_HDESTCHANGE);
		break;
	case HDE_SELFTEST:
		//s = " SELF TEST ";
		s.LoadString(CSM_MAINFRM_HDESTSELFTEST);
		break;
	case HDE_TEST:
		//s = " TEST ";
		s.LoadString(CSM_MAINFRM_HDESTTEST);
		break;
	case HDE_READY:
		s = " WAITING DIAPHRAGM ";
		// s.LoadString(CSM_MAINFRM_HDESTTEST);
		break;
	case HDE_TESTIO:
		s = " TESTING IO ";
		// s.LoadString(CSM_MAINFRM_HDESTTEST);
		break;	
	}

CSize size;
	{
	HGDIOBJ hOldFont = NULL;
	HFONT hFont = (HFONT)m_wndStatusBar.SendMessage(WM_GETFONT);
	CClientDC dc(NULL);
	if (hFont != NULL) 
		hOldFont = dc.SelectObject(hFont);
	size = dc.GetTextExtent(s);
	if (hOldFont != NULL) 
		dc.SelectObject(hOldFont);
	}

	// Update the pane to reflect the current state
UINT nID, nStyle;
int nWidth;
m_wndStatusBar.GetPaneInfo(2, nID, nStyle, nWidth);
nStyle |= SBPS_POPOUT; //  | SBT_OWNERDRAW ;
m_wndStatusBar.SetPaneInfo(2, nID, nStyle, size.cx);

pCmdUI->SetText(s);
pCmdUI->Enable(TRUE);
}

// visualizza VEOCITA` barra di stato
void CMainFrame::OnUpdateMeter(CCmdUI* pCmdUI)
{

CString speed; 
speed.Format (_T("Speed  %06.2lf m/min "),c_lineSpeed);

CSize size;
	{
	HGDIOBJ hOldFont = NULL;
	HFONT hFont = (HFONT)m_wndStatusBar.SendMessage(WM_GETFONT);
	CClientDC dc(NULL);
	if (hFont != NULL) 
		hOldFont = dc.SelectObject(hFont);
	size = dc.GetTextExtent(speed);
	if (hOldFont != NULL) 
		dc.SelectObject(hOldFont);
	}

	// Update the pane to reflect the current state
UINT nID, nStyle;
int nWidth;
m_wndStatusBar.GetPaneInfo(3, nID, nStyle, nWidth);
nStyle |= SBPS_POPOUT; //  | SBT_OWNERDRAW ;
m_wndStatusBar.SetPaneInfo(3, nID, nStyle, size.cx);

pCmdUI->SetText(speed);
pCmdUI->Enable(TRUE);
}


//----------------------------------------------
//		Finestre di allarmi
//----------------------------------------------
// attiva visualizzazione allarmi di sistema
//
void CMainFrame::OnViewSystem() 
{
// TODO: Add your command handler code here

CLineDoc *pDoc;
pDoc = (CLineDoc *) GetActiveDocument();

BOOL visible = (pDoc->systemAlarm.GetSafeHwnd() == NULL)?FALSE:TRUE;	

if (visible)
	{
	pDoc->systemAlarm.DestroyWindow();
	}
else
	{
	pDoc->systemAlarm.visualizza(this);
	}
}

void CMainFrame::OnUpdateViewSystem(CCmdUI* pCmdUI) 
{
CLineDoc *pDoc;
pDoc = (CLineDoc *) GetActiveDocument();

BOOL enable = (pDoc->systemAlarm.getNumLine() > 0)?TRUE:FALSE;	

pCmdUI->Enable(enable);	


BOOL visible = (pDoc->systemAlarm.GetSafeHwnd() == NULL)?FALSE:TRUE;	

pCmdUI->SetCheck(visible);	
}

void CMainFrame::OnViewTrend() 
{
	// TODO: Add your command handler code here
CLineDoc *pDoc;
pDoc = (CLineDoc *) GetActiveDocument();

BOOL visible = (pDoc->densitaAlarm.GetSafeHwnd() == NULL)?FALSE:TRUE;	

visible |= (pDoc->clusterAlarm.GetSafeHwnd() == NULL)?FALSE:TRUE;

if (visible)
	{
	pDoc->densitaAlarm.DestroyWindow();
	}
else
	{
	pDoc->densitaAlarm.visualizza(this);
	}

visible = (pDoc->clusterAlarm.GetSafeHwnd() == NULL)?FALSE:TRUE;

if (visible)
	{
	pDoc->clusterAlarm.DestroyWindow();
	}
else
	{
	pDoc->clusterAlarm.visualizza(this);
	}
}

void CMainFrame::OnUpdateViewTrend(CCmdUI* pCmdUI) 
{
	// TODO: Add your command update UI handler code here
CLineDoc *pDoc;
pDoc = (CLineDoc *) GetActiveDocument();

BOOL enable = (pDoc->densitaAlarm.getNumLine() > 0)?TRUE:FALSE;
	
enable |= (pDoc->clusterAlarm.getNumLine() > 0)?TRUE:FALSE;	

pCmdUI->Enable(enable);	


BOOL visible = (pDoc->densitaAlarm.GetSafeHwnd() == NULL)?FALSE:TRUE;
	
visible |= (pDoc->clusterAlarm.GetSafeHwnd() == NULL)?FALSE:TRUE;

pCmdUI->SetCheck(visible);	
	
}


//------------------------------------
//
//	Gestione Messaggi velocita` di linea
//			   
//------------------------------------

void CMainFrame::gestValLineSpeed (PVOID p,DWORD size) // Gestisce arrivo VAL_TESTSPEED
{
// Received Msg 
c_serMsgReceived = TRUE;


}




void CMainFrame::OnRemoteControl() 
{
// TODO: Add your command handler code here
CString s;

/*
if (c_remoteControl)
	s = "!!! ATTENZIONE !!!\n Collegare CSM10_AR ai segnali dell'impianto?";
else
	s = "!!! ATTENZIONE !!!\n Scollegare CSM10_AR dai segnali dell'impianto?";

if (AfxMessageBox((LPCTSTR)s,MB_ICONQUESTION | MB_YESNO)== IDNO)
	return;
c_remoteControl = !c_remoteControl;

Command command;
unsigned char cmdValue;

// Remote Control
cmdValue = (c_remoteControl?(unsigned char)1:(unsigned char)0);

command.cmd = VAL_REMOTE_CONTROL;
command.size = 1;
command.data = &cmdValue;

SendGeneralCommand(&command);

*/

}

void CMainFrame::OnRemotePausa() 
{
// TODO: Add your command handler code here
c_remotePausa = !c_remotePausa;
}

void CMainFrame::OnRemoteAbilitazione() 
{
// TODO: Add your command handler code here
c_remoteAbilitazione = !c_remoteAbilitazione;
}

void CMainFrame::OnRemoteAutotest() 
{
// TODO: Add your command handler code here
// Autotest 


if (AfxMessageBox(_T("Inviare Comando di AutoTest?"),MB_ICONQUESTION | MB_YESNO)== IDNO)
	return;

Command command;
command.cc = VAL_COMMAND;
command.nn =  AUTOTEST_CMD;

sendGeneralCommand(&command);

#ifdef _DEBUG
// Test
CLineDoc *pDoc;
pDoc = (CLineDoc *) GetActiveDocument();
if (pDoc->systemOnLine)
	{
	pDoc->appendSysMsg(HDE_SELFTEST);
	pDoc->UpdateAllViews(NULL);	
	}
//--------------------
#endif

}


void CMainFrame::OnRemoteCambio() 
{
// TODO: Add your command handler code here
// Cambio ASPO
/*
	
Command command;
unsigned char cmdValue;
if (AfxMessageBox("Inviare Comando di Cambio?",MB_ICONQUESTION | MB_YESNO)== IDNO)
	return;

// cambio 
cmdValue = (unsigned char) 1;

command.cmd = VAL_REMOTE_CAMBIO;
command.size = 1;
command.data = &cmdValue;

SendGeneralCommand(&command);

#ifdef _DEBUG
// Test
CLineDoc *pDoc;
pDoc = (CLineDoc *) GetActiveDocument();
if (pDoc->systemOnLine)
	{
	pDoc->appendSysMsg(HDE_CHANGE);
	pDoc->FileSave();
	pDoc->FileStart(TRUE);
	
	pDoc->UpdateAllViews(NULL);	
	}
//--------------------
#endif
*/

}

void CMainFrame::OnRemoteUpdate() 
{
// TODO: Add your command handler code here


/*
Command command;
unsigned char cmdValue;
CString s;
s.Format("Aggiornare il livello dei segnali?\nAbilitazione = %d\nPausa = %d\n",
	(c_remoteAbilitazione?1:0),
	(c_remotePausa?1:0));

if (AfxMessageBox((LPCTSTR)s,MB_ICONQUESTION | MB_YESNO)== IDNO)
	return;

// Abilitazione
cmdValue = (c_remoteAbilitazione?(unsigned char)1:(unsigned char)0);

command.cmd = VAL_REMOTE_ABILITAZIONE;
command.size = 1;
command.data = &cmdValue;

SendGeneralCommand(&command);

// Pausa
cmdValue = (c_remotePausa?(unsigned char)1:(unsigned char)0);

command.cmd = VAL_REMOTE_PAUSA;
command.size = 1;
command.data = &cmdValue;

SendGeneralCommand(&command);
*/

}

void CMainFrame::OnUpdateRemoteControl(CCmdUI* pCmdUI) 
{
// TODO: Add your command update UI handler code here
CProfile profile;

pCmdUI->Enable(profile.getProfileBool(_T("init"),_T("UseRemoteControl"),FALSE));
	
}

void CMainFrame::OnUpdateRemoteAbilitazione(CCmdUI* pCmdUI) 
{
// TODO: Add your command update UI handler code here
pCmdUI->Enable(c_remoteControl);
pCmdUI->SetCheck(c_remoteAbilitazione);
	
}

void CMainFrame::OnUpdateRemoteAutotest(CCmdUI* pCmdUI) 
{
// TODO: Add your command update UI handler code here
pCmdUI->Enable(c_remoteControl);
}


void CMainFrame::OnUpdateRemoteCambio(CCmdUI* pCmdUI) 
{
// TODO: Add your command update UI handler code here
pCmdUI->Enable(c_remoteControl);
}


void CMainFrame::OnUpdateRemotePausa(CCmdUI* pCmdUI) 
{
// TODO: Add your command update UI handler code here
pCmdUI->Enable(c_remoteControl);
pCmdUI->SetCheck(c_remotePausa);
}

void CMainFrame::OnUpdateRemoteUpdate(CCmdUI* pCmdUI) 
{
// TODO: Add your command update UI handler code here
pCmdUI->Enable(c_remoteControl);
}



void CMainFrame::OnConfiguraLocalizzazione() 
{
// TODO: Add your command handler code here
CProfile profile;
		
CString s;
//s = "Utilizzare Lingua Italiana?";
s.LoadString(SMSERVICE_ITALIAN_LANG);

if (AfxMessageBox((LPCTSTR)s,MB_YESNO | MB_ICONQUESTION) == IDYES)
	{
	profile.writeProfileString(_T("init"),_T("LangDll"),_T("NONE"));
	CString s;
	//s = "Per cambiare la lingua chiudere e riavviare il programma";
	s.LoadString(SMSERVICE_RESTART_LANG);

	AfxMessageBox((LPCTSTR)s,MB_ICONEXCLAMATION);
	return;
	}
	

// TODO: Add your command handler code here
CString szFilter ("dll Files (*.dll)|*.dll|All Files (*.*)|*.*||");

CFileDialog dialog (TRUE,(LPCTSTR) "*.dll",
	(LPCTSTR) "*.dll",0,(LPCTSTR) szFilter);

SetCurrentDirectory(_T("."));

if (dialog.DoModal() == IDOK)
	{
	CString dllName;
	dllName = dialog.GetPathName();
	profile.writeProfileString(_T("init"),_T("LangDll"),dllName);
	
	CString s;
	//s = "Per cambiare la lingua chiudere e riavviare il programma";
	s.LoadString(SMSERVICE_RESTART_LANG);
	AfxMessageBox((LPCTSTR)s,MB_ICONEXCLAMATION);
	}
}


void CMainFrame::OnConfiguraCluster() 
{
// TODO: Add your command handler code here
CDCluster dialog;
CLineDoc *pDoc;
pDoc = (CLineDoc *) GetActiveDocument();
if (pDoc != NULL)
	{
	dialog.setNumClasses(pDoc->c_difCoil.getNumClassi());
	}
// CG: This code was inserted by the Password Dialog Component
if (!CPasswordDlg::CheckPassword())
	return;


dialog.loadFromProfile();
if (dialog.DoModal() == IDOK)
	dialog.saveToProfile();
	
}


//----------------------------------------------
//		Finestre di allarmi
//----------------------------------------------
// attiva visualizzazione allarmi cluster

void CMainFrame::OnViewCluster() 
{
	// TODO: Add your command handler code here
CLineDoc *pDoc;
pDoc = (CLineDoc *) GetActiveDocument();

BOOL visible = (pDoc->clusterAlarm.GetSafeHwnd() == NULL)?FALSE:TRUE;	

if (visible)
	{
	pDoc->clusterAlarm.DestroyWindow();
	}
else
	{
	pDoc->clusterAlarm.visualizza(this);
	}
	
}


void CMainFrame::OnUpdateViewCluster(CCmdUI* pCmdUI) 
{
CLineDoc *pDoc;
pDoc = (CLineDoc *) GetActiveDocument();

BOOL enable = (pDoc->clusterAlarm.getNumLine() > 0)?TRUE:FALSE;	

pCmdUI->Enable(enable);	


BOOL visible = (pDoc->clusterAlarm.GetSafeHwnd() == NULL)?FALSE:TRUE;	

pCmdUI->SetCheck(visible);	
	
}

void CMainFrame::OnConfiguraColori() 
{
	// TODO: Add your command handler code here
CDClassColor dialog;
CLineDoc *pDoc;
pDoc = (CLineDoc *) GetActiveDocument();
if (pDoc != NULL)
	{
	dialog.setNumClasses(pDoc->c_difCoil.getNumClassi());
	}

dialog.loadFromProfile();

if (dialog.DoModal() == IDOK)
	{
	dialog.saveToProfile();
	CSM20ClassColorInvalidate();
	}
	
}

void CMainFrame::OnViewF4() 
{
// TODO: Add your command handler code here

	
CProfile profile;
CString procReport;
CString dirReport;

procReport = profile.getProfileString(_T("SERVER"), _T("ReportProg"), 
				_T("CSM10_ArReport.exe"));
dirReport = profile.getProfileString(_T("SERVER"), _T("ReportDir"), 
				_T("./"));
	

CWnd *report = CWnd::FindWindow(CSM_REPORT_CLASSNAME,NULL);
if (!report)
	{
	PROCESS_INFORMATION ProcessInformation;
	STARTUPINFO StartUpInfo;
	
	::GetStartupInfo (&StartUpInfo);
	BOOL fSuccess = CreateProcess((LPCTSTR)procReport,NULL,
				NULL,NULL,FALSE,NORMAL_PRIORITY_CLASS,
				NULL,(LPCTSTR)dirReport,
				&StartUpInfo,&ProcessInformation); 

	if (fSuccess) 
		{
		// Wait Creation Window RpcServer
		int res = WaitForInputIdle(ProcessInformation.hProcess,10000);// wait for 10 sec
		if (res != 0)
			{
			res = WaitForInputIdle(ProcessInformation.hProcess,10000);// wait for 10 sec
			if (res != 0)
				{
				CString s;
				s = "Fail Wait Idle Report " + procReport;
				AfxMessageBox (s);
				}
			}
	 	CloseHandle(ProcessInformation.hThread);
		CloseHandle(ProcessInformation.hProcess);
		CWnd *report = CWnd::FindWindow(CSM_REPORT_CLASSNAME,NULL);
		if(report)
			{
			report->ShowWindow(SW_SHOWMAXIMIZED);
			report->SetFocus( );
			report->SetWindowPos(&CWnd::wndTop, 0, 0, 0, 0,
			  SWP_NOMOVE|SWP_NOSIZE|SWP_SHOWWINDOW);
			}
		}
	else
		{
		CString s;
		s = "Unable to Create Process " + procReport;
		AfxMessageBox (s);
		return;
		}
	}
else
	{// already open set zOrder TopMost
	report->ShowWindow(SW_SHOWMAXIMIZED);
	report->SetFocus( );
	report->SetWindowPos(&CWnd::wndTop, 0, 0, 0, 0,
      SWP_NOMOVE|SWP_NOSIZE|SWP_SHOWWINDOW);
	}




}


// start encoder simulation
int CMainFrame::StartSimulEncoder(int millisec)
{

if (simulEncoderTimerId == 0)
	simulEncoderTimerId = SetTimer(1122,millisec,NULL);

return 0;
}


int CMainFrame::endSimulEncoder(void)
{

if (simulEncoderTimerId > 0)
	{
	KillTimer(simulEncoderTimerId);
	simulEncoderTimerId = 0;
	}
return 0;
}




