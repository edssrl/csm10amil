// DProduct.cpp : implementation file
//

#include "stdafx.h"
#include "GraphDoc.h"
#include "GraphView.h"
#include "dyntempl.h"
#include "CSM10_AR.h"
#include "DProduct.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CDProduct dialog


CDProduct::CDProduct(CWnd* pParent /*=NULL*/)
	: CDialog(CDProduct::IDD, pParent)
{
	//{{AFX_DATA_INIT(CDProduct)
	m_inspCode = _T("");
	m_coil = _T("");
	m_product = _T("");
	m_customer = _T("");
	m_nomeAsse = _T("");
	m_manualNomeAsse = -1;
	m_larghezza = _T("");
	m_spessore = _T("");
	//}}AFX_DATA_INIT
CProfile profile;
m_inspCode = profile.getProfileString (_T("OPTIONS"),_T("Inspection Code"),_T("Code A"));
m_coil = profile.getProfileString (_T("OPTIONS"),_T("Coil"),_T("1"));
m_product = profile.getProfileString (_T("OPTIONS"),_T("Product"),_T("PR100"));
m_customer = profile.getProfileString (_T("OPTIONS"),_T("Customer"),_T("Customer"));
m_larghezza = profile.getProfileString (_T("OPTIONS"),_T("Larghezza"),_T("1400.0"));
m_spessore = profile.getProfileString (_T("OPTIONS"),_T("Spessore"),_T("0.1"));

c_numericCoil = FALSE;

title = "";
}


void CDProduct::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CDProduct)
	DDX_Control(pDX, IDC_NOME_ASSE, m_ctrlNomeAsse);
	DDX_Control(pDX, IDC_COMBO_INSP_CODE, m_comboICode);
	DDX_CBString(pDX, IDC_COMBO_INSP_CODE, m_inspCode);
	DDX_Text(pDX, IDC_EDIT_COIL, m_coil);
	DDX_Text(pDX, IDC_EDIT_PRODUCT, m_product);
	DDX_Text(pDX, IDC_CUSTOMER, m_customer);
	DDV_MaxChars(pDX, m_customer, 20);
	DDX_Text(pDX, IDC_NOME_ASSE, m_nomeAsse);
	DDX_Radio(pDX, IDC_MANUAL_NOME_ASSE, m_manualNomeAsse);
	DDX_Text(pDX, IDC_LARGHEZZA, m_larghezza);
	DDX_Text(pDX, IDC_SPESSORE, m_spessore);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CDProduct, CDialog)
	//{{AFX_MSG_MAP(CDProduct)
	ON_BN_CLICKED(IDC_MANUAL_NOME_ASSE, OnManualNomeAsse)
	ON_BN_CLICKED(IDC_MANUALE, OnRadio2)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CDProduct message handlers

void CDProduct::OnOK() 
{
UpdateData(TRUE);
if (m_inspCode == _T(""))
	{
	Beep(1000,500);
	return;
	}

if (c_numericCoil)
	{// check coil tipo numerico
	if (m_coil.SpanExcluding(_T("0123456789")) != _T(""))
		{
		Beep(600,500);
		Sleep(100);
		Beep(1000,500);
		return;
		}
	}

CDialog::OnOK();
// TODO: Add extra validation here

CProfile profile;
profile.writeProfileString (_T("OPTIONS"),_T("Inspection Code"),m_inspCode);
profile.writeProfileString (_T("OPTIONS"),_T("Coil"),m_coil);
profile.writeProfileString (_T("OPTIONS"),_T("Product"),m_product );
profile.writeProfileString (_T("OPTIONS"),_T("Customer"),m_customer);

CString s;
//s.Format("%3.01lf",m_larghezza);
s = m_larghezza;
profile.writeProfileString (_T("OPTIONS"),_T("Larghezza"),s);
//s.Format("%3.01lf",m_spessore);
s = m_spessore;
profile.writeProfileString (_T("OPTIONS"),_T("Spessore"),s);

	
}

BOOL CDProduct::OnInitDialog() 
{
CDialog::OnInitDialog();
	
// TODO: Add extra initialization here

// m_comboICode.ResetContent();
for (int i=0;i<c_nomiRicette.GetSize();i++)
	{
	m_comboICode.AddString(c_nomiRicette[i]);
	}

if (title != "")
	SetWindowText(title);
	
m_ctrlNomeAsse.EnableWindow(m_manualNomeAsse);	
m_comboICode.SelectString(-1,m_inspCode);

return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CDProduct::OnManualNomeAsse() 
{
// TODO: Add your control notification handler code here

m_ctrlNomeAsse.EnableWindow(FALSE);	

}

void CDProduct::OnRadio2() 
{
// TODO: Add your control notification handler code here
m_ctrlNomeAsse.EnableWindow(TRUE);	
	
}
