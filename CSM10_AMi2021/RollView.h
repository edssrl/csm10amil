#if !defined(AFX_ROLLVIEW_H__49569FF0_D399_11D1_A6B1_00C026A019B7__INCLUDED_)
#define AFX_ROLLVIEW_H__49569FF0_D399_11D1_A6B1_00C026A019B7__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// RollView.h : header file
//

#include "Target.h"

/////////////////////////////////////////////////////////////////////////////
// CRollView view

class CRollView : public CView
{
protected:
	CRollView();           // protected constructor used by dynamic creation
	DECLARE_DYNCREATE(CRollView)
ViewType vType;

// Attributes
public:
Layout  layout;
// CListCtrlUp	c_listCtrl;
// CTargetBoard c_targetBoard;
// Numero di righe nuove da aggiornare su video
// settato da fillCtrl
// azzerato da draw
//int c_newRow;
//BOOL c_repaintAll;
//BOOL c_repaintLock;
// Dimensione pallini
int c_pxBRoll;
// Operations
public:
	void draw(void){CClientDC dc(this);OnDraw(&dc);};
	ViewType getViewType(void) {return(vType);};
	int fillCtrl(int nmaxRow);
	CRect getDrawRect(void);
	CRect getMeterRect(void);
	void drawPosDiaframma(CDC *pDC, CRect rectB);

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CRollView)
	public:
	virtual void OnInitialUpdate();
	virtual BOOL Create(LPCTSTR lpszClassName, LPCTSTR lpszWindowName, DWORD dwStyle, const RECT& rect, CWnd* pParentWnd, UINT nID, CCreateContext* pContext = NULL);
	protected:
	virtual void OnDraw(CDC* pDC);      // overridden to draw this view
	virtual void OnUpdate(CView* pSender, LPARAM lHint, CObject* pHint);
	//}}AFX_VIRTUAL

// Implementation
protected:
	virtual ~CRollView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

	// Generated message map functions
protected:
	//{{AFX_MSG(CRollView)
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_ROLLVIEW_H__49569FF0_D399_11D1_A6B1_00C026A019B7__INCLUDED_)
