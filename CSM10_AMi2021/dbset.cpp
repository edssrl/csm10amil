// DBSet.cpp : implementation file
//

#include "stdafx.h"
#include "MainFrm.h"
#include "resource.h"

#include "Dialog.h"

#include "GraphDoc.h"
#include "GraphView.h"
#include "dyntempl.h"

#include "CSM10_AR.h"
#include "DBSet.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// DBRicette

IMPLEMENT_DYNAMIC(DBRicette, CDaoRecordset)

DBRicette::DBRicette(CDaoDatabase* pdb)
	: CDaoRecordset(pdb)
{
	//{{AFX_FIELD_INIT(DBRicette)
	m_NOME = _T("");
	m_ALLARME_A = 0.0;
	m_ALLARME_B = 0.0;
	m_ALLARME_C = 0.0;
	m_ALLARME_D = 0.0;
	m_SPESSORE_MIN = 0.0;
	m_SPESSORE_MAX = 0.0;
	m_LEGA = _T("");
	m_LENGTH_ALLARME = 0.0;
	m_nFields = 9;
	//}}AFX_FIELD_INIT
	m_nDefaultType = dbOpenDynaset;

}


CString DBRicette::GetDefaultDBName()
{
	return _T("HOLE.MDB");
}

CString DBRicette::GetDefaultSQL()
{
	return _T("[RICETTE]");
}

void DBRicette::DoFieldExchange(CDaoFieldExchange* pFX)
{
	//{{AFX_FIELD_MAP(DBRicette)
	pFX->SetFieldType(CDaoFieldExchange::outputColumn);
	DFX_Text(pFX, _T("[NOME]"), m_NOME);
	DFX_Double(pFX, _T("[ALLARME_A]"), m_ALLARME_A);
	DFX_Double(pFX, _T("[ALLARME_B]"), m_ALLARME_B);
	DFX_Double(pFX, _T("[ALLARME_C]"), m_ALLARME_C);
	DFX_Double(pFX, _T("[ALLARME_D]"), m_ALLARME_D);
	DFX_Double(pFX, _T("[SPESSORE_MIN]"), m_SPESSORE_MIN);
	DFX_Double(pFX, _T("[SPESSORE_MAX]"), m_SPESSORE_MAX);
	DFX_Text(pFX, _T("[LEGA]"), m_LEGA);
	DFX_Double(pFX, _T("[LENGTH_ALLARME]"), m_LENGTH_ALLARME);
	//}}AFX_FIELD_MAP
}

/////////////////////////////////////////////////////////////////////////////
// DBRicette diagnostics

#ifdef _DEBUG
void DBRicette::AssertValid() const
{
	CDaoRecordset::AssertValid();
}

void DBRicette::Dump(CDumpContext& dc) const
{
	CDaoRecordset::Dump(dc);
}
#endif //_DEBUG


void DBRicette::Close() 
{
	// TODO: Add your specialized code here and/or call the base class


CDaoRecordset::Close();

}

BOOL DBRicette::openSelectID (CString &key,BOOL closeNotFound)
{
// TODO: Add your specialized code here and/or call the base class
// win10 check if db is open
if (!isDbOpen())
	return FALSE;

CString sqlString;
sqlString.Format(_T("SELECT * FROM %s WHERE NOME = \'%s\'"),
		GetDefaultSQL(),key);
try
	{Open (dbOpenDynaset,sqlString);} 
catch
	( CDaoException)
	{
	AfxMessageBox (_T("Error opening RecordSet"));
	return FALSE;
	}

// Check Record Found
if (IsEOF() && closeNotFound)
	{// Selezione Tabella Default manuale
	// Close Old query and DB
	Close();
	return FALSE;
	}

return TRUE;
}


BOOL DBRicette::openSelectName (CArray <CString,CString &> &keyName)
{
// win10 check if db is open
if (!isDbOpen())
	return FALSE;

CString sqlString;
sqlString.Format(_T("SELECT * FROM %s "),
		GetDefaultSQL());
try
	{Open (dbOpenDynaset,sqlString);} 
catch
	( CDaoException)
	{
	AfxMessageBox (_T("Error opening RecordSet"));
	return FALSE;
	}

for (int i=0;i<GetRecordCount();i++)
	{
	m_NOME.TrimRight();
	keyName.Add(m_NOME);
	MoveNext();
	}

Close();

return TRUE;
}



BOOL DBRicette::openSelectDefault (BOOL closeNotFound,BOOL addOnEmpty)
{
// win10 check if db is open
if (!isDbOpen())
	return FALSE;

CString sqlString;
sqlString.Format(_T("SELECT * FROM %s"),GetDefaultSQL());
try
	{Open (dbOpenDynaset,sqlString);} 
catch
	( CDaoException)
	{
	AfxMessageBox (_T("Error opening RecordSet"));
	return FALSE;
	}

// Check Record Found
if (IsEOF() && closeNotFound)
	{// Selezione Tabella Default manuale
	if (addOnEmpty&&CanAppend())
		{
		AddNew();
		m_NOME = _T("DEFAULT");
		m_ALLARME_A = 10.0;
		m_ALLARME_B = 10.0;
		m_ALLARME_C = 10.0;
		m_ALLARME_D = 10.0;
		m_SPESSORE_MIN = 1.0;
		m_SPESSORE_MAX = 100.0;
		m_LEGA = _T("X");
		Update();
		MoveFirst();
		return TRUE;
		}
	// Close Old query and DB
	Close();
	return FALSE;
	}

return TRUE;
}


/////////////////////////////////////////////////////////////////////////////
// DBOpzioni

IMPLEMENT_DYNAMIC(DBOpzioni, CDaoRecordset)

DBOpzioni::DBOpzioni(CDaoDatabase* pdb)
	: CDaoRecordset(pdb)
{
	//{{AFX_FIELD_INIT(DBOpzioni)
	m_NOME = _T("");
	m_ALLARME_ACU = FALSE;
	m_ALLARME_DURATA = 0.0;
	m_STAMPA_AUTO = FALSE;
	m_TL_CLASSE = _T("");
	m_TL_LUNGHEZZA = 0.0;
	m_TD_DENSITA = 0.0;
	m_TD_LUNGHEZZA = 0.0;
	m_TD_SCALA = 0.0;
	m_TL_SCALA = 0.0;
	m_MAPPA100 = FALSE;
	m_nFields = 11;
	//}}AFX_FIELD_INIT
	m_nDefaultType = dbOpenDynaset;
	// m_strFilter = "NOME = DEFAULT";

}


CString DBOpzioni::GetDefaultDBName()
{
	return _T("HOLE.MDB");
}

CString DBOpzioni::GetDefaultSQL()
{
	return _T("[OPZIONI]");
}

void DBOpzioni::DoFieldExchange(CDaoFieldExchange* pFX)
{
	//{{AFX_FIELD_MAP(DBOpzioni)
	pFX->SetFieldType(CDaoFieldExchange::outputColumn);
	DFX_Text(pFX, _T("[NOME]"), m_NOME);
	DFX_Bool(pFX, _T("[ALLARME_ACU]"), m_ALLARME_ACU);
	DFX_Double(pFX, _T("[ALLARME_DURATA]"), m_ALLARME_DURATA);
	DFX_Bool(pFX, _T("[STAMPA_AUTO]"), m_STAMPA_AUTO);
	DFX_Text(pFX, _T("[TL_CLASSE]"), m_TL_CLASSE);
	DFX_Double(pFX, _T("[TL_LUNGHEZZA]"), m_TL_LUNGHEZZA);
	DFX_Double(pFX, _T("[TD_DENSITA]"), m_TD_DENSITA);
	DFX_Double(pFX, _T("[TD_LUNGHEZZA]"), m_TD_LUNGHEZZA);
	DFX_Double(pFX, _T("[TD_SCALA]"), m_TD_SCALA);
	DFX_Double(pFX, _T("[TL_SCALA]"), m_TL_SCALA);
	DFX_Bool(pFX, _T("[MAPPA100]"), m_MAPPA100);
	//}}AFX_FIELD_MAP
}

/////////////////////////////////////////////////////////////////////////////
// DBOpzioni diagnostics

#ifdef _DEBUG
void DBOpzioni::AssertValid() const
{
	CDaoRecordset::AssertValid();
}

void DBOpzioni::Dump(CDumpContext& dc) const
{
	CDaoRecordset::Dump(dc);
}
#endif //_DEBUG



void DBOpzioni::Close() 
{
	// TODO: Add your specialized code here and/or call the base class
	
CDaoRecordset::Close();

}

BOOL DBOpzioni::openSelectDefault (BOOL closeNotFound,BOOL addOnEmpty)
{
// win10 check if db is open
if (!isDbOpen())
	return FALSE;

CString sqlString;
sqlString.Format(_T("SELECT * FROM %s"),GetDefaultSQL());
try
	{Open (dbOpenDynaset,sqlString);} 
catch
	( CDaoException)
	{
	AfxMessageBox (_T("Error opening RecordSet"));
	return FALSE;
	}

// Check Record Found
if (IsEOF() && closeNotFound)
	{// Selezione Tabella Default manuale
	// Close Old query and DB
	if ((addOnEmpty)&&(CanAppend()))
		{
		AddNew();
		m_NOME = _T("DEFAULT");
		m_ALLARME_ACU = FALSE;
		m_ALLARME_DURATA = 10.0;
		m_STAMPA_AUTO = FALSE;
		m_TL_CLASSE = _T("A");
		m_TL_LUNGHEZZA = 100.0;
		m_TD_DENSITA = 100.0;
		m_TD_LUNGHEZZA = 100.0;
		m_TD_SCALA = 100.0;
		m_TL_SCALA = 100.0;
		m_MAPPA100 = FALSE;
		Update();
		MoveFirst();
		return TRUE;
		}
	Close();
	return FALSE;
	}

return TRUE;
}

/////////////////////////////////////////////////////////////////////////////
// DBParametri

IMPLEMENT_DYNAMIC(DBParametri, CDaoRecordset)

DBParametri::DBParametri(CDaoDatabase* pdb)
	: CDaoRecordset(pdb)
{
	//{{AFX_FIELD_INIT(DBParametri)
	m_NOME = _T("");
	m_FCDL = 0.0;
	m_L_RESIDUA_STOP = 0.0;
	m_SOGLIA_A = 0.0;
	m_SOGLIA_B = 0.0;
	m_SOGLIA_C = 0.0;
	m_SOGLIA_D = 0.0;
	m_ALARM_PULSE = 0.0;
	m_ALARM_CLASS = 0;
	m_nFields = 9;
	//}}AFX_FIELD_INIT
	m_nDefaultType = dbOpenDynaset;


}


CString DBParametri::GetDefaultDBName()
{
	return _T("HOLE.MDB");
}

CString DBParametri::GetDefaultSQL()
{
	return _T("[PARAMETRI]");
}

void DBParametri::DoFieldExchange(CDaoFieldExchange* pFX)
{
	//{{AFX_FIELD_MAP(DBParametri)
	pFX->SetFieldType(CDaoFieldExchange::outputColumn);
	DFX_Text(pFX, _T("[NOME]"), m_NOME);
	DFX_Double(pFX, _T("[FCDL]"), m_FCDL);
	DFX_Double(pFX, _T("[L_RESIDUA_STOP]"), m_L_RESIDUA_STOP);
	DFX_Double(pFX, _T("[SOGLIA_A]"), m_SOGLIA_A);
	DFX_Double(pFX, _T("[SOGLIA_B]"), m_SOGLIA_B);
	DFX_Double(pFX, _T("[SOGLIA_C]"), m_SOGLIA_C);
	DFX_Double(pFX, _T("[SOGLIA_D]"), m_SOGLIA_D);
	DFX_Double(pFX, _T("[ALARM_PULSE]"), m_ALARM_PULSE);
	DFX_Long(pFX, _T("[ALARM_CLASS]"), m_ALARM_CLASS);
	//}}AFX_FIELD_MAP
}

/////////////////////////////////////////////////////////////////////////////
// DBParametri diagnostics

#ifdef _DEBUG
void DBParametri::AssertValid() const
{
	CDaoRecordset::AssertValid();
}

void DBParametri::Dump(CDumpContext& dc) const
{
	CDaoRecordset::Dump(dc);
}
#endif //_DEBUG


void DBParametri::Close() 
{
	// TODO: Add your specialized code here and/or call the base class
	
CDaoRecordset::Close();
}

BOOL DBParametri::openSelectDefault (BOOL closeNotFound,BOOL addOnEmpty)
{
// win10 check if db is open
if (!isDbOpen())
	return FALSE;

CString sqlString;
sqlString.Format(_T("SELECT * FROM %s"),GetDefaultSQL());
try
	{Open (dbOpenDynaset,sqlString);} 
catch
	( CDaoException)
	{
	AfxMessageBox (_T("Error opening RecordSet"));
	return FALSE;
	}


// Check Record Found
if (IsEOF() && closeNotFound)
	{// Selezione Tabella Default manuale
	if (addOnEmpty && CanAppend())
		{
		AddNew();
		m_NOME = _T("DEFAULT");
		m_FCDL = 1.0;
		m_L_RESIDUA_STOP = 0.0;
		m_SOGLIA_A = 10.0;
		m_SOGLIA_B = 40.0;
		m_SOGLIA_C = 100.0;
		m_SOGLIA_D = 500.0;
		m_ALARM_PULSE = 10.;
		Update();
		MoveFirst();
		return TRUE;
		}
	// Close Old query and DB
	Close();
	return FALSE;
	}

return TRUE;
}




/////////////////////////////////////////////////////////////////////////////
// DBReport

IMPLEMENT_DYNAMIC(DBReport, CDaoRecordset)

DBReport::DBReport(CDaoDatabase* pdb)
	: CDaoRecordset(pdb)
{
	//{{AFX_FIELD_INIT(DBReport)
	m_NOME = _T("");
	m_DATI_CLIENTE = FALSE;
	m_DATA_LAVORAZIONE = FALSE;
	m_LUNGHEZZA_BOBINA = FALSE;
	m_DESCR_PERIODICI = FALSE;
	m_DESCR_RANDOM = FALSE;
	m_SOGLIE_ALLARME = FALSE;
	m_MAPPA100 = FALSE;
	m_DALMETRO = 0.0;
	m_ALMETRO = 0.0;
	m_INTERVALLO1 = 0.0;
	m_INTERVALLO2 = 0.0;
	m_INTERVALLO3 = 0.0;
	m_DESCR_CROSSWEB = FALSE;
	m_DESCR_DOWNWEB = FALSE;
	m_DESCR_THRESHOLD = FALSE;
	m_DESCR_COMPRESSION = FALSE;
	m_nFields = 17;
	//}}AFX_FIELD_INIT
	m_nDefaultType = dbOpenDynaset;

}


CString DBReport::GetDefaultDBName()
{
	return _T("HOLE.MDB");
}

CString DBReport::GetDefaultSQL()
{
	return _T("[REPORT]");
}

void DBReport::DoFieldExchange(CDaoFieldExchange* pFX)
{
	//{{AFX_FIELD_MAP(DBReport)
	pFX->SetFieldType(CDaoFieldExchange::outputColumn);
	DFX_Text(pFX, _T("[NOME]"), m_NOME);
	DFX_Bool(pFX, _T("[DATI_CLIENTE]"), m_DATI_CLIENTE);
	DFX_Bool(pFX, _T("[DATA_LAVORAZIONE]"), m_DATA_LAVORAZIONE);
	DFX_Bool(pFX, _T("[LUNGHEZZA_BOBINA]"), m_LUNGHEZZA_BOBINA);
	DFX_Bool(pFX, _T("[DESCR_PERIODICI]"), m_DESCR_PERIODICI);
	DFX_Bool(pFX, _T("[DESCR_RANDOM]"), m_DESCR_RANDOM);
	DFX_Bool(pFX, _T("[SOGLIE_ALLARME]"), m_SOGLIE_ALLARME);
	DFX_Bool(pFX, _T("[MAPPA100]"), m_MAPPA100);
	DFX_Double(pFX, _T("[DALMETRO]"), m_DALMETRO);
	DFX_Double(pFX, _T("[ALMETRO]"), m_ALMETRO);
	DFX_Double(pFX, _T("[INTERVALLO1]"), m_INTERVALLO1);
	DFX_Double(pFX, _T("[INTERVALLO2]"), m_INTERVALLO2);
	DFX_Double(pFX, _T("[INTERVALLO3]"), m_INTERVALLO3);
	DFX_Bool(pFX, _T("[DESCR_CROSSWEB]"), m_DESCR_CROSSWEB);
	DFX_Bool(pFX, _T("[DESCR_DOWNWEB]"), m_DESCR_DOWNWEB);
	DFX_Bool(pFX, _T("[DESCR_THRESHOLD]"), m_DESCR_THRESHOLD);
	DFX_Bool(pFX, _T("[DESCR_COMPRESSION]"), m_DESCR_COMPRESSION);
	//}}AFX_FIELD_MAP
}

/////////////////////////////////////////////////////////////////////////////
// DBReport diagnostics

#ifdef _DEBUG
void DBReport::AssertValid() const
{
	CDaoRecordset::AssertValid();
}

void DBReport::Dump(CDumpContext& dc) const
{
	CDaoRecordset::Dump(dc);
}
#endif //_DEBUG



void DBReport::Close() 
{
	// TODO: Add your specialized code here and/or call the base class
if (isDbOpen())	
	CDaoRecordset::Close();


}

BOOL DBReport::openSelectDefault (BOOL closeNotFound,BOOL addOnEmpty)
{
// win10 check if db is open
if (!isDbOpen())
	return FALSE;

CString sqlString;
sqlString.Format(_T("SELECT * FROM %s"),GetDefaultSQL());
try
	{Open (dbOpenDynaset,sqlString);} 
catch
	( CDaoException)
	{
	AfxMessageBox (_T("Error opening RecordSet"));
	return FALSE;
	}

// Check Record Found
if (IsEOF() && closeNotFound)
	{// Selezione Tabella Default manuale
	if (addOnEmpty&&CanAppend())
		{
		AddNew();
		m_NOME = _T("DEFAULT");
		m_DATI_CLIENTE = FALSE;
		m_DATA_LAVORAZIONE = FALSE;
		m_LUNGHEZZA_BOBINA = FALSE;
		m_DESCR_PERIODICI = FALSE;
		m_DESCR_RANDOM = FALSE;
		m_SOGLIE_ALLARME = FALSE;
		m_MAPPA100 = FALSE;
		m_DALMETRO = 0.0;
		m_ALMETRO = 1000.0;
		m_INTERVALLO1 = 100.0;
		m_INTERVALLO2 = 100.0;
		m_INTERVALLO3 = 100.0;
		Update();
		MoveFirst();
		return TRUE;
		}
	// Close Old query and DB
	Close();
	return FALSE;
	}


return TRUE;
}
