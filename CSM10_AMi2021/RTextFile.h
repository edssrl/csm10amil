//------------------------------------------------------
//
//
//					RTextFile
//
// Legge ed interpreta File Testo formattati per linee
//
//------------------------------------------------------

class TextFile
{
CFile file;		// CFile object
CString nome;	// Nome File
CString separator;	// 
BOOL opened;
int numField;
//--------------------------------
// posizione inizio ultima riga letta da readline
ULONGLONG	c_linePosition;
// posizione nella linea dell'inizio del campo da fieldNumFromLine
ULONG		c_fieldOffsetInLine;
ULONGLONG   calcLastFieldOffsetInFile(void)
	{return(c_linePosition + c_fieldOffsetInLine);};
//-----------------------------------------
// posizione dell'estensione della chiave
// Prima le righe erano identificate solo dal primo campo della riga
// ora e` possibile associare un secondo campo come estensione della chiave
int c_extKeyPosition;
void TextFile::readLine (CString &line);
BOOL fieldNumFromLine (CString &line,int num,CString &field);

public:
	int calcNumFields(LPCTSTR sLine);
	void setExtKeyPosition(int pos);
TextFile (void) {numField = 0;opened = FALSE;separator = _T(",\r\n");
		c_extKeyPosition=-1;};
~TextFile (void) {if (opened) close();};

void addSeparator (char c){separator.Empty();separator += c;};
BOOL open (CString &name, CFile::OpenFlags mode = CFile::modeRead);
BOOL close (void);
// Aggiunta estensione di chiave
BOOL readField (CString &key,int field,CString &value,LPCTSTR keyExt=NULL);
BOOL readField (CString &key,int field,int &value,LPCTSTR keyExt=NULL);
BOOL readField (CString &key,int field,double &value,LPCTSTR keyExt=NULL);
// getLastKey cerca ultima chiave: specifico ilva ArcelorMittal
BOOL getLastKey(CString fType,CString &key,CString &subKey);

// modifica il valore di un campo. ( vincolo stessa lunghezza campo esistente )
bool writeField (CString key,int num,CString value,LPCTSTR keyExt);
}
;



