// RollView.cpp : implementation file
//

#include "stdafx.h"
#include "MainFrm.h"
#include "resource.h"
#include "Profile.h"

#include <AfxTempl.h>

#include "GraphDoc.h"
#include "GraphView.h"
#include "dyntempl.h"
#include "CSM10_AR.h"
// #include "ctrlext.h"
// #include "ListCtrlUp.h"

#include "RollView.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CRollView

IMPLEMENT_DYNCREATE(CRollView, CView)

CRollView::CRollView()
{
vType = ROLLVIEW;

layout.setViewType(ROLLVIEW);
layout.setMode(NOLABELV);

// Init layout Fix Attributes
CProfile profile;
CString s;


layout.fCaption.size = profile.getProfileInt(_T("RollmapLayout"),_T("CaptionSize"),120);
layout.fLabel.size = profile.getProfileInt(_T("RollmapLayout"),_T("LabelSize"),100);
layout.fNormal.size = profile.getProfileInt(_T("RollmapLayout"),_T("NormalSize"),110);


s = profile.getProfileString(_T("RollmapLayout"),_T("NewLine"),_T("3,6"));
while (s.GetLength() >0)
	{
	CString subS;
	subS = s.SpanExcluding(_T(",;"));
	s = s.Right(s.GetLength()-subS.GetLength()-1);
	int v;
	_stscanf((LPCTSTR)subS,_T("%d"),&v);
	if ((v < MAX_NUMLABEL)&&(v>=0))
		layout.c_nlAfterLabel [v] = 1;
	}

s = profile.getProfileString(_T("RollmapLayout"),_T("LabelOrder"),_T("0,1,2,3,4,5,6,7,8,9"));
int k = 0;
while (s.GetLength() >0)
	{
	CString subS;
	subS = s.SpanExcluding(_T(",;"));
	s = s.Right(s.GetLength()-subS.GetLength()-1);
	int v;
	_stscanf((LPCTSTR)subS,_T("%d"),&v);
	if ((v < MAX_NUMLABEL)&&(v>=0))
		layout.c_vOrderLabel [k++] = v;
	if (k>=MAX_NUMLABEL)
		break;
	}

s = profile.getProfileString(_T("RollmapLayout"),_T("PixelSepLabel"),
							 _T("10,10,10,10,10,10,10,10,10,10"));
k = 0;
while (s.GetLength() >0)
	{
	CString subS;
	subS = s.SpanExcluding(_T(",;"));
	s = s.Right(s.GetLength()-subS.GetLength()-1);
	int v;
	_stscanf((LPCTSTR)subS,_T("%d"),&v);
	layout.c_pxSepLabel [k++] = v;
	if (k>9)
		break;
	}

s = profile.getProfileString(_T("RollmapLayout"),_T("SCaption"),
							 _T("ROLLING MAP"));
layout.setCaption(s);
for (int i=0;i<MAX_NUMLABEL;i++)
	{
	CString sIndex;
	sIndex.Format (_T("SLabel%d"),i);
	s = profile.getProfileString(_T("RollmapLayout"),sIndex,sIndex);
	if (s != sIndex)
		layout.c_sLabel[i] = s;
	}

for (int i=0;(i<4);i++)
	{
	CString s;
	s = TCHAR('A'+i);
	layout.c_cLabel[i] = s;
	layout.c_cLabel[i].setTColor(CSM20GetClassColor(i));
	layout.c_cLabel[i].setTipoIcon(IPALLINO);
	}

// CalcDrawRect
layout.c_viewTopPerc = profile.getProfileInt(_T("RollmapLayout"),_T("ViewTop%"),10);
layout.c_viewBottomPerc = profile.getProfileInt(_T("RollmapLayout"),_T("ViewBottom%"),25);
layout.c_viewLeftPerc = profile.getProfileInt(_T("RollmapLayout"),_T("ViewLeft%"),8);
layout.c_viewRightPerc = profile.getProfileInt(_T("RollmapLayout"),_T("ViewRight%"),5);

// dimensione pallini
c_pxBRoll = profile.getProfileInt(_T("RollmapLayout"),_T("RollSize"),12);



// Don't update internal background
layout.setRgbDrawBackColor(RGB(0,0,0));

}

CRollView::~CRollView()
{
}


BEGIN_MESSAGE_MAP(CRollView, CView)
	//{{AFX_MSG_MAP(CRollView)
	ON_WM_ERASEBKGND()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CRollView drawing

CRect CRollView::getDrawRect(void)
{
CRect rcBounds,rect;

GetClientRect (rcBounds);
layout.CalcDrawRect(rcBounds,rect);
return (rect);
}



void CRollView::OnDraw(CDC* pDC)
{
// TODO: add draw code here
DOC_CLASS* pDoc = (DOC_CLASS* )GetDocument();
// TODO: add draw code here
// Call Base Class Draw
CRect rectB;
CRect rcBounds;
GetClientRect (rcBounds);
// Set base Rect
layout.setDrawRect(rcBounds);
// Call Base Class Draw
layout.setMaxY(pDoc->GetTrendDScalaY());
layout.setMinY(0.);

//-----------------------------------------
// Init vLabel 
// layout.setVFori	(pDoc->getFori());
layout.c_vLabel[0] = pDoc->GetVPosition();
layout.c_vLabel [1] = pDoc->GetTrendLVPositionDal();
layout.c_vLabel [2] = pDoc->GetVCliente();
layout.c_vLabel [3] = 0;		// maxValX 
layout.c_vLabel [4] = pDoc->getRotolo();
layout.c_vLabel [5] = pDoc->lega;
CTime d(pDoc->startTime);
layout.c_vLabel [6] = d.Format( _T("%A, %B %d, %Y") );
// visualizzazione threshold
layout.c_vLabel [7] = pDoc->formatStringThreshold();

// contatori a,b c,d,
CString s;
for(int i=0;i<pDoc->c_difCoil.getNumClassi();i++)
	{
	if (i>0)
		s += ",";
	CString s1;
	// contatore 
	s1.Format(_T(" %c=%2.0lf"),'A'+i,pDoc->c_difCoil.getTotDifetti('A'+i,0,(int)pDoc->GetVPosition()));
	s += s1;	
	}
layout.c_vLabel [8] = s;
//------------------------
// densita`
for(int i=0;i<pDoc->c_difCoil.getNumClassi();i++)
	{
	CString s1;
	// densita`
	double densita=pDoc->c_difCoil.getDensityLevel('A'+i,pDoc->c_densityCalcLength);
	s1.Format(_T(" %c=%3.0lf"),'A'+i,densita);
	layout.c_vLabel [9+i] = s1;
	if (densita > pDoc->c_difCoil.getAllarme ('A'+i))
		{
		layout.c_vLabel [9+i].setTColor(RGB(255,0,0));
		}
	else
		{
		layout.c_vLabel [9+i].setTColor(RGB(0,0,0));
		}

	}
//------------------------
// lunghezza di base per calcolo densita`
s.Format(_T("%4.0lf"),pDoc->c_densityCalcLength);
layout.c_vLabel [13] = s;

int numClassi = pDoc->c_difCoil.getNumClassi();

// cluster  label 14,15,16,17
for (int i=0;i<numClassi;i++)
	{	
	s.Format(_T("%d"),pDoc->c_clusterCounter[i]);
	layout.c_vLabel [14+i] = s;
	}

layout.setNumClassi(numClassi);
layout.setNumSchede(pDoc->c_difCoil.getNumSchede());

layout.draw(pDC);
layout.CalcDrawRect(rcBounds,rectB); 

// Disegno Posizione diaframmi
drawPosDiaframma(pDC,rectB);


	{
	//layout.OnEraseDrawBkgnd(pDC);
	pDoc->c_targetBoard.setDrawAttrib((int)layout.c_sizeFBanda,
							(int)layout.c_sizeBanda,
							c_pxBRoll,layout.c_bandePerCol);
	CRect clipRect(rectB);
	clipRect.TopLeft().x = rcBounds.TopLeft().x;
	clipRect.BottomRight().x = rcBounds.BottomRight().x;
	pDC->IntersectClipRect (clipRect);
	pDoc->c_targetBoard.draw(rectB,pDC);
	}
// c_listCtrl.MoveWindow(rectB);

// Sincronizzo contenuto 
//	c_listCtrl.UpdateWindow();

}

/////////////////////////////////////////////////////////////////////////////
// CRollView diagnostics

#ifdef _DEBUG
void CRollView::AssertValid() const
{
	CView::AssertValid();
}

void CRollView::Dump(CDumpContext& dc) const
{
	CView::Dump(dc);
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CRollView message handlers

void CRollView::OnInitialUpdate() 
{
CView::OnInitialUpdate();

CRect gRect,dRect;
GetClientRect(gRect);


DOC_CLASS* pDoc = (DOC_CLASS* )GetDocument();
	
// TODO: Add your specialized code here and/or call the base class
layout.CalcDrawRect (gRect,dRect);

// if (!c_listCtrl.Create (WS_VISIBLE | WS_CHILD | WS_BORDER | LVS_REPORT,dRect,this,0))
//	AfxMessageBox ("Error Create listCtrl");

// c_listCtrl.SetTextColor (RGB(0,0,0));

// c_listCtrl.image.Create(IDB_BITMAP_DIF1,16,1,RGB(255,255,255));	
// c_listCtrl.SetImageList(&c_listCtrl.image,TVSIL_NORMAL);

/*
CString sH ("POSITION");
for (int i=0;i<pDoc->c_difCoil.getNumSchede();i++)
	{
	CString s;
	s.Format (",%2d",i+1);
	sH += s;
	}
*/


CRect rcBounds,rectB;
GetClientRect (rcBounds);
layout.CalcDrawRect(rcBounds,rectB);
int nr = rectB.Size().cy / c_pxBRoll;
fillCtrl(nr);
}


// Torna numero di righe aggiunte
// Inserisce tutte le volte al piu` N righe dove N sono  quelle visualizzabili
// Default inserisce solo al piu` nMaxRow
int CRollView::fillCtrl(int nmaxRow)
{ 
int valret = 0;
DOC_CLASS* pDoc = (DOC_CLASS* )GetDocument();

int toIndex = pDoc->c_difCoil.GetSize();
int fromIndex = max (toIndex - nmaxRow,0);

pDoc->c_targetBoard.setEmpty();
for (int index=fromIndex;index <toIndex;index ++)
	{
	BOOL found = FALSE;
	// cerco schede
	int mSize = pDoc->c_difCoil.ElementAt(index).GetSize();
	for (int scIndex=0;scIndex<mSize;scIndex ++)
		{
		int clSize = pDoc->c_difCoil.ElementAt(index).ElementAt(scIndex).GetSize();
		//for (int clIndex=0;clIndex<clSize;clIndex ++)
		// default colore difetti + grossi
		for (int clIndex=(clSize-1);clIndex>=0;clIndex--)
			{
			if (pDoc->c_difCoil.ElementAt(index).ElementAt(scIndex).ElementAt(clIndex) > 0)
				{
				// found 
				CTarget t;
				t.setBanda (pDoc->c_difCoil.ElementAt(index).ElementAt(scIndex).banda);
				t.setSize (CSize(c_pxBRoll,c_pxBRoll));
				t.setColor (CSM20GetClassColor(clIndex));
				pDoc->c_targetBoard.add(pDoc->c_difCoil.ElementAt(index).posizione*1000,t);
				found = TRUE;
				}
			}
		}
	if (found)
		{
		int p1,p2;
		p1 = pDoc->c_difCoil.ElementAt(index).posizione*1000;
		p2 = pDoc->c_targetBoard.getLastPos();
		if (p1 > p2)
			valret ++;
		}
	}
if (pDoc->c_targetBoard.GetSize() > 0)
	pDoc->c_targetBoard.setLastPos(pDoc->c_targetBoard.ElementAt(pDoc->c_targetBoard.GetUpperBound()).getPos());
else
	pDoc->c_targetBoard.setLastPos(0);
return (valret);
}


BOOL CRollView::OnEraseBkgnd(CDC* pDC) 
{
// TODO: Add your message handler code here and/or call default

CRect r;
GetClientRect(&r);

// semaforo repaint all 
// vedi OnUpdate
// layout.setExcludeBkgr(!c_repaintAll);
// layout.setExcludeBkgLV(!c_repaintAll);

layout.OnEraseBkgnd(pDC,r);

return TRUE;
}



BOOL CRollView::Create(LPCTSTR lpszClassName, LPCTSTR lpszWindowName, DWORD dwStyle, const RECT& rect, CWnd* pParentWnd, UINT nID, CCreateContext* pContext) 
{
	// TODO: Add your specialized code here and/or call the base class
	
if (!CWnd::Create(lpszClassName, lpszWindowName, dwStyle, rect, pParentWnd, nID, pContext))
	return (FALSE);


return (layout.Create(this));

}



void CRollView::OnUpdate(CView* pSender, LPARAM lHint, CObject* pHint) 
{
	// TODO: Add your specialized code here and/or call the base class
	
if ((lHint==0) && (pHint == NULL))
	{
	CRect rcBounds,rectB;
	GetClientRect (rcBounds);
	layout.CalcDrawRect(rcBounds,rectB);
	int nr = rectB.Size().cy / c_pxBRoll;
	int newRow = fillCtrl(nr);

	CView::OnUpdate(pSender,lHint,pHint);	
	}
else
	{
	CRect rcBounds,rectB;
	GetClientRect (rcBounds);
	layout.CalcDrawRect(rcBounds,rectB);
	int nr = rectB.Size().cy / c_pxBRoll;
	int newRow = fillCtrl(nr);

	DOC_CLASS* pDoc = (DOC_CLASS* )GetDocument();

	if (newRow > 0)
		{
		pDoc->c_targetBoard.setDrawAttrib((int)layout.c_sizeFBanda,(int)layout.c_sizeBanda,
								c_pxBRoll,layout.c_bandePerCol);
		CRect clipRect(rectB);
		clipRect.TopLeft().x = rcBounds.TopLeft().x;
		CDC  *pDC;
		pDC = GetDC ();
		pDC->IntersectClipRect (clipRect);
		drawPosDiaframma(pDC,rectB);
		pDoc->c_targetBoard.scroll(newRow,rectB,clipRect,pDC);
		ReleaseDC(pDC);
		}
	if (lHint == 2)
		{ // aggiornamento stessa riga
		pDoc->c_targetBoard.setDrawAttrib((int)layout.c_sizeFBanda,(int)layout.c_sizeBanda,
								c_pxBRoll,layout.c_bandePerCol);
		CRect clipRect(rectB);
		clipRect.TopLeft().x = rcBounds.TopLeft().x;
		CDC  *pDC;
		pDC = GetDC ();
		pDC->IntersectClipRect (clipRect);
		pDoc->c_targetBoard.reDraw(pDoc->c_targetBoard.GetUpperBound(),rectB,pDC);
		ReleaseDC(pDC);
		}
	layout.c_vLabel[0] = pDoc->GetVPosition();
	// contatori a,b c,d,
	CString s;
	for(int i=0;i<pDoc->c_difCoil.getNumClassi();i++)
		{
		if (i>0)
			s += _T(",");
		CString s1;
		s1.Format(_T(" %c=%2.0lf"),'A'+i,pDoc->c_difCoil.getTotDifetti('A'+i,0,(int)pDoc->GetVPosition()));
		s += s1;
		}
	layout.c_vLabel [8] = s;
	//------------------------
	// densita`,
	for(int i=0;i<pDoc->c_difCoil.getNumClassi();i++)
		{
		CString s1;
		// densita`
		double densita=pDoc->c_difCoil.getDensityLevel('A'+i,pDoc->c_densityCalcLength);
		s1.Format(_T(" %c=%3.0lf"),'A'+i,densita);
		layout.c_vLabel [9+i] = s1;
		if (densita > pDoc->c_difCoil.getAllarme ('A'+i))
			{
			layout.c_vLabel [9+i].setTColor(RGB(255,0,0));
			}
		else
			{
			layout.c_vLabel [9+i].setTColor(RGB(0,0,0));
			}
		}
	//------------------------
	// lunghezza di base per calcolo densita`
	s.Format(_T("%4.0lf"),pDoc->c_densityCalcLength);
	layout.c_vLabel [13] = s;

	int numClassi = pDoc->c_difCoil.getNumClassi();

	// cluster  label 14,15,16,17
	for (int i=0;i<numClassi;i++)
		{	
		s.Format(_T("%d"),pDoc->c_clusterCounter[i]);
		layout.c_vLabel [14+i] = s;
		}
	}
}


void CRollView::drawPosDiaframma(CDC *pDC, CRect rectB)
{

DOC_CLASS* pDoc = (DOC_CLASS* )GetDocument();
CPen pen1,pen2,*oldPen;

if (!pDoc->c_doUseDiaframmi)
	return;
	
	// light grey
	pen1.CreatePen(PS_SOLID,1,RGB(196,196,196));
	oldPen = pDC->SelectObject(&pen1);
	double posLeft = pDoc->c_posDiaframmaSx;
	// 15-09-2004 corretto centrato +layout.c_sizeFBanda 
	int scPosLeft = rectB.left +layout.c_sizeFBanda+ (1.  * posLeft * (double)layout.c_sizeBanda / (layout.c_bandePerCol*pDoc->c_sizeBande));

	pDC->MoveTo(scPosLeft,rectB.top);
	pDC->LineTo(scPosLeft,rectB.bottom);

	// light grey
	pen2.CreatePen(PS_SOLID,1,RGB(196,196,196));
	pDC->SelectObject(&pen2);
	double posRight = pDoc->c_posDiaframmaDx;
	// 15-09-2004 corretto centrato +layout.c_sizeFBanda 
	int scPosRight = rectB.left +layout.c_sizeFBanda + (1.  * posRight * (double)layout.c_sizeBanda / (layout.c_bandePerCol*pDoc->c_sizeBande));
	
	pDC->MoveTo(scPosRight,rectB.top);
	pDC->LineTo(scPosRight,rectB.bottom);
	
	pDC->SelectObject(oldPen);
	pen1.DeleteObject();
	pen2.DeleteObject();
	// Fine disegno pos. Diaframma
	//---------------------------------------------------------

}

