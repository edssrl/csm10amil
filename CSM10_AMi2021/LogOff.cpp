//-------------------------------------------------------------------
//
//			   LogOff
//
//-------------------------------------------------------------------
#include "stdafx.h"

#define STRICT


#include "LogOff.h"

#define WIN32S  0x80000000l   // no manifest constance yet???

#ifdef USE_LOGMSG
void Log (LPCTSTR msg);
#endif

/****************************************************************************

    FUNCTION: LogOff (BOOL shutDown,BOOL reBoot)

    PURPOSE: LogOff and/or shutDown reBoot system

    COMMENTS:


****************************************************************************/

void LogOff (BOOL shutDown,BOOL reBoot)
{
HANDLE hToken;
TOKEN_PRIVILEGES tkp;
TCHAR szBuf[256];

//OutputDebugString("Setting token");
// Running on NT so need to change privileges
if (!OpenProcessToken(GetCurrentProcess(),
	TOKEN_ADJUST_PRIVILEGES | TOKEN_QUERY, &hToken)) 
	{
	wsprintf(szBuf, _T("ERR: OpenProcessToken #%d"), GetLastError ());
#ifdef USE_LOGMSG
		Log(szBuf);
#else
	    MessageBox(NULL, szBuf, NULL, MB_OK);
#endif
	return;
	}


//  Get the LUID for shutdown privilege
LookupPrivilegeValue(NULL, TEXT("SeShutdownPrivilege"), 
	&tkp.Privileges[0].Luid);
tkp.PrivilegeCount = 1;  // one privilege to set
tkp.Privileges[0].Attributes = SE_PRIVILEGE_ENABLED;

// Get shutdown privilege for this process.
if (!AdjustTokenPrivileges(hToken, FALSE, &tkp, 0, (PTOKEN_PRIVILEGES)NULL, 0)) 
	{
    wsprintf(szBuf, _T( "ERR: AdjustTokenPrivileges #%d"), GetLastError ());
#ifdef USE_LOGMSG
	Log(szBuf);
#else
	MessageBox(NULL, szBuf, NULL, MB_OK);
#endif
	}


//CloseHandle(hToken);   
	
if (reBoot)
	{
    // Shut down the system, and reboot the system.
    if (!ExitWindowsEx( EWX_REBOOT | EWX_FORCE, 0 )) 
		{
        wsprintf(szBuf, _T("ERR: ExitWindows #%d"), GetLastError ());
 #ifdef USE_LOGMSG
		Log(szBuf);
#else
	    MessageBox(NULL, szBuf, NULL, MB_OK);
#endif
		}
	}
else
	{
	if (shutDown)
		{
	    // Shut down the system, and force all applications closed.
	 if (!ExitWindowsEx(EWX_SHUTDOWN, 0)) 
			{
			wsprintf(szBuf, _T("ERR: ExitWindows #%d"), GetLastError ());
#ifdef USE_LOGMSG
			Log(szBuf);
#else
		    MessageBox(NULL, szBuf, NULL, MB_OK);
#endif
			}
		}
	else
		{
		// Log Off
	 if (!ExitWindowsEx(EWX_LOGOFF, 0)) 
			{
			wsprintf(szBuf, _T("ERR: ExitWindows #%d"), GetLastError ());
#ifdef USE_LOGMSG
			Log(szBuf);
#else
		    MessageBox(NULL, szBuf, NULL, MB_OK);
#endif
			}
		}
	
	}


}


