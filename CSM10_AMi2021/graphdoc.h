// GraphDoc.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CBaseDoc document

#include "FontInfo.h"
#include "Dialog.h"
#include "DProduct.h"
#include "Target.h"

#include "float.h"	// 

#include "ThEncoder.h"

#define ID_ENCODER_UPDATED	1123
#define ID_ENCODER_ALARM	1124

#define FILEDOCTYPE	_T("CSM10_AR2021")	
#define FILEDOCVER	7

#define  THISAPP CCSMApp
enum ViewType {UNKVIEW,INITVIEW,LINEVIEW,DENSVIEW,TOWERVIEW,ROLLVIEW,MULTILINEVIEW};

#define NMAXCLASSI 4

// dimensione dei ricevitori CSM10_ARD era 10
// spostata nel CLineDoc e configurabile 
// #define SIZE_BANDE	11.


#define COILTYPE DifCoil

struct DifBanda 
{
int banda;
int numClassi;
int classi [NMAXCLASSI];
DifBanda (void){numClassi = 0;banda = 0;};
BOOL Add (int v){if (numClassi<NMAXCLASSI)
					{classi[numClassi++] = v;
						return TRUE;}
				return (FALSE);};

int ElementAt(int i){if (i<numClassi)return(classi[i]);
					else
						return (-1);};

// Copy constructor
DifBanda (DifBanda &source)
	{
	for (int i=0;i<source.numClassi;i++)
		classi[i] = source.classi[i];
	numClassi = source.numClassi;
	banda = source.banda;
	};

~DifBanda (void) {;};
void clear (void) {numClassi = 0;};
void setBanda (int b) {banda = b;};
int  GetSize(void ) {return (numClassi);};

BOOL save(CFile *cf)
	{
	cf->Write((void *)&banda,sizeof(banda));
	cf->Write((void *)&numClassi,sizeof(numClassi));
	for (int i=0;i<numClassi;i++)
		{
		int v;
		v = classi[i];
		cf->Write((void *)&v,sizeof(v));
		}
	return TRUE;};
BOOL load(CFile *cf)
	{int nc;
	cf->Read((void *)&banda,sizeof(banda));
	cf->Read((void *)&nc,sizeof(nc));
	for (int i=0;i<nc;i++)
		{
		int v;
		cf->Read((void *)&v,sizeof(v));
		Add(v);
		}
	return TRUE;};

DifBanda& operator = (DifBanda& source)
{
clear();
for (int i=0;i<source.numClassi;i++)
	Add(source.classi[i]);
banda = source.banda;
return (*this);};


};


struct DifMetro : public CArray <DifBanda,DifBanda &>
{
// Attribute
// riferimento metraggio
int posizione;
int totalCode [NMAXCLASSI];	// somma totali per qs metro per ogni code
 // Method
DifMetro (void){clear();};
// Copy constructor
DifMetro (DifMetro &source)
	{
	memcpy((void *)totalCode,(void *)source.totalCode,sizeof(totalCode));
	posizione = source.posizione;
	SetSize(source.GetSize());
	for (int i=0;i<source.GetSize();i++)
		  SetAt(i,source.ElementAt(i));
	};
~DifMetro (void)
	{
	RemoveAll();
	};
void clear (void) {posizione = 0;
			for (int i=0;i<NMAXCLASSI;i++) totalCode[i] = 0;
			for (int i=0;i<GetSize();i++)ElementAt(i).clear();};
// void fill  (int v) {for (int i=0;i<getNumSchede();i++)dBanda[i].fill(v);};

// Torna numero fori totali in qs metro per classe code
double getValNum(int code){double val = 0.;
		val = totalCode [code-'A'];
		return(val);};


void calcTotalCode (BOOL doClear)
	{
	for (int code=0;code < NMAXCLASSI;code++)
		{
		if (doClear)
			totalCode[code] = 0;
		for (int i=0;i<GetSize();i++)
			{
			int iv;
			// Torna -1 se invalido
			iv = ElementAt(i).ElementAt(code);
			if (iv > 0)
				totalCode[code] += iv;
			}
		}
	}

// torna numero fori di classe code per la banda scheda

double getValNum(int scheda,int code)
	{
	double val = 0.;
	for (int i=0;i<GetSize();i++)
		{
		if (ElementAt(i).banda == scheda)
			val = (double)ElementAt(i).ElementAt(code-'A');
		}
		return(val);};

// torna numero fori di classe code per la banda scheda
double getValNumInScheda(int scheda)
	{double val = 0.;
	for (int i=0;i<GetSize();i++)
		{
		if (ElementAt(i).banda == scheda)
			{
			for (int j=0;j<ElementAt(i).GetSize();j++)
				val += (double)ElementAt(i).ElementAt(j);
			}
		}
		return(val);};

BOOL save(CFile *cf)
	{int numSchede;
	cf->Write((void *)&totalCode,sizeof(totalCode));
	// Free Unused memory
	FreeExtra();
	cf->Write((void *)&posizione,sizeof(posizione));
	numSchede = GetSize();
	cf->Write((void *)&numSchede,sizeof(numSchede));
	for (int i=0;i<numSchede;i++)
		if (!ElementAt(i).save(cf)) return FALSE;
	return TRUE;};
BOOL load(CFile *cf,int ver)
	{int numSchede;
	cf->Read((void *)&totalCode,sizeof(totalCode));
	cf->Read((void *)&posizione,sizeof(posizione));
	cf->Read((void *)&numSchede,sizeof(numSchede));
	SetSize(numSchede);
	for (int i=0;i<numSchede;i++)
		if (!ElementAt(i).load(cf)) return FALSE;
	return TRUE;};
DifMetro& operator = (DifMetro& source)
	 	{
		clear();
		posizione = source.posizione;
		if (source.GetSize() != GetSize())
			SetSize(source.GetSize());
		for (int i=0;i<source.GetSize();i++)
			  SetAt(i,source.ElementAt(i));
		memcpy((void *)totalCode,(void *)source.totalCode,sizeof(totalCode));
		return (*this);};
/*
void format (CString	&s1,CString &s2)
	{CString str("");
	for (int i =0;i<getNumSchede();i++)
		{
		dBanda[i].format(str);
		s1 += str;
		}
	};

// RectB viene modificato durante la stampa
int print(CRect &rectB,CDC* pdc)
	{
	CPoint pr;
	int deltax;
	pr.x = rectB.left;
	pr.y = rectB.top;

	TEXTMETRIC tm;
	pdc->GetTextMetrics(&tm);

	for (int i=0;i<getNumSchede();i++)
		{
		deltax = dBanda[i].print(pr,pdc);
		pr.x += deltax;
		}
	rectB.top += tm.tmHeight;
	return (pr.y);
	};
*/
};

struct QuattroClassi
	{
	double totalA;
 	double totalB;
	double totalC;
	double totalD;
	QuattroClassi(void){totalA = 0;totalB = 0;totalC = 0;totalD = 0;};
	};



struct  Cluster
	{
	Cluster::Cluster(void)
		{c_soglia=1;c_lunghezza=1;c_lastClusterPos=0;};
	int 	c_soglia;
	int		c_lunghezza;
	int		c_lastClusterPos;
	};
	 

class DifCoil : public CArray <DifMetro,DifMetro &>
{
double meter;				// posizione attuale
// double trueSizeEncod;	// Dimensione corretta tra due interrupt
double largeSize;			// larghezza singole bande mm
int numeroClassi;			
int numeroSchede;
int baseStepX;				// in mm

CCriticalSection	c_pCs;

CArray <QuattroClassi,QuattroClassi &> c_totali;

public:
CArray <double,double &> c_sogliaAllarme;

CArray <Cluster,Cluster &> c_clusterArray;

public:

void updateTotali(int classe, int banda, int v)
	{
	switch(classe)
		{
		case 0:
			c_totali[banda].totalA += v;
			break;			
		case 1:
			c_totali[banda].totalB += v;
			break;			
		case 2:
			c_totali[banda].totalC += v;
			break;			
		case 3:
			c_totali[banda].totalD += v;
			break;			
		}
	};

int Add (DifMetro &dMetro,BOOL update = FALSE)
	{
	if (update)
		{
		CArray<DifMetro,DifMetro &>::SetAt(GetUpperBound(),dMetro);
		return 1;
		}
	else
		return(CArray<DifMetro,DifMetro &>::Add(dMetro));
	};

int getMeterBaseStepX(void) {return(baseStepX/1000);};
void setMmBaseStepX(int millimetri) {baseStepX = millimetri;};

DifCoil(void )
	{
	meter = 0.;// Init Schede
	largeSize = 1500.; // Default 1.5 mt
	numeroClassi = 4;
	m_nGrowBy = 1000;	// cresce di 1000 metri per volta
	baseStepX = 1000;	// step ogni metro default
	};
~DifCoil (void)
	{
	for (int i=0;i<GetSize();i++)
		ElementAt(i).RemoveAll();
	RemoveAll();
	c_sogliaAllarme.RemoveAll();
	c_clusterArray.RemoveAll();
	c_totali.RemoveAll();
	};

// Copy operator
DifCoil& operator = (DifCoil& source)
{
clear();
meter = source.meter;			// posizione attuale
largeSize = source.largeSize;		// larghezza singole bande mm
numeroClassi = source.numeroClassi;
numeroSchede = source.numeroSchede;
baseStepX = source.baseStepX;

c_totali.RemoveAll();
for (int i=0;i<source.c_totali.GetSize();i++)
	{
	c_totali.Add (source.c_totali.ElementAt(i));
	}

RemoveAll();
for (int i=0;i<source.GetSize();i++)
	{
	Add (source.ElementAt(i));
	}

c_sogliaAllarme.RemoveAll();
for (int i=0;i<source.c_sogliaAllarme.GetSize();i++)
	{
	c_sogliaAllarme.Add (source.c_sogliaAllarme.ElementAt(i));
	}

c_clusterArray.RemoveAll();
for (int i=0;i<source.c_clusterArray.GetSize();i++)
	{
	c_clusterArray.Add (source.c_clusterArray.ElementAt(i));
	}

return (*this);
};


void clear(int numSchede,int nClassi){
			numeroClassi = nClassi;
			numeroSchede = numSchede;
			clear();};
void clear(void )
			{
			for (int i=0;i < GetSize();i++)
				ElementAt(i).clear();
			RemoveAll();
			meter = 0.;
			c_totali.RemoveAll();
			c_totali.SetSize(numeroSchede);
			};
// Implementazione delle interfacce di GroupSchede 
public:

double getMeter ()
		{double m;
		CSingleLock csl (&c_pCs);
		csl.Lock();
		m = meter;
		csl.Unlock();
		return(m);};
double getAllarme (int code){if ((code - 'A') < c_sogliaAllarme.GetSize()) return(c_sogliaAllarme[code-'A']);
			return (0.);};
int getNumSchede (void){return(numeroSchede);};
int getNumClassi (void){return(numeroClassi);};


void setMeter (double m){
		CSingleLock csl (&c_pCs);
		csl.Lock();
		meter = m;
		csl.Unlock();};
void setLargeSize (int mm){largeSize = (double ) mm;};
//void setTrueSizeEncod (double dmm){trueSizeEncod = dmm;};

double getIncMeter (void){return(1.);};

// torna numero difetti della classe code
// disp metri dalla posizione attuale 
double getValAlarmNumLStep(int code,int disp = 0)
	{double v = getValNumAt(code,(int) (meter-disp));
	if (v >= getAllarme(code))
		return (1.); // era v
	return (0.);};

double getValNumLStep(int code,int disp = 0)
	{
	return(getValNumAt(code,(int) (meter-disp)));};

// algoritmo di calcolo dell'indice
// a partire dalla posizione in metri
// torna -1 se index non trovato
// Sfrutto monotonicita` delle posizioni crescenti
int getOIndexFromPos(int pos)
	{int idx = -1;
	// Non ci sono elementi
	if (GetSize() <= 0) 
		return (idx);
	// Ultima posizione memorizzata < della posizione cercata
	// sicuramente NON esiste
	if ((int )ElementAt(GetUpperBound()).posizione < pos)
		return (idx);
	// procedo con ricerche a ritroso
	for (int i=GetUpperBound();i>=0;i--)
		{
		if ((int )ElementAt(i).posizione == pos)
			{
			idx = i;
			break;
			}
		}
	return (idx);};

// Provare a sostituire una ricerca per bisezione
int getIndexFromPos(int pos)
	{int idx = -1;
	// Non ci sono elementi
	if (GetSize() <= 0) 
		return (idx);
	// Ultima posizione memorizzata < della posizione cercata
	// sicuramente NON esiste
	if ((int )ElementAt(GetUpperBound()).posizione < pos)
		return (idx);
	// Provare a sostituire una ricerca per bisezione
	// procedo con ricerche a ritroso

	int lIndex = 0;
	int rIndex = GetUpperBound();
	// fix bug 26-04-05 da > a >= 
	// se 1 solo difetto non lo trova!
	while ((rIndex - lIndex) >= 0)
		{
		int lPos;
		int rPos;
		rPos = (int )ElementAt(rIndex).posizione;
		if (rPos == pos)
			{
			idx = rIndex;
			break;
			}
		lPos = (int )ElementAt(lIndex).posizione;
		if (lPos == pos)
			{
			idx = lIndex;
			break;
			}
		int mIndex = lIndex + (rIndex - lIndex)/2;
		int mPos = (int )ElementAt(mIndex).posizione;
		// Che fortuna !!
		if (pos == mPos)
			{
			idx = mIndex;
			break;
			}
		// cambio estremi
		if (pos > mPos)
			{// Bisezione funzioni continue
			// not found
			if (lIndex == mIndex)
				break;
			lIndex = mIndex;
			}
		if (pos < mPos)
			{// Not found
			if (rIndex == mIndex)
				break;
			rIndex = mIndex;
			}
		}
	return (idx);};
	

// somma valori di tutte le schede per quel codice
double getValNumAt(int code,int pos)
	{
	double val=0.;
	int idx = getIndexFromPos(pos);
	if (idx >= 0)
		val = ElementAt(idx).getValNum(code);
	return(val);};

	
// Somma tutti i difetti di code fra metro from e metro To
// appartenenti a scheda
double getAllDifetti(int scheda,int code)
{
double val=0.;
// Nessun elemnto nell'array
if (GetSize() == 0)
	return (val);
if (scheda < c_totali.GetSize())
	{
	switch(code)
		{
		case 'A' : val = (double)c_totali[scheda].totalA;
				break;
		case 'B' : val = (double)c_totali[scheda].totalB;
				break;
		case 'C' : val = (double)c_totali[scheda].totalC;
				break;
		case 'D' : val = (double)c_totali[scheda].totalD;
				break;
		}
	}

return (val);
}

// torna valore densita` per metro quadrato
double getTotalDensity(int classe)
	{double d,area;
	area = meter*(0.001)*largeSize;
	if (area==0.) return -1.; 
	d = (double) getTotDifetti(classe,0,meter)/area;
	return(d);};

// torna valore densita` per metro quadrato
double getTotalDensity(int classe,double from,double to)
	{double d,area;
	area = (0.001)*(to - from)*largeSize;
	if (area==0.) return -1; 
	d = (double) getTotDifetti(classe,from,to)/area;
	return(d);};

double getDensityLevel(int classe,double densityCalcLength,double pos=-1.)
	{// default position
	if (pos == -1.)
		pos = meter;
	// larghezza in metri
	double width = largeSize ;
	if (width <= 0)
		width = DBL_MIN;
	double from = pos - ((densityCalcLength*1000.)/width);
		if (from < 0.)
			from = 0.;
		// difetti
	double densityLevel = getTotDifetti(classe,from,pos);
		// difetti Per m2
		// densityLevel /=  ((position-from)/1000.)*width; 
		// difetti per m2 in 1000 m
		// densityLevel *= 1000.;
		// difetti per m2 in x metri che dipende dalle impostazioni threshold
	return densityLevel;};

// Somma tutti i difetti di code fra metro from e metro To
// appartenenti a scheda
double getTotDifetti(int scheda,int code,int from,int to)
{
double val=0.;
// Nessun elemnto nell'array
if (GetSize() == 0)
	return (val);
for (int i=GetUpperBound();i>=0;i--)
	{
	int pos = (int )ElementAt(i).posizione;
	if ((pos > from)&&
		(pos <= to)) 
		{
		val += ElementAt(i).getValNum(scheda,code);
		}
	// sfrutto monotonicita`
	if (pos < from)
		break;
	}
return (val);
}

// Somma tutti i difetti di code fra metro from e metro To
// di qualunque scheda
double getTotDifetti(int code,int from,int to)
{
double val=0.;
// Nessun elemnto nell'array
if (GetSize() == 0)
	return (val);
for (int i=GetUpperBound();i>=0;i--)
	{
	int pos = (int )ElementAt(i).posizione;
	if ((pos > from)&&
		(pos <= to)) 
		{
		val += ElementAt(i).getValNum(code);
		}
	// sfrutto monotonicita`
	if (pos < from)
		break;
	}
return (val);
}

// Somma tutti i difetti presenti nel metro index
double getTotDifetti(int index)
{
double val=0.;
// Nessun elemento nell'array
if (GetSize() == 0)
	return (val);

for (int k=0;k<ElementAt(index).GetSize();k++)
	val += ElementAt(index).getValNum('A'+k);
return (val);
}


// Somma tutti i difetti fra metro from e metro To
// di qualunque scheda e tutti i code
double getTotDifetti(int from,int to)
{
double val=0.;
// Nessun elemnto nell'array
if (GetSize() == 0)
	return (val);
for (int i=GetUpperBound();i>=0;i--)
	{
	int pos = (int )ElementAt(i).posizione;
	if ((pos >  from)&&
		(pos <= to)) 
		{
//		for (int k=0;k<ElementAt(i).GetSize();k++)
			for (int j=0;j<getNumClassi();j++)
				val += ElementAt(i).getValNum('A'+j);
		}
	// sfrutto monotonicita`
	if (pos < from)
		break;
	}
return (val);
}

// valore massimo per qualunque scheda
double getMaxDensFromTo(int from ,int to)
	{
	double Mval=0.;
	// Nessun elemnto nell'array
	if (GetSize() == 0)
		return (Mval);
	for (int k=0;k<getNumSchede();k++)
		{
		double dv = 0.;
		for (int i=GetUpperBound();i>=0;i--)
			{
			int pos = (int )ElementAt(i).posizione;
			if ((pos > from)&&
				(pos <= to)) 
				dv += ElementAt(i).getValNumInScheda(k);
			// sfrutto monotonicita`
			if (pos < from)
				break;
			}
		Mval = max(dv,Mval);
		}
	return (Mval);
	};

// Valore max trendl tra metro from e metro to
double getMaxTrendLFromTo(int code,int from,int to)
	{
	double Mval=0.;
	// Nessun elemnto nell'array
	if (GetSize() == 0)
		return (Mval);
	for (int i=GetUpperBound();i>=0;i--)
		{
		int pos = (int )ElementAt(i).posizione;
		if ((pos > from)&&
			(pos <= to)) 
			{
			Mval = max(ElementAt(i).getValNum(code),Mval);
			}
		// sfrutto monotonicita`
		if (pos < from)
			break;
		}
	return (Mval);
	};

// Valore min trendl tra metro from e metro to
double getMinTrendLFromTo(int code,int from,int to)
	{
	double mval=0.;
	// Nessun elemnto nell'array
	if (GetSize() == 0)
		return (mval);
	for (int i=GetUpperBound();i>=0;i--)
		{
		int pos = (int )ElementAt(i).posizione;
		if ((pos > from)&&
			(pos <= to)) 
			{
			mval = min(ElementAt(i).getValNum(code),mval);
			}
		// sfrutto monotonicita`
		if (pos < from)
			break;
		}
	return (mval);
	};

double getValRelativeTLStep(int code,int disp = 0)
	{double val;
	val = getValNumLStep(code,disp);
	val = val / getValMeanTL(code);
	return (val);
	};

// Indici su getMaxTrend
double getMaxRelativeTL(int code,int from,int to)
	{double val;
	val = getMaxTrendLFromTo(code,from,to);
	val = val / getValMeanTL(code);
	return (val);
	};

double getMinRelativeTL(int code,int from,int to)
	{double val;
	val = getMinTrendLFromTo(code,from,to);
	val = val / getValMeanTL(code);
	return (val);
	};


double getValMeanTL(int code)
{
double val=0.;
// Nessun elemnto nell'array
if (GetSize() == 0)
	return (val);
for (int i=GetUpperBound();i>=0;i--)
	{
	val += ElementAt(i).getValNum(code);
	}
// trovo tutti i difetti di tipo code
// li divido per la lunghezza del supporto
val /= getMeter(); 
return (val);
};
//--------------------------------------------------
// riversa in dCoil i difetti dei metri da from a to
// torna numero di metri difettosi
int dump (DifCoil &dCoil,int from,int to)
{
if (GetSize() <= 0)
	return 0;
int n=0;
for (int i=GetUpperBound();i>=0;i--)
	{
	int pos = (int )ElementAt(i).posizione;
	if ((pos > from)&&
		(pos <= to)) 
		{
		DifMetro dM;
		dM = ElementAt(i);
		dM.posizione = (int ) ( (to - from) + dM.posizione - getMeter());
		if (dM.posizione < 0)
			dM.posizione = 0;
		dCoil.InsertAt(0,dM);
		n ++;
		}
	// sfrutto monotonicita` per velocita` 
	if (pos < from)
		break;
	}
return (n);};


//----------------------------
// Save and load
BOOL save(CFile *cf)
	{
	// Free Unused memory
	FreeExtra();
	cf->Write((void *)&meter,sizeof(meter));
	//cf->Write((void *)&trueSizeEncod,sizeof(trueSizeEncod));
	cf->Write((void *)&largeSize,sizeof(largeSize));
	cf->Write((void *)&numeroClassi,sizeof(numeroClassi));
	cf->Write((void *)&numeroSchede,sizeof(numeroSchede));
	cf->Write((void *)&baseStepX,sizeof(baseStepX));
	int nDifMetri;
	nDifMetri = GetSize();
	cf->Write((void *)&nDifMetri,sizeof(nDifMetri));
	for (int i=0;i<nDifMetri;i++)
		if (!ElementAt(i).save(cf)) return FALSE;
	int n;
	n = c_sogliaAllarme.GetSize();
	cf->Write((void *)&n,sizeof(n));
	for (int i=0;i<n;i++)
		{
		double d;
		d = c_sogliaAllarme[i];
		cf->Write((void *)&d,sizeof(d));
		}
	n = c_clusterArray.GetSize();
	cf->Write((void *)&n,sizeof(n));
	for (int i=0;i<n;i++)
		{
		double d;
		d = c_clusterArray[i].c_soglia;
		cf->Write((void *)&d,sizeof(d));
		d = c_clusterArray[i].c_lunghezza;
		cf->Write((void *)&d,sizeof(d));
		}
	return TRUE;};

BOOL load(CFile *cf,int ver)
	{
	clear();
	cf->Read((void *)&meter,sizeof(meter));
	// cf->Read((void *)&trueSizeEncod,sizeof(trueSizeEncod));
	cf->Read((void *)&largeSize,sizeof(largeSize));
	cf->Read((void *)&numeroClassi,sizeof(numeroClassi));
	cf->Read((void *)&numeroSchede,sizeof(numeroSchede));
	cf->Read((void *)&baseStepX,sizeof(baseStepX));
	int nDifMetri;
	cf->Read((void *)&nDifMetri,sizeof(nDifMetri));
	SetSize(nDifMetri);
	for (int i=0;i<GetSize();i++)
		if (!ElementAt(i).load(cf,ver)) return FALSE;
	int nSAlarm;
	cf->Read((void *)&nSAlarm,sizeof(nSAlarm));
	c_sogliaAllarme.SetSize(nSAlarm);
	for (int i=0;i<nSAlarm;i++)
		{
		double d;
		cf->Read((void *)&d,sizeof(d));
		c_sogliaAllarme[i] = d;
		}
	if (ver >= 6)
		{
		int nCluster;
		cf->Read((void *)&nCluster,sizeof(nCluster));
		c_clusterArray.SetSize(nCluster);
		for (int i=0;i<nCluster;i++)
			{
			double d;
			cf->Read((void *)&d,sizeof(d));
			c_clusterArray[i].c_soglia = d;
			cf->Read((void *)&d,sizeof(d));
			c_clusterArray[i].c_lunghezza = d;
			}
		}
	else
		c_clusterArray.SetSize(0);

	return TRUE;};

};



#define INFINITO 0x7fffffff
// Dati per singola scheda 
// Tiene ultimo valore 

typedef  CArray <int ,int &> ValS;

class Scheda : public CObject
{
// Items
public:
int id;			// identificativo 0-7

CArray <ValS,ValS &> gValS; // A B C D per classi di difetti
CArray <int,int &> totalS;

int size; // Larghezza banda in mm
// Oper
public:
Scheda (void);

// Modificata lavora su indici e non su metri
// Somma tutti i difetti di code fra indice from e indice To

double getTotDifetti(int code,int from,int to);

double getTotal(int code);
// Torna valore in fori a m2 relativi alla banda
double getValTrendL(int code,int disp = 0);

// Torna valore in fori relativi alla banda
// 10 98
double getNumTrendL(int code,int disp = 0);

// Torna valore in fori a m2 relativi alla banda
double getValDensita(int code,int index,int disp=0);

void fillScheda (unsigned char *v)
	{
	//16-06-97
	if (size == INFINITO)
		return;
	for (int i=0;i<gValS.GetSize();i++)
		{
		int lv;
		lv = v[i];
		gValS[i].Add(lv);totalS[i]+=lv;
		}
	};


void fillScheda (int *v)
	{
	//16-06-97
	if (size == INFINITO)
		return;
	for (int i=0;i<gValS.GetSize();i++)
		{
		gValS[i].Add(v[i]);totalS[i]+=v[i];
		}
	};
BOOL save(CFile *cf);
BOOL load(CFile *cf);
int getSize(int code){
	ASSERT ((code-'A')<gValS.GetSize());
	return(gValS[code-'A'].GetSize());};
	
void setLarge(int millimeter){size = millimeter;};
void Reset (int nCl) {
			for(int i=0;i<gValS.GetSize();i++)
				{gValS[i].RemoveAll( );totalS[i]=0;}
			gValS.SetSize(nCl);
			totalS.SetSize(nCl);
			setLarge(INFINITO); // Init infinito
			};
};

class GroupSchede : public CObject
{
public:

double meter;			// posizione
double trueSizeEncod;	// Dimensione corretta tra due interrupt
double largeSize;		// larghezza banda mm
// Scheda schede[7];
CArray <Scheda,Scheda &> schede;
int numeroClassi;

CArray <double,double &> allarmeS;


GroupSchede (void){meter = 0.;// Init Schede
			largeSize = 1500.; // Default 1.5 mt
			trueSizeEncod = 10000.0;	// default 10 metri
			numeroClassi = 4;
			};
~GroupSchede (void)
			{
			for (int i=0;i < getNumSchede();i++)
				schede[i].Reset(numeroClassi);
			 schede.RemoveAll();
			allarmeS.RemoveAll();
			};
void init(int numSchede,int nClassi){
			numeroClassi = nClassi;
			for (int i=0;i < getNumSchede();i++)
				schede[i].Reset(numeroClassi);
			schede.RemoveAll();
			schede.SetSize(numSchede);
			for (int i=0; i< getNumSchede();i++)
				schede[i].id = i;
			meter = 0.;
			};

int getNumSchede (void){return(schede.GetSize());};
int getNumClassi (void){return(numeroClassi);};

void setAllarme (int code,double v) 
		{ASSERT ((code - 'A')<allarmeS.GetSize());
			allarmeS[code-'A'] = v;};

void setLarge(int index,int millimeter)
	{if ((index>=0)&&(index<getNumSchede()))
		schede[index].setLarge(millimeter);};


double getAllarme (int code )
		{ASSERT ((code - 'A')<allarmeS.GetSize());
			return(allarmeS[code-'A']);};

int getAlarm(int code)
		{ASSERT ((code - 'A')<allarmeS.GetSize());
			return((int)allarmeS[code-'A']);};

//-----------------------------------------------
// Funzioni di riempimento riferite agli indici

void fillScheda (int index,int *lp)
	{ASSERT (index < getNumSchede());
		schede[index].fillScheda(lp);};

void fillScheda (int index,unsigned char *cp)
	{ASSERT (index < getNumSchede());
		schede[index].fillScheda(cp);};

//----------------------------

// Somma tutti i difetti di code fra metro from e metro To
double getTotDifetti(int scheda,int code,int from,int to)
	{double v=0.;
	double corr = trueSizeEncod/1000.;
	int indexFrom,indexTo;
	indexTo = (int) ceil(to/corr); 
	indexFrom = (int) ceil(from/corr);
	ASSERT (scheda < schede.GetSize());
	v = schede[scheda].getTotDifetti(code,indexFrom,indexTo);		
	return v;};

// Somma tutti i difetti di code fra metro from e metro To
double getTotDifetti(int code,int from,int to)
	{double v=0.;
	double corr = trueSizeEncod/1000.;
	int indexFrom,indexTo;
	indexTo = (int) ceil(to/corr); 
	indexFrom = (int) ceil(from/corr);
	for (int i=0;i<getNumSchede();i++)
		v += schede[i].getTotDifetti(code,indexFrom,indexTo);		
	return v;};

// Somma tutti i difetti di code fra metro from e metro To
double getTotDifetti(int from,int to)
	{double v=0.;
	for (int i=0;i<numeroClassi;i++)
		v += getTotDifetti('A'+i,from,to);
	return v;};



// disp indica posizione arretrata rispetto
// attuale (disp = 0 ->	ultimo valore)
double getValAlarmTL(int code,int disp = 0)
	{double val = 0.;
	
	val = getValTrendL(code,disp);
	if (val < allarmeS[code-'A']) 
		val = 0.;
	return (val);
	};

// disp indica posizione arretrata rispetto
// attuale (disp = 0 ->	ultimo valore)
double getValAlarmTLStep(int code,int disp = 0)
	{double val = 0.;
	
	val = getValTrendLStep(code,disp);
	if (val < allarmeS[code-'A']) 
		val = 0.;
	return (val);
	};


double getValMeanTL(int code)
	{double val=0.;
	for (int i=0;i<getNumSchede();i++)
		val += schede[i].getTotal(code);
	// totale difetti
	// divido per area totale
	if (meter > 0.)
		val /= ((largeSize/1000.)*meter); 
	else
		val = 0.;	
	return (val);
	};

double getValRelativeTLStep(int code,int disp = 0)
	{double val;
	val = getValTrendLStep(code,disp);
	val = val / getValMeanTL(code);
	return (val);
	};

// Indici su getMaxTrend
double getMaxRelativeTL(int code,int from,int to)
	{double val;
	val = getMaxTrendLFromTo(code,from,to);
	val = val / getValMeanTL(code);
	return (val);
	};

double getMinRelativeTL(int code,int from,int to)
	{double val;
	val = getMinTrendLFromTo(code,from,to);
	val = val / getValMeanTL(code);
	return (val);
	};

double getValNumLStep(int code,int disp = 0)
	{
	double val=0.;for (int i=0;i<getNumSchede();i++) 
		val += schede[i].getNumTrendL(code,disp);
//	val *= 1000000.;// Coeff mm2 in m2
//	val /= (largeSize * trueSizeEncod);
	return(val);};


double getValTrendLStep(int code,int disp = 0)
	{
	double val=0.;for (int i=0;i<getNumSchede();i++) 
		val += schede[i].getValTrendL(code,disp);
	val *= 1000000.;// Coeff mm2 in m2
	val /= (largeSize * trueSizeEncod);
	return(val);};


double getValTrendL(int code,int disp = 0)
	{
	double corr = trueSizeEncod/1000.;
	int indexDisp;
	// Troncare in offset dal fondo -> ceil
	indexDisp = (int) ceil(disp /corr); 
	double val=0.;for (int i=0;i<getNumSchede();i++) 
		val += schede[i].getValTrendL(code,indexDisp);
	val *= 1000000.;
	val /= (largeSize * trueSizeEncod);
	return(val);};

double getValTrendL(int code,double disp = 0.)
	{
	double corr = trueSizeEncod/1000.;
	int indexDisp;
	// Troncare in offset dal fondo -> ceil
	indexDisp = (int) ceil(disp /corr); 
	double val=0.;for (int i=0;i<getNumSchede();i++) 
		val += schede[i].getValTrendL(code,indexDisp);
	val *= 1000000.;
	val /= (largeSize * trueSizeEncod);
	return(val);};

double getValDensita(int index,int code,int metri,int disp = 0)
	{double val=0.;
	ASSERT (index < getNumSchede());
	double corr = trueSizeEncod/1000.;
	int indexM,indexDisp;
	indexM = (int) (metri/corr); 
	indexDisp = (int) (disp/corr);

	val = schede[index].getValDensita(code,indexM,indexDisp);
	// Torna densita` riferita alla singola banda 
	// per decina di metri teorici
	val /= corr;
	return(val);};

double getRelativeMeter (int index)
				{return((double)((trueSizeEncod/1000.))*(index+1));};
double getMeter (){return(meter);};
void setMeter (double m){meter = m;};
void incMeter (void){meter += (double)((trueSizeEncod/1000.));};//Era castizzato ad int
double getIncMeter (void){return((double)(trueSizeEncod/1000.));};//Era castizzato ad int

double getLargeSize (){return(largeSize);};
void setLargeSize (double v){largeSize = v;};
double getTrueSizeEncod (){return(trueSizeEncod);};
void setTrueSizeEncod (double v){trueSizeEncod = v;};

BOOL save(CFile *cf){
		// Save meter,TrueSize,LargeSize
		cf->Write((void *)&meter,sizeof(meter));
		cf->Write((void *)&trueSizeEncod,sizeof(trueSizeEncod));
		cf->Write((void *)&largeSize,sizeof(largeSize));
		cf->Write((void *)&numeroClassi,sizeof(numeroClassi));
		for (int i=0;i<numeroClassi;i++)
			{
			double al = allarmeS[i];
			cf->Write((void *)&al,sizeof(al));
			}
		int numSchede = getNumSchede();
		cf->Write((void *)&numSchede,sizeof(numSchede));

		for (int i=0;i<numSchede;i++)
		{if (!schede[i].save(cf)) return FALSE;}
			return TRUE;};

BOOL load(CFile *cf,int fileVer){
		cf->Read((void *)&meter,sizeof(meter));
		cf->Read((void *)&trueSizeEncod,sizeof(trueSizeEncod));
		cf->Read((void *)&largeSize,sizeof(largeSize));
		cf->Read((void *)&numeroClassi,sizeof(numeroClassi));
		for (int i=0;i<numeroClassi;i++)
			{
			double al;
			cf->Read((void *)&al,sizeof(al));
			allarmeS[i] = al;
			}
		int numSchede;
		if	(fileVer < 02)
			numSchede = 7;
		else
			cf->Read((void *)&numSchede,sizeof(numSchede));
		init(numSchede,numeroClassi);
		for (int i=0;i<numSchede;i++) 
		{if (!schede[i].load(cf)) return FALSE;}
			return TRUE;};
int getSize(int index,int code){ASSERT (index < getNumSchede());
	return(schede[index].getSize(code));};

void Reset(void){
	for (int i=0;i<getNumSchede();i++)
		schede[i].Reset(numeroClassi);
	allarmeS.SetSize(numeroClassi);};

double getValDensFromTo(int index,int code,int from,int to)
	{
	if (from < 0) return 0.;
	if (to < 0) return 0.;
	if (from > meter) return 0.; // Fuori range
	if (to > meter) to = (int )meter;
	int delta;
	if ((delta=to-from) <= 0) return 0.;
	int disp;
	if ((disp=(int)meter-to) < 0) disp = 0;
  	return (getValDensita(index,code,delta,disp));
	};

double getMaxDensFromTo(int from,int to)
	{double max = 0.;
	double val = 0.;
	for (int i=0;i<getNumSchede();i++)
		{
		for (int j=0;j<numeroClassi;j++)
			{
  			val = getValDensFromTo(i,'A'+i,from,to);
			max = (max>val)?max:val;
			}
		}
	return (max);
	};

// Valore max trendl tra metro from e metro to
double getMaxTrendLFromTo(int code,int from,int to)
	{
	if (from < 0) return 0.;
	if (to < 0) return 0.;
	if (from > meter) return 0.; // Fuori range
	if (to > meter) to = (int )meter;
	int delta;
	if ((delta=to-from) <= 0) return 0.;
	int disp;
	if ((disp=(int)meter-to) < 0) disp = 0;
	double val;
	double max = 0.;
	for (int i=disp;i<(delta+disp);i+=10)
		{
		val = getValTrendL(code,i);
		max = (max>val)?max:val;
		}
	return (max);
	};

// Valore max trendl tra metro from e metro to
double getMinTrendLFromTo(int code,int from,int to)
	{
	if (from < 0) return 0.;
	if (to < 0) return 0.;
	if (from > meter) return 0.; // Fuori range
	if (to > meter) to = (int )meter;
	int delta;
	if ((delta=to-from) <= 0) return 0.;
	int disp;
	if ((disp=(int)meter-to) < 0) disp = 0;
	double val;
	double min = getValTrendL(code,disp);
	for (int i=disp;i<(delta+disp);i+=10)
		{
		val = getValTrendL(code,i);
		min = (min<val)?min:val;
		}
	return (min);
	};



};



/////////////////////////////////////////////////////////////////////////////
// CLineDoc document
// 1) da finestra prima di iniziare ispeziore del rotolo
// 2) in automatico con data e ora
// 3) a fine rotolo da finestra (se l'operatore non inserisce i dati il
//		cercafori non fa ispezione)
// 4) da rete con file MDB
// 5) da rete con file TXT


enum	PlantInfoMode  {PLANTINFO_START_DIALOG=0,PLANTINFO_NODATA,PLANTINFO_STOP_DIALOG,PLANTINFO_MDB,PLANTINFO_TXT};
enum    RotoloNameMode {ROTOLO_NAME_EXT=0,ROTOLO_NAME_TIME,ROTOLO_NAME_NUMCOIL};

class CLineDoc : public CDocument
{
// protected:
public:
	CLineDoc();           // protected constructor used by dynamic creation
	// DECLARE_DYNCREATE(CLineDoc)
	DECLARE_DYNAMIC(CLineDoc)

#ifndef CSM10_AMI_REPORT
CThEncoder	c_thEncoder;
#endif

// Attributes
public:
CTargetBoard c_targetBoard;

//DifCoil c_difCoil;
COILTYPE c_difCoil;
// GroupSchede schede;
// Report 100 metri
// DifRep100  dif100;
// AS400 File Param

public:
CString rotolo;		// Nome compatibile as400
int rotoloExt;		// Estensione numerica

long fileLogPosition;	// Ultima scrittura in file log

CString lega;
CString stato;
double spessore;
CString c_cliente;
double c_larghezza;
time_t startTime;
// nome base del rotolo ottenuto dalla dialog box con dati
// inseriti dall'utente
CString	c_prodCoil;

// modo di importare i dati di impianto
PlantInfoMode  c_plantInfoMode;
BOOL loadPlantInfoFromDialog(CDProduct* pProd,BOOL interactive,BOOL test);
BOOL updateInfoPlantDb(LPCTSTR nomeFile);
BOOL loadPlantInfoFromDb(void);
// new mode 2021 from txtFile
bool loadPlantInfoFromTxt(void);
//----------------------------------
CString createTitle(BOOL warmStart,BOOL passOne);
BOOL renameFile(CString fileName);
BOOL c_useManualTitle;
CString c_manualTitle;
CString c_homeDir;		// home directory
BOOL	c_disableF1OnAlarm;
BOOL	c_enableExitOnAlarm;
BOOL	c_statoAlarmAutotest;
// tempo assoluto prima del quale lo stato e` ignorato
// serve per evitare che esca subito dopo un F1
CTime	c_blindStatoHde;

// dimensione in byte dei valori delle classi
// ricevuti da VAL_COUNTER
int c_sizeClassi;
double c_sizeBande;
	
// base di calcolo densita`  
double c_densityCalcLength;

// configurazione messaggio allarme su schede/ricevitori autotest
BOOL c_skipRxInAutotestMsg;
// configurazione indice prima scheda classe AB nell'HW
// puo` essere 0 ( normale) 1 schede ab e cd invertite
int	c_firstBoardClassAB;

// Ricetta
public:
CString	nomeRicetta;
double	sogliaA;
double	sogliaB;
double	sogliaC;
double	sogliaD;

// Configurazione Allarmi
int secAlarmSound;

CDAllarmi densitaAlarm;
CDAllarmi periodAlarm;
CDAllarmi systemAlarm;
CDAllarmi clusterAlarm;

// contatore cluster per visualizzazione sotto grafici
int		c_clusterCounter [4];
// valore di stato cluster su output gestito da threadEncoder
BOOL c_signalCluster;
int	c_signalClusterTime;		// durata impulso allarme cluster			
int c_fixSignalClusterTime;		// valore dal .ini


BOOL diskAlarmActive;
BOOL serAlarmActive;

BOOL serMsgReceived;
// Parametri
public:
double	c_fattCorrDimLin;		// Centimetro reale
double	c_lunResStop;			// Dimensione residua allo stop
long	c_holeClassDigiOut;		// classe fori utilizzata per allarme digitale

// Stati sistema
// Indica Sistema attivo
BOOL systemOnLine;
BOOL systemOnTest;

BOOL c_doUseIo;			// abilita uso pagina IO 

// Indica sistema trasferisce 100 m
BOOL systemLoading;
// I dati sono stati caricati 
BOOL loadedData;
// stato HDE
int c_statoHde;		// stato del micro ricevuto e visualizzato
// valori input
unsigned short c_valInput;
// comando nuovo per disegno
BOOL c_doUseDiaframmi;
double c_posDiaframmaSx;
double c_posDiaframmaDx;
double c_posDiaframma; 
// comando nuovo per test 
int c_rawPosDiaframmaSx;
int c_rawPosDiaframmaDx;

//------------------------
// Layout
double TrendLScalaY;
double TrendDScalaY;
int TrendLScalaX;

// valore in metri di ogni step asse X
// 10 metri primi sistemi
// 1 metro sistema circuit foil
// int BaseStepX;
int TrendDScalaX;

// identifica ID vista attiva (serve per CDynViewDocTempl)
int viewID;

// Nome fabbrica
CString c_company;

// Operations
public:

CLineDoc& operator = (CLineDoc& source);
// clear all alarm
// serve per abilitazione F1
void clearAlarm (void);
//
CString& getCompany(void) {return(c_company);};
void appendSysMsg(int evento);

// Format string for threshold visualization
CString formatStringThreshold();

// generazione difetti random 
void HoleGenerator(void); 
// Timer setting demo
int millisec;
// valore in metri tra due difetti
int c_holeRate;

// Aggiornamento allarmi soglie difetti
void updateDifAlarm(void); // usata da holeGeneartor e gestValCounter
void updateDifCluster(void); // usata da holeGeneartor e gestValCounter
void terminateSignalCluster(void);

// Calcolo dinamico bande visualizzate
BOOL setSizeBande(void);
// -----------------
// combina nome rotolo con estensione
RotoloNameMode c_rotoloNameMode;
CString getRotolo(void)
	{CString s0,s1(rotolo);
	if ((c_rotoloNameMode==ROTOLO_NAME_EXT)&&
//	if (((c_plantInfoMode==PLANTINFO_START_DIALOG)||
//		(c_plantInfoMode==PLANTINFO_STOP_DIALOG))&&
		(rotoloExt > 0)&&(s1 != _T("")))
		{s0.Format (_T("_%02d"),rotoloExt);
		s1 += s0;}
		return (s1);
	if (c_rotoloNameMode==ROTOLO_NAME_TIME)
		{

		}
	};

ViewType GetActualViewType(void);
int GetActualViewID(void);
int GetViewID(CView* pView);
// Chiamata da CDocTemplate per abilitare cambio viste
BOOL isChangeViewEnabled(int id ){
	if ((id == ID_VIEW_AS_BEGIN)||
		(id == ID_VIEW_AS_END))
		{// prima e ultima view init e IO offLine
		if (id == ID_VIEW_AS_END)
			return (c_doUseIo&&(!systemOnLine));
		else
			return(!systemOnLine);
		}
	else
		return(systemOnLine);
	};


// init memory at start every coil
void newCoil (BOOL warmStart);

// generazione report mdb
BOOL genReportDbFori(LPCTSTR nomeFile);

CString vFori;
int GetForiCod(void) {if (vFori == "A") return 'A';
					if (vFori == "B") return 'B';
					if (vFori == "C") return 'C';
					if (vFori == "D") return 'D';
					return('A');
					};
CString &getFori(void){return(vFori);};
// Insert Values
void SetTrendLScalaY (double v){TrendLScalaY = v;UpdateAllViews(NULL);};
void SetTrendLScalaX (int v){TrendLScalaX = v;UpdateAllViews(NULL);};
void SetTrendDScalaY (double v){TrendDScalaY = v;UpdateAllViews(NULL);};
void SetTrendDScalaX (int v){TrendDScalaX = v;UpdateAllViews(NULL);};

// Available For Views
double GetTrendLScalaY (void){ return(TrendLScalaY);};
int GetTrendLScalaX (void){ return(TrendLScalaX);};
double GetTrendDScalaY (void){ return(TrendDScalaY);};
int GetTrendDScalaX (void){ return(TrendDScalaX);};
// Dimensione in metri di ogni step

// double GetTrendStep (void){ return(c_fattCorrDimLin * BaseStepX);};
double GetTrendStep (void){ return(c_difCoil.getMeterBaseStepX());};


// Operations
public:
// modificata struttura memo difetti 
// da schede a DifCoil
void GetVPosition(CString &str)
	{str.Format (_T("%4.0lf"),c_difCoil.getMeter());};
int GetVPosition(void )
	{return ((int ) c_difCoil.getMeter());};

int getVTotalLength(void )
	{int v = (int ) (c_difCoil.getMeter() - c_lunResStop);
	return (v>0?v:0);};


int GetTrendLVPositionDal(void )
	{int v;
	v = (int )(c_difCoil.getMeter() - TrendLScalaX);
	v = v>0 ? v : 0;
	return v;};

int GetTrendDVPositionDal(void )
	{int v;
	v = (int )(c_difCoil.getMeter()-TrendDScalaX);
	v = v>0 ? v : 0;
	return v;}; 


void GetVPositionDal(CString &str,ViewType type)
 	{int v;
	switch (type)
		{case DENSVIEW:
			v = GetTrendDVPositionDal(); 
			break;
		default:
		case LINEVIEW :
			v = GetTrendLVPositionDal();
			break;
		}
	str.Format (_T("%d"),v);};

void GetVPositionDal(CString &str)
	{
	GetVPositionDal(str,GetActualViewType());
	};

CString &GetVCliente(void)
	{return (c_cliente);};

// estrae dalla directory estensione numerica dei file esistenti
int getLastRotoloExt(CString &baseRotolo);

// Modificati valori di configurazione visualizzazione
BOOL UpdateBaseDoc(void);

// Chiusura acquisizione
void FileSaveStop(BOOL doSave=TRUE); 
// salvataggio dati
void FileSave(BOOL warmSave = TRUE);
// inizializzazione comuni a Start e Cambio Asse 
void FileStart(BOOL warmStart); 

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CLineDoc)
	public:
	virtual void Serialize(CArchive& ar);   // overridden for document i/o
	virtual void OnCloseDocument();
	virtual BOOL CanCloseFrame(CFrameWnd* pFrame);
	protected:
	virtual BOOL OnNewDocument();
	//}}AFX_VIRTUAL

// Implementation
public:
	void setTitle(LPCTSTR title);
	CString c_fileName;
	CString getFileName(void);
	void gestValExtCounter(PVOID p, DWORD size);
	// Gestione sistema micro (receive)
	void gestValHole (PVOID p,DWORD size); // Gestisce arrivo VAL_COUNTER
	void gestValPeriod (PVOID p,DWORD size); // Gestisce arrivo VAL_PERIOD
	void gestValAlarm (PVOID p,DWORD size); // Gestisce arrivo VAL_COUNTER
	void gestValTestSer (PVOID p,DWORD size); // Gestisce arrivo VAL_PERIOD
	void gestValStatoHde (PVOID p,DWORD size); // Gestisce arrivo VAL_COUNTER
	void gestValStop(PVOID p,DWORD size);
	void gestValClose (PVOID p,DWORD size); 
	void gestValOpen (PVOID p,DWORD size); 
	void gestValDiaframma (PVOID p,DWORD size); 
	void gestValExtDiaframma (PVOID p,DWORD size); 
	void gestValInput (PVOID p,DWORD size); 
	void gestValRisAutotest(PVOID p,DWORD size);
	void gestValFt (PVOID p,DWORD size); // Gestisce arrivo VAL_FT
	// variabile globale, devo passare da thread encoder a 
	// getsValEncoder con mainFrame->Message, metto ultimo valore qui
private:	
	BOOL c_valAlarmEncoder;
	CCriticalSection c_cs;
public:
	void setValAlarmEncoder(BOOL v);
	BOOL getValAlarmEncoder(void);
	void gestValAlEncoder (void); 

	// Gestione sistema micro (send)
	void sendStartStop (BOOL start,BOOL test);
	void sendAlarmDif (void);
	void sendValSoglie (void);
	void sendValFcdl (void);
	void sendValAlPulse (void);
	void sendValOutput (unsigned short v);
	void sendIOStartStop (BOOL start);
	void sendStartTestDiaf (BOOL left);
	void sendValClassDigitalOut (void);
	void sendAlarmEncoder (int val);
	void sendAutotest (void);
	void sendValComPc (void);
	void sendAlarmDif (BOOL alarm);

	BOOL save (CFileBpe *cf,CProgressCtrl *progress=NULL);
	BOOL load (CFileBpe *cf,CProgressCtrl *progress=NULL);
	BOOL rotoloSelect(void); 

	virtual ~CLineDoc();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

	// Generated message map functions
protected:
	//{{AFX_MSG(CLineDoc)
	afx_msg void OnTestgrafico();
	afx_msg void OnViewAggiorna();
	afx_msg void OnFileStart();
	afx_msg void OnFileStop();
	afx_msg void OnViewPeriodici();
	afx_msg void OnViewTrend();
	afx_msg void OnUpdateViewPeriodici(CCmdUI* pCmdUI);
	afx_msg void OnUpdateViewTrend(CCmdUI* pCmdUI);
	afx_msg void OnUpdateFileStart(CCmdUI* pCmdUI);
	afx_msg void OnUpdateFileStop(CCmdUI* pCmdUI);
	afx_msg void OnViewSystem();
	afx_msg void OnUpdateViewSystem(CCmdUI* pCmdUI);
	afx_msg void OnFileTstart();
	afx_msg void OnUpdateFileTstart(CCmdUI* pCmdUI);
	afx_msg void OnConfiguraProdotto();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnAutotest();
	// Valore ricevuto msg valFT
	int c_valFt1;
	// Valore ricevuto msg ValFT
	int c_valFt2;
	afx_msg void OnUpdateAutotest(CCmdUI *pCmdUI);
	afx_msg void OnFileStartPsw();
};
