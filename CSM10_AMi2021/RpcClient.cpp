// RpcClient.cpp : implementation file
//

#include "stdafx.h"
#include "resource.h"
#include "Profile.h"
#include "Registry.h"

#include "RpcClient.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


/////////////////////////////////////////////////////////////////////////////
// CRpcClient

CRpcClient::CRpcClient()
{
c_procServer = "C:/USER/RpcServer/Debug/RpcServer.exe";
c_dirServer = "C:/USER/RpcServer";
c_serMsgReceived = FALSE;
c_rpcOffLine = FALSE;
}

CRpcClient::~CRpcClient()
{
}



/////////////////////////////////////////////////////////////////////////////
// CRpcClient message handlers

void CRpcClient::rpcRegister(CString& wndName,int thisId,int remoteId,RPCTYPE type)
{
CString strValue;

// Nessuna connessione richiesta
if (c_rpcOffLine)
	return;


// Init addressId
c_thisRpcId = thisId;
c_remoteRpcId = remoteId;
c_thisWnd = wndName;
c_type = type;


CProfile profile;
// Init Server Process
// CProfile profile;
// CRegistry registry ("EDS","SIST120VE");


//c_dirServer = registry.GetProfileString(_T("SERVER"), _T("Path"), 
//				"");
c_dirServer = profile.getProfileString(_T("SERVER"), _T("Path"), 
				_T(""));

c_procServer = c_dirServer;

//c_procServer += registry.GetProfileString(_T("SERVER"), _T("Name"), 
//				"RpcServer.exe");

c_procServer += profile.getProfileString(_T("SERVER"), _T("Name"), 
				_T("RpcServer.exe"));

// Remote on serialPort
strValue = profile.getProfileString(_T("init"), _T("commDevice"), 
				_T("COM1"));

// Offline?
c_rpcOffLine = profile.getProfileBool(_T("init"), _T("OffLineMode"),FALSE);

// Create Server process if needed
RegisterClient (strValue,c_remoteRpcId);

// Enable Abort Notify
RegisterClient (c_thisWnd,c_thisRpcId,4,TRUE);
}



BOOL CRpcClient::RegisterClient (CString &nome,RPCID id,int frameSize,BOOL abortNotify)
{
URpcInfo rpcI;
ClientInfo cInfo;
COPYDATASTRUCT cds;

// Nessuna connessione richiesta
if (c_rpcOffLine)
	return TRUE;


int i;
for (i =0;i<nome.GetLength();i++)
	cInfo.c_name [i] = nome [i];
while (i < CINFOSIZENAME)
	cInfo.c_name[i++] = 0;

cInfo.c_id = id;
cInfo.c_type = c_type;
cInfo.c_FrameSize = frameSize;
cInfo.c_abortNotify = abortNotify;


rpcI.infoDMCS.dest = RPCID_SERVER;
rpcI.infoDMCS.sender = c_thisRpcId;
rpcI.infoDMCS.command = REGISTER_CLIENT;
rpcI.infoDMCS.size = sizeof (cInfo);



cds.dwData = rpcI.lwInfo;
cds.cbData = sizeof (cInfo);
cds.lpData = (void *) &cInfo;

CWnd *server = CWnd::FindWindow(RPC_SERVER_CLASSNAME,NULL);
if (!server)
	{
	PROCESS_INFORMATION ProcessInformation;
	STARTUPINFO StartUpInfo;
	
	::GetStartupInfo (&StartUpInfo);
	BOOL fSuccess = CreateProcess((LPCTSTR)c_procServer,NULL,
				NULL,NULL,FALSE,CREATE_NEW_CONSOLE | NORMAL_PRIORITY_CLASS,
				NULL,(LPCTSTR)c_dirServer,
				&StartUpInfo,&ProcessInformation);
  

	if (fSuccess) 
		{
		// Wait Creation Window RpcServer
		WaitForInputIdle(ProcessInformation.hProcess,5000); // wait for 5 sec
	 	CloseHandle(ProcessInformation.hThread);
		CloseHandle(ProcessInformation.hProcess);

		server = CWnd::FindWindow(RPC_SERVER_CLASSNAME,NULL);
		}
	else
		{
		DWORD err = GetLastError();
		CString s;
		s.Format (_T("Unable to Create SERVER %s Err. %d"),c_procServer,err);
		AfxMessageBox (s);
		return(TRUE);
		}
	}

if (server)
	{
	server->SendMessage (WM_COPYDATA,
		(WPARAM)AfxGetApp()->m_pMainWnd->GetSafeHwnd(),
		// (WPARAM) this,
		(LPARAM)&cds);
	}
else
	{
	CString s;
	s = _T("Not Found SERVER ") + c_procServer;
	AfxMessageBox (s);
	}
	

return(TRUE);
}


// Server Data/Request through WM_COPYDATA
LRESULT CRpcClient::OnServerRequest(WPARAM wParam, LPARAM lParam)
{

COPYDATASTRUCT *pcds = (COPYDATASTRUCT *) lParam;
URpcInfo rpcInfo;

rpcInfo.lwInfo = pcds->dwData;

// Test For Local Command
if (rpcInfo.infoDMCS.dest == c_thisRpcId)
	{
	serverRequest (rpcInfo.infoDMCS.command,
				   pcds->cbData,
				   pcds->lpData);
					
	}

return 1;
}


/////////////////////////////////////////////////////////////////////////////
// CRpcClient message handlers


BOOL CRpcClient::SendGeneralCommand(Command *command)
{
URpcInfo rpcI;
COPYDATASTRUCT cds;


// Nessuna connessione richiesta
if (c_rpcOffLine)
	return TRUE;

// Dest HC16

rpcI.infoDMCS.dest = c_remoteRpcId;
rpcI.infoDMCS.sender = c_thisRpcId;
rpcI.infoDMCS.command = (char )command->cmd;
rpcI.infoDMCS.size = (char)command->size;



cds.dwData = rpcI.lwInfo;
cds.cbData = command->size;
cds.lpData = (void *) command->data;

CWnd *server = CWnd::FindWindow(RPC_SERVER_CLASSNAME,NULL);
if (!server)
	{
	AfxMessageBox (_T("SERVER not Found"));
	return(FALSE);
	}

if (server)
	{
	return (server->SendMessage (WM_COPYDATA,
			(WPARAM)AfxGetApp()->m_pMainWnd->GetSafeHwnd(),
			// (WPARAM) this,
			(LPARAM)&cds));
	}
else
	AfxMessageBox (_T("Not Found SERVER"));

return(FALSE);
}


BOOL CRpcClient::serverRequest (int cmd,int size,void *pter)
{

// Override this virtual function in CMainFrame

return FALSE;
}
