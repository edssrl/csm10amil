// CSM.h : main header file for the CSM application
//

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"       // main symbols




/////////////////////////////////////////////////////////////////////////////
// CCSMApp:
// See CSM.cpp for the implementation of this class
//

#include "CDaoDbCreate.h"

#include "EnumPrinters.h"



#ifdef MAIN
CDaoDbCreate *dBase = NULL;
#else
extern CDaoDbCreate *dBase;
#endif

class CCSMApp : public CWinApp
{
HMODULE		hModRes;	// Resource handle modificato 
HMODULE		hDefRes;	// Resource handle originario	

CEnumPrinters	m_PrinterControl ;

public:
	CCSMApp();
	~CCSMApp(){if (dBase != NULL) 
			{
			delete dBase;
			dBase = NULL;}
			};

// DBase Declaration
CDynViewDocTemplate* pDocTemplate;
CDocument *GetDocument (void ) 
	{POSITION pos = pDocTemplate->GetFirstDocPosition(); 
	return(pDocTemplate->GetNextDoc(pos));};

// Print setting
void SetPrintLandscape(void);
void SetPrintPortrait(void);
void savePrinterSettings(void){// save the printer selection for a next run restore
					m_PrinterControl.SavePrinterSelection(m_hDevMode, m_hDevNames) ;
					};

// return the name of the currently selected printer
CString GetDefaultPrinter(void);
void  SetIniPath();


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CCSMApp)
	public:
	virtual BOOL InitInstance();
	virtual int ExitInstance();
	//}}AFX_VIRTUAL

// Implementation

	//{{AFX_MSG(CCSMApp)
	afx_msg void OnAppAbout();
	afx_msg void OnFilePrintSetup();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};
/////////////////////////////////////////////////////////////////////////////
