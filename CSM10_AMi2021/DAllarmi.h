// DAllarmi.h : header file
//

#include "Color.h"
#include "FontInfo.h"
#include "CFileBpe.h"


/////////////////////////////////////////////////////////////////////////////
// CDAllarmi dialog
#ifndef  CDALLARMI
#define CDALLARMI 

class CDAllarmi : public CDialog
{
FontInfo c_fNormal;
FontInfo c_fCaption;

// Accelerator personale allarmi
HACCEL c_hAccel;


// Construction
public:
	CDAllarmi& operator=(CDAllarmi& source);
	void visualizza(CWnd *pWnd,int secAlarmSound=0);
	void insert(CString &str,CWnd *pWnd,int secAlarmSound = 5);
	CDAllarmi(CWnd* pParent = NULL);   // standard constructor

CString c_alarmType;
BOOL c_MemoData;

// devo scrivere solo attivazione e disattivazione allarmi
BOOL c_statusDensClassA;
BOOL c_statusDensClassB;
BOOL c_statusDensClassC;
BOOL c_statusDensClassD;


void SetTitle(CString &title){c_alarmType = title;};

BOOL c_alarmSoundActive;
int c_secAlarmSoundToEnd;
void setAlarmSound(int sec);
void randomMoveWindow(void );	 // Random Move

// 1998
void fixMoveWindow(void);

UINT c_soundID;

void setMemoData (BOOL v) {c_MemoData = v;};

CArray <CString,CString&> c_messages;
void clearMessages (void){if (GetSafeHwnd() != NULL)DestroyWindow();	
						c_messages.RemoveAll();
						c_statusDensClassA=FALSE;c_statusDensClassB=FALSE;
						c_statusDensClassC=FALSE;c_statusDensClassD=FALSE;
						};

void appendMsg(CString &cStr)
	{CString s;
	if (c_MemoData)
		{CTime t = CTime::GetCurrentTime();
		s = t.Format(_T("%x %X"));
		s += _T(" ")+cStr;
		}
	else
		s = cStr;
	c_messages.Add(s);
	};

// private Serialization
BOOL save(CFileBpe *cf);
BOOL load(CFileBpe *cf,bool notUnicode,int ver);

// stampa 
int print(int index,int numLine,CRect &p,CDC* pdc);
int getNumLine (void ) {return (c_messages.GetSize());};
int getSizeY (CDC* pDC);
int getSizeChar (CDC* pDC);


// Dialog Data
	//{{AFX_DATA(CDAllarmi)
	enum { IDD = IDD_ALLARMI };
	CEdit	m_Message1;
	//}}AFX_DATA

BOOL isActive (void){return(c_alarmSoundActive);};

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CDAllarmi)
	public:
	virtual BOOL DestroyWindow();
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CDAllarmi)
	afx_msg void OnTimer(UINT nIDEvent);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

#endif