#include "stdafx.h"
#include "InfoPlant.h"
#include "Profile.h"


CInfoPlant::CInfoPlant(void) :
	c_nomeFile(_T("")),
	c_cliente(_T("")),
	c_lega(_T("")),
	c_stato(_T("")),
	c_rotolo(_T(""))
	{
	CProfile profile;
	c_nomeFile = profile.getProfileString(_T("InfoPlant"),_T("NomeFileTxt"),_T("CoilData.txt"));
	}


CInfoPlant::~CInfoPlant(void)
	{
	}


// carica da file, tipo aspo = "B" oppure "S" 
// Formato
// COIL:0000123456
// CLIENTE:nomeDelCliente
// ORDINE:IdOrdine

bool CInfoPlant::loadFromTxt()
	{
	if (!open (c_nomeFile))
		return false;
	// creazione nome file da campi 
	readField(CString("COIL"),1,c_rotolo);
	readField(CString("CLIENTE"),1,c_cliente);
	readField(CString("ORDINE"),1,c_ordine);
	close();
	return true;
	}

