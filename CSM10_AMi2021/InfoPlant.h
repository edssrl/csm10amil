#pragma once

#include "RTextFile.h"

class CInfoPlant : public TextFile
	{
	CString c_rotolo;
	CString c_cliente;
	CString c_ordine;
	CString c_lega;
	CString c_stato;
	double c_larghezza;
	double c_spessore;
//----------------------
// Nome file info plant
	CString c_nomeFile;
	// chiavi in uso per accesso al file txt,
	// trovate quando si caricano dati di targa del coil
	// usate per cambiare stato nel file txt dopo aver generato il report
	CString c_currentKey;
	CString c_currentSubKey;
	public:
		CInfoPlant(void);
		~CInfoPlant(void);
		// carica da file 
		bool loadFromTxt();
		
	
	};

