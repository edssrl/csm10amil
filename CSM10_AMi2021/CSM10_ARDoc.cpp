// CSM10_AR.cpp : implementation of the CSM10_ARDoc class
//

#include "stdafx.h"
#include "resource.h"


#include "GraphDoc.h"
#include "GraphView.h"
#include "dyntempl.h"
#include "CSM10_AR.h"

#include "DCustomer.h"
#include "DCompany.h"
#include "DProduct.h"
// #include "DSimOption.h"

#include "CSM10_ARDoc.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CSM10_ARDoc

IMPLEMENT_DYNCREATE(CSM10_ARDoc, CLineDoc)

BEGIN_MESSAGE_MAP(CSM10_ARDoc, CLineDoc)
	//{{AFX_MSG_MAP(CSM10_ARDoc)
	ON_COMMAND(ID_OPTIONS_COMPANY, OnOptionsCompany)
	ON_COMMAND(ID_OPTIONS_CUSTOMER, OnOptionsCustomer)
	ON_COMMAND(ID_OPTIONS_SIMUL, OnOptionsSimul)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CSM10_ARDoc construction/destruction

CSM10_ARDoc::CSM10_ARDoc()
{
	// TODO: add one-time construction code here

}

CSM10_ARDoc::~CSM10_ARDoc()
{
}

BOOL CSM10_ARDoc::OnNewDocument()
{
	if (!CLineDoc::OnNewDocument())
		return FALSE;

	// TODO: add reinitialization code here
	// (SDI documents will reuse this document)

	return TRUE;
}



/////////////////////////////////////////////////////////////////////////////
// CSM10_ARDoc serialization

void CSM10_ARDoc::Serialize(CArchive& ar)
{
	if (ar.IsStoring())
	{
		// TODO: add storing code here
	}
	else
	{
		// TODO: add loading code here
	}
}

/////////////////////////////////////////////////////////////////////////////
// CSM10_ARDoc diagnostics

#ifdef _DEBUG
void CSM10_ARDoc::AssertValid() const
{
	CDocument::AssertValid();
}

void CSM10_ARDoc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CSM10_ARDoc commands



void CSM10_ARDoc::OnOptionsCompany() 
{
	// TODO: Add your command handler code here
CDCompany company;

company.DoModal();
}

void CSM10_ARDoc::OnOptionsCustomer() 
{
	// TODO: Add your command handler code here
CDCustomer customer;

customer.DoModal();
		
}

void CSM10_ARDoc::OnOptionsSimul() 
{
	// TODO: Add your command handler code here
// CDSimOption sOpt;

// sOpt.DoModal();
	
}

