//----------------------------------------------------------
//
//			Gestione stato allami
//
//----------------------------------------------------------

#include "stdafx.h"
#include "StatoAllarmi.h"

void StatoAllarmi::draw (CDC *pdc,CRect rect)
{

// ogni oggetto occupa 8 pixel 5 per simbolo 3 per separatore
// ogni 8 oggetti separatore  1.5
CPoint p0 (rect.left,rect.top+5);
CPoint p1 (p0);
CPen penOff,penOn,penFault,*oldPen;
penOff.CreatePen(PS_SOLID | PS_ENDCAP_SQUARE   ,4,RGB(96,96,96));
penOn.CreatePen(PS_SOLID | PS_ENDCAP_SQUARE   ,4,RGB(0,255,0));
penFault.CreatePen(PS_SOLID | PS_ENDCAP_SQUARE ,4,RGB(255,0,0));
oldPen = pdc->SelectObject(&penOff);

// CPU
if (1)
	{
	for (int i=0;i<alNumCpu;i++)
	{
	p0.x += 8;
//	if ((i%8) == 0)
	// No separatore aggiuntivo
//		p0.x +=4;
	p1 = p0;
	p1.y += 4;
	pdc->MoveTo(p1);
	p1.y = rect.bottom - 10;
	switch(cpu[i])
		{
		default:
		case AL_OFF :
		case AL_OK1 :
			pdc->SelectObject(&penOff);
			break;
		case AL_OK :
			pdc->SelectObject(&penOn);
			break;
		case AL_ON :
			pdc->SelectObject(&penFault);
			break;
		}
	pdc->LineTo(p1);
	}
	}
// ricevitori
p0.x += 8;
if (1)
	{
	// for (int i=0;i<alNumReceiver;i+=2)
	for (int i=0;i<alNumReceiver;i++)	
		{
		p0.x += 6;
		// No separatore aggiuntivo
		if ((i%16) == 0)
			// separatore aggiuntivo
			p0.x += 3;
		// scheda bassa
		p1 = p0;
		// tolgo 5 sopra e 6 sotto = 11 a meta`
		// + 2 in basso spazio in mezzo 4
		// riga intera
		// p1.y += ((rect.Size().cy-11)/2 + 3);
		pdc->MoveTo(p1);
		// Fine riga
		p1.y = rect.bottom - 6;
		switch(receiver[i])
			{
			default:
			case AL_OFF :
			case AL_OK1 :
				pdc->SelectObject(&penOff);
				break;
			case AL_OK :
				pdc->SelectObject(&penOn);
				break;
			case AL_ON :
				pdc->SelectObject(&penFault);
				break;
			}
		pdc->LineTo(p1);
	
		continue;
		// Caso numero bande dispari
		// viene disegnata mezza scheda non attiva
		// scheda sopra
		p1 = p0;
		pdc->MoveTo(p1);
		// Fine mezza riga 
		p1.y += ((rect.Size().cy-11)/2 - 3);
		switch(receiver[i+1])
			{
			default:
			case AL_OFF :
			case AL_OK1 :
				pdc->SelectObject(&penOff);
				break;
			case AL_OK :
				pdc->SelectObject(&penOn);
				break;
			case AL_ON :
				pdc->SelectObject(&penFault);
				break;
			}
		pdc->LineTo(p1);
		}
	}
p0.x += 8;

// Ricevitori
if (0)
	{
	for (int i=0;i<alNumLaser;i++)
	{
	p0.x += 3;
	if ((i%8) == 0)
		// separatore aggiuntivo
		p0.x += 3;
	if (i %2 == 1)
		{
		p1 = p0;
		pdc->MoveTo(p1);
		// Fine meta` riga 
		// parte alta
		p1.y += ((rect.Size().cy -11)/2)-3;
		}
	else
		{
		p1 = p0;
		p1.y += ((rect.Size().cy-11)/2 +3);
		pdc->MoveTo(p1);
		// Fine riga
		p1 = p0;	
		p1.y = rect.bottom - 6 ;
		}
	
	switch(laserKO[i])
		{
		default:
		case AL_OFF :
		case AL_OK1 :
			pdc->SelectObject(&penOff);
			break;
		case AL_OK :
			pdc->SelectObject(&penOn);
			break;
		case AL_ON :
			pdc->SelectObject(&penFault);
			break;
		}
	pdc->LineTo(p1);
	}
	}	
pdc->SelectObject(oldPen);
penOff.DeleteObject();
penOn.DeleteObject();
penFault.DeleteObject();
}
