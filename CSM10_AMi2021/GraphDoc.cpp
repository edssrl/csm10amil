// GraphDoc.cpp : implementation file
//

#include "stdafx.h"
#include "MainFrm.h"
#include "resource.h"

#include "Endian.h"

#include "GraphDoc.h"
#include "GraphView.h"
#include "RollView.h"
#include "TowerView.h"
#include "MLineView.h"

#include "dyntempl.h"
#include "CSM10_AR.h"

#include "DCustomer.h"
#include "DCompany.h"
#include "DProduct.h"
#include "DCluster.h"

// #include "DSimOption.h"


#include "DbFori.h"
#include "DbInfoPlant.h"
#include "InfoPlant.h"

#include "Dialog.h"
//#include "RTextFile.h"
#include "DBSet.h"			// DB
#include "Endian.h"
#include "pwdlg.h"			// Password


struct RandDif 
	{
	int pos;
	int scheda;
	int classe;
	RandDif (void){pos = -1;scheda = -1;classe=-1;};
	}randDif;



#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif



//////////////////////////////////////////////////////////////////////
//	Class Scheda

Scheda::Scheda (void)
{
id = 0;
// Clear vector, init totali, 4 classi ; size infinito
Reset(4);
}


// Somma tutti i difetti di code fra metro from e metro To
double Scheda::getTotDifetti(int code,int from,int to)
{
double tot = 0.;
int indexFrom,indexTo;
int i;
// indexFrom = from/10;

// indexTo = to/10;

//16-06-97
if (size == INFINITO)
	return (0.);

indexFrom = from;
indexTo = to;

if (indexFrom<0) return (0.);
if (indexFrom>=indexTo) return (0.);
if (indexTo<0) return (0.);


if (indexTo > gValS[code-'A'].GetSize()) return (0.);
if (indexFrom>gValS[code-'A'].GetSize()) return (0.);

for (i = indexFrom;i<indexTo;i ++)
	{
	tot += (double )gValS[code-'A'][i];
	}

return(tot);
}

// Funzione di calcolo Num Longitudinale
// torna valori di Num disp decametri prima ultimo 
// Valore disponibile

double Scheda::getNumTrendL(int code,int disp)
{
double trend = 0.;
int index;

//16-06-97
if (size == INFINITO)
	return (0.);

// Disp in metri;
// int dispIndex = disp / 10;
int dispIndex = disp;

index = gValS[code-'A'].GetUpperBound() - dispIndex;
if (index < 0)
	trend = 0.;
else
	trend = (double )gValS[code-'A'][index];
return(trend);
}


// Funzione di calcolo Trend Longitudinale
// torna valori di trend disp decametri prima ultimo 
// Valore disponibile
// Per avere

double Scheda::getValTrendL(int code,int disp)
{
double trend = 0.;
int index;

// Disp in metri;
// int dispIndex = disp / 10;
int dispIndex = disp;

//16-06-97
if (size == INFINITO)
	return (0.);

index = gValS[code-'A'].GetUpperBound() - dispIndex;
if (index < 0)
	trend = 0.;
else
	trend = (double )gValS[code-'A'][index];

return(trend);
}

double Scheda::getTotal(int code)
{
double total=0.;
if (gValS[code-'A'].GetSize() > 0)
	{
	total = totalS[code-'A'];
	}

return(total);
}

// Funzione di calcolo Densita` Trasverasle
// Torna Valore densita` media relativa a 'tot metri'
// disp metri prima della fine acquisizione
// 0----|.............|-------F
//	    <------metri->disp	 
// se disp = 0 allora torna ultimi totmetri
double Scheda::getValDensita(int code,int metri,int disp)
{
double trend = 0.;
//int nSample = metri/10;

int nSample = metri;

int fromIndex,toIndex;

toIndex = ((gValS[code-'A'].GetUpperBound()-(disp))>0) ? (gValS[code-'A'].GetUpperBound()-(disp)):gValS[code-'A'].GetUpperBound();
fromIndex = ((toIndex-nSample)>0) ? (toIndex-nSample):0;
if (fromIndex == 0)
	nSample = (gValS[code-'A'].GetSize() > 0)?gValS[code-'A'].GetSize():1;
for (int i=fromIndex;i<=toIndex;i++) 
	{
	trend += (double)gValS[code-'A'][i];
	}
trend /= (double) nSample;


trend /= (size/1000.); // size in mm
return(trend);
}

BOOL Scheda::save(CFile *cf)
{
// Molto Brutale				

// Save ID
cf->Write((void *)&id,sizeof(int));

// Save size larghezza scheda in mm
cf->Write((void *)&size,sizeof(int));

int numClassi = gValS.GetSize();
// Save numero classi
cf->Write((void *)&numClassi,sizeof(int));


// Save Code
int code = 'A';
for (int i=0;i<gValS.GetSize();i++)
	{
	int g;
	cf->Write((void *)&code,sizeof(int));
	// save totalA
	cf->Write((void *)&totalS[i],sizeof(int));
	// save Size
	g = gValS[i].GetSize();
	cf->Write((void *)&g,sizeof(int));

	for (int j =0;j<gValS.GetSize();j++)
		{
		g = gValS[i][j];
		cf->Write((void *)&g,sizeof(int));
		}
	code ++;
	}

return (TRUE);
}



BOOL Scheda::load(CFile *cf)
{
// Molto Brutale				

// Load ID
cf->Read((void *)&id,sizeof(int));

// numero schede variabile (14-11-97)
// Eliminato controllo su id scheda
/*----------------------
if (id > 7)
	{
	CString s;
	s.Format ("Load File ID Scheda == %d",id);
	AfxGetMainWnd()->MessageBox(s,"Errore");
	return FALSE;
	}
---------------------------------*/


// Load size larghezza in mm
cf->Read((void *)&size,sizeof(int));

// Load numero classi
int numClassi;
cf->Read((void *)&numClassi,sizeof(int));

gValS.SetSize (numClassi);
totalS.SetSize(numClassi);

for (int i=0;i<numClassi;i++)
	{
	int g;
	// Load Code A
	cf->Read((void *)&g,sizeof(int));
	if (g != (i+'A'))
		{
		CString s;
		s.Format (_T("Load File Code %d Scheda == %d"),'A'+i,g);
		AfxGetMainWnd()->MessageBox(s,_T("Error"));
		return FALSE;
		}

	// load totalA
	cf->Read((void *)&g,sizeof(int));
	totalS[i] = g;

	// load Size
	// Clear ARRAY		   CArray
	gValS[i].RemoveAll();
	int sizeArray;
	cf->Read((void *)&sizeArray,sizeof(int));
	// Load Data
	for (int j =0;j<sizeArray;j++)
		{int nb;
		nb=cf->Read((void *)&g,sizeof(int));
		if (nb>0)
			gValS[i].Add(g);
		else
			{
			CString s;
			s.Format (_T("Load File EOF Scheda"));
			AfxGetMainWnd()->MessageBox(s,_T("Error"));
			return FALSE;
			}
		}
	}


return (TRUE);
}




/////////////////////////////////////////////////////////////////////////////
// CLineDoc

// IMPLEMENT_DYNCREATE(CLineDoc, CDocument)
IMPLEMENT_DYNAMIC(CLineDoc, CDocument)

// Torna 0 per InitView 
// Torna 1 per LineView
// Torna 2 per BarView
// Torna 0 per InitView 
// Torna 1 per LineView
// Torna 1 per BarView

int CLineDoc::GetActualViewID(void)
{
POSITION pos;

pos = GetFirstViewPosition();
ASSERT (pos);

viewID = (( CDynViewDocTemplate *)GetDocTemplate())->
					GetViewID(GetNextView(pos));

return (viewID);
}


ViewType CLineDoc::GetActualViewType(void)
{
POSITION pos;
ViewType vt;

pos = GetFirstViewPosition();
ASSERT (pos);

CInitView *view;
view = (CInitView *) GetNextView(pos);

//viewType  = (( CDynViewDocTemplate *)GetDocTemplate())->
//					GetViewID(GetNextView(pos));

vt = view->getViewType();

return (vt);
}


//----------------------------------------------------------------------------
CLineDoc::CLineDoc()
	: c_valFt1(0)
	, c_valFt2(0)
{
c_enableExitOnAlarm = FALSE;
//CCSMApp* pApp = (CCSMApp*)AfxGetApp();
// Init numero schede
CProfile profile;

c_difCoil.setMmBaseStepX(profile.getProfileInt(_T("Init"), _T("BaseStepX"),1)*1000); 
// BaseStepX = 1; 

int numSchede = profile.getProfileInt(_T("Init"), _T("NumeroBande"),70); 
// Dimensione singola banda
c_sizeBande = profile.getProfileInt(_T("Init"), _T("SizeBande"),22); 
// Numero classi
int numClassi = profile.getProfileInt(_T("Init"), _T("NumeroClassi"),4); 
// dimensione in byte dei dati ricevuti da VAL_COUNTER 1=byte
// 2 = short , 4 == long
int sizeClassi = profile.getProfileInt(_T("Init"), _T("SizeDataClassi"),1); 

c_skipRxInAutotestMsg = profile.getProfileBool(_T("Init"), _T("SkipRxInAutotestMsg"),FALSE); ;

// configurazione indice prima scheda classe AB nell'HW
// puo` essere 0 ( normale) 1 schede ab e cd invertite
c_firstBoardClassAB = profile.getProfileInt(_T("Init"), _T("firstBoardClassAB"),0); 

c_homeDir = profile.getProfileString(_T("Init"), _T("HomeDir"),_T("C:/user/eds")); 

// utilizzo pagina test IO
c_doUseIo = profile.getProfileBool(_T("init"), _T("TestIo"),FALSE); 
// utilizzo diaframmi
c_doUseDiaframmi = profile.getProfileBool(_T("init"), _T("UseDiaframmi"),TRUE); 

// lunghezza di calcolo densita`
c_densityCalcLength = 1000.;

memset (c_clusterCounter,0,sizeof(c_clusterCounter));

switch(sizeClassi)
	{
	case 1:
	case 2:
	case 4:
		break;
	default:
		{
		CString	resMsg;
		resMsg.LoadString(CSM_GRAPHDOC_INICLASSI);
	//	AfxMessageBox ("ERRORE: INI File -> [init] [NumeroClassi] fuori range");
		AfxMessageBox (resMsg);
		sizeClassi = 1;
		}
		break;
	}

c_sizeClassi = sizeClassi;

if ((numClassi > MAX_NUMCLASSI)||(numClassi < 0))
	{
	CString	resMsg;
	resMsg.LoadString(CSM_GRAPHDOC_INICLASSI);
//	AfxMessageBox ("ERRORE: INI File -> [init] [NumeroClassi] fuori range");
	AfxMessageBox (resMsg);
	numClassi = MAX_NUMCLASSI;
	}
 
// RemoveAll + init var 
c_difCoil.clear(numSchede,numClassi);

// Base Page Init
TrendLScalaY = 100.;
TrendDScalaY = 100.;
TrendLScalaX = 100;
TrendDScalaX = 100;

c_rotoloNameMode=ROTOLO_NAME_EXT;

c_plantInfoMode = (PlantInfoMode) profile.getProfileInt(_T("Init"), _T("PlantInfoMode"),0); 
if (c_plantInfoMode<PLANTINFO_START_DIALOG)
	{
	c_plantInfoMode=PLANTINFO_START_DIALOG;
	}

if (c_plantInfoMode>PLANTINFO_MDB)
	{
	c_plantInfoMode=PLANTINFO_START_DIALOG;
	}

switch(c_plantInfoMode)
	{
	case PLANTINFO_START_DIALOG:
		{
		// c_rotoloNameMode=ROTOLO_NAME_EXT;	
		c_rotoloNameMode=(RotoloNameMode)profile.getProfileInt(_T("Init"), _T("RotoloNameMode"),0); 
		}
		break;
	case PLANTINFO_NODATA:
		c_rotoloNameMode=ROTOLO_NAME_TIME;	
		break;
	case PLANTINFO_STOP_DIALOG:
		c_rotoloNameMode=ROTOLO_NAME_TIME;	
		break;
	case PLANTINFO_MDB:
		c_rotoloNameMode=ROTOLO_NAME_TIME;	
		break;
	}

viewID = 0;

fileLogPosition = -1;

systemOnLine = FALSE;
systemOnTest = FALSE;

systemLoading = FALSE;
// Idati sono stati caricati da file (no Save)
loadedData = FALSE;

diskAlarmActive = FALSE;
serAlarmActive = FALSE;

c_holeClassDigiOut = 0;

// Init title alarm
//CString t = "Allarme Densita'";
CString t;
t.LoadString (CSM_GRAPHDOC_ALDENS);
densitaAlarm.SetTitle(t);

// t = "Allarme Periodici";
//t.LoadString (CSM_GRAPHDOC_ALPER);
// periodAlarm.SetTitle (t);

// t = "Allarme Sistema";
t.LoadString (CSM_GRAPHDOC_ALSYS);
systemAlarm.SetTitle (t);

// t = "Allarme Cluster";
t.LoadString (CSM_GRAPHDOC_ALCLUSTER);
clusterAlarm.SetTitle (t);

// Allarme scheda Scollegata
serMsgReceived = FALSE;

// valore in metri tra due difetti
c_holeRate = profile.getProfileInt(_T("Simulazione"), _T("DeltaPosHole"),5); 
c_fixSignalClusterTime = profile.getProfileInt(_T("Cluster"), _T("SignalTimeSec"),5); 
c_signalClusterTime = time(NULL);
//----------------
c_prodCoil = "";


//---------------------
// stato hde micro
c_statoHde = -1;

c_lunResStop = 0;

rotoloExt = 0;

//---------------------
c_useManualTitle = FALSE;
c_manualTitle = _T("");


spessore = 0.;

// Timer setting demo
millisec = 500;

// 
c_posDiaframmaSx = 0.;
c_posDiaframmaDx = 0.;
c_rawPosDiaframmaSx = 0.;
c_rawPosDiaframmaDx = 0.;

c_valInput = 0;

c_signalCluster = FALSE;

c_disableF1OnAlarm = profile.getProfileBool(_T("Init"), _T("DisableF1OnAlarm"),TRUE);
c_enableExitOnAlarm = profile.getProfileBool(_T("Init"), _T("EnableExitOnAlarm"),TRUE);
// 8-10-19 inserito stato autotest .ini
c_statoAlarmAutotest = profile.getProfileBool(_T("init"), _T("AlarmAutotest"),FALSE);

}

BOOL CLineDoc::UpdateBaseDoc(void)
{
// Read Config Default Value From DBase
DBOpzioni dbOpzioni(dBase);

if (dbOpzioni.openSelectDefault ())
	{
	secAlarmSound = (int )dbOpzioni.m_ALLARME_DURATA;
	// Nessun allarme = time 0
	if (!dbOpzioni.m_ALLARME_ACU)
		secAlarmSound = 0;

	// Base Update
	vFori = dbOpzioni.m_TL_CLASSE;
	if (TrendLScalaX != (int ) dbOpzioni.m_TL_LUNGHEZZA)
		{
		TrendLScalaX = (int ) dbOpzioni.m_TL_LUNGHEZZA;
//		BaseScalaX = (int )schede.getMeter(); // Base Visualizzazione attuale posizione
		}
	TrendLScalaY = dbOpzioni.m_TL_SCALA;
	
	TrendDScalaY = dbOpzioni.m_TD_SCALA;
	TrendDScalaX = (int )dbOpzioni.m_TD_LUNGHEZZA;
		
	dbOpzioni.Close();
	}


// Read Config Default Value From DBase
DBParametri dbParametri(dBase);
if (dbParametri.openSelectDefault ())
	{
	c_fattCorrDimLin = dbParametri.m_FCDL;
	c_lunResStop = dbParametri.m_L_RESIDUA_STOP;
	sogliaA = dbParametri.m_SOGLIA_A;
	sogliaB = dbParametri.m_SOGLIA_B;
	sogliaC = dbParametri.m_SOGLIA_C;
	sogliaD = dbParametri.m_SOGLIA_D;
	c_holeClassDigiOut = dbParametri.m_ALARM_CLASS;
	dbParametri.Close();
	}

// Read Config Default Value From DBase
DBRicette dbRicette(dBase);
if (dbRicette.openSelectID (nomeRicetta))
	{
	c_difCoil.c_sogliaAllarme.RemoveAll();
	c_difCoil.c_sogliaAllarme.Add(dbRicette.m_ALLARME_A);
	c_difCoil.c_sogliaAllarme.Add(dbRicette.m_ALLARME_B);
	c_difCoil.c_sogliaAllarme.Add(dbRicette.m_ALLARME_C);
	c_difCoil.c_sogliaAllarme.Add(dbRicette.m_ALLARME_D);
	c_densityCalcLength = dbRicette.m_LENGTH_ALLARME;
	dbRicette.Close();
	}

CDCluster dialog;
Cluster cl;
dialog.loadFromProfile();

c_difCoil.c_clusterArray.RemoveAll();
// a
cl.c_lunghezza = dialog.m_distanceA;
cl.c_soglia = dialog.m_pinholeA;
c_difCoil.c_clusterArray.Add(cl);
cl.c_lastClusterPos = 0;
// b
cl.c_lunghezza = dialog.m_distanceB;
cl.c_soglia = dialog.m_pinholeB;
cl.c_lastClusterPos = 0;
c_difCoil.c_clusterArray.Add(cl);
// c
cl.c_lunghezza = dialog.m_distanceC;
cl.c_soglia = dialog.m_pinholeC;
cl.c_lastClusterPos = 0;
c_difCoil.c_clusterArray.Add(cl);
// d
cl.c_lunghezza = dialog.m_distanceD;
cl.c_soglia = dialog.m_pinholeD;
cl.c_lastClusterPos = 0;
c_difCoil.c_clusterArray.Add(cl);

setValAlarmEncoder(FALSE);


// CDCustomer customer;
// c_cliente = customer.m_name;

/*
CDSimOption sOpt;
millisec = (int ) (1000./sOpt.m_LineSpeed);
if (millisec < 50)
	millisec = 50;
c_holeRate = sOpt.m_holeRate;

CDCompany cmp;

c_company = cmp.m_name;

CProfile profile;
TrendDScalaY = (double) profile.getProfileInt(_T("init"), _T("TowerYScale"),30); 
*/

UpdateAllViews(NULL);
return TRUE;
}

BOOL CLineDoc::OnNewDocument()
{
if (!CDocument::OnNewDocument())
		return FALSE;

// Set Title
setTitle ((LPCTSTR)" ");

// Clear and reset data structure on new Coil
// FALSE -> coldStart
// newCoil (FALSE);

//if (!UpdateBaseDoc())
//	return FALSE;

return TRUE;
}

// Parametro indica warmStart
void CLineDoc::newCoil(BOOL warmStart )
{
// Init numero schede
CProfile profile;
int numSchede = profile.getProfileInt(_T("init"), _T("NumeroBande"),7); 
int numClassi = profile.getProfileInt(_T("init"), _T("NumeroClassi"),4); 

CMainFrame* pFrame = (CMainFrame*)AfxGetMainWnd();
// ASSERT (pFrame!=NULL);

if (pFrame)
	{
	pFrame->c_stAllarmi.setNumLaser(numSchede);
	if (numClassi <= 2)
		{
		pFrame->c_stAllarmi.setNumReceiver(numSchede);
		}
	else
		// 4 classi
		{
		pFrame->c_stAllarmi.setNumReceiver(numSchede*2);
		}

	int numCpu = profile.getProfileInt(_T("init"), _T("NumeroCpu"),1); 
	pFrame->c_stAllarmi.setNumCpu(numCpu);
	}

// Clear Allarmi in formato testo
densitaAlarm.clearMessages();
periodAlarm.clearMessages();
systemAlarm.clearMessages();
clusterAlarm.clearMessages();
// clear stato allarmi
memset (c_clusterCounter,0,sizeof(c_clusterCounter));


if (warmStart)
	{// copy last lunResStop of defect
	COILTYPE dCoil;
	dCoil.clear(numSchede,numClassi);
	dCoil.setMmBaseStepX(profile.getProfileInt(_T("Init"), _T("BaseStepX"),1)*1000); 
	c_difCoil.dump(dCoil,(int)getVTotalLength(),(int)c_difCoil.getMeter());
	c_difCoil.clear(numSchede,numClassi);
	c_difCoil = dCoil;
	}
else
	c_difCoil.clear(numSchede,numClassi);

// Aggiorno Parametri di calcolo
c_difCoil.setLargeSize(1400);

// Applico fattore correttivo ai 1 metri
// c_difCoil.setTrueSizeEncod(c_fattCorrDimLin * 1000.);
double corrPos;
corrPos = c_lunResStop + 0.;
c_difCoil.setMeter((int ) corrPos);
randDif.pos = ((int) corrPos);
// empty RollMap data structure
c_targetBoard.setEmpty();
c_targetBoard.setLastPos(0);

}



CLineDoc::~CLineDoc()
{

}


BEGIN_MESSAGE_MAP(CLineDoc, CDocument)
	//{{AFX_MSG_MAP(CLineDoc)				 
	ON_COMMAND(ID_TESTGRAFICO, OnTestgrafico)
	ON_COMMAND(ID_FILE_START, OnFileStart)
	ON_COMMAND(ID_FILE_STOP, OnFileStop)
	ON_UPDATE_COMMAND_UI(ID_FILE_START, OnUpdateFileStart)
	ON_UPDATE_COMMAND_UI(ID_FILE_STOP, OnUpdateFileStop)
	ON_COMMAND(ID_FILE_TSTART, OnFileTstart)
	ON_UPDATE_COMMAND_UI(ID_FILE_TSTART, OnUpdateFileTstart)
	ON_COMMAND(ID_CONFIGURA_PRODOTTO, OnConfiguraProdotto)
	//}}AFX_MSG_MAP
	ON_COMMAND(ID_AUTOTEST, &CLineDoc::OnAutotest)
	ON_UPDATE_COMMAND_UI(ID_AUTOTEST, &CLineDoc::OnUpdateAutotest)
	ON_COMMAND(ID_FILE_START_PSW, &CLineDoc::OnFileStartPsw)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CLineDoc diagnostics

#ifdef _DEBUG
void CLineDoc::AssertValid() const
{
	CDocument::AssertValid();
}

void CLineDoc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CLineDoc serialization

CLineDoc&  CLineDoc::operator = (CLineDoc& source)
{
// Dati Lavorazione
// Save rotolo
rotolo = source.rotolo;
// Save rotoloExt
rotoloExt = source.rotoloExt;

// save title
c_fileName = source.c_fileName;

// Save lega
lega = source.lega;

// Save ricetta
nomeRicetta = source.nomeRicetta; 

// Save stato
stato = source.stato; // insert EOS

// Save cliente
c_cliente = source.c_cliente; // insert EOS

// Save startTime
startTime = source.startTime;


// save spessore larghezza
spessore = source.spessore;
c_larghezza = source.c_larghezza;

// Dati System Dependent
c_fattCorrDimLin = source.c_fattCorrDimLin;
c_lunResStop = source.c_lunResStop;

// Save Difetti
c_difCoil = source.c_difCoil;


// Save Allarmi
densitaAlarm = source.densitaAlarm;

periodAlarm = source.periodAlarm;

systemAlarm = source.systemAlarm;

clusterAlarm = source.clusterAlarm;

return (*this);
}



BOOL CLineDoc::save(CFileBpe *cf,CProgressCtrl *progress)
{
TCHAR idFileType[] = FILEDOCTYPE;

// Update Progress
if (progress != NULL)
	progress->StepIt();
// Save ID
cf->Write((void *)idFileType,sizeof(idFileType));

// Ver 02 = numero schede variabili (14-11-97)
int ver = FILEDOCVER; 
// Save ID
cf->Write((void *)&ver,sizeof(int));

// Dati Lavorazione
// Save rotolo
int size = sizeof(TCHAR) * (rotolo.GetLength() + 1); // insert EOS
TCHAR c = '\0';
cf->Write((void *)&size,sizeof(int));
cf->Write((void *)((LPCTSTR)rotolo),size -sizeof(TCHAR));
cf->Write((void *)&c,sizeof (c));
// Save rotoloExt
cf->Write((void *)&rotoloExt,sizeof(rotoloExt));



// Save lega
size = sizeof(TCHAR) * (lega.GetLength() + 1); // insert EOS
c = '\0';
cf->Write((void *)&size,sizeof(int));
cf->Write((void *)((LPCTSTR)lega),size -sizeof(TCHAR));
cf->Write((void *)&c,sizeof (c));

// Save ricetta
size = sizeof(TCHAR) * (nomeRicetta.GetLength() + 1); // insert EOS
c = '\0';
cf->Write((void *)&size,sizeof(int));
cf->Write((void *)((LPCTSTR)nomeRicetta),size -sizeof(TCHAR));
cf->Write((void *)&c,sizeof (c));


// Save stato
size = sizeof(TCHAR) * (stato.GetLength() + 1); // insert EOS
c = '\0';
cf->Write((void *)&size,sizeof(int));
cf->Write((void *)((LPCTSTR)stato),size -sizeof(TCHAR));
cf->Write((void *)&c,sizeof (c));

// Save cliente
size = sizeof(TCHAR) * (c_cliente.GetLength() + 1); // insert EOS
c = '\0';
cf->Write((void *)&size,sizeof(int));
cf->Write((void *)((LPCTSTR)c_cliente),size -sizeof(TCHAR));
cf->Write((void *)&c,sizeof (c));

// Save startTime
cf->Write((void *)&startTime,sizeof(startTime));


// save spessore larghezza
cf->Write((void *)&spessore,sizeof(spessore));
cf->Write((void *)&c_larghezza,sizeof(c_larghezza));

// Dati System Dependent
cf->Write((void *)&c_fattCorrDimLin,sizeof(c_fattCorrDimLin));
cf->Write((void *)&c_lunResStop,sizeof(c_lunResStop));


// Dati System Dependent soglie di riconoscimento
cf->Write((void *)&sogliaA,sizeof(sogliaA));
cf->Write((void *)&sogliaB,sizeof(sogliaB));
cf->Write((void *)&sogliaC,sizeof(sogliaC));
cf->Write((void *)&sogliaD,sizeof(sogliaD));

// Dati di posizione diaframmi 
cf->Write((void *)&c_posDiaframmaSx,sizeof(c_posDiaframmaSx));
cf->Write((void *)&c_posDiaframmaDx,sizeof(c_posDiaframmaDx));


// Update Progress
if (progress != NULL)
	progress->StepIt();

// Save Difetti
c_difCoil.save(cf);

// Update Progress
if (progress != NULL)
	progress->StepIt();

// Save Allarmi
densitaAlarm.save(cf);

// Update Progress
if (progress != NULL)
	progress->StepIt();
periodAlarm.save(cf);

// Update Progress
if (progress != NULL)
	progress->StepIt();
systemAlarm.save(cf);

if (progress != NULL)
	progress->StepIt();
clusterAlarm.save(cf);

// save report 100 metri
// Update Progress
//if (progress != NULL)
//	progress->StepIt();
//dif100.save(cf);

// Update Progress
if (progress != NULL)
	progress->StepIt();
return TRUE;
}

BOOL CLineDoc::load(CFileBpe *cf, CProgressCtrl *progress)
{
// Tentativo dati caricati da file
loadedData = FALSE;
bool notUnicode = false;

TCHAR idFileType[32];

// Update Progress
if (progress != NULL)
	progress->StepIt();

// Load ID
cf->Read((void *)idFileType,sizeof(FILEDOCTYPE));
if (_tcscmp(idFileType,FILEDOCTYPE))
	{
	cf->SeekToBegin();
	char charIdFileType[32];
	cf->Read((void *)charIdFileType,sizeof(FILEDOCTYPE)/2);
	if(strcmp(charIdFileType,"CSM10_AR2021"))
		{
		CString s;
		s.Format (_T("Load: Wrong File Format"));
		AfxGetMainWnd()->MessageBox(s,_T("Error"));
		return (FALSE);
		}
	// else non unicode
	notUnicode = true;
	}
int ver; 
// Load Ver
cf->Read((void *)&ver,sizeof(int));
if ((ver > FILEDOCVER)||
	(ver < 3))	// 3 ultima release fissa 
	{
	CString s;
	s.Format (_T("Load: Wrong File Version (found %d, need %d)"),ver,FILEDOCVER);
	AfxGetMainWnd()->MessageBox(s,_T("Error"));
	return (FALSE);
	}


// Load	rotolo
int size;
cf->Read((void *)&size,sizeof(int));
if (notUnicode)
	{
	CStringA str;
	LPCSTR pc;
	pc = str.GetBuffer(size+sizeof(char));
	if (pc == NULL)
		{
		CString s;
		s.Format (_T("Load File Alloc Failed on size %d"),size);
		AfxGetMainWnd()->MessageBox(s,_T("Error"));
		return FALSE;
		}
	cf->Read((void *)pc,size);
	str.ReleaseBuffer();
	rotolo = CString(str);
	}
else
	{
	LPCTSTR pc;
	pc = rotolo.GetBuffer(size+sizeof(TCHAR));
	if (pc == NULL)
		{
		CString s;
		s.Format (_T("Load File Alloc Failed on size %d"),size);
		AfxGetMainWnd()->MessageBox(s,_T("Error"));
		return FALSE;
		}
	cf->Read((void *)pc,size);
	rotolo.ReleaseBuffer();
	}
//------------ End load rotolo


// Load	rotoloExt
cf->Read((void *)&rotoloExt,sizeof(rotoloExt));
//------------ End load rotoloExt


// Load	lega
cf->Read((void *)&size,sizeof(int));
if (notUnicode)
	{
	CStringA str;
	LPCSTR pc;
	pc = str.GetBuffer(size+sizeof(char));
	if (pc == NULL)
		{
		CString s;
		s.Format (_T("Load File Alloc Failed on size %d"),size);
		AfxGetMainWnd()->MessageBox(s,_T("Error"));
		return FALSE;
		}
	cf->Read((void *)pc,size);
	str.ReleaseBuffer();
	lega = CString(str);
	}
else{
	LPCTSTR pc;
	pc = lega.GetBuffer(size+sizeof(TCHAR));
	if (pc == NULL)
		{
		CString s;
		s.Format (_T("Load File Alloc Failed on size %d"),size);
		AfxGetMainWnd()->MessageBox(s,_T("Error"));
		return FALSE;
		}

	cf->Read((void *)pc,size);
	lega.ReleaseBuffer();
	}
//------------------- End load lega

// Load	nomeRicetta
cf->Read((void *)&size,sizeof(int));
if (notUnicode)
	{
	CStringA str;
	LPCSTR pc;
	pc = str.GetBuffer(size+sizeof(char));
	if (pc == NULL)
		{
		CString s;
		s.Format (_T("Load File Alloc Failed on size %d"),size);
		AfxGetMainWnd()->MessageBox(s,_T("Error"));
		return FALSE;
		}
	cf->Read((void *)pc,size);
	str.ReleaseBuffer();
	nomeRicetta = CString(str);
	}
else
	{
	LPCTSTR pc;
	pc = nomeRicetta.GetBuffer(size+sizeof(TCHAR));
	if (pc == NULL)
		{
		CString s;
		s.Format (_T("Load File Alloc Failed on size %d"),size);
		AfxGetMainWnd()->MessageBox(s,_T("Error"));
		return FALSE;
		}

	cf->Read((void *)pc,size);
	nomeRicetta.ReleaseBuffer();
	}
//------------------- End load nomeRicetta


// Load	stato
cf->Read((void *)&size,sizeof(int));
if (notUnicode)
	{
	CStringA str;
	LPCSTR pc;
	pc = str.GetBuffer(size+sizeof(char));
	if (pc == NULL)
		{
		CString s;
		s.Format (_T("Load File Alloc Failed on size %d"),size);
		AfxGetMainWnd()->MessageBox(s,_T("Error"));
		return FALSE;
		}
	cf->Read((void *)pc,size);
	str.ReleaseBuffer();
	stato = CString(str);
	}
else
	{
	LPCTSTR pc;
	pc = stato.GetBuffer(size+sizeof(TCHAR));
	if (pc == NULL)
		{
		CString s;
		s.Format (_T("Load File Alloc Failed on size %d"),size);
		AfxGetMainWnd()->MessageBox(s,_T("Error"));
		return FALSE;
		}

	cf->Read((void *)pc,size);
	stato.ReleaseBuffer();
	}
//------------------- End load stato

// Load	bolla
cf->Read((void *)&size,sizeof(int));
if (notUnicode)
	{
	CStringA str;
	LPCSTR pc;
	pc = str.GetBuffer(size+sizeof(char));
	if (pc == NULL)
		{
		CString s;
		s.Format (_T("Load File Alloc Failed on size %d"),size);
		AfxGetMainWnd()->MessageBox(s,_T("Error"));
		return FALSE;
		}
	cf->Read((void *)pc,size);
	str.ReleaseBuffer();
	c_cliente = CString(str);
	}
else
	{
	LPCTSTR pc;
	pc = c_cliente.GetBuffer(size+sizeof(TCHAR));
	if (pc == NULL)
		{
		CString s;
		s.Format (_T("Load File Alloc Failed on size %d"),size);
		AfxGetMainWnd()->MessageBox(s,_T("Error"));
		return FALSE;
		}

	cf->Read((void *)pc,size);
	c_cliente.ReleaseBuffer();
	}
//------------------- End load bolla


// Load startTime
cf->Read((void *)&startTime,sizeof(startTime));

// Load spessore e larghezza
cf->Read((void *)&spessore,sizeof(spessore));
cf->Read((void *)&c_larghezza,sizeof(c_larghezza));

// Dati System Dependent
cf->Read((void *)&c_fattCorrDimLin,sizeof(c_fattCorrDimLin));
cf->Read((void *)&c_lunResStop,sizeof(c_lunResStop));

// Dati System Dependent soglie di riconoscimento
if(ver > 3)
	{
	cf->Read((void *)&sogliaA,sizeof(sogliaA));
	cf->Read((void *)&sogliaB,sizeof(sogliaB));
	cf->Read((void *)&sogliaC,sizeof(sogliaC));
	cf->Read((void *)&sogliaD,sizeof(sogliaD));	
	}

// Dati di posizione diaframmi 
if(ver > 4)
	{
	cf->Read((void *)&c_posDiaframmaSx,sizeof(c_posDiaframmaSx));
	cf->Read((void *)&c_posDiaframmaDx,sizeof(c_posDiaframmaDx));
	}
else
	{
	c_posDiaframmaSx = 0;
	c_posDiaframmaDx = 0;
	}

// Update Progress
if (progress != NULL)
	progress->StepIt();
// Load schede
if (c_difCoil.load(cf,ver)== FALSE)
//if (schede.load(cf)== FALSE)
	return(FALSE);	// Fail

// Update Progress
if (progress != NULL)
	progress->StepIt();
// Load Allarmi
if (densitaAlarm.load(cf,notUnicode,ver) == FALSE)
	return FALSE;

// Update Progress
if (progress != NULL)
	progress->StepIt();
if (periodAlarm.load(cf,notUnicode,ver) == FALSE)
	return FALSE;

// Update Progress
if (progress != NULL)
	progress->StepIt();
if (systemAlarm.load(cf,notUnicode,ver) == FALSE)
	return FALSE;

if(ver >= 6)
	{
	// Update Progress
	if (progress != NULL)
		progress->StepIt();
	if (clusterAlarm.load(cf,notUnicode,ver) == FALSE)
		return FALSE;
	}
else
	clusterAlarm.clearMessages();


// Update Progress
//if (progress != NULL)
//	progress->StepIt();
// load report 100 metri
// Scommentare 
//if (dif100.load(cf,ver) == FALSE)
//	return FALSE;

// Update Progress
if (progress != NULL)
	progress->StepIt();
// Idati sono stati caricati da file
loadedData = TRUE;
UpdateAllViews(NULL);
return TRUE;
}


void CLineDoc::Serialize(CArchive& ar)
{
	if (ar.IsStoring())
	{
		// TODO: add storing code here
	}
	else
	{
		// TODO: add loading code here
	}
}

/////////////////////////////////////////////////////////////////////////////
// CLineDoc commands

int cn = 0;

void CLineDoc::OnTestgrafico() 
{
	// TODO: Add your command handler code here
if (!systemOnLine)
	return;

HoleGenerator();
return;

}



void CLineDoc::HoleGenerator(void) 
{
BOOL defPres = FALSE;

if (!systemOnLine)
	return;

if (randDif.pos < 0)
	{
	randDif.pos = rand() % c_holeRate;
	randDif.scheda = rand() % c_difCoil.getNumSchede();
	randDif.classe = rand() % c_difCoil.getNumClassi();
	}
else
	{
	DifMetro difMetro;
	int pos = (int ) c_difCoil.getMeter();
	pos += (int)c_difCoil.getMeterBaseStepX();
	if ((pos >= randDif.pos)&&
		(pos < 950))
		{
		DifBanda difBanda1;
		for (int j=0;j<c_difCoil.getNumClassi();j++)
			{
			if (j == randDif.classe)
				{
				defPres = TRUE;
				int v;
				v = 1;
				difBanda1.Add(v);
				}
			else
				{
				int v;
				v = 0;
				difBanda1.Add(v);
				}
			}
		difMetro.clear();
		for (int i=0;i<c_difCoil.getNumSchede();i++)
			{
			if (i == randDif.scheda)
				{
				difBanda1.setBanda(i);
				difMetro.Add(difBanda1);
				}
			}					
		difMetro.posizione = pos; 
		
		randDif.pos+= rand() % c_holeRate;
		randDif.scheda = rand() % c_difCoil.getNumSchede();
		randDif.classe = rand() % c_difCoil.getNumClassi();
		}
	if (defPres)
		{
		// pre calcolo  totali per classe 
		difMetro.calcTotalCode(TRUE);
		c_difCoil.Add(difMetro);
		}
	c_difCoil.setMeter(pos);
	// Chiamare una funzione per il calcolo delle regioni 
	// da aggiornare

	// 1 solo aggiornamento
	// 0 repaint all
	// vedere funzioni OnUpdate
	// di CView
	UpdateAllViews(NULL,1);
	}
updateDifAlarm();
updateDifCluster();

return;
}



void CLineDoc::OnViewAggiorna() 
{
	// TODO: Add your command handler code here
UpdateBaseDoc();	
UpdateAllViews(NULL);
}

//------------------------------------
//
//	Gestione Messagi tipo stato HDE
//
//------------------------------------
 
void CLineDoc::gestValStatoHde (PVOID p,DWORD size) // Gestisce arrivo VAL_COUNTER
{

// Received Msg from serial
serMsgReceived = TRUE;



if (size != 1)
	{// errore dimensione 
	return;
	}

int nuovoStato;
nuovoStato = (int ) *((int *) p);


BOOL genMsg = FALSE;
if ((nuovoStato == HDE_HOLD)&&
	(c_statoHde == HDE_INSPECTION)&&
	systemOnLine)
	{
	if (!systemOnLine)
	return;

	CString allarmeSistema;
	CString s;
	s.LoadString(CSM_GRAPHDOC_HOLDENTER);
	// allarmeSistema.Format("Laser NON funzionante\r\n");
	allarmeSistema.Format (_T("m. %d: %s"),(int)c_difCoil.getMeter(),s);
	systemAlarm.appendMsg(allarmeSistema);
	genMsg = TRUE;
	}

if ((nuovoStato == HDE_INSPECTION)&&
	(c_statoHde == HDE_HOLD)&&
	systemOnLine)
	{
	CString allarmeSistema;
	CString s;
	s.LoadString(CSM_GRAPHDOC_HOLDEXIT);
	// allarmeSistema.Format("Laser NON funzionante\r\n");
	allarmeSistema.Format (_T("m. %d: %s"),(int)c_difCoil.getMeter(),s);
	systemAlarm.appendMsg(allarmeSistema);
	genMsg = TRUE;
	}

// avviamento automatico 
if ((nuovoStato == HDE_INSPECTION)&&
	(c_statoHde != HDE_INSPECTION)&&
	!systemOnLine)
	{// comando di start?
	if (c_plantInfoMode==PLANTINFO_NODATA)
		{
		gestValOpen(NULL,0);
		}
	}

// chiusura automatico se almeno due stati wait_start in ispezione
if ((nuovoStato == HDE_WAITSTART)&&
	systemOnLine)
	{// comando di stop?
	// ricommentato altrimenti non esce piu` se manca abilitazione
	// 
	// if (c_plantInfoMode==PLANTINFO_NODATA)
	if (CTime::GetCurrentTime().GetTime() > c_blindStatoHde.GetTime())
		{
		gestValStop(NULL,0);
		}
	}


if (genMsg)
	systemAlarm.visualizza(AfxGetMainWnd(),secAlarmSound);
	
c_statoHde = nuovoStato;
}

//------------------------------------
//
//	Gestione Messagi Diaframma 
//				   
//------------------------------------

void CLineDoc::gestValDiaframma (PVOID p,DWORD size) 
{// Gestisce arrivo VAL_DIAFRAMMA
// Received Msg from serial
serMsgReceived = TRUE;

// posizione del diaframma attivo anche fuori ispezione
// per test IO
//if (!systemOnLine)
//	return;


if (size != 2)
	{// errore dimensione 
	return;
	}

unsigned short from = *((unsigned short *) p);
unsigned short to;
_swab((char *)&from,(char *)&to,2);


c_posDiaframma = (double ) to;
}

//------------------------------------
//
//	Gestione Messagi Diaframma 
//				   
//------------------------------------

void CLineDoc::gestValExtDiaframma (PVOID p,DWORD size) 
{// Gestisce arrivo VAL_DIAFRAMMA
// Received Msg from serial
serMsgReceived = TRUE;

//if (!systemOnLine)
//	return;

if (size != 4)
	{// errore dimensione 
	return;
	}

short* sp;

sp = (short *) p;
short from = *sp;
short to;
_swab((char *)&from,(char *)&to,2);
double tmpSxPos;
tmpSxPos = (double ) to;

// test io
c_rawPosDiaframmaSx = to;

sp ++;
from = *sp;
_swab((char *)&from,(char *)&to,2);
double tmpDxPos;
// test io
c_rawPosDiaframmaDx = to;

tmpDxPos = 1. * c_sizeBande * c_difCoil.getNumSchede() - (double )to;

BOOL update = FALSE;
if (tmpSxPos != c_posDiaframmaSx)
	update = TRUE;

if (tmpDxPos != c_posDiaframmaDx)
	update = TRUE;

c_posDiaframmaDx = tmpDxPos;
c_posDiaframmaSx = tmpSxPos;


// redraw all 
if (update)
	UpdateAllViews (NULL,0);

}


//------------------------------------
//
//	Gestione Messagi tipo ValHole
//
// arrivano singoli difetti con 2 word [banda 1-112] - [contatore]
// nessun bisogno di swap
//------------------------------------

void CLineDoc::gestValHole (PVOID p,DWORD size) // Gestisce arrivo VAL_COUNTER
{
// Received Msg from serial
serMsgReceived = TRUE;
DifMetro difMetro;
BOOL updateExisting=FALSE;

// Only system Enabled
if (!systemOnLine)
	return;
// estraggo posizione

// posizione da leggere su encoder
// da fare
// dPos = leggi posizione encoder
// Create new metro or update existing
// Calcolo posizione corretta da lResStop
double corrPos;
corrPos = c_difCoil.getMeter();
corrPos += c_lunResStop;

if (((c_difCoil.getMeter()==(int)corrPos))&&
	(c_difCoil.GetSize()>0)&&
	(c_difCoil.GetAt(c_difCoil.GetUpperBound()).posizione == (int)corrPos))
	{
	difMetro = c_difCoil.GetAt(c_difCoil.GetUpperBound());
	updateExisting = TRUE;
	}
else
	difMetro.posizione = corrPos;

unsigned long  *lDest;
lDest = (unsigned long *) p; 

int nCl = min (c_difCoil.getNumClassi(),1);
int numField = 1;	// sono campi long ora sono parametrizzati

// Formato su 1 mt
// banda,ValA[0];
//

DifBanda difBanda;
BOOL bandaDefPres = FALSE;
BOOL metroDefPres = FALSE;

difBanda.clear();
bandaDefPres = FALSE;
int ib = lDest[0]-1;
int iv = lDest[1];
difBanda.setBanda(ib);
difBanda.Add (iv);
if (iv)
	bandaDefPres = TRUE;

if (bandaDefPres)
	{
	metroDefPres = TRUE;
	difMetro.Add(difBanda);	
	}

BOOL viewScroll = FALSE;
if (metroDefPres)
	{
	if (!updateExisting) 
		{
		// precalcolo totali per classe (true inizializzo counter)
		difMetro.calcTotalCode(TRUE);
		// solo classe A,banda , numero dif
		c_difCoil.updateTotali(0,ib,iv);
		c_difCoil.Add(difMetro,FALSE); // add
		viewScroll = TRUE;
		}
	else
		{
		// precalcolo totali per classe
		difMetro.calcTotalCode(TRUE);
		// solo classe A
		c_difCoil.updateTotali(0,ib,iv);
		c_difCoil.Add(difMetro,TRUE); // update
		// c_difCoil.SetAt(c_difCoil.GetUpperBound(),difMetro);
		viewScroll = FALSE;
		}
	} 
	
	
// Update Metraggio
BOOL redrawAll = FALSE;

//{
// Salto di metraggio ridisegno tutto
// ora posizione non arriva con i difetti ! ma da encoder
//int deltaP;
//deltaP = (int) corrPos - (int)c_difCoil.getMeter();
//deltaP -= (int)c_difCoil.getMeterBaseStepX();
//if (deltaP > 0)
//	redrawAll = TRUE;
//c_difCoil.setMeter((int) corrPos);
//}

//schede.incMeter();

// Aggiornamento View

// Aggiorno Visualizzazione
CRect r;
// 1 significa solo aggiornamento
// 0 repaint all
if (redrawAll)
	UpdateAllViews (NULL,0);
else
	{
	if (viewScroll)
		UpdateAllViews (NULL,1); // scroll 
	else
		UpdateAllViews (NULL,2); // redraw
	}
updateDifAlarm();
updateDifCluster();
}
//------------------------------------
//
//	Gestione Messagi tipo ValExtCounter
// Estesi per essere divisi su piu pacchetti
//
//------------------------------------


void CLineDoc::gestValExtCounter(PVOID p, DWORD size)
{
// Offset nel vettore difetti di qs gruppo di dati
short offset;

// Received Msg from serial
serMsgReceived = TRUE;

// Only system Enabled
if (!systemOnLine)
	return;

unsigned char  *cDest;
unsigned short *sDest;
unsigned long  *lDest;
cDest = (unsigned char *) p; 

short sOff;
sOff = (*((short *)cDest));
_swab ((char *)&sOff,(char *)&offset,2);

cDest += sizeof(short);

long sPos,dPos;
sPos = *((long *)cDest); 
swaplong (&sPos,&dPos,1);

cDest += sizeof(long);

// incremento pointer
sDest = (unsigned short *) cDest;
lDest = (unsigned long *)  cDest;

int nCl = min (c_difCoil.getNumClassi(),4);
int numField = (size-sizeof(long)-2)/c_sizeClassi; // erano campi long ora sono parametrizzati

if (c_sizeClassi == 2)
	{// swap short
	unsigned short sv,sd;
	unsigned short *ps;
	ps = (unsigned short *) cDest; 
	for (int i=0;i<numField;i++)
		{
		sv = *ps;
		_swab((char *)&sv,(char *)&sd,2);
		*ps = sd;
		ps++;
		}
	}
if (c_sizeClassi == 4)
	{// swap long
	long lv,ld;
	long *pl;
	pl = (long *) cDest; 
	for (int i=0;i<numField;i++)
		{
		lv = *pl;
		swaplong(&lv,&ld,1);
		*pl = ld;
		pl++;
		}
	}


// Formato su 1 mt
// Offset,Posiz,ValA[0],ValB[0],ValC[0],ValD[0],....., ValD[6] .....;
//
// Totale 1+4+4*4*7 byte (size char)*(4 classi)*(7 schede)
// 1+4+112 byte
// Riceve qualunque numero di schede (14-11-97)


// Fill dati Schede 
// debug purpose simulation for testpack

/*
dPos = (int)c_difCoil.getMeter() + (int)c_difCoil.getMeterBaseStepX();
dPos -= (int)c_lunResStop;
*/

// Calcolo posizione corretta da lResStop
double corrPos;
corrPos = c_lunResStop + dPos;

int lastIndex = min (c_difCoil.getNumSchede(),numField/nCl);
DifMetro difMetro;
DifBanda difBanda;
BOOL bandaDefPres = FALSE;
BOOL metroDefPres = FALSE;

// getsione allarmi solo su secondo pacchetto
BOOL updateExisting = FALSE;
// Create new metro or update existing
if ((offset != 0)&&
	((corrPos==0) || (c_difCoil.getMeter()==(int)corrPos))&&
	(c_difCoil.GetSize()>0)&&
	(c_difCoil.GetAt(c_difCoil.GetUpperBound()).posizione == (int)corrPos))
	{
	difMetro = c_difCoil.GetAt(c_difCoil.GetUpperBound());
	updateExisting = TRUE;
	}

for (int scIndex = 0;scIndex<lastIndex;scIndex ++)
	{
	difBanda.clear();
	bandaDefPres = FALSE;
	for (int j=0;j<nCl;j++)
		{
		int v=0;
		switch(c_sizeClassi)
			{
			case 1:
				v = (int ) cDest[j];
				break;
			case 2:
				v = (int ) sDest[j];
				break;
			case 4:
				v = (int ) lDest[j];
				break;
			}
		if (v)	bandaDefPres = TRUE;
		
		difBanda.Add (v);	
		}
	difBanda.setBanda(scIndex+offset*224/(c_sizeClassi*nCl));
	if (bandaDefPres)
		{
		metroDefPres = TRUE;
		difMetro.Add(difBanda);	
		}
	cDest += nCl; // al piu` 4 Classi
	sDest += nCl;
	lDest += nCl;
	}

BOOL viewScroll = FALSE;
if (metroDefPres)
	{
	if (dPos > 0)
		{
		difMetro.posizione = (int ) corrPos;
		}
	else
		{// incremento automatico, 
		//	lresStop tenuto conto in inizializzazione
		if (offset == 0)
			difMetro.posizione = (int)c_difCoil.getMeter() + (int)c_difCoil.getMeterBaseStepX();
		}
	if ((c_difCoil.GetSize() == 0)||
		((c_difCoil.GetAt(c_difCoil.GetUpperBound())).posizione != difMetro.posizione))
		{
		// precalcolo totali per classe (true inizializzo counter)
		difMetro.calcTotalCode(TRUE);
		c_difCoil.Add(difMetro);
		viewScroll = TRUE;
		}
	else
		{
		// precalcolo totali per classe
		difMetro.calcTotalCode(FALSE);
		c_difCoil.SetAt(c_difCoil.GetUpperBound(),difMetro);
		viewScroll = FALSE;
		}
	} 
	
// Update Metraggio
BOOL redrawAll = FALSE;
if (dPos > 0)
	{
	// Salto di metraggio ridisegno tutto
	int deltaP;
	deltaP = (int) corrPos - (int)c_difCoil.getMeter();
	deltaP -= (int)c_difCoil.getMeterBaseStepX();
	if (deltaP > 0)
		redrawAll = TRUE;
	c_difCoil.setMeter((int) corrPos);
	}
else
	{
	// incremento automatico, 
	//	lresStop tenuto conto in inizializzazione
	if (offset == 0)
		c_difCoil.setMeter(c_difCoil.getMeter() + (int)c_difCoil.getMeterBaseStepX());
	}
//schede.incMeter();

// Aggiornamento View

// Aggiorno Visualizzazione
CRect r;
// 1 significa solo aggiornamento
// 0 repaint all
if (redrawAll)
	UpdateAllViews (NULL,0);
else
	{
	if (viewScroll)
		UpdateAllViews (NULL,1); // scroll 
	else
		UpdateAllViews (NULL,2); // redraw
	}

// secondo pacchetto test allarmi densita`
if (offset)
	{
	updateDifAlarm();
	updateDifCluster();
	}
}


// ----------------------------------------
// Aggiornamento allarmi soglie difetti
void CLineDoc::updateDifAlarm() 
{

CString allarme,s;
BOOL alarm = FALSE;

// densita A
if (c_difCoil.getNumClassi() > 0)
	{
	CString str;
	double densita=c_difCoil.getDensityLevel('A',c_densityCalcLength);
	str.Format(_T("%6.0lf"),densita);
	if (densita > c_difCoil.getAllarme ('A'))
		{
		if (!densitaAlarm.c_statusDensClassA)
			{
			// 
			densitaAlarm.c_statusDensClassA=TRUE;
			//
			CString resMsg;
			resMsg.LoadString (CSM_GRAPHDOC_SOGLIADIFA);
			// allarme.Format("Superata soglia difetti tipo A (%8.2lf) al m. %d\r\n",trendA,
			allarme.Format(resMsg,densita,
				(int)c_difCoil.getMeter());
			densitaAlarm.appendMsg(allarme);
			alarm = TRUE;			
			}
		}
	else
		{
		if (densitaAlarm.c_statusDensClassA)
			{
			// 
			densitaAlarm.c_statusDensClassA=FALSE;
			//
			CString resMsg;
			resMsg.LoadString (CSM_GRAPHDOC_NO_SOGLIADIFA);
			// allarme.Format("Superata soglia difetti tipo A (%8.2lf) al m. %d\r\n",trendA,
			allarme.Format(resMsg,densita,
				(int)c_difCoil.getMeter());
			densitaAlarm.appendMsg(allarme);
			alarm = TRUE;			
			}
		}
	}

// densita B
if (c_difCoil.getNumClassi() > 1)
	{
	CString str;
	double densita=c_difCoil.getDensityLevel('B',c_densityCalcLength);
	str.Format(_T("%6.0lf"),densita);
	if (densita > c_difCoil.getAllarme ('B'))
		{
		if (!densitaAlarm.c_statusDensClassB)
			{
			// 
			densitaAlarm.c_statusDensClassB=TRUE;
			//
			CString resMsg;
			resMsg.LoadString (CSM_GRAPHDOC_SOGLIADIFB);
			// allarme.Format("Superata soglia difetti tipo A (%8.2lf) al m. %d\r\n",trendA,
			allarme.Format(resMsg,densita,
				(int)c_difCoil.getMeter());
			densitaAlarm.appendMsg(allarme);
			alarm = TRUE;			
			}
		}
	else
		{
		if (densitaAlarm.c_statusDensClassB)
			{
			// 
			densitaAlarm.c_statusDensClassB=FALSE;
			//
			CString resMsg;
			resMsg.LoadString (CSM_GRAPHDOC_NO_SOGLIADIFB);
			// allarme.Format("Superata soglia difetti tipo B (%8.2lf) al m. %d\r\n",trendA,
			allarme.Format(resMsg,densita,
				(int)c_difCoil.getMeter());
			densitaAlarm.appendMsg(allarme);
			alarm = TRUE;			
			}
		}

	}
//
// densita C
if (c_difCoil.getNumClassi() > 2)
	{
	CString str;
	double densita=c_difCoil.getDensityLevel('C',c_densityCalcLength);
	str.Format(_T("%6.0lf"),densita);
	if (densita > c_difCoil.getAllarme ('C'))
		{
		if (!densitaAlarm.c_statusDensClassC)
			{
			// 
			densitaAlarm.c_statusDensClassC=TRUE;
			//
			CString resMsg;
			resMsg.LoadString (CSM_GRAPHDOC_SOGLIADIFC);
			// allarme.Format("Superata soglia difetti tipo A (%8.2lf) al m. %d\r\n",trendA,
			allarme.Format(resMsg,densita,
				(int)c_difCoil.getMeter());
			densitaAlarm.appendMsg(allarme);
			alarm = TRUE;			
			}
		}
	else
		{
		if (densitaAlarm.c_statusDensClassC)
			{
			// 
			densitaAlarm.c_statusDensClassC=FALSE;
			//
			CString resMsg;
			resMsg.LoadString (CSM_GRAPHDOC_NO_SOGLIADIFC);
			// allarme.Format("Superata soglia difetti tipo A (%8.2lf) al m. %d\r\n",trendA,
			allarme.Format(resMsg,densita,
				(int)c_difCoil.getMeter());
			densitaAlarm.appendMsg(allarme);
			alarm = TRUE;			
			}
		}
	}
	
// densita D
if (c_difCoil.getNumClassi() > 3)
	{
	CString str;
	double densita=c_difCoil.getDensityLevel('D',c_densityCalcLength);
	str.Format(_T("%6.0lf"),densita);
	if (densita > c_difCoil.getAllarme ('D'))
		{
		if (!densitaAlarm.c_statusDensClassD)
			{
			// 
			densitaAlarm.c_statusDensClassD=TRUE;
			//
			CString resMsg;
			resMsg.LoadString (CSM_GRAPHDOC_SOGLIADIFD);
			// allarme.Format("Superata soglia difetti tipo A (%8.2lf) al m. %d\r\n",trendA,
			allarme.Format(resMsg,densita,
				(int)c_difCoil.getMeter());
			densitaAlarm.appendMsg(allarme);
			alarm = TRUE;			
			}
		}
	else
		{
		if (densitaAlarm.c_statusDensClassD)
			{
			// 
			densitaAlarm.c_statusDensClassD=FALSE;
			//
			CString resMsg;
			resMsg.LoadString (CSM_GRAPHDOC_NO_SOGLIADIFD);
			// allarme.Format("Superata soglia difetti tipo A (%8.2lf) al m. %d\r\n",trendA,
			allarme.Format(resMsg,densita,
				(int)c_difCoil.getMeter());
			densitaAlarm.appendMsg(allarme);
			alarm = TRUE;			
			}
		}
	}

if (alarm)
	{
// Invio micro allarme ( non serve + )
//	sendAlarmDif ();
// Allarme Densita non visualizza +
	densitaAlarm.visualizza(AfxGetMainWnd(),secAlarmSound);
	}
}


// ----------------------------------------
// Aggiornamento allarmi soglie cluster
void CLineDoc::updateDifCluster(void) 
{
int from,to;

CString allarme,s;
BOOL alarm = FALSE;

// densita A
if (c_difCoil.getNumClassi() > 0)
	{
	CString str;
	to = c_difCoil.getMeter();
	// ora i cluster funzionano cosi` => 
	// si cercano un gruppo di difetti all'interno di una certa lunghezza di materiale
	// se si trova cluster, allora memo posizione 
	// da li` in poi riparto nella ricerca dalla posizione c_lastClusterPos
	// utilizzata c_lastClusterPos per fare 
	from = to - c_difCoil.c_clusterArray[0].c_lunghezza;
	if (from < c_difCoil.c_clusterArray[0].c_lastClusterPos)
		from = c_difCoil.c_clusterArray[0].c_lastClusterPos;
	double nph = c_difCoil.getTotDifetti('A',from,to);
	str.Format(_T("%6.0lf"),nph);
	if (nph > c_difCoil.c_clusterArray[0].c_soglia)
		{
//		if (!clusterAlarm.c_statusDensClassA)
			{
			// 
			clusterAlarm.c_statusDensClassA = TRUE;
			//
			c_clusterCounter[0]++;
			// update posizione attuale
			c_difCoil.c_clusterArray[0].c_lastClusterPos = c_difCoil.getMeter();
			CString resMsg;
			resMsg.LoadString (CSM_GRAPHDOC_INICLUSTERA);
			// allarme.Format("Superata soglia difetti tipo A (%8.2lf) al m. %d\r\n",trendA,
			allarme.Format(resMsg,nph,
				(int)c_difCoil.getMeter());
			clusterAlarm.appendMsg(allarme);
			alarm = TRUE;
			// setto segnale di allarme cluster  
			c_signalCluster = TRUE;
			// setto istante di attivazione  
			c_signalClusterTime = time(NULL);
			}
		}
	else
		{
		if (clusterAlarm.c_statusDensClassA)
			{
			// 
			clusterAlarm.c_statusDensClassA=FALSE;
			//
			CString resMsg;
			resMsg.LoadString (CSM_GRAPHDOC_ENDCLUSTERA);
			// allarme.Format("Superata soglia difetti tipo A (%8.2lf) al m. %d\r\n",trendA,
			//allarme.Format(resMsg,nph,
			//	(int)c_difCoil.getMeter());
			//clusterAlarm.appendMsg(allarme);
			alarm = TRUE;			
			}
		}
	}

// densita B
if (c_difCoil.getNumClassi() > 1)
	{
	CString str;
	to = c_difCoil.getMeter();
	from = to - c_difCoil.c_clusterArray[1].c_lunghezza;
	if (from < c_difCoil.c_clusterArray[1].c_lastClusterPos)
		from = c_difCoil.c_clusterArray[1].c_lastClusterPos;
	double nph = c_difCoil.getTotDifetti('B',from,to);
	str.Format(_T("%6.0lf"),nph);
	if (nph > c_difCoil.c_clusterArray[1].c_soglia)
		{
//		if (!clusterAlarm.c_statusDensClassB)
			{
			// 
			clusterAlarm.c_statusDensClassB=TRUE;
			//
			c_clusterCounter[1]++;
			// update posizione attuale
			c_difCoil.c_clusterArray[1].c_lastClusterPos = c_difCoil.getMeter();

			CString resMsg;
			resMsg.LoadString (CSM_GRAPHDOC_INICLUSTERB);
			// allarme.Format("Superata soglia difetti tipo B (%8.2lf) al m. %d\r\n",trendA,
			allarme.Format(resMsg,nph,
				(int)c_difCoil.getMeter());
			clusterAlarm.appendMsg(allarme);
			alarm = TRUE;	
			// setto segnale di allarme cluster  
			c_signalCluster = TRUE;
			// setto istante di attivazione  
			c_signalClusterTime = time(NULL);

			}
		}
	else
		{
		if (clusterAlarm.c_statusDensClassB)
			{
			// 
			clusterAlarm.c_statusDensClassB=FALSE;
			//
			CString resMsg;
			resMsg.LoadString (CSM_GRAPHDOC_ENDCLUSTERB);
			// allarme.Format("Superata soglia difetti tipo B (%8.2lf) al m. %d\r\n",trendA,
//			allarme.Format(resMsg,nph,
//				(int)c_difCoil.getMeter());
//			clusterAlarm.appendMsg(allarme);
			alarm = TRUE;	

			}
		}
	}
//
// densita C
if (c_difCoil.getNumClassi() > 2)
	{
	CString str;
	to = c_difCoil.getMeter();
	from = to - c_difCoil.c_clusterArray[2].c_lunghezza;
	if (from < c_difCoil.c_clusterArray[2].c_lastClusterPos)
		from = c_difCoil.c_clusterArray[2].c_lastClusterPos;
	double nph = c_difCoil.getTotDifetti('C',from,to);
	str.Format(_T("%6.0lf"),nph);
	if (nph > c_difCoil.c_clusterArray[2].c_soglia)
		{
//		if (!clusterAlarm.c_statusDensClassC)
			{
			// 
			clusterAlarm.c_statusDensClassC=TRUE;
			//
			c_clusterCounter[2]++;
			// update posizione attuale
			c_difCoil.c_clusterArray[2].c_lastClusterPos = c_difCoil.getMeter();

			CString resMsg;
			resMsg.LoadString (CSM_GRAPHDOC_INICLUSTERC);
			// allarme.Format("Superata soglia difetti tipo C (%8.2lf) al m. %d\r\n",trendA,
			allarme.Format(resMsg,nph,
				(int)c_difCoil.getMeter());
			clusterAlarm.appendMsg(allarme);
			alarm = TRUE;
			// setto segnale di allarme cluster  
			c_signalCluster = TRUE;
			// setto istante di attivazione  
			c_signalClusterTime = time(NULL);

			}
		}
	else
		{
		if (clusterAlarm.c_statusDensClassC)
			{
			// 
			clusterAlarm.c_statusDensClassC=FALSE;
			//
			CString resMsg;
			resMsg.LoadString (CSM_GRAPHDOC_ENDCLUSTERC);
			// allarme.Format("Superata soglia difetti tipo C (%8.2lf) al m. %d\r\n",trendA,
//			allarme.Format(resMsg,nph,
//				(int)c_difCoil.getMeter());
//			clusterAlarm.appendMsg(allarme);
			alarm = TRUE;			
			}
		}
	}
	
// densita D
if (c_difCoil.getNumClassi() > 3)
	{
	CString str;
	to = c_difCoil.getMeter();
	from = to - c_difCoil.c_clusterArray[3].c_lunghezza;
	if (from < c_difCoil.c_clusterArray[3].c_lastClusterPos)
		from = c_difCoil.c_clusterArray[3].c_lastClusterPos;
	double nph = c_difCoil.getTotDifetti('D',from,to);
	str.Format(_T("%6.0lf"),nph);
	if (nph > c_difCoil.c_clusterArray[3].c_soglia)
		{
//		if (!clusterAlarm.c_statusDensClassD)
			{
			// 
			clusterAlarm.c_statusDensClassD=TRUE;
			//
			c_clusterCounter[3]++;
			// update posizione attuale
			c_difCoil.c_clusterArray[3].c_lastClusterPos = c_difCoil.getMeter();

			CString resMsg;
			resMsg.LoadString (CSM_GRAPHDOC_INICLUSTERD);
			// allarme.Format("Superata soglia difetti tipo D (%8.2lf) al m. %d\r\n",trendA,
			allarme.Format(resMsg,nph,
				(int)c_difCoil.getMeter());
			clusterAlarm.appendMsg(allarme);
			alarm = TRUE;		
			// setto segnale di allarme cluster  
			c_signalCluster = TRUE;
			// setto istante di attivazione  
			c_signalClusterTime = time(NULL);

			}
		}
	else
		{
		if (clusterAlarm.c_statusDensClassD)
			{
			// 
			clusterAlarm.c_statusDensClassD=FALSE;
			//
			CString resMsg;
			resMsg.LoadString (CSM_GRAPHDOC_ENDCLUSTERD);
			// allarme.Format("Superata soglia difetti tipo D (%8.2lf) al m. %d\r\n",trendA,
//			allarme.Format(resMsg,nph,
//				(int)c_difCoil.getMeter());
//			clusterAlarm.appendMsg(allarme);
			alarm = TRUE;			
			}
		}
	}

if (alarm)
	{
// Invio micro allarme ( non serve + )
//	sendAlarmDif ();
// Allarme Densita non visualizza +
	clusterAlarm.visualizza(AfxGetMainWnd(),secAlarmSound);
	}

}

// termina segnale cluster se passati x sec
void CLineDoc::terminateSignalCluster(void)
{

if (time(NULL) >= (c_signalClusterTime + c_fixSignalClusterTime))
	// setto segnale di allarme cluster  
	c_signalCluster = FALSE;

}


// ----------------------------------------
// Aggiornamento allarmi soglie difetti
/*----
void CLineDoc::updateDifAlarm() 
{
CString allarme,s;
BOOL alarm = FALSE;

if (c_difCoil.getValAlarmNumLStep('A',0))
	{
	CString resMsg;
	double trend = c_difCoil.getValNumLStep('A',0);
	resMsg.LoadString (CSM_GRAPHDOC_SOGLIADIFA);
	// allarme.Format("Superata soglia difetti tipo A (%8.2lf) al m. %d\r\n",trendA,
	allarme.Format(resMsg,trend,
		(int)c_difCoil.getMeter());
	densitaAlarm.appendMsg(allarme);
 	alarm = TRUE;
 	}
if (c_difCoil.getValAlarmNumLStep('B',0))
	{
	CString resMsg;
	double trend = c_difCoil.getValNumLStep('B',0);
	resMsg.LoadString (CSM_GRAPHDOC_SOGLIADIFB);
	// allarme.Format("Superata soglia difetti tipo B (%8.2lf) al m. %d\r\n",trendB,
	allarme.Format(resMsg,trend,
			(int)c_difCoil.getMeter());
	alarm = TRUE;
	densitaAlarm.appendMsg(allarme);
 	}
if (c_difCoil.getValAlarmNumLStep('C',0))
	{
	CString resMsg;
	double trend = c_difCoil.getValNumLStep('C',0);
	resMsg.LoadString (CSM_GRAPHDOC_SOGLIADIFC);
	// allarme.Format("Superata soglia difetti tipo C (%8.2lf) al m. %d\r\n",trendC,
	allarme.Format(resMsg,trend,
			(int)c_difCoil.getMeter());
	alarm = TRUE;
	densitaAlarm.appendMsg(allarme);
 	}
if (c_difCoil.getValAlarmNumLStep('D',0))
	{
	CString resMsg;
	double trend = c_difCoil.getValNumLStep('D',0);
	resMsg.LoadString (CSM_GRAPHDOC_SOGLIADIFD);
	// allarme.Format("Superata soglia difetti tipo D (%8.2lf) al m. %d\r\n",trendD,
	allarme.Format(resMsg,trend,
			(int)c_difCoil.getMeter());
	alarm = TRUE;
	densitaAlarm.appendMsg(allarme);
 	}
if (alarm)
	{
	// Invio micro allarme
	sendAlarmDif ();
	// Allarme Densita
	densitaAlarm.visualizza(AfxGetMainWnd(),secAlarmSound);
	}
}
------------------------------------------------------------------------------------*/

//------------------------------------
//
//	Gestione Messagi tipo ValPeriod
//
//------------------------------------


void CLineDoc::gestValPeriod (PVOID p,DWORD size) // Gestisce arrivo VAL_PERIOD
{
if (!systemOnLine)
	return;

}


//------------------------------------
//
//	Gestione Messagi tipo ValPeriod
//
//------------------------------------


void CLineDoc::gestValFt (PVOID p,DWORD size) // Gestisce arrivo VAL_FT
{
// ok fatto
// arrivano due int valori ft sx e dx
serMsgReceived = TRUE;

// in ispezione skip
if (systemOnLine)
	return;

unsigned int *pInt = (unsigned int *) p;

c_valFt1 = *pInt;
pInt ++;
c_valFt2 = *pInt;


}
//------------------------------------
//
//	Gestione Messagi tipo ValAlarm
//				   Allarmi di sistema
//------------------------------------
// ok fatto

void CLineDoc::setValAlarmEncoder(BOOL v){CSingleLock csl (&c_cs); 
	csl.Lock();
	if (c_statoHde != HDE_INSPECTION)
		c_valAlarmEncoder=FALSE;
	else
		c_valAlarmEncoder=v;
	csl.Unlock();};	

BOOL CLineDoc::getValAlarmEncoder(void){BOOL v; CSingleLock csl (&c_cs); 
	csl.Lock();v=c_valAlarmEncoder;csl.Unlock();return(v);};	



void CLineDoc::gestValAlEncoder (void) 
{
// allarme encoder
// Received Msg from serial
// l'encoder non arriva in qs caso da seriale 
// serMsgReceived = TRUE;

BOOL alActive = FALSE;

CMainFrame* pFrame = (CMainFrame*)AfxGetMainWnd();

if (pFrame == NULL)
	return;

int al = getValAlarmEncoder();

// allarme encoder 
if ((al != pFrame->c_stAllarmi.encoder)&&
	(systemOnLine))
	{
	if (al)
		{
		CString allarmeSistema;
		CString s;
		s.LoadString(CSM_GRAPHDOC_ENCODERNOK);
		// allarmeSistema.Format("Encoder Failure\r\n");
		allarmeSistema.Format (_T("m. %d: %s"),(int)c_difCoil.getMeter(),s);
		systemAlarm.appendMsg(allarmeSistema);
		alActive = TRUE;
		this->sendAlarmEncoder(TRUE);
		}
	else
		{
		this->sendAlarmEncoder(FALSE);
		}
	}
pFrame->c_stAllarmi.encoder = al;

}

//------------------------------------
//
//	Gestione Messagi tipo ValAlarm
//				   Allarmi di sistema
//------------------------------------
// ok fatto
void CLineDoc::gestValAlarm (PVOID p,DWORD size) 
{
// arrivano due interi da 4 byte
// Received Msg from serial
serMsgReceived = TRUE;


BOOL alActive = FALSE;
BOOL showMessage = FALSE;

CMainFrame* pFrame = (CMainFrame*)AfxGetMainWnd();

unsigned int *upc;
upc = (unsigned int *)p;

int al;

// bit [0] Diaframma sporco sx 
al = (int) upc[0] & 0x01;
if (al && (al != pFrame->c_stAllarmi.ftSporco1)&&
	(systemOnLine))
	{
	/* No scritta !
	CString allarmeSistema;
	CString s;
	s.LoadString(CSM_GRAPHDOC_FTNOK);
	allarmeSistema.Format("FT Sporco \r\n");
	allarmeSistema.Format ("m. %d: %s",(int)c_difCoil.getMeter(),s);
	systemAlarm.appendMsg(allarmeSistema);
	*/
	alActive = TRUE;
	}
pFrame->c_stAllarmi.ftSporco1 = al;

// bit [1] Diaframma sporco dx 	
al = (upc[0] & 0x02)?1:0;
if (al && (al != pFrame->c_stAllarmi.ftSporco2)&&
	(systemOnLine))
	{
	/* No scritta !
	CString allarmeSistema;
	CString s;
	s.LoadString(CSM_GRAPHDOC_FTNOK);
	// allarmeSistema.Format("FT Sporco \r\n");
	allarmeSistema.Format ("m. %d: %s",(int)c_difCoil.getMeter(),s);
	systemAlarm.appendMsg(allarmeSistema);*/
	alActive = TRUE;	
	}
pFrame->c_stAllarmi.ftSporco2 = al;

// bit [2] Led tx rotto
al = (upc[0] & 0x04)?1:0;
if (al && (al != pFrame->c_stAllarmi.laser)&&
	(systemOnLine))
	{
	CString allarmeSistema;
	CString s;
	s.LoadString(CSM_GRAPHDOC_LASERNOK);
	allarmeSistema.Format(_T("Led TX NON funzionante\r\n"));
	allarmeSistema.Format (_T("m. %d: %s"),(int)c_difCoil.getMeter(),s);
	systemAlarm.appendMsg(allarmeSistema);
	alActive = TRUE;
	showMessage = TRUE;
	}
pFrame->c_stAllarmi.laser = al;


al = (upc[0] & 0x08)?1:0;
if (al && (al != pFrame->c_stAllarmi.otturatore1)&&
	(systemOnLine))
	{
	CString allarmeSistema;
	CString s;
	s.LoadString(CSM_GRAPHDOC_OTTURA1NOK);
	// allarmeSistema.Format("Otturatore1 NON in posizione\r\n");
	allarmeSistema.Format (_T("m. %d: %s"),(int)c_difCoil.getMeter(),s);
	systemAlarm.appendMsg(allarmeSistema);
	alActive = TRUE;
	showMessage = TRUE;
	}
pFrame->c_stAllarmi.otturatore1 = al;

al = (upc[0] & 0x10)?1:0;
if (al && (al != pFrame->c_stAllarmi.otturatore2)&&
	(systemOnLine))
	{
	CString allarmeSistema;
	CString s;
	s.LoadString(CSM_GRAPHDOC_OTTURA2NOK);
	// allarmeSistema.Format("Otturatore2 NON in posizione\r\n");
	allarmeSistema.Format (_T("m. %d: %s"),(int)c_difCoil.getMeter(),s);
	systemAlarm.appendMsg(allarmeSistema);
	alActive = TRUE;
	showMessage = TRUE;
	}
pFrame->c_stAllarmi.otturatore2 = al;


// skip 1 byte
unsigned char mask;
// skip 1 byte
upc += 1;
for (int i=0;i< min(pFrame->c_stAllarmi.alNumCpu,7);i++)	// 1 byte per le CPU
	{  
	if ((i%8) == 0)
		mask = 0x01;
	al = (upc[i/8] & mask)?1:0;
	if (al && (al != pFrame->c_stAllarmi.cpu[i])&&
		(systemOnLine))
		{
		CString allarmeSistema;
		CString s,s1;
		s.LoadString(CSM_GRAPHDOC_CPUNOK);
		// allarmeSistema.Format("CPU %d NON funzionante\r\n",i);
		s1.Format (s,i+1);
		allarmeSistema.Format (_T("m. %d: %s"),(int)c_difCoil.getMeter(),s1);
		systemAlarm.appendMsg(allarmeSistema);
		alActive = TRUE;
		showMessage = TRUE;
		}
	pFrame->c_stAllarmi.cpu[i] = al;
	mask = mask << 1;
	}

if (showMessage)
	{
	systemAlarm.visualizza(pFrame,secAlarmSound);
	}
if (alActive&&c_enableExitOnAlarm)
	{// close inspection

#ifndef CSM10_AMI_REPORT
if (!systemOnLine)
	return;

// SAVE not save -> gestValClose() eseguito
// FileSaveStop(FALSE);
// ora si salva perche` valclose (-> filesave) non arriva
// Disable Micro
sendStartStop (FALSE,FALSE);

FileSaveStop(TRUE);
#endif
	}


}

//------------------------------------
//
//	Gestione Messagi stato Hw Input 
//				   
//------------------------------------

// ok fatto
void CLineDoc::gestValInput (PVOID p,DWORD size) 
{// Gestisce arrivo VAL_INPUT
// arrivano 4 interi 32 bit uno per ogni ingresso
// Received Msg from serial
serMsgReceived = TRUE;

if (size != 4)
	{// errore dimensione 
	return;
	}

unsigned int *from = ((unsigned int *) p);
unsigned short sval;

sval = 0;
for (int i=0;i<size;i++)
	{
	if (from[i])
		sval |= ( 1 << i);
	}


c_valInput = sval;

}

//--------------------------------------
//
//	Gestione Messagi tipo ValRisAutotest
//		 Allarmi laser rotti
//--------------------------------------

// ok fatto
void CLineDoc::gestValRisAutotest(PVOID p,DWORD size)
{// fatto
// formato 1 int per il modulo ( 1-10) + n-1 ( 16 )valori per celle 
// Received Msg from serial
serMsgReceived = TRUE;

BOOL alActive = FALSE;
CMainFrame* pFrame = (CMainFrame*)AfxGetMainWnd();

unsigned int *upc;
upc = (unsigned int *)p;

// modulo 1-10 
int modulo = *upc;
// partiamo da 0
modulo --; 
if (modulo < 0)
	modulo = 0;

// avanti pointer, skip numero modulo
upc++;

for (int i=0;i<(min(16,size-1)) ;i++)
	{//allarmi in logica negata aggiunto NOT !
	if (i+modulo*16 >=  pFrame->c_stAllarmi.alNumReceiver)
		break;
	int al = !upc[i];
	if (al && (al != pFrame->c_stAllarmi.receiver[i+modulo*16])&&
		(!systemOnLine))
		{
		CString allarmeSistema;
		CString cls;
			CString fmt;
			fmt.LoadString(CSM_GRAPHDOC_RICNOK);
			CString s;
			// Par Cella,Classi,Receiver 
			switch (c_difCoil.getNumClassi())
				{
				case 1:
					{
					int cell = i+modulo*16;
					cls = _T("A");// niente classe
					// 4 celle per ricevitore
					s.Format(fmt,cell+1,cell/4+1);
					}
					break;
				case 2:
					cls = _T("A/B");
					// s.Format(fmt,modulo+1,cls,i+1+modulo*16);
					s.Format(fmt,modulo+1,i+1+modulo*16);
					break;
				case 4:
				default:
					if (i%2 == 0)
						cls = _T("A/B");
					else
						cls = _T("C/D");
					s.Format(fmt,modulo+1,i+1+modulo*16);
				}
		// allarmeSistema.Format(_T("RICEVITORE Classi %s Numero %d NON funzionante\r\n"),cls,i/2);
		// Modificato in 
		// allarmeSistema.Format(_T("CELLA %d Classi %s Rx %d NON funzionante\r\n"),cls,i/2);
		allarmeSistema.Format (_T("m. %d: %s"),(int)c_difCoil.getMeter(),s);
		systemAlarm.appendMsg(allarmeSistema);
		alActive = TRUE;
		}
		if (pFrame->c_stAllarmi.receiver[i+modulo*16] == AL_OFF)
			{// primo aggiornamento
			pFrame->c_stAllarmi.receiver[i+modulo*16] = (al == AL_ON)?AL_ON:AL_OK1;
  			}
		else
			{// secondo aggiornamento
			pFrame->c_stAllarmi.receiver[i+modulo*16] = (pFrame->c_stAllarmi.receiver[i+modulo*16] == AL_OK1)?al:
							pFrame->c_stAllarmi.receiver[i+modulo*16] || al;
  			}

	}

// 8-10-19 inserito stato autotest .ini
CProfile profile;
c_statoAlarmAutotest = pFrame->c_stAllarmi.isHdeAlarmOn();
profile.writeProfileString(_T("init"), _T("AlarmAutotest"), 
						c_statoAlarmAutotest?_T("TRUE"):_T("FALSE") );



/*

int al;
// dimensione variabile
for (int i=0;i< min(pFrame->c_stAllarmi.alNumLaser,(int )(size -1));i++)
	{  
	al = upc[i];
	// allarme conosciuto
	if (al && (al != pFrame->c_stAllarmi.laserKO[i+(modulo*16)])&&
		systemOnLine)
		{
		CString allarmeSistema;
		CString fmt;
		fmt.LoadString(CSM_GRAPHDOC_LASNOK);
		CString s;
		s.Format(fmt,(i+1)+modulo*16);
		// allarmeSistema.Format("LASER %d NON funzionante\r\n",i);
		allarmeSistema.Format ("m. %d: %s",(int)c_difCoil.getMeter(),s);
		systemAlarm.appendMsg(allarmeSistema);
		alActive = TRUE;
		}
	pFrame->c_stAllarmi.laserKO[i+(modulo*16)] = al;
	}

if (alActive)
	{
	systemAlarm.visualizza(pFrame,secAlarmSound);
	}
*/
	
}


//------------------------------------
//
//	Invia Allarme difetti
//				   
//------------------------------------


void CLineDoc::sendAlarmDif (void)
{


/*
struct Command command;
#ifndef CSM10_AMI_REPORT

command.cmd = VAL_ALARMDIF;

command.size = 0;
command.data = NULL;

CMainFrame* pFrame = (CMainFrame*)AfxGetMainWnd();
pFrame->SendGeneralCommand(&command);

#endif
*/

}


//------------------------------------
//
//	Invia Allarme PC ( encoder )
//				   
//------------------------------------


void CLineDoc::sendAlarmDif (BOOL alarm)
{

#ifndef CSM10_AMI_REPORT
struct Command command;
command.cc = VAL_ALARM_PC;

// logica negata
if (alarm)
	command.nn = 01;
else
	command.nn = 11;

CMainFrame* pFrame = (CMainFrame*)AfxGetMainWnd();
pFrame->sendGeneralCommand(&command);

#endif


}


//------------------------------------
//
//	Invia pacchetto per test seriale 
//				   
//------------------------------------


void CLineDoc::sendValComPc (void )
{

#ifndef CSM10_AMI_REPORT
struct Command command;
command.cc = VAL_COM_PC;

command.nn = 00;

CMainFrame* pFrame = (CMainFrame*)AfxGetMainWnd();
pFrame->sendGeneralCommand(&command);

#endif


}

//------------------------------------
//
//	Invia comando autotest 
//				   
//------------------------------------


void CLineDoc::sendAutotest (void)
{

// clear stato autotest 
CMainFrame* pFrame = (CMainFrame*)AfxGetMainWnd();

#ifndef CSM10_AMI_REPORT
// cancellati tutti allarmi su invio autotest
clearAlarm();

#ifndef _DEBUG
pFrame->c_stAllarmi.clearReceiver();	
// clear allarmi sistema testo
systemAlarm.clearMessages();
struct Command command;
command.cc = VAL_COMMAND;
command.nn = AUTOTEST_CMD;
pFrame->sendGeneralCommand(&command);
#else
pFrame->c_stAllarmi.setTestAlarm();
CProfile profile;
c_statoAlarmAutotest = pFrame->c_stAllarmi.isHdeAlarmOn();
profile.writeProfileString(_T("init"), _T("AlarmAutotest"), 
						c_statoAlarmAutotest?_T("TRUE"):_T("FALSE") );

#endif

#endif

}
//------------------------------------
//
//	Invia Allarme difetti
//				   
//------------------------------------


void CLineDoc::sendAlarmEncoder (int val)
{



#ifndef CSM10_AMI_REPORT
struct Command command;
command.cc = VAL_ALARM_PC;

// logica negata
if (val)
	command.nn = 0;
else
	command.nn = 1;

CMainFrame* pFrame = (CMainFrame*)AfxGetMainWnd();
pFrame->sendGeneralCommand(&command);

#endif


}

//------------------------------------
//
//	Invia start o stop message
//				   
//------------------------------------


void CLineDoc::sendStartStop (BOOL start,BOOL test)
{


#ifndef CSM10_AMI_REPORT
struct Command command;
char tipo;
command.cc = VAL_COMMAND;

if (start)
	{
	if (test)
		tipo = START_TESTCMD;
	else
		tipo = STARTCMD;
	}
else
	{
	if (test)
		tipo = STOP_TESTCMD;
	else
		tipo = STOPCMD;
	}

command.nn = tipo;

CMainFrame* pFrame = (CMainFrame*)AfxGetMainWnd();
pFrame->sendGeneralCommand(&command);

#endif

}

//------------------------------------
//
//	Invia Valore soglie
//				   
//------------------------------------


void CLineDoc::sendValSoglie (void)
{


#ifndef CSM10_AMI_REPORT
struct Command command;
short sFrom[4];
command.cc = VAL_SOGLIA;

sFrom[0] = (short) sogliaA;
sFrom[1] = (short) sogliaB;
sFrom[2] = (short) sogliaC;
sFrom[3] = (short) sogliaD;

// 1 sola classe
command.nn = sFrom[0];

CMainFrame* pFrame = (CMainFrame*)AfxGetMainWnd();
pFrame->sendGeneralCommand(&command);

#endif

}

//------------------------------------------------
//
//	Invia Valore fattore di correzione dim lin
//				   
//------------------------------------------------


void CLineDoc::sendValFcdl (void)
{


/*
#ifndef CSM10_AMI_REPORT
struct Command command;
short sFrom,sTo;
command.cmd = VAL_FCDL;

sFrom = (short) (c_fattCorrDimLin * 1000.);

// Swap data (short format)
_swab ((char *)&sFrom,(char *)&sTo,1*sizeof(short));

command.size = 1 * sizeof (short);
command.data = (void *) &sTo;

CMainFrame* pFrame = (CMainFrame*)AfxGetMainWnd();
pFrame->SendGeneralCommand(&command);

#endif
*/

}

///------------------------------------------------
//
//	Invia Valore output
//				   
//------------------------------------------------


void CLineDoc::sendValOutput (unsigned short v)
{


#ifndef CSM10_AMI_REPORT
CMainFrame* pFrame = (CMainFrame*)AfxGetMainWnd();
struct Command command;
command.cc = VAL_OUTPUT;

for (int i=0;i<6;i++)
	{
	if ((v >> i) & 0x01)
		command.nn = 10 + (i+1);	// on 
	else
		command.nn = (i+1);			// off
	
	pFrame->sendGeneralCommand(&command);
	}
#endif

}

//------------------------------------
//
//	Invia Input-Output start o stop message
//				   
//------------------------------------


void CLineDoc::sendIOStartStop (BOOL start)
{


#ifndef CSM10_AMI_REPORT
struct Command command;
command.cc = VAL_COMMAND;

if (start)
	command.nn = START_TEST_IO;
else
	command.nn = STOP_TEST_IO;


CMainFrame* pFrame = (CMainFrame*)AfxGetMainWnd();

pFrame->sendGeneralCommand(&command);

#endif


}

//------------------------------------------------
//
//	Invia Start test diaframma
//				   
//------------------------------------------------


void CLineDoc::sendStartTestDiaf (BOOL left)
{

/*


#ifndef CSM10_AMI_REPORT
struct Command command;
unsigned char cv[2];
command.cmd = VAL_START_TESTDIAF;

if (left)
	{
	cv[0] = 1;	
	cv[1] = 0;
	}
else
	{
	cv[0] = 0;
	cv[1] = 1;
	}

command.size = 2;
command.data = (void *) cv;

CMainFrame* pFrame = (CMainFrame*)AfxGetMainWnd();

pFrame->SendGeneralCommand(&command);

#endif
*/

}


///------------------------------------------------
//
//	Invia Valore durata impulso di allarme
//				   
//------------------------------------------------


void CLineDoc::sendValAlPulse (void)
{


#ifndef CSM10_AMI_REPORT
struct Command command;
short sFrom;

command.cc = VAL_ALPULSE;

DBParametri dbPar(dBase);
 if (dbPar.openSelectDefault ())
	{
	// Move Init Data from Db to Dialog
	sFrom = (short) dbPar.m_ALARM_PULSE;
	
	// invio parametro come decina di msec
	command.nn = sFrom/10;

	CMainFrame* pFrame = (CMainFrame*)AfxGetMainWnd();
	pFrame->sendGeneralCommand(&command);
	dbPar.Close();
	}

#endif
}

///------------------------------------------------
//
//	Invia classe da utilizzare per uscita digitale allarme
//				   
//------------------------------------------------

void CLineDoc::sendValClassDigitalOut (void)
{

/*
#ifndef CSM10_AMI_REPORT
struct Command command;
char c;

command.cmd = VAL_HOLES_CLASS_DIGIOUT;

// Hole class marking 1,2,3,4 e non 0,1,2,3
c = (char) c_holeClassDigiOut + 1;

command.size = sizeof(char);
command.data = (void *) &c;

CMainFrame* pFrame = (CMainFrame*)AfxGetMainWnd();
pFrame->SendGeneralCommand(&command);

#endif

*/
}

//------------------------------------
//
//	Gestione Messagi watch dog seriale
//				   
//------------------------------------

void CLineDoc::gestValTestSer (PVOID p,DWORD size) // Gestisce arrivo VAL_PERIOD
{
// Received Msg from serial
serMsgReceived = TRUE;

#ifndef CSM10_AMI_REPORT

if (!systemOnLine)
	return;
#endif

}

//------------------------------------
//
//	Gestione Messagi Stop Acquisizione 
//				   
//------------------------------------

void CLineDoc::gestValStop (PVOID p,DWORD size) 
{// Gestisce arrivo VAL_STOP

// Received Msg from serial
serMsgReceived = TRUE;

#ifndef CSM10_AMI_REPORT

if (!systemOnLine)
	return;

// SAVE not save -> gestValClose() eseguito
// FileSaveStop(FALSE);
// ora si salva perche` valclose (-> filesave) non arriva
FileSaveStop(TRUE);

#endif

}

//------------------------------------
//
//	Gestione Messagi File Close 
//				   
//------------------------------------

void CLineDoc::gestValClose (PVOID p,DWORD size) 
{// Gestisce arrivo VAL_FILECLOSE
#ifndef CSM10_AMI_REPORT

// Received Msg from serial
serMsgReceived = TRUE;
if (!systemOnLine)
	return;

FileSave();

#endif
}

//------------------------------------
//
//	Gestione Messagi File Open 
//				   
//------------------------------------

void CLineDoc::gestValOpen (PVOID p,DWORD size) 
{// Gestisce arrivo VAL_FILEOPEN
// Received Msg from serial
serMsgReceived = TRUE;

#ifndef CSM10_AMI_REPORT

if (systemOnLine)
	return;

// TRUE warmStart
OnFileStart();

// Display empty data
UpdateAllViews(NULL);

#endif

}

//------------------------------------------------
// Inizio Lavorazione
//------------------------------------------------

void CLineDoc::OnUpdateFileStart(CCmdUI* pCmdUI) 
{
CMainFrame* pMFrame = (CMainFrame*)AfxGetMainWnd();
ASSERT_VALID(pMFrame);
ASSERT(pMFrame->IsFrameWnd());

// TODO: Add your command update UI handler code here
#ifdef _DEBUG
// pCmdUI->Enable((!systemOnLine)&&(!serAlarmActive));
pCmdUI->Enable((!systemOnLine)&&
	//(!pMFrame->c_stAllarmi.isHdeAlarmOn())&&
	(!c_statoAlarmAutotest)&&
	(!serAlarmActive));
#else
// inserita condizione nessun allarme per abilitazione F1
if (c_disableF1OnAlarm)
	pCmdUI->Enable((!systemOnLine)&&
	// (!pMFrame->c_stAllarmi.isHdeAlarmOn())&&
	(!c_statoAlarmAutotest)&&
	(!serAlarmActive));
else
	pCmdUI->Enable((!systemOnLine));
#endif
}

// loadPlantInfo 

BOOL CLineDoc::loadPlantInfoFromDialog(CDProduct* pProd,BOOL interactive,BOOL test)
{

DBRicette dbRicette(dBase);
dbRicette.openSelectName (pProd->c_nomiRicette);
if (pProd->c_nomiRicette.GetSize()<=0)
	{
	CString resMsg;
	resMsg.LoadString (CSM_GRAPHDOC_NORICETTA);
	// AfxMessageBox ("Nessuna ricetta Selezionabile"); 
	AfxMessageBox (resMsg); 
	return FALSE;
	}

pProd->m_manualNomeAsse = FALSE;
if (test)
	{
	pProd->m_coil = _T("Test");
	}

pProd->c_numericCoil = (c_rotoloNameMode == ROTOLO_NAME_NUMCOIL);

if (interactive)
	{
	if (pProd->DoModal() != IDOK)
		return FALSE;
	c_useManualTitle = pProd->m_manualNomeAsse;
	if(c_useManualTitle)
		c_manualTitle = pProd->m_nomeAsse;
	}
else
	{
	if (c_rotoloNameMode == ROTOLO_NAME_NUMCOIL)
		{// rotolo e` numerico e deve essere incrementato
		// non uso rotolo ext
		int nc;
		_stscanf((LPCTSTR)pProd->m_coil,_T("%d"),&nc);
		nc++;
		pProd->m_coil.Format(_T("%d"),nc);
		}
	// load last param 
	//pProd->m_coil = "";
	//pProd->m_product = "";
	//pProd->m_customer = "";
	//pProd->m_larghezza = "1400.0";
	//pProd->m_spessore = "0.1";
	}

lega = pProd->m_product;
c_cliente = pProd->m_customer;
spessore = _tstof((LPCTSTR)pProd->m_spessore);
c_larghezza = _tstof((LPCTSTR)pProd->m_larghezza);
c_prodCoil = pProd->m_coil;

// if this rotolo is different 
// previous clear rotolo ext
if (rotolo != pProd->m_coil)
	{
	rotolo = pProd->m_coil;
	rotoloExt = 0;
	}
else
	{
	// increment rotolo ext
	rotoloExt ++;
	}

nomeRicetta = pProd->m_inspCode;
return TRUE;
}

void CLineDoc::OnFileStartPsw()
{
// TODO: Add your command handler code here
// clear all alarm before F1 without autotest

if (!CPasswordDlg::CheckPassword())
	return;
	
clearAlarm();

OnFileStart();

}


void CLineDoc::OnFileStart() 
{
CString namerotolo,namerotoloExt;

if (systemOnLine)
	return;

// constructor automaticamente carica valori default. da .ini
CDProduct prod;
if (!loadPlantInfoFromDialog(&prod,
	(c_plantInfoMode==PLANTINFO_START_DIALOG),
	FALSE	// no test
//	||(c_plantInfoMode==PLANTINFO_NODATA)
	))
	// operatore ha premuto annulla
	return;


// blind exit by status
c_blindStatoHde = CTime::GetCurrentTime();
c_blindStatoHde += CTimeSpan(0,0,0,1);	// gg,hh,mm,ss

// Start Sistema
systemOnLine = TRUE;
	
// Init Alarm flag
diskAlarmActive = FALSE;
serAlarmActive = FALSE;

// Wait at least TOT sec
serMsgReceived = TRUE;

// load default Dbase settings
UpdateBaseDoc();

// init title
// FALSE coldStart
FileStart(FALSE);

// Calcola dimensione delle bande per ciascuna scheda
// setSizeBande();

// Send Micro Valori soglie
sendValSoglie ();
// Send Micro Valori FCDL
sendValFcdl ();
// Send Micro Valori Durata impulso allarme
sendValAlPulse ();
// Send Micro classe allarme uscita digitale
sendValClassDigitalOut ();

// Enable Micro no test
sendStartStop (TRUE,FALSE);

CMainFrame* pMFrame = (CMainFrame*)AfxGetMainWnd();
ASSERT_VALID(pMFrame);
ASSERT(pMFrame->IsFrameWnd());

#ifndef CSM10_AMI_REPORT
// Start simulation
CProfile profile;
if(profile.getProfileBool(_T("Simulazione"),_T("AutoRun"),FALSE))
	pMFrame->StartSimulEncoder(millisec); 

// false no autodelete
c_thEncoder.setPulseMetro(c_fattCorrDimLin*1000);
c_thEncoder.setPDoc(this);
c_thEncoder.setPWnd(AfxGetMainWnd());
c_thEncoder.launch(FALSE);
#endif

// Modify View Visualizza vista di default
CFrameWnd* pFrame;
pFrame = pMFrame->GetActiveFrame();
THISAPP* pApp = (THISAPP*)AfxGetApp();
pApp->pDocTemplate->ReplaceView(pFrame,1,TRUE);
}



// Gestione File start per Test
void CLineDoc::OnFileTstart() 
{
// TODO: Add your command handler code here
CString namerotolo,namerotoloExt;

#ifndef CSM10_AMI_REPORT

if (systemOnLine)
	return;

if (!CPasswordDlg::CheckPassword())
	return;

/*
CDProduct prod;
DBRicette dbRicette(dBase);
dbRicette.openSelectName (prod.c_nomiRicette);
if (prod.c_nomiRicette.GetSize()<=0)
	{
	CString resMsg;
	resMsg.LoadString (CSM_GRAPHDOC_NORICETTA);
	// AfxMessageBox ("Nessuna ricetta Selezionabile"); 
	return;
	}
*/
// constructor automaticamente carica valori default. da .ini
CDProduct prod;
if (!loadPlantInfoFromDialog(&prod,
	TRUE, // PLANTINFODIALOG
	TRUE	// test mode
	))
	// operatore ha premuto annulla
	return;

// prod.m_inspCode = prod.c_nomiRicette[0];

lega = prod.m_product;
rotolo = prod.m_coil;
nomeRicetta = prod.m_inspCode;

// blind exit by status
c_blindStatoHde = CTime::GetCurrentTime();
c_blindStatoHde += CTimeSpan(0,0,0,1);	// gg,hh,mm,ss


// Start Sistema
systemOnLine = TRUE;
systemOnTest = TRUE;
	
// Init Alarm flag
diskAlarmActive = FALSE;
serAlarmActive = FALSE;

// Wait at least TOT sec
serMsgReceived = TRUE;

// load default Dbase settings
UpdateBaseDoc();

// init title
// FALSE -> cold start
FileStart(FALSE);

// Calcola dimensione delle bande per ciascuna scheda
// setSizeBande();

// Send Micro classe allarme uscita digitale
sendValClassDigitalOut ();

// Send Micro Valori soglie
sendValSoglie ();

// Enable Micro YES test
sendStartStop (TRUE,TRUE);

CMainFrame* pMFrame = (CMainFrame*)AfxGetMainWnd();
ASSERT_VALID(pMFrame);
ASSERT(pMFrame->IsFrameWnd());

// Start simulation
pMFrame->StartSimulEncoder(1000); 

// Modify View Visualizza vista di default
CFrameWnd* pFrame;
pFrame = pMFrame->GetActiveFrame();
THISAPP* pApp = (THISAPP*)AfxGetApp();
pApp->pDocTemplate->ReplaceView(pFrame,1,TRUE);

#endif
}

void CLineDoc::OnUpdateFileTstart(CCmdUI* pCmdUI) 
{
// TODO: Add your command update UI handler code here
pCmdUI->Enable(!systemOnLine);
}

// Crea titolo ( e mnome asse) 
// passOne serve x dialog a fine asse che deve avere titolo datora all'inizio 
// e titolo diverso alla fine
CString CLineDoc::createTitle(BOOL warmStart,BOOL passOne)
{ 
// Set Title
CString title;
CTime cTime(startTime);
switch(c_plantInfoMode)
	{
	case PLANTINFO_STOP_DIALOG:
		{
		if (passOne)
			{
			CString tFormatRes;
			tFormatRes.LoadString(CSM_GRAPHDOC_DATEFORMAT);
			title = cTime.Format(_T("%Y-%m-%d-%H-%M-%S"));
			rotolo = title;
			c_rotoloNameMode=ROTOLO_NAME_TIME;
			}
		else
			{
			c_rotoloNameMode=ROTOLO_NAME_EXT;
			if (c_useManualTitle)
				{
				title = c_manualTitle;
				if (warmStart)
					{
					// save rotoloExt
					rotoloExt = getLastRotoloExt(title);
					rotoloExt ++;
					CString s;
					s.Format(_T("_%02d"),rotoloExt);
					title += s;
					}
				}
			else
				{
				CString tFormatRes;
				tFormatRes.LoadString(CSM_GRAPHDOC_DATEFORMAT);
				title = cTime.Format(_T("%Y-%m-%d"));
				title += "-";
				CString l;
				l = lega.Left(5);
				if (l.GetLength() > 0)
					{
					title += l;
					title += "-";
					}
				CString baseFileRotolo;
				baseFileRotolo = title;
				baseFileRotolo += rotolo;
				// save rotoloExt
				rotoloExt = getLastRotoloExt(baseFileRotolo);
				rotoloExt ++;
				// extIncluded
				title += getRotolo();
				}
			}
		}
		break;
	case PLANTINFO_START_DIALOG:
		{
		if (c_useManualTitle)
			{
			title = c_manualTitle;
			if (warmStart)
				{
				// save rotoloExt
				rotoloExt = getLastRotoloExt(title);
				rotoloExt ++;
				CString s;
				s.Format(_T("_%02d"),rotoloExt);
				title += s;
				}
			}
		else
			{
			if (c_rotoloNameMode == ROTOLO_NAME_TIME)
				{
				CString tFormatRes;
				tFormatRes.LoadString(CSM_GRAPHDOC_DATEFORMAT);
				title = cTime.Format(_T("%Y-%m-%d-%H-%M-%S"));
				rotolo = title;
				}
			else
				{
				if (c_rotoloNameMode == ROTOLO_NAME_NUMCOIL)
					{// Utilizza lega e num coil, numcoil=rotolo e` numerico!!
					 // al cambio asse si incrementa numCoil di 1
					if (warmStart)
						{// cambio incremento di 1
						int v;
						_stscanf((LPCTSTR)rotolo,_T("%d"),&v);
						v ++;
						rotolo.Format(_T("%d"),v);
						}
					title = lega;
					title += "-";
					title += rotolo;
					// save rotoloExt
	//				rotoloExt = getLastRotoloExt(baseFileRotolo);
	//				rotoloExt ++;
					// extIncluded
	//				title += getRotolo();		
					}
				else	
					{
					CString tFormatRes;
					tFormatRes.LoadString(CSM_GRAPHDOC_DATEFORMAT);
					title = cTime.Format("%Y-%m-%d");
					title += "-";
					CString l;
					l = lega.Left(5);
					if (l.GetLength() > 0)
						{
						title += l;
						title += "-";
							}
					CString baseFileRotolo;
					baseFileRotolo = title;
					baseFileRotolo += rotolo;
					// save rotoloExt
					rotoloExt = getLastRotoloExt(baseFileRotolo);
					rotoloExt ++;
					// extIncluded
					title += getRotolo();
					}
				}
			}
		}
		break;
	case PLANTINFO_MDB:
		{
		CString tFormatRes;
		tFormatRes.LoadString(CSM_GRAPHDOC_DATEFORMAT);
		title = cTime.Format("%Y-%m-%d-%H-%M-%S");
		rotolo = title;
		}
		break;
	case PLANTINFO_NODATA:
		{
		CString tFormatRes;
		tFormatRes.LoadString(CSM_GRAPHDOC_DATEFORMAT);
		title = cTime.Format("%Y-%m-%d-%H-%M-%S");
		// aggiungo solo parte di nome coil inserito dall'utente 
		// nella dialog con dati asse
		title += c_prodCoil;
		rotolo = title;
		}
		break;
	}

return(title);
}


//-----------------------------------------------
//	Codice comune a OnFileStart
//	e gestFileOpen
//
//
//
// Parametro TRUE per warmStart

void CLineDoc::FileStart(BOOL warmStart ) 
{
CTime time = CTime::GetCurrentTime();
// save date and time start 
startTime = time.GetTime();

// True passOne
CString s;
s = createTitle(warmStart,TRUE);	
setTitle((LPCTSTR)s);

// init all data structure in new coil job
newCoil (warmStart);

UpdateBaseDoc();
}

//-----------------------------------------------
// Stop sistema

void CLineDoc::OnUpdateFileStop(CCmdUI* pCmdUI) 
{
	// TODO: Add your command update UI handler code here

// pCmdUI->Enable(systemOnLine&&(!systemLoading));
pCmdUI->Enable(systemOnLine);
	
}


// Comando di chiusura
void CLineDoc::OnFileStop() 
{
// TODO: Add your command handler code here

if (!systemOnLine)
	return;

if (!systemOnTest)
	{
	CProfile profile;
	BOOL usePasswdOnF2;
	usePasswdOnF2 = profile.getProfileBool(_T("init"),_T("UsePasswdOnF2"),FALSE);
	
	if (usePasswdOnF2 && (!CPasswordDlg::CheckPassword()))
		return;
	}

CString msgRes;
msgRes.LoadString (CSM_GRAPHDOC_STOPINSP);
// if (AfxMessageBox("STOP INSPECTION\nPress OK to STOP INSPECTION\nPress CANCEL to CONTINUE",MB_OKCANCEL|MB_DEFBUTTON2|MB_ICONEXCLAMATION) == IDCANCEL)
if (AfxMessageBox(msgRes,MB_OKCANCEL|MB_DEFBUTTON2|MB_ICONEXCLAMATION) == IDCANCEL)
		return;

// Disable Micro
sendStartStop (FALSE,FALSE);
FileSaveStop(); 


}



void CLineDoc::FileSaveStop(BOOL doSave) 
{
// TODO: Add your command handler code here
CMainFrame* pMFrame = (CMainFrame*)AfxGetMainWnd();
ASSERT_VALID(pMFrame);
ASSERT(pMFrame->IsFrameWnd());

if (!systemOnLine)
	return;

// Stop simulation
#ifndef CSM10_AMI_REPORT
pMFrame->endSimulEncoder(); 
// stop thread encoder
c_thEncoder.waitTerminate();
#endif

// Sistema Fuori linea
systemOnLine = FALSE;
// Codice comune con il test
systemOnTest = FALSE;

if (doSave)
	// Save Data FALSE no warmSave
	FileSave(FALSE);

// Sistema Fuori linea
systemOnLine = FALSE;
// Codice comune con il test
systemOnTest = FALSE;

// Videata iniziale
CFrameWnd* pFrame = (CFrameWnd*)AfxGetMainWnd();
ASSERT_VALID(pFrame);
ASSERT(pFrame->IsFrameWnd());
pFrame = pFrame->GetActiveFrame();

THISAPP* pApp = (THISAPP*)AfxGetApp();
pApp->pDocTemplate->ReplaceView(pFrame,0,TRUE);

// clear toolbar stato tx/rx cpu shutter etc
pMFrame->c_stAllarmi.clearReceiver();


}

void CLineDoc::FileSave(BOOL warmSave) 
{
#ifndef CSM10_AMI_REPORT
// Idati sono stati caricati RealTime
loadedData = TRUE;

CDProduct prod;
if (c_plantInfoMode==PLANTINFO_STOP_DIALOG)
	{
	systemOnLine = FALSE;
	
	loadPlantInfoFromDialog(&prod,TRUE,FALSE);	// no test
	// false -> pass2 per StopDialog
	CString s;
	s = createTitle(FALSE,FALSE);
	setTitle((LPCTSTR)s);

	}

CMainFrame *frame = (CMainFrame *)AfxGetMainWnd();
frame->progressCreate();
frame->progressSetRange(0, 8);
frame->progressSetStep(2);
CProgressCtrl *progress;
progress = frame->getProgressCtrl();

// Save Data  only for new Data
CFileBpe cf;
CFileException e;
CString title,titleMdb,titlePdf;
CTime st(startTime);
title = c_homeDir;
title += "/";
title += _T("DD") + st.Format ("%m%Y");


SECURITY_ATTRIBUTES	 sa;

sa.nLength= sizeof(SECURITY_ATTRIBUTES);
sa.lpSecurityDescriptor=NULL;
sa.bInheritHandle = TRUE;

// Show Progress
frame->progressStepIt();

::CreateDirectory((LPCTSTR)title,&sa);
// save month folder for pdf
titlePdf = title;
titlePdf += _T("/Pdf");
::CreateDirectory((LPCTSTR)titlePdf,&sa);
titlePdf += _T("/");
titlePdf += GetTitle(); 
titlePdf += _T(".pdf");
// -------------------- 
title += _T("/");
title += GetTitle(); 
titleMdb = title + _T(".mdb");
title += _T(".csm");

// Rename previous file with same name
renameFile(title);
// Rename previous file with same name
renameFile(titleMdb);

// generate report dettagliato fori mdb
genReportDbFori((LPCTSTR) titleMdb);
if (c_plantInfoMode==PLANTINFO_MDB)
	{
	updateInfoPlantDb(titleMdb);
	loadPlantInfoFromDb();
	}

if(!cf.Open(title, CFile::modeCreate | CFile::modeWrite | CFile::typeBinary, &e ) )
	{
	CString s;
	s.Format(_T("File could not be opened %d"),e.m_cause);
	AfxMessageBox(s);
	}
else
	{
	save(&cf,progress);
	cf.Close();
	}

// Show Progress
frame->progressStepIt();

// Show Progress
// frame->progressStepIt();

// Gestione stampa automatica
CFrameWnd* pFrame = (CFrameWnd*)AfxGetMainWnd();
ASSERT_VALID(pFrame);
ASSERT(pFrame->IsFrameWnd());
pFrame = pFrame->GetActiveFrame();

bool print2Pdf = false;


CCSMApp* pApp = (CCSMApp*) AfxGetApp();
if (pApp->GetDefaultPrinter() == MICROSOFT_PDF_PRINTER)
	print2Pdf = true;

DBOpzioni dbOpzioni(dBase);
if (dbOpzioni.openSelectDefault ())
	{
	if (dbOpzioni.m_STAMPA_AUTO)
		{
		if (c_plantInfoMode!=PLANTINFO_MDB)
			{
			CInitView *view;
			POSITION pos;
			//int actualView = pApp->pDocTemplate->GetViewID(this);
			pApp->pDocTemplate->ReplaceView(pFrame,0,TRUE);
			pos = GetFirstViewPosition();
			view = (CInitView *) GetNextView(pos);
			
			// Stampa automatica parte solo testo
			view->onlyTextReport = TRUE;
			view->directPrint = TRUE; 
			if (print2Pdf)
				{
				view->setDocPath(titlePdf);
				}
			else
				view->setDocPath(_T(""));
	   		view->OnFilePrint();

			view->onlyTextReport = FALSE;
			view->directPrint = FALSE; 	
			pApp->pDocTemplate->ReplaceView(pFrame,1,TRUE);
			}
		}
	}
dbOpzioni.Close();

// Show Progress
frame->progressStepIt();
// Close Progress
frame->progressDestroy();
if (warmSave&&
	(c_plantInfoMode == PLANTINFO_STOP_DIALOG))
	{
	systemOnLine = TRUE;
	// ormai ci siamo persi il gestFileOpen
	// TRUE warmStart
	FileStart(TRUE);
	// Display empty data
	UpdateAllViews(NULL);
	}
#endif
}



void CLineDoc::OnViewPeriodici() 
{

}

void CLineDoc::OnViewTrend() 
{
// TODO: Add your command handler code here

}
void CLineDoc::OnViewSystem() 
{
	
}


void CLineDoc::OnUpdateViewPeriodici(CCmdUI* pCmdUI) 
{

// TODO: Add your command update UI handler code here
pCmdUI->Enable(loadedData || systemOnLine);
// pCmdUI->SetCheck(periodAlarm.isActive ());
	
}

void CLineDoc::OnUpdateViewTrend(CCmdUI* pCmdUI) 
{

// TODO: Add your command update UI handler code here

pCmdUI->Enable(loadedData || systemOnLine);
// pCmdUI->SetCheck(densitaAlarm.isActive ());
	
}


void CLineDoc::OnUpdateViewSystem(CCmdUI* pCmdUI) 
{
	// TODO: Add your command update UI handler code here
pCmdUI->Enable(loadedData || systemOnLine);
// pCmdUI->SetCheck(systemAlarm.isActive ());
	
}

void CLineDoc::OnCloseDocument() 
{
	// TODO: Add your specialized code here and/or call the base class
CDocument::OnCloseDocument();
}

BOOL CLineDoc::CanCloseFrame(CFrameWnd* pFrame) 
{
	// TODO: Add your specialized code here and/or call the base class
if (systemOnLine)
	{	
	CString msgRes;
	msgRes.LoadString (CSM_GRAPHDOC_EXITSTOPINSP);
//	if (AfxMessageBox("WARNING !!! The System is running\nPress OK to STOP the inspection\nPress CANCEL to continue the inspection",MB_OKCANCEL|MB_DEFBUTTON2|MB_ICONEXCLAMATION) == IDCANCEL)
	if (AfxMessageBox(msgRes,MB_OKCANCEL|MB_DEFBUTTON2|MB_ICONEXCLAMATION) == IDCANCEL)
		return FALSE;
	OnFileStop();
	}
	
	return CDocument::CanCloseFrame(pFrame);
}



// Una volta formato il nome base del rotolo 
// Viene cercato il file con stesso nome ed edstensione piu` 
// alta 
int CLineDoc::getLastRotoloExt(CString &baseRotolo)
{

CTime st(startTime);
CString dir = c_homeDir;
dir += _T("/");
dir += _T("DD");
dir += st.Format (_T("%m%Y"));

TCHAR curDir [256];
if (!GetCurrentDirectory (255,curDir))
	return (0);

if (!SetCurrentDirectory(dir))
	return (0);	// directory !exist

CFileFind fileSearch;
CString searchString;
searchString = baseRotolo;
// add ext wildCard
searchString += _T("_*.csm");

int extension = 0;

if (fileSearch.FindFile (searchString))
	{
	BOOL go = TRUE;
	while (go)
		{
		go = fileSearch.FindNextFile();
		if (!fileSearch.IsDirectory())
			{
			CString fileName;
			fileName = fileSearch.GetFileName();
			// Estraggo estensione numerica
			CString sr (fileName.Right(4));
			sr.MakeUpper();
			if (sr == _T(".CSM"))
				{
				fileName = fileName.Left(fileName.GetLength()-4);
				int i = 0;
				while (isdigit(fileName[fileName.GetLength()-i-1]))
					{
					i++;
					if (i >= fileName.GetLength())
						break;
					}
				if (i>0)
					{
					int vExt;
					_stscanf(fileName.Right(i),_T("%d"),&vExt);
					if (vExt > extension)
						extension = vExt;
					}
				}
			}
		}
	}
fileSearch.Close();

SetCurrentDirectory(curDir);

return (extension);
}


BOOL CLineDoc::genReportDbFori(LPCTSTR nomeFile)
{
CDaoDbCreate dB;
CString strValue;

CProfile profile;
strValue = profile.getProfileString(_T("DataBase"), _T("DbForiDefinition"), 
				_T("DbFori.def"));

// Init DataBase Object And Open It
dB.load(strValue);

try{dB.Open(nomeFile);}
catch (CDaoException *e)
	{
	AfxGetMainWnd()->MessageBox (e->m_pErrorInfo->m_strDescription,_T("dBFori.Open"));
	return FALSE;
	}

CDbFori dBFori(&dB);

// win10 check if db is open
if (!dBFori.isDbOpen())
	return FALSE;

CString sqlString;
sqlString.Format (_T("SELECT * FROM %s "),
				  dBFori.GetDefaultSQL());


try
	{dBFori.Open (dbOpenDynaset,sqlString);} 
catch
	(CDaoException *e)
	{// record non esiste
	
	e->Delete();
	return TRUE;
	}

for (int i=0;i<c_difCoil.GetSize();i++)
	{
	double pos;
	// Posizione a posteriori (valore 10 indica da 0 a 10)
	pos =  c_difCoil[i].posizione; // posizione del metro dif
	// NOT trepassing total length
	if (pos >= getVTotalLength())
		break;
	int numItem = 0;
	// nel metro ogni scheda
	for (int j=0;j<c_difCoil[i].GetSize();j++)
		{
		for (int k=0;k<c_difCoil[i].ElementAt(j).GetSize();k++)
			{
			int v = c_difCoil[i].ElementAt(j).ElementAt(k);
			if (v > 0)
				{// Bande cominciano da 1 non da zero
				//sItem.Format ("M.%7.1lf,%3d,%c,%3d   ",
				//	pos,v,'A'+k,pDoc->c_difCoil[i].ElementAt(j).banda+1);
				
				dBFori.AddNew();
				dBFori.m_NOME_ASSE = rotolo;
				dBFori.m_POSIZIONE = pos;
				dBFori.m_CLASSE = TCHAR('A'+k);
				dBFori.m_NUM_FORI = v;
				dBFori.m_BANDA = c_difCoil[i].ElementAt(j).banda+1;
				dBFori.Update();
				}
			}
	   	}
	}
	

dBFori.Close();
dB.Close();

return TRUE;
}

void CLineDoc::appendSysMsg(int evento)
{

if (!systemOnLine)
	return;

switch(evento )
	{
	case HDE_INSPECTION:
		{
		CString allarmeSistema;
		CString s;
		s = "Abilitazione\r\n";
		allarmeSistema.Format (_T("m. %d: %s"),(int)c_difCoil.getMeter(),s);
		systemAlarm.appendMsg(allarmeSistema);
		}
		break;
	case HDE_CHANGE:
		{
		CString allarmeSistema;
		CString s;
		s = "Cambio\r\n";
		allarmeSistema.Format (_T("m. %d: %s"),(int)c_difCoil.getMeter(),s);
		systemAlarm.appendMsg(allarmeSistema);
		}
		break;
	case HDE_SELFTEST:
		{
		CString allarmeSistema;
		CString s;
		s = "Autotest\r\n";
		allarmeSistema.Format (_T("m. %d: %s"),(int)c_difCoil.getMeter(),s);
		systemAlarm.appendMsg(allarmeSistema);
		}
		break;
	case HDE_HOLD:
		{
		CString allarmeSistema;
		CString s;
		s = "Pausa\r\n";
		allarmeSistema.Format (_T("m. %d: %s"),(int)c_difCoil.getMeter(),s);
		systemAlarm.appendMsg(allarmeSistema);
		}
		break;
	}
systemAlarm.visualizza(AfxGetMainWnd(),secAlarmSound);
return;
}

// ad ogni fine asse
// aggiunge un asse ad infoPlantDb
BOOL CLineDoc::updateInfoPlantDb(LPCTSTR nomeFile)
{
CDaoDbCreate dB;
CString strValue;

CProfile profile;
strValue = profile.getProfileString(_T("DataBase"), _T("DbInfoPlantDefinition"), 
				_T("DbInfoPlant.def"));

// Init DataBase Object And Open It
dB.load(strValue);
strValue = profile.getProfileString(_T("DataBase"), _T("DbInfoPlantName"), 
				_T("InfoPlant.mdb"));

try{dB.Open(strValue);}
catch (CDaoException *e)
	{
	AfxGetMainWnd()->MessageBox (e->m_pErrorInfo->m_strDescription,_T("dBInfoPlant.Open"));
	return FALSE;
	}


CDbInfoPlant dBInfoPlant(&dB);
// win10 check if db is open
if (!dBInfoPlant.isDbOpen())
	return FALSE;



CString sqlString;
sqlString.Format (_T("SELECT * FROM %s "),
				  dBInfoPlant.GetDefaultSQL());
	
try
	{dBInfoPlant.Open (dbOpenDynaset,sqlString);} 
catch
	(CDaoException *e)
	{// record non esiste
	
	e->Delete();
	return TRUE;
	}


dBInfoPlant.AddNew();
dBInfoPlant.m_NOME_FILE_TMP = nomeFile;
dBInfoPlant.m_NOME_ASSE_DEF = "";
dBInfoPlant.m_NOME_ASSE_TMP = GetTitle();
dBInfoPlant.m_STATO = FALSE;
dBInfoPlant.m_LEGA = lega;
dBInfoPlant.m_CLIENTE = c_cliente;
dBInfoPlant.m_SPESSORE = spessore;
dBInfoPlant.m_LARGHEZZA = c_larghezza;
dBInfoPlant.Update();
	

dBInfoPlant.Close();
dB.Close();

return TRUE;
}

// Temporizzato apre infoPlant.mdb
// cerca record con stato TRUE
// carica informazioni dell'impianto
// elimina record
BOOL CLineDoc::loadPlantInfoFromDb(void)
{
CDaoDbCreate dB;
CString strValue;

CProfile profile;
strValue = profile.getProfileString(_T("DataBase"), _T("DbInfoPlantDefinition"), 
				_T("DbInfoPlant.def"));

// Init DataBase Object And Open It
dB.load(strValue);
strValue = profile.getProfileString(_T("DataBase"), _T("DbInfoPlantName"), 
				_T("InfoPlant.mdb"));

try{dB.Open(strValue);}
catch (CDaoException *e)
	{
	AfxGetMainWnd()->MessageBox (e->m_pErrorInfo->m_strDescription,_T("dBFori.Open"));
	return FALSE;
	}


CDbInfoPlant dBInfoPlant(&dB);
// win10 check if db is open
if (!dBInfoPlant.isDbOpen())
	return FALSE;

CString sqlString;
sqlString.Format (_T("SELECT * FROM %s WHERE STATO = TRUE"),
				  dBInfoPlant.GetDefaultSQL());
	
try
	{dBInfoPlant.Open (dbOpenDynaset,sqlString);} 
catch
	(CDaoException *e)
	{// record non esiste
	e->Delete();
	return FALSE;
	}

try
	{dBInfoPlant.MoveFirst ();} 
catch
	(CDaoException *e)
	{// record non esiste
	e->Delete();
	dBInfoPlant.Close();
	dB.Close();
	return FALSE;
	}

while(!dBInfoPlant.IsEOF())
	{	
	dBInfoPlant.m_NOME_FILE_TMP.TrimRight();
	dBInfoPlant.m_CLIENTE.TrimRight();
	dBInfoPlant.m_LEGA.TrimRight();
	dBInfoPlant.m_NOME_ASSE_DEF.TrimRight();

	CLineDoc  tmpDoc;
	CFileBpe bpf;
	CString nomeFileTmp;
	nomeFileTmp = dBInfoPlant.m_NOME_FILE_TMP.Left(dBInfoPlant.m_NOME_FILE_TMP.GetLength()-3);
	nomeFileTmp += "csm";
	if(!bpf.Open(nomeFileTmp,CFile::modeRead,NULL))
		{// file non esiste // elimino record
		// ok delete record
		dBInfoPlant.Delete();
		dBInfoPlant.MoveNext();
		continue;
		}

	tmpDoc.load(&bpf);
	bpf.Close();

	tmpDoc.c_cliente = dBInfoPlant.m_CLIENTE;
	tmpDoc.c_larghezza = dBInfoPlant.m_LARGHEZZA;
	tmpDoc.spessore = dBInfoPlant.m_SPESSORE;
	tmpDoc.lega = dBInfoPlant.m_LEGA;
	tmpDoc.rotolo = dBInfoPlant.m_NOME_ASSE_DEF;

	// Save Data  only for new Date
	CFileBpe cf;
	CFileException e;
	CString title;
	CTime st(tmpDoc.startTime);
	title = c_homeDir;
	title += "/";
	title += _T("DD") + st.Format (_T("%m%Y"));

	SECURITY_ATTRIBUTES	 sa;

	sa.nLength= sizeof(SECURITY_ATTRIBUTES);
	sa.lpSecurityDescriptor=NULL;
	sa.bInheritHandle = TRUE;

	::CreateDirectory((LPCTSTR)title,&sa);
	title += _T("/");
	title += dBInfoPlant.m_NOME_ASSE_DEF; 
	title += _T(".csm");

	if(!bpf.Open((LPCTSTR) title, CFile::modeCreate | CFile::modeWrite | CFile::typeBinary, &e ) )
		{
		CString s;
		s.Format(_T("File could not be opened %d"),e.m_cause);
		AfxMessageBox(s);
		}
	else
		{
		tmpDoc.save(&bpf);
		bpf.Close();
		}
	DBOpzioni dbOpzioni(dBase);
	if (dbOpzioni.openSelectDefault ())
		{
		if (dbOpzioni.m_STAMPA_AUTO)
			{
			CLineDoc saveCurDoc;
			// save current document
			saveCurDoc = *this;
			// load tmp document
			*this = tmpDoc;
			// -------------------------
			c_fileName = dBInfoPlant.m_NOME_ASSE_DEF;

			// set data loaded
			loadedData = TRUE;
			
			CInitView *view;
			POSITION pos;
			pos = GetFirstViewPosition();
			view = (CInitView *) GetNextView(pos);
			CCSMApp* pApp = (CCSMApp*)AfxGetApp();
			CMainFrame* pFrame = (CMainFrame *)AfxGetMainWnd();
			int actualView = pApp->pDocTemplate->GetViewID(view);
			pApp->pDocTemplate->ReplaceView(pFrame,0,TRUE);
			pos = GetFirstViewPosition();
			view = (CInitView *) GetNextView(pos);
			
			// Stampa automatica parte solo testo
			view->onlyTextReport = TRUE;
			view->directPrint = TRUE; 
		
	   		view->OnFilePrint();

			view->onlyTextReport = FALSE;
			view->directPrint = FALSE; 	
			// reload previous document
			*this = saveCurDoc;
			pApp->pDocTemplate->ReplaceView(pFrame,actualView,TRUE);
			// Set autoPrinted variable
				}
		dbOpzioni.Close();
		}
	// ok delete record
	dBInfoPlant.Delete();

	dBInfoPlant.MoveNext();
	}	

dBInfoPlant.Close();
dB.Close();

return TRUE;
}

CString CLineDoc::getFileName()
{

return(c_fileName);

}

void CLineDoc::setTitle(LPCTSTR title)
{

c_fileName = title;
CDocument::SetTitle(title);

}

void CLineDoc::OnConfiguraProdotto() 
{
// TODO: Add your command handler code here
CDProduct prod;
DBRicette dbRicette(dBase);
dbRicette.openSelectName (prod.c_nomiRicette);

if (prod.c_nomiRicette.GetSize()<=0)
	{
	CString resMsg;
	resMsg.LoadString (CSM_GRAPHDOC_NORICETTA);
	// AfxMessageBox ("Nessuna ricetta Selezionabile"); 
	AfxMessageBox (resMsg); 
	return;
	}

prod.m_manualNomeAsse = FALSE;


//if (!systemOnLine)
//	prod.c_useComboRicette = TRUE;

if ((prod.DoModal()==IDOK)&&
	(systemOnLine))
	{
/* attivazione solo al prossimo asse
	// update current variable
	lega = prod.m_product;
	spessore = atof((LPCTSTR)prod.m_spessore);
	c_cliente = prod.m_customer;
	c_larghezza = atof((LPCTSTR)prod.m_larghezza);
	// disabilitato manuale
//	c_useManualTitle = prod.m_manualNomeAsse;
//	c_manualTitle = prod.m_nomeAsse;
	c_useManualTitle = FALSE;
	rotolo = prod.m_coil;
	CString s;
//	s = calcBaseFileName(c_tipoAspo,TRUE);
//	rotoloExt = getLastRotoloExt(s);
*/
	UpdateAllViews(NULL);	
	}
}
	
CString CLineDoc::formatStringThreshold()
{
double da,db,dc,dd;
CString s;

da = sogliaA;
db = sogliaB;
dc = sogliaC;
dd = sogliaD;
s = "";
switch(c_difCoil.getNumClassi())
	{
	case 4:
		{
		s.Format(_T(" A=%2.0lf-%2.0lf  B=%2.0lf-%2.0lf  C=%2.0lf-%2.0lf  D>%2.0lf"),
							da,db,db,dc,dc,dd,dd);     
		}
		break;
	case 3:
		{
		s.Format(_T(" A=%2.0lf-%2.0lf  B=%2.0lf-%2.0lf  C>%2.0lf"),
							da,db,db,dc,dc);     
		}
		break;
	case 2:
		{
		s.Format(_T(" A=%2.0lf-%3.0lf  B>%2.0lf"),da,db,db);
		}
		break;
	case 1:
		{
		s.Format(_T(" A=%2.0lf"),da);
		}	
		break;

	default:
		s.Format(_T(" Errore configurazione classi"));

	}

return (s);
}


//-----------------------------------
// Rinomina i file presenti sul disco
// con lo stesso nome fileName
// da filename.xxx a filename#yy.xxx
// dove yy e` numero 
BOOL CLineDoc::renameFile(CString fileName)
{
BOOL found = FALSE;
CString searchFileName;
CFileFind fileSearch;

searchFileName = fileName;

// cerco qs file
int nameModifier = 1;
while (fileSearch.FindFile (searchFileName))
	{// TROVATO
	// modifico nome file inserendo #
	CString ext = fileName.Right(4);
	searchFileName = fileName.Left(fileName.GetLength()-4);
	CString fNameMod;
	fNameMod.Format(_T("#%d"),nameModifier);
	searchFileName += fNameMod;
	searchFileName += ext;
	nameModifier++;
	}

// Ok a qs punto o searchFileName == fileName-> non c'era
// oppure esiste e deve essere modificato con nuovo nome
if (fileName != searchFileName)
	{
	return(MoveFileEx(fileName,searchFileName,MOVEFILE_WRITE_THROUGH));
	}
return TRUE;	
}


void CLineDoc::OnAutotest()
{
// TODO: Add your command handler code here
if (systemOnLine)
	return;

CString s;
s.LoadString(CSM_GRAPHDOC_AUTOTEST);

// if (AfxMessageBox("Send AutoTest command to HDE?",MB_ICONQUESTION | MB_YESNO)== IDNO)
if (AfxMessageBox(s,MB_ICONQUESTION | MB_YESNO)== IDNO)
	return;

#ifdef _DEBUG
// Test
if (systemOnLine)
	{
	appendSysMsg(HDE_SELFTEST);
	UpdateAllViews(NULL);	
	}
//--------------------
#endif

sendAutotest();

}


void CLineDoc::OnUpdateAutotest(CCmdUI *pCmdUI)
{
BOOL enableAutotest;

enableAutotest = !(systemOnLine || serAlarmActive);

pCmdUI->Enable(enableAutotest);

}


void CLineDoc::clearAlarm (void)
{
	// clear stato autotest 
CMainFrame* pFrame = (CMainFrame*)AfxGetMainWnd();

#ifndef CSM10_AMI_REPORT
// cancellati tutti allarmi su invio autotest
pFrame->c_stAllarmi.clearReceiver();
pFrame->c_stAllarmi.okReceiver();
pFrame->c_stAllarmi.okAlarm();
pFrame->c_stAllarmi.okCpu();
#endif
}

