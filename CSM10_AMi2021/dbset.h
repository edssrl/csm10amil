// DBSet.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// DBRicette DAO recordset

class DBRicette : public CDaoRecordset
{
public:
	DBRicette(CDaoDatabase* pDatabase = NULL);
	DECLARE_DYNAMIC(DBRicette)

// Field/Param Data
	//{{AFX_FIELD(DBRicette, CDaoRecordset)
	CString	m_NOME;
	double	m_ALLARME_A;
	double	m_ALLARME_B;
	double	m_ALLARME_C;
	double	m_ALLARME_D;
	double	m_SPESSORE_MIN;
	double	m_SPESSORE_MAX;
	CString	m_LEGA;
	double	m_LENGTH_ALLARME;
	//}}AFX_FIELD

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(DBRicette)
	public:
	virtual CString GetDefaultDBName();		// Default database name
	virtual CString GetDefaultSQL();		// Default SQL for Recordset
	virtual void DoFieldExchange(CDaoFieldExchange* pFX);  // RFX support
	virtual void Close();
	//}}AFX_VIRTUAL

// Implementation
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif
public:
BOOL openSelectDefault (BOOL closeNotFound = TRUE,BOOL addOnEmpty = TRUE);
BOOL openSelectID (CString &key,BOOL closeNotFound = TRUE);
BOOL openSelectName (CArray <CString,CString &> &keyName);
// win10
BOOL isDbOpen(){return ((m_pDatabase != NULL) && m_pDatabase->IsOpen());};
};
/////////////////////////////////////////////////////////////////////////////
// DBOpzioni DAO recordset

class DBOpzioni : public CDaoRecordset
{

public:
	DBOpzioni(CDaoDatabase* pDatabase = NULL);
	~DBOpzioni(void){};

public:
	DECLARE_DYNAMIC(DBOpzioni)

// Field/Param Data
	//{{AFX_FIELD(DBOpzioni, CDaoRecordset)
	CString	m_NOME;
	BOOL	m_ALLARME_ACU;
	double	m_ALLARME_DURATA;
	BOOL	m_STAMPA_AUTO;
	CString	m_TL_CLASSE;
	double	m_TL_LUNGHEZZA;
	double	m_TD_DENSITA;
	double	m_TD_LUNGHEZZA;
	double	m_TD_SCALA;
	double	m_TL_SCALA;
	BOOL	m_MAPPA100;
	//}}AFX_FIELD

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(DBOpzioni)
	public:
	virtual CString GetDefaultDBName();		// Default database name
	virtual CString GetDefaultSQL();		// Default SQL for Recordset
	virtual void DoFieldExchange(CDaoFieldExchange* pFX);  // RFX support
	virtual void Close();
	//}}AFX_VIRTUAL

// Implementation
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif
public:
BOOL openSelectDefault (BOOL closeNotFound = TRUE,BOOL addOnEmpty = TRUE);
// win10
BOOL isDbOpen(){return ((m_pDatabase != NULL) && m_pDatabase->IsOpen());};
};
/////////////////////////////////////////////////////////////////////////////
// DBParametri DAO recordset

class DBParametri : public CDaoRecordset
{

public:
	DBParametri(CDaoDatabase* pDatabase = NULL);
	~DBParametri(void){};

public:
	DECLARE_DYNAMIC(DBParametri)

// Field/Param Data
	//{{AFX_FIELD(DBParametri, CDaoRecordset)
	CString	m_NOME;
	double	m_FCDL;
	double	m_L_RESIDUA_STOP;
	double	m_SOGLIA_A;
	double	m_SOGLIA_B;
	double	m_SOGLIA_C;
	double	m_SOGLIA_D;
	double	m_ALARM_PULSE;
	long	m_ALARM_CLASS;
	//}}AFX_FIELD

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(DBParametri)
	public:
	virtual CString GetDefaultDBName();		// Default database name
	virtual CString GetDefaultSQL();		// Default SQL for Recordset
	virtual void DoFieldExchange(CDaoFieldExchange* pFX);  // RFX support
	virtual void Close();
	//}}AFX_VIRTUAL

// Implementation
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif
public:
BOOL openSelectDefault (BOOL closeNotFound = TRUE,BOOL addOnEmpty = TRUE);
// win10
BOOL isDbOpen(){return ((m_pDatabase != NULL) && m_pDatabase->IsOpen());};
};
/////////////////////////////////////////////////////////////////////////////
// DBReport DAO recordset

class DBReport : public CDaoRecordset
{

public:
	DBReport(CDaoDatabase* pDatabase = NULL);
	~DBReport(void){};
public:
	DECLARE_DYNAMIC(DBReport)

// Field/Param Data
	//{{AFX_FIELD(DBReport, CDaoRecordset)
	CString	m_NOME;
	BOOL	m_DATI_CLIENTE;
	BOOL	m_DATA_LAVORAZIONE;
	BOOL	m_LUNGHEZZA_BOBINA;
	BOOL	m_DESCR_PERIODICI;
	BOOL	m_DESCR_RANDOM;
	BOOL	m_SOGLIE_ALLARME;
	BOOL	m_MAPPA100;
	double	m_DALMETRO;
	double	m_ALMETRO;
	double	m_INTERVALLO1;
	double	m_INTERVALLO2;
	double	m_INTERVALLO3;
	BOOL	m_DESCR_CROSSWEB;
	BOOL	m_DESCR_DOWNWEB;
	BOOL	m_DESCR_THRESHOLD;
	BOOL	m_DESCR_COMPRESSION;
	//}}AFX_FIELD

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(DBReport)
	public:
	virtual CString GetDefaultDBName();		// Default database name
	virtual CString GetDefaultSQL();		// Default SQL for Recordset
	virtual void DoFieldExchange(CDaoFieldExchange* pFX);  // RFX support
	virtual void Close();
	//}}AFX_VIRTUAL

// Implementation
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif
public:
BOOL openSelectDefault (BOOL closeNotFound = TRUE,BOOL addOnEmpty = TRUE);
// win10
BOOL isDbOpen(){return ((m_pDatabase != NULL) && m_pDatabase->IsOpen());};
};

