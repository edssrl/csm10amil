#if !defined(AFX_DCLASSCOLOR_H__BC8FF4FB_8AA1_482D_B7D3_74D93011AA19__INCLUDED_)
#define AFX_DCLASSCOLOR_H__BC8FF4FB_8AA1_482D_B7D3_74D93011AA19__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// DClassColor.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CDClassColor dialog

class CDClassColor : public CDialog
{
// Construction
public:
	CDClassColor(CWnd* pParent = NULL);   // standard constructor

	int	c_numClasses;
// Dialog Data
	//{{AFX_DATA(CDClassColor)
	enum { IDD = IDD_COLOR_DIALOG };
	CString	m_colorA;
	CString	m_colorB;
	CString	m_colorBig;
	CString	m_colorC;
	CString	m_colorD;
	//}}AFX_DATA

	BOOL	saveToProfile(void);
	BOOL	loadFromProfile(void);
	COLORREF Hex2Rgb(CString color);
	int Hex2Int(CString strHex);
	CString Rgb2Hex(COLORREF color);
	COLORREF getRgbColorA(void){return(Hex2Rgb(m_colorA));};
	COLORREF getRgbColorB(void){return(Hex2Rgb(m_colorB));};
	COLORREF getRgbColorC(void){return(Hex2Rgb(m_colorC));};
	COLORREF getRgbColorD(void){return(Hex2Rgb(m_colorD));};
	COLORREF getRgbColorBig(void){return(Hex2Rgb(m_colorBig));};

	int enable(void);
	void setNumClasses(int numClasses){c_numClasses = numClasses;};
// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CDClassColor)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CDClassColor)
	afx_msg void OnButtonColora();
	afx_msg void OnButtonColorb();
	afx_msg void OnButtonColorbig();
	afx_msg void OnButtonColorc();
	afx_msg void OnButtonColord();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
public:
	CButton m_ctrlButtonA;
	CButton m_ctrlButtonB;
	CButton m_ctrlButtonBig;
	CButton m_ctrlButtonC;
	CButton m_ctrlButtonD;
	CEdit m_ctrlColorA;
	CEdit m_ctrlColorB;
	CEdit m_ctrlColorBig;
	CEdit m_ctrlColorC;
	CEdit m_ctrlColorD;
	virtual BOOL OnInitDialog();
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DCLASSCOLOR_H__BC8FF4FB_8AA1_482D_B7D3_74D93011AA19__INCLUDED_)
