#if !defined(AFX_DPRODUCT_H__02A312B1_D462_11D1_A6B2_00C026A019B7__INCLUDED_)
#define AFX_DPRODUCT_H__02A312B1_D462_11D1_A6B2_00C026A019B7__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// DProduct.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CDProduct dialog

class CDProduct : public CDialog
{
// Construction
public:
	CDProduct(CWnd* pParent = NULL);   // standard constructor
	CArray <CString,CString &> c_nomiRicette;
// Dialog Data
	//{{AFX_DATA(CDProduct)
	enum { IDD = IDD_PRODUCT };
	CButton		m_ctrlNomeAsse;
	CComboBox	m_comboICode;
	CString	m_inspCode;
	CString	m_coil;
	CString	m_product;
	CString	m_customer;
	CString	m_nomeAsse;
	int		m_manualNomeAsse;
	CString	m_larghezza;
	CString	m_spessore;
	//}}AFX_DATA

CString title;
BOOL c_numericCoil;
void setTitle (LPCTSTR t) {title = t;};
// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CDProduct)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CDProduct)
	virtual void OnOK();
	virtual BOOL OnInitDialog();
	afx_msg void OnManualNomeAsse();
	afx_msg void OnRadio2();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DPRODUCT_H__02A312B1_D462_11D1_A6B2_00C026A019B7__INCLUDED_)
