//----------------------------------------------------------
//
//			Gestione stato allami
//
//----------------------------------------------------------

#define AL_NMAX_CPU	 24
#define AL_NMAX_RECEIVER 300
#define AL_NMAX_LASER 300

#define AL_OFF	-1	// allarme non definito
#define AL_OK	0	// allarme non attivo
#define AL_ON	1	// allarme attivo	
#define AL_OK1	2	// allarme non attivo	

// Ogni ricevitore e` doppio con due bit
// 4 classi
// quelli pari A/B Ch1
// quelli dispari C/D Ch1
// 2 classi
// quelli pari A/B Ch1
// quelli dispari A/B Ch2 


struct StatoAllarmi
	{
	int alNumCpu;
	int alNumReceiver;
	int alNumLaser;
	int laser;
	int encoder;
	int otturatore1;
	int otturatore2;
	int ftSporco1;
	int ftSporco2;
	int cpu [AL_NMAX_CPU];
	int receiver [AL_NMAX_RECEIVER];
	int laserKO  [AL_NMAX_LASER];

	StatoAllarmi (void)
		{
		setInitState();			
		};
	void setInitState (void)
		{
		alNumCpu = 7;
		alNumReceiver = 112;
		alNumLaser = 112;
		laser = AL_OFF;
		encoder = AL_OFF;
		otturatore1 = AL_OFF;
		otturatore2 = AL_OFF;
		ftSporco1 = AL_OFF;
		ftSporco2 = AL_OFF;
		for (int i=0;i<AL_NMAX_CPU;i++) cpu[i] = AL_OFF;
		for (int i=0;i<AL_NMAX_RECEIVER;i++) 
			{
			receiver[i] = AL_OFF;
			}
		for (int i=0;i<AL_NMAX_LASER;i++) 
			laserKO[i] = AL_OFF;
		};
	void setNumLaser(int n){if (n<=AL_NMAX_LASER) alNumLaser=n;};
	void setNumCpu(int n){if (n<=AL_NMAX_CPU) alNumCpu=n;};
	void setNumReceiver(int n){if (n<=AL_NMAX_RECEIVER) alNumReceiver=n;};
	void draw (CDC *pdc,CRect rect);
	void clearReceiver(void){memset(receiver,AL_OFF,sizeof(receiver));};
	// only for testing purposes
	void okReceiver(void){memset(receiver,AL_OK,sizeof(receiver));};
	void okAlarm(void){
		laser = AL_OK;
		// escluso encoder
		// encoder = AL_OK;
		otturatore1 = AL_OK;
		otturatore2 = AL_OK;
		ftSporco1 = AL_OK;
		ftSporco2 = AL_OK;};
	void okCpu(void){memset(cpu,AL_OK,sizeof(cpu));};
	//------------------------------
	void setTestAlarm()
	{otturatore1 = AL_ON;};
	// condizione di allarme su una delle sorgenti ( OR logico )
	// in uso per disabilitare F1 15-03-18
	bool isHdeAlarmOn(void)
		{// si considera attivo sia se in allarme sia se non eseguito
		bool alOn = (laser == AL_ON);
		// encoder puo` essere in allarme ma F1 attivo
		// alOn |=	(encoder == AL_ON);
		alOn |=	(otturatore1 == AL_ON);
		alOn |=	(otturatore2 == AL_ON);
		alOn |= (ftSporco1 == AL_ON);
		alOn |= (ftSporco2 == AL_ON);
		for (int i=0;i<alNumCpu;i++) 
			alOn |= (cpu[i] == AL_ON);
		for (int i=0;i<alNumReceiver;i++) 
			{
			alOn |= (receiver[i] != AL_OK);
			}
		for (int i=0;i<alNumLaser;i++)
			alOn |= (laserKO[i] == AL_ON);
		return alOn;
		};
	

	};

