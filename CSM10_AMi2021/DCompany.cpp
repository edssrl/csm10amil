// DCompany.cpp : implementation file
//

#include "stdafx.h"
#include "GraphDoc.h"
#include "GraphView.h"
#include "dyntempl.h"

#include "CSM10_AR.h"
#include "DCompany.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CDCompany dialog


CDCompany::CDCompany(CWnd* pParent /*=NULL*/)
	: CDialog(CDCompany::IDD, pParent)
{
	//{{AFX_DATA_INIT(CDCompany)
	m_name = _T("");
	//}}AFX_DATA_INIT
CProfile profile;
m_name = profile.getProfileString (_T("OPTIONS"),_T("Company"),_T("Company"));
}


void CDCompany::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CDCompany)
	DDX_Text(pDX, IDC_EDIT_NAME, m_name);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CDCompany, CDialog)
	//{{AFX_MSG_MAP(CDCompany)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CDCompany message handlers

void CDCompany::OnOK() 
{
	// TODO: Add extra validation here

CDialog::OnOK();
CProfile profile;
profile.writeProfileString (_T("OPTIONS"),_T("Company"),m_name);

}
