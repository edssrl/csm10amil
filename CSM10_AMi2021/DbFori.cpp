// DbFori.cpp : implementation file
//

#include "stdafx.h"
#include "GraphDoc.h"
#include "GraphView.h"
#include "dyntempl.h"

#include "CSM10_AR.h"
#include "DbFori.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CDbFori

IMPLEMENT_DYNAMIC(CDbFori, CDaoRecordset)

CDbFori::CDbFori(CDaoDatabase* pdb)
	: CDaoRecordset(pdb)
{
	//{{AFX_FIELD_INIT(CDbFori)
	m_BANDA = 0.0;
	m_CLASSE = _T("");
	m_NOME_ASSE = _T("");
	m_NUM_FORI = 0.0;
	m_POSIZIONE = 0.0;
	m_nFields = 5;
	//}}AFX_FIELD_INIT
	m_nDefaultType = dbOpenDynaset;
}


CString CDbFori::GetDefaultDBName()
{
	return _T("C:\\User\\Eds\\hole\\CSM10_ARIL\\HOLE.MDB");
}

CString CDbFori::GetDefaultSQL()
{
	return _T("[REPORT_FORI]");
}

void CDbFori::DoFieldExchange(CDaoFieldExchange* pFX)
{
	//{{AFX_FIELD_MAP(CDbFori)
	pFX->SetFieldType(CDaoFieldExchange::outputColumn);
	DFX_Double(pFX, _T("[BANDA]"), m_BANDA);
	DFX_Text(pFX, _T("[CLASSE]"), m_CLASSE);
	DFX_Text(pFX, _T("[NOME_ASSE]"), m_NOME_ASSE);
	DFX_Double(pFX, _T("[NUM_FORI]"), m_NUM_FORI);
	DFX_Double(pFX, _T("[POSIZIONE]"), m_POSIZIONE);
	//}}AFX_FIELD_MAP
}

/////////////////////////////////////////////////////////////////////////////
// CDbFori diagnostics

#ifdef _DEBUG
void CDbFori::AssertValid() const
{
	CDaoRecordset::AssertValid();
}

void CDbFori::Dump(CDumpContext& dc) const
{
	CDaoRecordset::Dump(dc);
}
#endif //_DEBUG
