// CSMService.cpp : Defines the class behaviors for the application.
//

#include "stdafx.h"
#include "CSMService.h"

#include "MainFrm.h"
#include "CSMServiceDoc.h"
#include "SMService.h"
#include "..\Profile.h"
#include "LogOff.h"
#include "Versione.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CCSMServiceApp

BEGIN_MESSAGE_MAP(CCSMServiceApp, CWinApp)
	//{{AFX_MSG_MAP(CCSMServiceApp)
	ON_COMMAND(ID_APP_ABOUT, OnAppAbout)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG_MAP
	// Standard file based document commands
	ON_COMMAND(ID_FILE_NEW, CWinApp::OnFileNew)
	ON_COMMAND(ID_FILE_OPEN, CWinApp::OnFileOpen)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CCSMServiceApp construction

CCSMServiceApp::CCSMServiceApp()
{
	// TODO: add construction code here,
	// Place all significant initialization in InitInstance
}

/////////////////////////////////////////////////////////////////////////////
// The one and only CCSMServiceApp object

CCSMServiceApp theApp;

/////////////////////////////////////////////////////////////////////////////
// CCSMServiceApp initialization

BOOL CCSMServiceApp::InitInstance()
{

AfxEnableControlContainer();

	// Standard initialization
	// If you are not using these features and wish to reduce the size
	//  of your final executable, you should remove from the following
	//  the specific initialization routines you do not need.


	// Change the registry key under which our settings are stored.
	// TODO: You should modify this string to be something appropriate
	// such as the name of your company or organization.
	//SetRegistryKey(_T("Local AppWizard-Generated Applications"));

	LoadStdProfileSettings();  // Load standard INI file options (including MRU)
	CString resVerIt;
	if (!resVerIt.LoadString(CSM_RESVERDLL))
		{
		AfxMessageBox ("Attenzione Stringa Versione non caricabile");
		return (FALSE);		
		}
	CProfile profile;
	CString dllName;
	dllName = profile.getProfileString("init","LangDll","NONE");
	if (dllName != "NONE")
		{
		if ((hModRes = AfxLoadLibrary((LPCTSTR)dllName)) != NULL)
			{
			hDefRes = AfxGetResourceHandle();
			AfxSetResourceHandle (hModRes);	
			CString s;
			if (!s.LoadString(CSM_RESVERDLL))
				{
				AfxMessageBox ("Attenzione DLL di localizzazione non corretta");
				return (FALSE);		
				}
			if (s != resVerIt)
				{
				AfxMessageBox ("Attenzione Versione DLL di localizzazione non corretta");
				return (FALSE);		
				}
			}
		}

	// Register the application's document templates.  Document templates
	//  serve as the connection between documents, frame windows and views.

	CSingleDocTemplate* pDocTemplate;
	pDocTemplate = new CSingleDocTemplate(
		IDR_MAINFRAME,
		RUNTIME_CLASS(CCSMServiceDoc),
		RUNTIME_CLASS(CMainFrame),       // main SDI frame window
		RUNTIME_CLASS(CSMService));
	AddDocTemplate(pDocTemplate);

	// Parse command line for standard shell commands, DDE, file open
	CCommandLineInfo cmdInfo;
	ParseCommandLine(cmdInfo);

	// Dispatch commands specified on the command line
	if (!ProcessShellCommand(cmdInfo))
		return FALSE;

	// The one and only window has been initialized, so show and update it.
	m_pMainWnd->ShowWindow(SW_SHOW);
	m_pMainWnd->UpdateWindow();

	return TRUE;
}


/////////////////////////////////////////////////////////////////////////////
// CAboutDlg dialog used for App About

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// Dialog Data
	//{{AFX_DATA(CAboutDlg)
	enum { IDD = IDD_ABOUTBOX };
	CString	m_versione;
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CAboutDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	//{{AFX_MSG(CAboutDlg)
		// No message handlers
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
	#ifdef _DEBUG
	TCHAR m = 'd';
#else
	TCHAR m = 'r';
#endif

	//{{AFX_DATA_INIT(CAboutDlg)
	m_versione.Format("CSMService V.%4.02lf%c  %s",VERSIONE,m,DATAVERSIONE);
	//}}AFX_DATA_INIT

	} 

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CAboutDlg)
	DDX_Text(pDX, IDC_VERSIONE, m_versione);
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
	//{{AFX_MSG_MAP(CAboutDlg)
		// No message handlers
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

// App command to run the dialog
void CCSMServiceApp::OnAppAbout()
{
	CAboutDlg aboutDlg;
	aboutDlg.DoModal();
}

/////////////////////////////////////////////////////////////////////////////
// CCSMServiceApp message handlers


int CCSMServiceApp::ExitInstance() 
{
	// TODO: Add your specialized code here and/or call the base class

if (hDefRes != NULL)
	{
	AfxSetResourceHandle (hDefRes);	
	AfxFreeLibrary (hModRes);
	}
	
	
	
CProfile profile;
BOOL logOff;
logOff = profile.getProfileBool("init","LogOffOnExit",FALSE);

if (logOff)
	{
	// logOff no shutdown, no reboot
	LogOff(FALSE,FALSE);
	}

	
	return CWinApp::ExitInstance();
}
