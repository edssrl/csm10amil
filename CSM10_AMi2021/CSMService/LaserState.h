#if !defined(AFX_LASERSTATE_H__F5C6A740_C461_11D5_AAFB_00C026A019B7__INCLUDED_)
#define AFX_LASERSTATE_H__F5C6A740_C461_11D5_AAFB_00C026A019B7__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// LaserState.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CLaserState window

#include <vector>
using namespace std;

#define LASER_UNK		0
#define LASER_OK		1
#define LASER_SLOW		2	 
#define LASER_FAIL_A1	3 
#define LASER_FAIL_A2	4 
#define LASER_FAIL_B1	5 

struct LState
{
LState (void)
{code = LASER_UNK;option = 1;value=0;};
int code;
int option;
// aggiunto valore analogico ricevuto
int value;
};

class CLaserState : public CStatic
{
// Construction
public:
	CLaserState();

// Attributes
public:
	vector<LState> c_laserSt;		// stato dei laser 
// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CLaserState)
	//}}AFX_VIRTUAL

// Implementation
public:
	int getNumLaser();
	void setBaseLaserNumber(int base);
	void setLaserNumber(int nl);
	void setLaserSt(int id,int st,int opt=0,int value=0);
	void setAlarmBit(int nb);
	int  getBaseLaserNumber();

	virtual ~CLaserState();

private:
	int c_numLaser;
	int c_numBase;
	int c_alarmBit;

	// Generated message map functions
protected:
	//{{AFX_MSG(CLaserState)
	afx_msg void OnPaint();
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_LASERSTATE_H__F5C6A740_C461_11D5_AAFB_00C026A019B7__INCLUDED_)
