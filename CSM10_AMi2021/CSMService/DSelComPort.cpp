// DSelComPort.cpp : implementation file
//

#include "stdafx.h"
#include "CSMService.h"
#include "DSelComPort.h"
#include "afxdialogex.h"

#include "..\Profile.h"

// CDSelComPort dialog
/////////////////////////////////////////////////////////////////////////////
// CDSelComPort dialog


CDSelComPort::CDSelComPort(CWnd* pParent /*=NULL*/)
	: CDialog(CDSelComPort::IDD, pParent)
{
	//{{AFX_DATA_INIT(CDSelComPort)
	m_SelComPort = _T("");
	//}}AFX_DATA_INIT
	//  m_selSpeed = 0;
	m_selSpeed = _T("");
}


void CDSelComPort::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CDSelComPort)
	DDX_CBString(pDX, IDC_COMBO_SEL_COMPORT, m_SelComPort);
	//}}AFX_DATA_MAP
	//  DDX_CBIndex(pDX, IDC_COMBO_SEL_SPEED, m_selSpeed);
	//  DDV_MinMaxInt(pDX, m_selSpeed, 9600, 19200);
	DDX_CBString(pDX, IDC_COMBO_SEL_SPEED, m_selSpeed);
}


BEGIN_MESSAGE_MAP(CDSelComPort, CDialog)
	//{{AFX_MSG_MAP(CDSelComPort)
		// NOTE: the ClassWizard will add message map macros here
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

int CDSelComPort::loadFromProfile(void)
{
CProfile profile;

m_SelComPort	= profile.getProfileString(_T("SerialPort"), _T("Device"),_T("COM1")); 
m_selSpeed	= profile.getProfileString(_T("SerialPort"), _T("Speed"),_T("9600")); 

	return 0;
}


int CDSelComPort::saveToProfile(void)
{
CProfile profile;
profile.writeProfileString(_T("SerialPort"), _T("Device"),m_SelComPort);
profile.writeProfileString(_T("SerialPort"), _T("Speed"),m_selSpeed); 


	return 0;
}


int CDSelComPort::getSpeed(void)
{
int speed;

speed = atoi ((LPCTSTR)m_selSpeed);
return speed;
}

