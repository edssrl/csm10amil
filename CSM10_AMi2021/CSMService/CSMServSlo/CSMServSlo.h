// CSMServSlo.h : main header file for the CSMSERVSLO DLL
//

#if !defined(AFX_CSMSERVSLO_H__C0B80032_3381_4A7F_8F3A_4FD87B07F158__INCLUDED_)
#define AFX_CSMSERVSLO_H__C0B80032_3381_4A7F_8F3A_4FD87B07F158__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"		// main symbols

/////////////////////////////////////////////////////////////////////////////
// CCSMServSloApp
// See CSMServSlo.cpp for the implementation of this class
//

class CCSMServSloApp : public CWinApp
{
public:
	CCSMServSloApp();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CCSMServSloApp)
	//}}AFX_VIRTUAL

	//{{AFX_MSG(CCSMServSloApp)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CSMSERVSLO_H__C0B80032_3381_4A7F_8F3A_4FD87B07F158__INCLUDED_)
