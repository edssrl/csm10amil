; CLW file contains information for the MFC ClassWizard

[General Info]
Version=1
ClassCount=1
Class1=CCSMServSloApp
LastClass=CCSMServSloApp
NewFileInclude2=#include "CSMServSlo.h"
ResourceCount=6
NewFileInclude1=#include "stdafx.h"
Resource1=IDD_MESSAGGI
Resource2=IDD_SMSERVICE_FORM
Resource3=IDD_ABOUTBOX (Italian (Italy))
Resource4=IDR_MAINFRAME
Resource5=IDR_MAINFRAME (Italian (Italy))
Resource6=IDR_MAINFRAME1 (Italian (Italy))

[CLS:CCSMServSloApp]
Type=0
HeaderFile=CSMServSlo.h
ImplementationFile=CSMServSlo.cpp
Filter=N

[DLG:IDD_MESSAGGI]
Type=1
ControlCount=6
Control1=IDOK,button,1342242817
Control2=IDCANCEL,button,1342242816
Control3=IDC_STATIC,static,1342308352
Control4=IDC_STATIC,static,1342308352
Control5=IDC_FROMCODE,edit,1350631552
Control6=IDC_TOCODE,edit,1350631552

[DLG:IDD_SMSERVICE_FORM]
Type=1
ControlCount=13
Control1=IDC_LIST_BLACKMSG,SysListView32,1350631425
Control2=IDC_MSG_AGGIORNA,button,1342242816
Control3=IDC_TESTSX,button,1342242816
Control4=IDC_TESTDX,button,1342242816
Control5=IDC_LASERDX,static,1342308352
Control6=IDC_MSGSX,static,1342308352
Control7=IDC_MSGDX,static,1342308352
Control8=IDC_STATIC,static,1342308352
Control9=IDC_STATIC,static,1342308352
Control10=IDC_MSG_BBOX,static,1342308353
Control11=IDC_LASERSX,static,1342308352
Control12=IDC_STATIC,button,1342177287
Control13=IDC_LEGEND,static,1342177294

[DLG:IDD_ABOUTBOX (Italian (Italy))]
Type=1
ControlCount=4
Control1=IDC_STATIC,static,1342177283
Control2=IDC_VERSIONE,static,1342308480
Control3=IDC_STATIC,static,1342308352
Control4=IDOK,button,1342373889

[MNU:IDR_MAINFRAME]
Type=1
Command1=ID_FILE_CANCELLAMEM
Command2=ID_FILE_LANGUAGE
Command3=ID_APP_EXIT
Command4=ID_EDIT_CUT
Command5=ID_EDIT_COPY
Command6=ID_VIEW_TOOLBAR
Command7=ID_VIEW_STATUS_BAR
Command8=ID_VISUALIZZA_MESSAGGI
Command9=ID_APP_ABOUT
CommandCount=9

[ACL:IDR_MAINFRAME (Italian (Italy))]
Type=1
Command1=ID_FILE_NEW
Command2=ID_FILE_OPEN
Command3=ID_FILE_SAVE
Command4=ID_EDIT_UNDO
Command5=ID_EDIT_CUT
Command6=ID_EDIT_COPY
Command7=ID_EDIT_PASTE
Command8=ID_EDIT_UNDO
Command9=ID_EDIT_CUT
Command10=ID_EDIT_COPY
Command11=ID_EDIT_PASTE
Command12=ID_NEXT_PANE
Command13=ID_PREV_PANE
CommandCount=13

[TB:IDR_MAINFRAME1 (Italian (Italy))]
Type=1
Command1=ID_FILE_NEW
Command2=ID_FILE_OPEN
Command3=ID_FILE_SAVE
Command4=ID_EDIT_CUT
Command5=ID_EDIT_COPY
Command6=ID_EDIT_PASTE
Command7=ID_FILE_PRINT
Command8=ID_APP_ABOUT
CommandCount=8

[TB:IDR_MAINFRAME (Italian (Italy))]
Type=1
Command1=ID_EDIT_CUT
Command2=ID_EDIT_COPY
Command3=ID_APP_ABOUT
CommandCount=3

