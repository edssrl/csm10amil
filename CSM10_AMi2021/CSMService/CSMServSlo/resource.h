//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by CSMServSlo.rc
//
#define IDD_ABOUTBOX                    100
#define IDD_SMSERVICE_FORM              102
#define IDR_MAINFRAME                   128
#define IDR_MAINFRAME1                  129
#define IDD_MESSAGGI                    130
#define IDB_BITMAP_LEGEND               131
#define IDB_BITMAP_LEGEND2              132
#define IDB_BITMAP_LEGEND1              133
#define IDC_LIST_BLACKMSG               1000
#define IDC_MSG_AGGIORNA                1001
#define IDC_TESTSX                      1003
#define IDC_TESTDX                      1004
#define IDC_LASERDX                     1005
#define IDC_MSGSX                       1006
#define IDC_MSGDX                       1007
#define IDC_MSG_BBOX                    1008
#define IDC_VERSIONE                    1008
#define IDC_FROMCODE                    1009
#define IDC_LASERSX                     1010
#define IDC_TOCODE                      1010
#define IDC_LEGEND                      1012
#define ID_FILE_CANCELLAMEM             32771
#define ID_VISUALIZZA_MESSAGGI          32772
#define ID_FILE_LANGUAGE                32773
#define CSM_RESVERDLL                   44540
#define SMSERVICE_TIMEOUT_SX            44541
#define SMSERVICE_TIMEOUT_DX            44542
#define SMSERVICE_TIMEOUT_MSG           44543
#define SMSERVICE_SEND_FAIL_MSG         44544
#define SMSERVICE_SENDING_MSG           44545
#define SMSERVICE_OK_MSG                44546
#define SMSERVICE_SET_SHIELD_DX         44547
#define SMSERVICE_SEND_FAIL_TEST        44548
#define SMSERVICE_RUNNING_TEST_DX       44549
#define SMSERVICE_SET_SHIELD_SX         44550
#define SMSERVICE_RUNNING_TEST_SX       44551
#define SMSERVICE_BLIND_HOLE            44552
#define SMSERVICE_OK_TEST               44553
#define CSMSERVICE_CLEAR_MSG            44554
#define CSMSERVICE_FAIL_CLEARMEM        44555
#define CSMSERVICE_OK_CLEARMEM          44556
#define IDS_HDLIST_BBOXMSG              61205
#define SMSERVICE_RESTART_LANG          61206
#define SMSERVICE_ITALIAN_LANG          61207

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        4000
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         4000
#define _APS_NEXT_SYMED_VALUE           4000
#endif
#endif
