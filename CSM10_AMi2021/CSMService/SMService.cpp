// SMService.cpp : implementation file
//

#include "stdafx.h"
#include "CSMService.h"

#include "CSMServiceDoc.h"
#include "MainFrm.h"

#include "BlackBox.h"
#include "SMService.h"
#include "..\Profile.h"
#include "DMessaggi.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CSMService

IMPLEMENT_DYNCREATE(CSMService, CFormView)

CSMService::CSMService()
	: CFormView(CSMService::IDD)

{
	//{{AFX_DATA_INIT(CSMService)
	m_msgTestDx = _T("");
	m_msgTestSx = _T("");
	m_msgBBox = _T("");
	//}}AFX_DATA_INIT

// No test
c_sideTest = 0;
c_isPreview = FALSE;

c_leftMarg = 20;		// margine sinistro report in mm
c_rightMarg = 10;	// margine destro report in mm
c_topMarg = 20;		// margine alto report in mm
c_bottomMarg = 15;	// margine basso report in mm

}

CSMService::~CSMService()
{
}

void CSMService::DoDataExchange(CDataExchange* pDX)
{
	CFormView::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CSMService)
	DDX_Control(pDX, IDC_LEGEND, m_ctrlLegend);
	DDX_Control(pDX, IDC_TESTSX, m_testSx);
	DDX_Control(pDX, IDC_TESTDX, m_testDx);
	DDX_Control(pDX, IDC_MSG_AGGIORNA, m_bBoxUpdate);
	DDX_Control(pDX, IDC_LASERDX, m_laserDx);
	DDX_Control(pDX, IDC_LASERSX, m_laserSx);
	DDX_Control(pDX, IDC_LIST_BLACKMSG, m_msgList);
	DDX_Text(pDX, IDC_MSGDX, m_msgTestDx);
	DDX_Text(pDX, IDC_MSGSX, m_msgTestSx);
	DDX_Text(pDX, IDC_MSG_BBOX, m_msgBBox);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CSMService, CFormView)
	//{{AFX_MSG_MAP(CSMService)
	ON_BN_CLICKED(IDC_MSG_AGGIORNA, OnMsgAggiorna)
	ON_COMMAND(ID_EDIT_CUT, OnEditCut)
	ON_COMMAND(ID_EDIT_COPY, OnEditCopy)
	ON_UPDATE_COMMAND_UI(ID_EDIT_COPY, OnUpdateEditCopy)
	ON_UPDATE_COMMAND_UI(ID_EDIT_CUT, OnUpdateEditCut)
	ON_BN_CLICKED(IDC_TESTDX, OnTestdx)
	ON_BN_CLICKED(IDC_TESTSX, OnTestsx)
	ON_WM_TIMER()
	ON_COMMAND(ID_VISUALIZZA_MESSAGGI, OnVisualizzaMessaggi)
	ON_COMMAND(ID_FILE_PRINT, OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_PREVIEW, OnFilePrintPreview)
	//}}AFX_MSG_MAP
	ON_UPDATE_COMMAND_UI(ID_VISUALIZZA_MESSAGGI, &CSMService::OnUpdateVisualizzaMessaggi)
	ON_UPDATE_COMMAND_UI(ID_FILE_CANCELLAMEM, &CSMService::OnUpdateFileCancellamem)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CSMService diagnostics

#ifdef _DEBUG
void CSMService::AssertValid() const
{
	CFormView::AssertValid();
}

void CSMService::Dump(CDumpContext& dc) const
{
	CFormView::Dump(dc);
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CSMService message handlers

void CSMService::OnInitialUpdate() 
{
CFormView::OnInitialUpdate();
	
// TODO: Add your specialized code here and/or call the base class
m_msgList.LoadColumnHeadings(IDS_HDLIST_BBOXMSG);
m_msgList.AdjustColumnWidths();	
m_msgList.modeFullRowSelect(true);

// limiti dei bbmsg visualizzati
CDmessaggi dialog;
dialog.loadFromProfile();
c_fromCode = dialog.m_fromCode;
c_toCode = dialog.m_toCode;

//-------------------------
// Init numero celle
CProfile profile;
int numLaser = profile.getProfileInt("init","NumeroBande",60);

m_laserSx.setLaserNumber(numLaser/2);
m_laserDx.setLaserNumber(numLaser-(numLaser/2));

m_laserSx.setBaseLaserNumber(1);
m_laserDx.setBaseLaserNumber(numLaser/2+1);

int nBitAlarm = profile.getProfileInt("init","AlarmBit",3);


m_laserSx.setAlarmBit(nBitAlarm);
m_laserDx.setAlarmBit(nBitAlarm);

if (nBitAlarm < 3)
	{
	CBitmap* hdb1;
	hdb1 = new CBitmap;
	if (nBitAlarm == 2)
		hdb1->LoadBitmap(IDB_BITMAP_LEGEND2);
	else
		hdb1->LoadBitmap(IDB_BITMAP_LEGEND1);
	HBITMAP  hdb2 = m_ctrlLegend.SetBitmap((HBITMAP)*hdb1);
	if (hdb2 != NULL)
		DeleteObject(hdb2);
	}

if (nBitAlarm != 1)
	m_ctrlLegend.ShowWindow(TRUE);
 
EnableBBox();
}

void CSMService::OnMsgAggiorna() 
{
// Invio msg di richiesta bbox
/* Non implementato
Command cmd;

cmd.cmd = INQ_BLACKBOX;
cmd.size = 0;
cmd.data = NULL;

CMainFrame *frame = (CMainFrame *)AfxGetMainWnd();
if (!frame->SendGeneralCommand(&cmd,(RPCID) frame->c_hcSysId))
	{
	// m_msgBBox = "Invio del comando aggiornamento messaggi FALLITO";
	m_msgBBox.LoadString(SMSERVICE_SEND_FAIL_MSG);
	}
else
	{
	// m_msgBBox = "Aggiornamento messaggi in corso";
	m_msgBBox.LoadString( SMSERVICE_SENDING_MSG);
	m_msgList.DeleteAllItems();
	}

c_sideTest = 1;
CProfile profile;
int sec = profile.getProfileInt("init","TimeoutBlackBox",5);
sec *= 1000;
c_timerId = SetTimer(ID_TIMER_BLACKBOX,sec,NULL);
enableButton(FALSE);

*/
}

BOOL CSMService::addBBMsg(BlackRec bRec,BOOL last)
{

CCSMServiceDoc *pDoc;
pDoc = (CCSMServiceDoc *)GetDocument();
if (pDoc == NULL)
	return FALSE;

BlackMsg bMsg;
bMsg = pDoc->c_blackBox.getMsg(&bRec);

if ((bRec.type >= c_fromCode)&&
	(bRec.type <= c_toCode))
	{
	CString s;
	s.Format("%d",bRec.type);
	int row = m_msgList.GetItemCount();
	m_msgList.AddRow((LPCTSTR)s);
	m_msgList.AddItem(row,1,bMsg.msg);
	m_msgList.AddItem(row,2,bRec.val,"%d");
	}

if (last)
	{
	KillTimer(c_timerId);
	//m_msgBBox = "Aggiornamento messaggi Terminato con successo";
	m_msgBBox.LoadString(SMSERVICE_OK_MSG);
	enableButton(TRUE);
	UpdateData(FALSE);
	Beep(500,300);
	Sleep(200);
	Beep(1000,300);
	}

return TRUE;
}

void CSMService::OnEditCut() 
{
// TODO: Add your command handler code here

m_msgList.DeleteSelection(TRUE,TRUE);
	
}

void CSMService::OnEditCopy() 
{
	// TODO: Add your command handler code here
m_msgList.copySelToClipboard(TRUE);	
}

void CSMService::OnUpdateEditCopy(CCmdUI* pCmdUI) 
{
// TODO: Add your command update UI handler code here
pCmdUI->Enable(m_msgList.GetSelectedCount());	
}

void CSMService::OnUpdateEditCut(CCmdUI* pCmdUI) 
{
// TODO: Add your command update UI handler code here
pCmdUI->Enable(m_msgList.GetSelectedCount());	
}

void CSMService::OnTestdx() 
{
// TODO: Add your control notification handler code here

CProfile profile;
CString s;
// s = profile.getProfileString("Messaggi","OnTestDx","!! ATTENZIONE !! Assicurarsi di aver posizionato lso schermo sulla parte destra della Testa");
s.LoadString(SMSERVICE_SET_SHIELD_DX); 

if (AfxMessageBox((LPCTSTR)s,MB_OKCANCEL | MB_ICONEXCLAMATION)==IDCANCEL)
	return;

// start test Dx
// start test Sx
for(int i=0;i<m_laserDx.getNumLaser();i++)
	{
	m_laserDx.setLaserSt(i,LASER_UNK);
	}
m_laserDx.Invalidate();
// test dx even
c_sideTest = 2;

// Invio msg 
Command cmd;
// char ch;
cmd.cc = VAL_COMMAND;

cmd.nn = DX_OFFLINETEST_CMD;

CMainFrame *frame = (CMainFrame *)AfxGetMainWnd();
if (!frame->sendGeneralCommand(&cmd))
	{
	// m_msgTestDx = "Invio del comando di test FALLITO";
	m_msgTestDx.LoadString(SMSERVICE_SEND_FAIL_TEST);
	}
else
	{
	// m_msgTestDx = "Test Offline lato Destro in corso";
	m_msgTestDx.LoadString(SMSERVICE_RUNNING_TEST_DX);
	}
int sec = profile.getProfileInt("init","TimeoutTestOffline",5);
sec *= 1000;
c_timerId = SetTimer(ID_TIMER_TESTOFFLINE,sec,NULL);
enableButton(FALSE);
UpdateData(FALSE);	
	
}

void CSMService::OnTestsx() 
{
// TODO: Add your control notification handler code here
CProfile profile;
CString s;
// s = profile.getProfileString("Messaggi","OnTestSx","");
s.LoadString(SMSERVICE_SET_SHIELD_SX);

if (AfxMessageBox((LPCTSTR)s,MB_OKCANCEL | MB_ICONEXCLAMATION)==IDCANCEL)
	return;

// start test Sx
for(int i=0;i<m_laserSx.getNumLaser();i++)
	{
	m_laserSx.setLaserSt(i,LASER_UNK);
	}
m_laserSx.Invalidate();
// Invio msg 
Command cmd;
// char ch;
cmd.cc = VAL_COMMAND;
// test sx odd
c_sideTest = 1;

cmd.nn = SX_OFFLINETEST_CMD;

CMainFrame *frame = (CMainFrame *)AfxGetMainWnd();
if (!frame->sendGeneralCommand(&cmd))
	{
	// m_msgTestSx = "Invio del comando di test FALLITO";
	m_msgTestSx.LoadString(SMSERVICE_SEND_FAIL_TEST);
	}
else
	{
	// m_msgTestSx = "Test Offline lato Sinistro in corso";
	m_msgTestSx.LoadString(SMSERVICE_RUNNING_TEST_SX);
	}
int sec = profile.getProfileInt("init","TimeoutTestOffline",5);
sec *= 1000;
c_timerId = SetTimer(ID_TIMER_TESTOFFLINE,sec,NULL);
enableButton(FALSE);
UpdateData(FALSE);	
}

//--------------------------------------------------------
// aggiornamento visualizzazione 
// valori bitmapped non utilizzati in qs applicazione
// VAL_RISAUTOTEST	id = 55		Risultato Autotest 
// Comando:	!cc;mm,n,n,�..,n;	lunghezza = 39 byte 4+3+32 => 2*16 = 32
//
//		Cc[1,2] = 55
//		mm [4,5] = 01,02�..10	numero del modulo da 1 a 10 (1 = lato conettore)
//		n [7] = 0 o 1		0 = ricevitore not OK	1 = ricevitore OK
//		n [9] = 0 o 1 		0 = ricevitore not OK	1 = ricevitore OK 
//		ecc.	numero di celle per ciascun modulo = 16
void CSMService::onTestOffline(int size,int *pCell,int threshold)
{
CProfile profile;
BOOL slowMode = FALSE;
CLaserState* pLaser;
CString *pStatic;

if (c_sideTest <= 0)
	return;

// ricavo fattore moltiplicativo = modulo
int module = *pCell;
// moduli 1-10 index -> 0-9
module --;

// ricavo numero celle dalla dimensione del pacchetto
// tolgo primo valore == numero modulo
int numCells = (size-1);

// avanzo pointer
pCell++;

int laserval[100];
int *pLaserval = laserval;
memset(laserval,0,sizeof(laserval));

// gestione valori
if (threshold >= 0)
	{
	for(int i=0;i<numCells;i++)
		{// sostituisco valori con 0 e 1 dopo confronto con soglia
		pLaserval[i] = pCell[i];
		if (pCell[i] >= threshold)
			pCell[i] = 0;
		else
			pCell[i] = 1;
		}
	}


// il modulo tre lato sx solo  prime otto celle
if ((c_sideTest == 1)&&
	(module == 3))
	numCells = 8;
// il modulo tre lato dx solo ultime otto celle
if ((c_sideTest != 1)&&
	(module == 3))
	{
	numCells = 8;
	// sposto pointer
	pCell += 8;
	pLaserval += 8;
	}

int nBitAlarm = profile.getProfileInt("init","AlarmBit",3);

if (c_sideTest == 1)
	{// sinistra - moduli 3 interi piu` 1/2 del 4 ( 1-8)
	pLaser = &m_laserSx;
	pStatic = &m_msgTestSx;
	}
else
	{// destra - 1/2 del 4 ( 9 - 16) piu` 3 moduli interi  
	pLaser = &m_laserDx;
	pStatic = &m_msgTestDx;
	// offset parte comunque da zero!
	module -= 3;
	}

for(int i=0;i<numCells;i++)
	{
	// qs va bene lato sx
	int idx = module*16+i;
	if (c_sideTest != 1)
		{// lato dx
		// 8+module*16+i
		idx = (module==0) ? i : (8+(module-1)*16 + i);
		}
	switch(*pCell)
		{
		default:
			pLaser->setLaserSt(idx,LASER_UNK,0,pLaserval[i]);
			break;
		case 0 :
			// Ok
			pLaser->setLaserSt(idx,LASER_OK,0,pLaserval[i]);
			break;
		case 1 :	// Bit 0 class A
		case 2 :	// Bit 1 class A
		case 3 :	// Bit 1 class A
		case 8 :	// Bit 2 class B
		case 9 :	// Bit 2 class B
		case 10 :	// Bit 2 class B
		case 11 :	// Bit 2 class B
			// fail
			if (nBitAlarm == 1)					
				{
				pLaser->setLaserSt(idx,LASER_FAIL_A1,0,laserval[i]);
				break;
				}
			if (*pCell & 0x01)
				{
				pLaser->setLaserSt(idx,LASER_FAIL_A1,0,laserval[i]);
				}
			// fail
			if (*pCell & 0x02)
				pLaser->setLaserSt(idx,LASER_FAIL_A2,0,laserval[i]);
	
			if (*pCell & 0x08)
				// fail
				pLaser->setLaserSt(idx,LASER_FAIL_B1,0,laserval[i]);
			break;
		case 0xff:
			// slow rotation
			pLaser->setLaserSt(idx,LASER_SLOW,0,laserval[i]);
			slowMode = TRUE;
			break;
		}
	pCell ++;
	}

if (slowMode)
	{
	// *pStatic = "Foro occluso o bassa velocita`del disco"; 
	CString s;
	s.LoadString(SMSERVICE_BLIND_HOLE);
	*pStatic = (LPCTSTR)s; 
	}
else
	{
	// *pStatic = "Test terminato con successo"; 
	CString s;
	s.LoadString(SMSERVICE_OK_TEST);
	*pStatic = (LPCTSTR)s; 
	}


// sia sx che dx hanno moduli numerati localmente 0-3
if (module >= 3)
	{
	c_sideTest = 0;
	KillTimer(c_timerId);
	enableButton(TRUE);
	pLaser->Invalidate();
	UpdateData(FALSE);
	Beep(500,300);
	Sleep(200);
	Beep(1000,300);
	}
}


void CSMService::OnTimer(UINT nIDEvent) 
{
// TODO: Add your message handler code here and/or call default

if (nIDEvent == ID_TIMER_TESTOFFLINE)
	{
	if (c_sideTest == 1)
		{	
		// m_msgTestSx = "TIMEOUT Test Offline lato Sinistro";	
		m_msgTestSx.LoadString(SMSERVICE_TIMEOUT_SX);	

		}
	else
		{
		//m_msgTestDx = "TIMEOUT Test Offline lato Destro";
		m_msgTestDx.LoadString(SMSERVICE_TIMEOUT_DX);
		}
	c_sideTest = 0;
	enableButton(TRUE);
	}
if (nIDEvent == ID_TIMER_BLACKBOX)
	{
	// m_msgBBox = "TIMEOUT Aggiornamento Messaggi";
	m_msgBBox.LoadString(SMSERVICE_TIMEOUT_MSG);
	enableButton(TRUE);
	}
KillTimer(nIDEvent);
Beep(1500,300);
}

void CSMService::enableButton(BOOL enable)
{

m_testSx.EnableWindow(enable);
m_testDx.EnableWindow(enable);
m_bBoxUpdate.EnableWindow(enable);

UpdateData(FALSE);


}

void CSMService::OnVisualizzaMessaggi() 
{
// TODO: Add your command handler code here
CDmessaggi dialog;

dialog.loadFromProfile();

if (dialog.DoModal() == IDOK)
	{
	dialog.saveToProfile();
	c_fromCode = dialog.m_fromCode;
	c_toCode = dialog.m_toCode;
	}
	
}

//---------------------------------------------------------
//
//			 STAMPA
//
//---------------------------------------------------------

// Sequenza
// OnPreparePrinting
// DoPreparePrinting

// Loop OnPrint -> 1 sola pagina quindi molto facile
// OnEndPrinting
// Init device dependent

void CSMService::OnFilePrint() 
{
// TODO: Add your command handler code here
// TODO: Add your command handler code here
CView::OnFilePrint();	
}

void CSMService::OnFilePrintPreview() 
{
// TODO: Add your command handler code here
c_isPreview = TRUE;
CView::OnFilePrintPreview();	
}


BOOL CSMService::OnPreparePrinting(CPrintInfo* pInfo) 
{
	// TODO: call DoPreparePrinting to invoke the Print dialog box
BOOL retVal = DoPreparePrinting (pInfo);
if (!retVal)
	{
	return(retVal);
	}

// Set max Number page	1
// 
CDC dc;
dc.Attach (pInfo->m_pPD->m_pd.hDC);
// save
CRect r;
r.SetRect(0, 0, 
             dc.GetDeviceCaps(HORZRES), 
             dc.GetDeviceCaps(VERTRES)) ;

r = calcPrintInternalRect(&dc,r);

pInfo->SetMaxPage((UINT) 1);
dc.Detach();
return retVal;	

}


void CSMService::OnBeginPrinting(CDC* pDC, CPrintInfo* pInfo) 
{
// TODO: Add your specialized code here and/or call the base class
CFormView::OnBeginPrinting(pDC, pInfo);
}


void CSMService::OnPrint(CDC* pDC, CPrintInfo* pInfo) 
{
// TODO: Add your specialized code here and/or call the base class
CRect rectBorder;
// GetClientRect
CRect rcBounds = calcPrintInternalRect(pDC,pInfo->m_rectDraw,&rectBorder);

// draw border
CCSMServiceDoc* pDoc = (CCSMServiceDoc* )GetDocument();
CPen p,*op;
p.CreatePen (PS_SOLID,2,RGB(0,0,0));
op = pDC->SelectObject(&p);
pDC->MoveTo(rectBorder.left,rectBorder.top);
pDC->LineTo(rectBorder.right,rectBorder.top);
pDC->LineTo(rectBorder.right,rectBorder.bottom);
pDC->LineTo(rectBorder.left,rectBorder.bottom);
pDC->LineTo(rectBorder.left,rectBorder.top);
pDC->SelectObject (op);


prLimit = rcBounds;

int page = pInfo->m_nCurPage;



CFont fontNormal,*pold;
fontNormal.CreatePointFont (120,"Arial",pDC);
pold = pDC->SelectObject(&fontNormal); 
CString s,strRes;
pDC->SetTextAlign(TA_CENTER | TA_TOP);
//strRes.Format ("Page %d",page);
strRes.LoadString (CSM_GRAPHVIEW_REPORTPAGINA);
s.Format (strRes,page);
CSize pagSize;
pagSize = pDC->GetTextExtent(s);
CPoint point;
// allineato al centro
point.x = rectBorder.left+(rectBorder.right-rectBorder.left)/2;;
point.y = rectBorder.bottom + pagSize.cy;
pDC->TextOut (point.x,point.y,s,s.GetLength()); // Skip Cr/Lf

// data ora allineata a dx
CString formatRes;
formatRes.LoadString (CSM_GRAPHVIEW_DATETIMEFORMAT);
CTime t;
t = CTime::GetCurrentTime();
s = t.Format(formatRes);
pDC->SetTextAlign(TA_RIGHT | TA_TOP);
point.x = rectBorder.right;
point.y = rectBorder.top - 2 * pagSize.cy;
pDC->TextOut (point.x,point.y,s,s.GetLength()); // Skip Cr/Lf

// allineato al centro
//s = "File : ";
// s += pDoc->getFileName();
//pDC->SetTextAlign(TA_CENTER | TA_TOP);
//point.x = rectBorder.left+(rectBorder.right-rectBorder.left)/2;
//point.y = rectBorder.top - 2 * pagSize.cy;
//pDC->TextOut (point.x,point.y,s,s.GetLength()); // Skip Cr/Lf

// Cercafori allineato a sx
s.LoadString (SMSERVICE_REPORT_APP);
// s = "Cercafori - EDS Srl";
pDC->SetTextAlign(TA_LEFT | TA_TOP);
point.x = rectBorder.left;
point.y = rectBorder.top - 2*pagSize.cy;
pDC->TextOut (point.x,point.y,s,s.GetLength()); // Skip Cr/Lf

pDC->SelectObject(pold); 

// resto del report
pDC->SetTextAlign(TA_LEFT | TA_TOP);

reportTest(pDC,rcBounds);

}

void CSMService::OnEndPrinting(CDC* pDC, CPrintInfo* pInfo) 
{
// TODO: Add your specialized code here and/or call the base class
CFormView::OnEndPrinting(pDC, pInfo);
}

void CSMService::OnEndPrintPreview(CDC* pDC, CPrintInfo* pInfo, POINT point, CPreviewView* pView) 
{
// TODO: Add your specialized code here and/or call the base class
CFormView::OnEndPrintPreview(pDC, pInfo, point, pView);
}



CRect CSMService::calcPrintInternalRect(CDC* pdc,CRect rcBounds,CRect* border )
{

CRect rectB;
// GetClientRect

CSize printSize,trueSize;
// in stampa tolgo bordi
// pixel
printSize.cx = pdc->GetDeviceCaps(HORZRES);
printSize.cy = pdc->GetDeviceCaps(VERTRES);

// millimetri
trueSize.cx = pdc->GetDeviceCaps(HORZSIZE);
trueSize.cy = pdc->GetDeviceCaps(VERTSIZE);

// calcolo px per mm
CSize pixel;
pixel.cy = printSize.cy/trueSize.cy;
pixel.cx = printSize.cx/trueSize.cx;

/* Tolgo 2 cm a destra ed 1 a sx */
rcBounds.left += c_leftMarg * pixel.cx;
rcBounds.right -= c_rightMarg * pixel.cx;
/* Tolgo 3 cm in alto e 2 in basso */	
rcBounds.top += c_topMarg * pixel.cy;
rcBounds.bottom -= c_bottomMarg * pixel.cy;

CRect rect (rcBounds);

if (border != NULL)
	*border = rcBounds;

// determino interno per scrittura e grafica
// 5 millimetri in meno
rect.DeflateRect (5 * pixel.cx,5 * pixel.cy);



return(rect);
}



void CSMService::reportTest(CDC* pDC,CRect rcBounds)
{
CCSMServiceDoc* pDoc = (CCSMServiceDoc*) GetDocument();
// TODO: add draw code here
pDC->SaveDC();

// intestazione report
// provo con CPage
CPage*	ps= new CPage(rcBounds,pDC,FALSE,MM_TEXT);

double bTop,bLeft,bRight,bBottom;

bTop = ps->ConvertYToInches(rcBounds.top);
bLeft = ps->ConvertXToInches(rcBounds.left);
bBottom = ps->ConvertYToInches(rcBounds.bottom);
bRight = ps->ConvertXToInches(rcBounds.right);

// Intestazione
//--------------
	TABLEHEADER* pTable1 = NULL;        
	pTable1=new TABLEHEADER;  
	pTable1->SetSkip=TRUE;	// no fill 
	pTable1->PointSize=24;
	pTable1->LineSize=1;    // default shown only for demp purposes
	pTable1->NumPrintLines=1;
	pTable1->UseInches=TRUE;
	pTable1->AutoSize=FALSE;
	pTable1->Border=TRUE;
	pTable1->FillFlag=FILL_LTGRAY;
	pTable1->NumColumns=1;
	pTable1->NumRows = 0;
	pTable1->StartRow=bTop;
	pTable1->StartCol=bLeft;
	pTable1->EndCol=bRight;
	pTable1->HeaderLines=1;
	// Intestazione: 
	//pTable1->ColDesc[0].Init((bRight-bLeft),"OFFLINE TEST REPORT",FILL_NONE);
	CString str;
	str.LoadString(SMSERVICE_REPORT_TITLE);
	pTable1->ColDesc[0].Init((bRight-bLeft),str,FILL_NONE);
	ps->setRealPrint(TRUE);
	ps->Table(pTable1);	


//-----------------------------------------------------------------------------
// Intestazione TopLeft
	TABLEHEADER* pTable2 = NULL;        
	pTable2=new TABLEHEADER;  
	pTable2->SetSkip=TRUE;	// no fill 
	pTable2->PointSize=14;
	pTable2->LineSize=1;    // default shown only for demo purposes
	pTable2->NumPrintLines=1;
	pTable2->UseInches=TRUE;
	pTable2->AutoSize=TRUE;
	pTable2->Border=TRUE;
	pTable2->VLines = FALSE;	//	true draw vertical seperator lines
	pTable2->HLines = TRUE;    // ditto on horizontal lines
	pTable2->FillFlag=FILL_LTGRAY;
	pTable2->NumColumns=1;
	pTable2->NumRows = 0;
	pTable2->StartRow=pTable1->EndRow+0.3;
	pTable2->StartCol=bLeft;
	pTable2->EndCol=bRight;
	pTable2->NoHeader=FALSE;
	pTable2->HeaderLines=1;
	ps->setRealPrint(TRUE);
	// str = "Head #1 Left : ";
	str.LoadString (SMSERVICE_REPORT_HEAD_1_LEFT);
	BOOL failure = 0;
	BOOL slow = 0;
	CString failList;
	BOOL invalid = 0;
	for (int i=0;i<m_laserSx.getNumLaser();i++)
		{
		switch(m_laserSx.c_laserSt[i].code)
			{
			case LASER_OK:
				break;
			case LASER_SLOW:
				// failList = "Blind Hole or disk low speed";
				failList.LoadString(SMSERVICE_BLIND_HOLE);
				slow = 1;
				break;
			case LASER_UNK:
				// failList = " TEST NOT EXECUTED ";
				failList.LoadString(SMSERVICE_TEST_NOT_EXECUTED);
				invalid = 1;
				break;
			default:
				if ((!slow)&&(!invalid))
					{
					CString s;
					s.Format("%3d ",m_laserSx.getBaseLaserNumber()+i);
					failList += s;
					failure ++;
					}
			}
		}
	if (failure||invalid||slow)
		{
		CString s;
		//str += "FAIL";
		s.LoadString(SMSERVICE_FAIL);
		str += s;
		}
	else
		str += "OK";
	pTable2->ColDesc[0].Init((bRight-bLeft),str,FILL_NONE);
	ps->Table(pTable2);

	// Top Left
	// Tabella 
#define CELL_X_ROW	20
	// calcolo numero di righe necessarie
	int numRows = slow ? 1 : (1 + failure/CELL_X_ROW);
	TABLEHEADER* pTable3 = NULL;        
	pTable3=new TABLEHEADER;  
	pTable3->SetSkip=TRUE;	// no fill 
	pTable3->PointSize=11;
	pTable3->LineSize=1;    // default shown only for demp purposes
	pTable3->NumPrintLines=1;
	pTable3->UseInches=TRUE;
	pTable3->AutoSize=FALSE;
	pTable3->Border=TRUE;
	pTable3->VLines = TRUE;	//	true draw vertical seperator lines
	pTable3->HLines = TRUE;    // ditto on horizontal lines
	pTable3->FillFlag=FILL_NONE;
	pTable3->NumColumns=2;
	pTable3->NumRows = numRows;
	pTable3->StartRow= pTable2->EndRow;  // +0.1
	pTable3->StartCol=bLeft;
	pTable3->EndCol=bRight;
	pTable3->NoHeader = FALSE;
	pTable3->HeaderLines=1;
	ps->setRealPrint(TRUE);
	// intestazioni tabella  
	//str = "Total Cells";
	str.LoadString(SMSERVICE_REPORT_TOTAL_CELL);
	pTable3->ColDesc[0].Init((bRight-bLeft)/5,str,FILL_NONE);
	//str = "Cells failure";
	str.LoadString(SMSERVICE_REPORT_CELL_FAILURE);
	pTable3->ColDesc[1].Init((bRight-bLeft)*4/5,str,FILL_NONE);
	ps->Table(pTable3);	

	// valori tabella
	str.Format("%d",m_laserSx.getNumLaser());
	ps->Print(pTable3,0,0,16,TEXT_CENTER,(LPCTSTR)str);
	for (int i=0;i<numRows;i++)
		{
		str = failList.Mid(i*CELL_X_ROW*4,CELL_X_ROW*4);
		ps->Print(pTable3,i,1,16,TEXT_LEFT,(LPCTSTR)str);	
		}
//-----------------------------------------------------------------------------
// Intestazione TopRight
	TABLEHEADER* pTable4 = NULL;        
	pTable4=new TABLEHEADER;  
	pTable4->SetSkip=TRUE;	//  fill 
	pTable4->PointSize=14;
	pTable4->LineSize=1;    // default shown only for demo purposes
	pTable4->NumPrintLines=1;
	pTable4->UseInches=TRUE;
	pTable4->AutoSize=TRUE;
	pTable4->Border=TRUE;
	pTable4->VLines = FALSE;	//	true draw vertical seperator lines
	pTable4->HLines = TRUE;    // ditto on horizontal lines
	pTable4->FillFlag=FILL_LTGRAY;
	pTable4->NumColumns=1;
	pTable4->NumRows = 0;
	pTable4->StartRow=pTable3->EndRow+0.3;
	pTable4->StartCol=bLeft;
	pTable4->EndCol=bRight;
	pTable4->NoHeader=FALSE;
	pTable4->HeaderLines=1;
	ps->setRealPrint(TRUE);
	//str = "Head #1 Right : ";
	str.LoadString (SMSERVICE_REPORT_HEAD_1_RIGHT);
	failure = 0;
	slow = 0;
	failList="";
	invalid=0;
	for (int i=0;i<m_laserDx.getNumLaser();i++)
		{
		switch(m_laserDx.c_laserSt[i].code)
			{
			case LASER_OK:
				break;
			case LASER_SLOW:
				//failList = "Blind Hole or disk low speed";
				failList.LoadString(SMSERVICE_BLIND_HOLE);
				slow = 1;
				break;
			case LASER_UNK:
				//failList = " TEST NOT EXECUTED ";
				failList.LoadString(SMSERVICE_TEST_NOT_EXECUTED);
				invalid = 1;
				break;
			default:
				if ((!slow)&&(!invalid))
					{
					CString s;
					s.Format("%3d ",m_laserDx.getBaseLaserNumber()+i);
					failList += s;
					failure ++;
					}
			}
		}
	if (failure||invalid||slow)
		{
		CString s;
		//str += "FAIL";
		s.LoadString(SMSERVICE_FAIL);
		str += s;
		}
	else
		str += "OK";
	pTable4->ColDesc[0].Init((bRight-bLeft),str,FILL_NONE);
	ps->Table(pTable4);

	// Top Right
	// calcolo numero di righe necessarie
	numRows = slow ? 1 : (1 + failure/CELL_X_ROW);
	// Tabella 
	TABLEHEADER* pTable5 = NULL;        
	pTable5=new TABLEHEADER;  
	pTable5->SetSkip=TRUE;	// no fill 
	pTable5->PointSize=11;
	pTable5->LineSize=1;    // default shown only for demp purposes
	pTable5->NumPrintLines=1;
	pTable5->UseInches=TRUE;
	pTable5->AutoSize=FALSE;
	pTable5->Border=TRUE;
	pTable5->VLines = TRUE;	//	true draw vertical seperator lines
	pTable5->HLines = TRUE;    // ditto on horizontal lines
	pTable5->FillFlag=FILL_NONE;
	pTable5->NumColumns=2;
	pTable5->NumRows = numRows;
	pTable5->StartRow= pTable4->EndRow;  // +0.1
	pTable5->StartCol=bLeft;
	pTable5->EndCol=bRight;
	pTable5->NoHeader = FALSE;
	pTable5->HeaderLines=1;
	ps->setRealPrint(TRUE);
	// intestazioni tabella 
	//str = "Total cells";
	str.LoadString(SMSERVICE_REPORT_TOTAL_CELL);
	pTable5->ColDesc[0].Init((bRight-bLeft)/5,str,FILL_NONE);
	//str = "Cells failure";
	str.LoadString(SMSERVICE_REPORT_CELL_FAILURE);
	pTable5->ColDesc[1].Init((bRight-bLeft)*4/5,str,FILL_NONE);
	ps->Table(pTable5);	

	// valori tabella
	str.Format("%d",m_laserDx.getNumLaser());
	ps->Print(pTable5,0,0,16,TEXT_CENTER,(LPCTSTR)str);
	for (int i=0;i<numRows;i++)
		{
		str = failList.Mid(i*CELL_X_ROW*4,CELL_X_ROW*4);
		ps->Print(pTable5,i,1,16,TEXT_LEFT,(LPCTSTR)str);	
		}

	delete pTable1;
	delete pTable2;
	delete pTable3;
	delete pTable4;
	delete pTable5;

///---------------	

}


// Set / unset visible for black box system
int CSMService::EnableBBox(void)
{

	m_msgList.ShowWindow(SW_HIDE);
	m_bBoxUpdate.ShowWindow(SW_HIDE);


return 0;
}


// disable menu view_messages
void CSMService::OnUpdateVisualizzaMessaggi(CCmdUI *pCmdUI)
{
// TODO: Add your command update UI handler code here

	pCmdUI->Enable(FALSE);
}


void CSMService::OnUpdateFileCancellamem(CCmdUI *pCmdUI)
{

// TODO: Add your command update UI handler code here
pCmdUI->Enable(FALSE);

}
