// BlackBox.h: interface for the CBlackBox class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_BLACKBOX_H__895DE730_C303_11D5_AAF9_00C026A019B7__INCLUDED_)
#define AFX_BLACKBOX_H__895DE730_C303_11D5_AAF9_00C026A019B7__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#pragma warning(disable:4786)
#include <map>
using namespace std;

struct BlackRec
	{
	unsigned char  type ;
	unsigned char  val ;
	time_t		   date;   
	BlackRec(void){type = 0;val = 0;date= -1;};
	};

struct BlackMsg
	{
	int type;
	CString msg;
	};

typedef map<int, BlackMsg, less<int> > BlackMsgMap;


class CBlackBox  
{
public:

BlackMsgMap c_mapMsg;

public:
	BOOL loadMsg(CString fileName);
	BlackMsg getMsg(BlackRec *pRec);
	CBlackBox();
	virtual ~CBlackBox();
};

#endif // !defined(AFX_BLACKBOX_H__895DE730_C303_11D5_AAF9_00C026A019B7__INCLUDED_)
