//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by CSMServEng.rc
//
#define IDD_ABOUTBOX                    100
#define IDD_SMSERVICE_FORM              102
#define IDD_DSELCOMPORT                 104
#define IDR_MAINFRAME                   128
#define IDR_MAINFRAME1                  129
#define IDD_MESSAGGI                    130
#define IDB_BITMAP_LEGEND               131
#define IDB_BITMAP_LEGEND2              132
#define IDB_BITMAP_LEGEND1              133
#define IDD_SEL_COMPORT                 138
#define IDC_LIST_BLACKMSG               1000
#define IDC_MSG_AGGIORNA                1001
#define IDC_TESTSX                      1003
#define IDC_TESTDX                      1004
#define IDC_LASERDX                     1005
#define IDC_MSGSX                       1006
#define IDC_MSGDX                       1007
#define IDC_MSG_BBOX                    1008
#define IDC_VERSIONE                    1008
#define IDC_FROMCODE                    1009
#define IDC_LASERSX                     1010
#define IDC_TOCODE                      1010
#define IDC_LEGEND                      1012
#define IDC_COMBO_SEL_COMPORT           1049
#define IDC_COMBO_SEL_SPEED             1050
#define ID_FILE_CANCELLAMEM             32771
#define ID_VISUALIZZA_MESSAGGI          32772
#define ID_FILE_LANGUAGE                32773
#define CSM_GRAPHVIEW_DATETIMEFORMAT    44472
#define CSM_GRAPHVIEW_REPORTPAGINA      44479
#define SMSERVICE_REPORT_APP            44480
#define CSM_RESVERDLL                   44540
#define SMSERVICE_TIMEOUT_SX            44541
#define SMSERVICE_TIMEOUT_DX            44542
#define SMSERVICE_TIMEOUT_MSG           44543
#define SMSERVICE_SEND_FAIL_MSG         44544
#define SMSERVICE_SENDING_MSG           44545
#define SMSERVICE_OK_MSG                44546
#define SMSERVICE_SET_SHIELD_DX         44547
#define SMSERVICE_SEND_FAIL_TEST        44548
#define SMSERVICE_RUNNING_TEST_DX       44549
#define SMSERVICE_SET_SHIELD_SX         44550
#define SMSERVICE_RUNNING_TEST_SX       44551
#define SMSERVICE_BLIND_HOLE            44552
#define SMSERVICE_OK_TEST               44553
#define CSMSERVICE_CLEAR_MSG            44554
#define CSMSERVICE_FAIL_CLEARMEM        44555
#define CSMSERVICE_OK_CLEARMEM          44556
#define IDS_HDLIST_BBOXMSG              61205
#define SMSERVICE_RESTART_LANG          61206
#define SMSERVICE_ITALIAN_LANG          61207
#define SMSERVICE_REPORT_TITLE          61208
#define SMSERVICE_REPORT_HEAD_1_LEFT    61209
#define SMSERVICE_REPORT_HEAD_1_RIGHT   61210
#define SMSERVICE_REPORT_HEAD_2_LEFT    61211
#define SMSERVICE_REPORT_HEAD_2_RIGHT   61212
#define SMSERVICE_TEST_NOT_EXECUTED     61213
#define SMSERVICE_FAIL                  61214
#define SMSERVICE_REPORT_TOTAL_CELL     61215
#define SMSERVICE_REPORT_CELL_FAILURE   61216

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        4000
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         4000
#define _APS_NEXT_SYMED_VALUE           4000
#endif
#endif
