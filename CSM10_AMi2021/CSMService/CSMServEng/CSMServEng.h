// CSMServEng.h : main header file for the CSMSERVENG DLL
//

#if !defined(AFX_CSMSERVENG_H__9BAAD176_AFE4_42C2_83C6_CF114F3D978E__INCLUDED_)
#define AFX_CSMSERVENG_H__9BAAD176_AFE4_42C2_83C6_CF114F3D978E__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"		// main symbols

/////////////////////////////////////////////////////////////////////////////
// CCSMServEngApp
// See CSMServEng.cpp for the implementation of this class
//

class CCSMServEngApp : public CWinApp
{
public:
	CCSMServEngApp();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CCSMServEngApp)
	//}}AFX_VIRTUAL

	//{{AFX_MSG(CCSMServEngApp)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CSMSERVENG_H__9BAAD176_AFE4_42C2_83C6_CF114F3D978E__INCLUDED_)
