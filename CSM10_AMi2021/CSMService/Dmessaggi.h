#if !defined(AFX_DMESSAGGI_H__9C819DA1_C47F_11D5_AAFB_00C026A019B7__INCLUDED_)
#define AFX_DMESSAGGI_H__9C819DA1_C47F_11D5_AAFB_00C026A019B7__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// Dmessaggi.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CDmessaggi dialog

class CDmessaggi : public CDialog
{
// Construction
public:
	void loadFromProfile();
	void saveToProfile();
	CDmessaggi(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CDmessaggi)
	enum { IDD = IDD_MESSAGGI };
	int		m_fromCode;
	int		m_toCode;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CDmessaggi)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CDmessaggi)
		// NOTE: the ClassWizard will add member functions here
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DMESSAGGI_H__9C819DA1_C47F_11D5_AAFB_00C026A019B7__INCLUDED_)
