// LaserState.cpp : implementation file
//

#include "stdafx.h"
#include "CSMService.h"
#include "LaserState.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CLaserState

CLaserState::CLaserState()
{
// default 24 laser
setLaserNumber(24);
setBaseLaserNumber(0);
setAlarmBit(1);
}

CLaserState::~CLaserState()
{
c_laserSt.clear();
}


BEGIN_MESSAGE_MAP(CLaserState, CStatic)
	//{{AFX_MSG_MAP(CLaserState)
	ON_WM_PAINT()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CLaserState message handlers

void CLaserState::OnPaint() 
{
CPaintDC dc(this); // device context for painting
	
// TODO: Add your message handler code here
CRect r,rl;
GetClientRect(&r);

// Numero cella inizio/fine
CSize sz = dc.GetTextExtent(CString("0"));

// calcolo dimensione x
rl = r;
rl.DeflateRect(3,3,3,3);
// spazio per label asse x
rl.bottom = rl.bottom - sz.cy ;
// spazio contatori
rl.top = rl.top + sz.cy;

int xSize = ((rl.bottom-rl.top)/2)*1;

r.right = r.left + xSize*c_numLaser + 6; // deflate

rl.right = rl.left + xSize;

dc.Draw3dRect(&r,RGB(255,255,255),RGB(64,64,64));
CBrush brushWhite(RGB(255, 255, 255));
CBrush brushGreen(RGB(0, 255, 0));
CBrush brushRed(RGB(255, 0, 0));
CBrush brushBlue(RGB(0,0,255));
CBrush brushGrey(RGB(196, 196, 196));

UINT bm = dc.SetBkMode (TRANSPARENT);
UINT rv; 
rv = dc.SetTextAlign(TA_CENTER | TA_BOTTOM);
COLORREF bcolor = dc.SetBkColor (RGB(196,196,196));

CFont fnt;

for (int i=0;i<c_numLaser;i++)
	{
	CBrush* pOldBrush;
	dc.FillSolidRect(&rl,RGB(196,196,196));

	{
	dc.SetBkMode (OPAQUE);
	dc.SetBkColor(RGB(240,240,240));
	CString s;
	s.Format("%02d",c_laserSt[i].value);
	dc.TextOut(rl.left + (rl.right-rl.left)/2,r.top+sz.cy+1,s);
	dc.SetBkMode (TRANSPARENT);
	}

	switch(c_laserSt[i].code)
		{
		default:
		case LASER_UNK:
// In versione DEBUG disegna sempre i pallini anche appena acceso
// xche` cosi` riesco a debuggare la grafica senza attivare il test
// e faccio + veloce
#ifdef _DEBUG
			{
			pOldBrush = dc.SelectObject(&brushWhite);
			int radius;
			radius = rl.Height()/3;
			CRect rc;
			rc = rl;
			rc.bottom = rc.top + radius;
			if (c_alarmBit > 2)
				{
				dc.Ellipse(&rc);
				}
			rc.top = rc.bottom;
			rc.bottom = rc.top + radius;
			dc.Ellipse(&rc);
			rc.top = rc.bottom;
			rc.bottom = rc.top + radius;
			dc.Ellipse(&rc);
			}
#else
			pOldBrush = dc.SelectObject(&brushWhite);
			dc.Ellipse(&rl);
#endif
			break;
		case LASER_OK:
			pOldBrush = dc.SelectObject(&brushGreen);
			dc.Ellipse(&rl);
			break;
		case LASER_FAIL_A1:
		case LASER_FAIL_A2:
		case LASER_FAIL_B1:
			if (c_laserSt[i].option == 0)
				{
				pOldBrush = dc.SelectObject(&brushRed);
				dc.Ellipse(&rl);
				}
			else
				{
				CRect rc;
				int radius;
				radius = rl.Height()/3;
				rc = rl;
				rc.bottom = rc.top + radius;
				if (c_alarmBit > 2)
					{
					// bit 0
					if (c_laserSt[i].option & 0x01)
						{
						pOldBrush = dc.SelectObject(&brushRed);
						dc.Ellipse(&rc);
						}
					else
						{
						pOldBrush = dc.SelectObject(&brushGreen);
						dc.Ellipse(&rc);
						}
					}
				rc.top = rc.bottom;
				rc.bottom = rc.top + radius;
				// bit 1
				if (c_laserSt[i].option & 0x02)
					{
					pOldBrush = dc.SelectObject(&brushRed);
					dc.Ellipse(&rc);
					}
				else
					{
					pOldBrush = dc.SelectObject(&brushGreen);
					dc.Ellipse(&rc);
					}
				rc.top = rc.bottom;
				rc.bottom = rc.top + radius;
				// bit 3
				if (c_laserSt[i].option & 0x08)
					{
					pOldBrush = dc.SelectObject(&brushRed);
					dc.Ellipse(&rc);
					}
				else
					{
					pOldBrush = dc.SelectObject(&brushGreen);
					dc.Ellipse(&rc);
					}
				}

			break;
		case LASER_SLOW:
			pOldBrush = dc.SelectObject(&brushBlue);
			dc.Ellipse(&rl);
			break;
		}

//	dc.Ellipse(&rl);
	// put back the old objects
	dc.SelectObject(pOldBrush);
	// scritta
	// primo piu` multipli di 8
	if ((i == 0)||
		((i+1)%8 == 0))
		{
		CString s;
		s.Format("%d",c_numBase + i);
		dc.TextOut(rl.left + (rl.right-rl.left)/2,r.bottom-1,s);
		}
	// new pos
	rl.left = rl.right;
	rl.right = rl.left + xSize;
	}
	
// Do not call CStatic::OnPaint() for painting messages
}

void CLaserState::setLaserNumber(int nl)
{
//-----------
LState lState;
lState.code = LASER_UNK;
lState.option = 0; 
// 

c_laserSt.clear();

c_numLaser = nl;
for (int i=0;i<c_numLaser;i++)
	// c_laserSt.push_back(LASER_UNK);
	c_laserSt.push_back(lState);
}

void CLaserState::setLaserSt(int id, int st,int opt,int value)
{
LState lState;
lState.code = st;
lState.option = opt;
lState.value = value;

if (id < c_laserSt.size())
	// c_laserSt[id] = st;
	c_laserSt[id] = lState;
}

void CLaserState::setBaseLaserNumber(int base)
{
c_numBase = base;
}

int CLaserState::getBaseLaserNumber()
{
return(c_numBase);
}

void CLaserState::setAlarmBit(int b)
{
c_alarmBit = b;
}

int CLaserState::getNumLaser()
{
return (c_numLaser);
}
