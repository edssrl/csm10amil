// MainFrm.h : interface of the CMainFrame class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_MAINFRM_H__ABED2568_C21D_11D5_AAF8_00C026A019B7__INCLUDED_)
#define AFX_MAINFRM_H__ABED2568_C21D_11D5_AAF8_00C026A019B7__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "..\Csm10Comm.h"

class CMainFrame : public CFrameWnd, public Csm10Comm
{
	
protected: // create from serialization only
	CMainFrame();
	DECLARE_DYNCREATE(CMainFrame)

// Attributes
public:
	int c_hcSysId;
	int c_ntSysId;
// Operations
public:
	virtual BOOL serverRequest (int cmd,int size,CString dataStr);
	virtual HWND getSafeHwnd(void ){return(GetSafeHwnd());};
// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CMainFrame)
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CMainFrame();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:  // control bar embedded members
	CStatusBar  m_wndStatusBar;
	CToolBar    m_wndToolBar;

	int			c_alarmThreshold;
// Generated message map functions
protected:
	//{{AFX_MSG(CMainFrame)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnRxSerChar();
	afx_msg void OnFileCancellamem();
	afx_msg void OnFileLanguage();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_MAINFRM_H__ABED2568_C21D_11D5_AAF8_00C026A019B7__INCLUDED_)
