; CLW file contains information for the MFC ClassWizard

[General Info]
Version=1
LastClass=CSMService
LastTemplate=CDialog
NewFileInclude1=#include "stdafx.h"
NewFileInclude2=#include "CSMService.h"
LastPage=0

ClassCount=7
Class1=CCSMServiceApp
Class2=CCSMServiceDoc
Class3=CSMService
Class4=CMainFrame

ResourceCount=7
Resource1=IDD_ABOUTBOX
Resource2=IDR_MAINFRAME
Resource3=IDD_SMSERVICE_FORM
Resource4=IDD_MESSAGGI
Resource5=IDR_MAINFRAME1 (Italian (Italy))
Class5=CAboutDlg
Class6=CLaserState
Resource6=IDD_ABOUTBOX (Italian (Italy))
Class7=CDmessaggi
Resource7=IDR_MAINFRAME (Italian (Italy))

[CLS:CCSMServiceApp]
Type=0
HeaderFile=CSMService.h
ImplementationFile=CSMService.cpp
Filter=N
BaseClass=CWinApp
VirtualFilter=AC
LastObject=CCSMServiceApp

[CLS:CCSMServiceDoc]
Type=0
HeaderFile=CSMServiceDoc.h
ImplementationFile=CSMServiceDoc.cpp
Filter=N

[CLS:CMainFrame]
Type=0
HeaderFile=MainFrm.h
ImplementationFile=MainFrm.cpp
Filter=T
BaseClass=CFrameWnd
VirtualFilter=fWC
LastObject=CMainFrame




[CLS:CAboutDlg]
Type=0
HeaderFile=CSMService.cpp
ImplementationFile=CSMService.cpp
Filter=D
BaseClass=CDialog
VirtualFilter=dWC

[DLG:IDD_ABOUTBOX]
Type=1
ControlCount=4
Control1=IDC_STATIC,static,1342177283
Control2=IDC_STATIC,static,1342308352
Control3=IDC_STATIC,static,1342308352
Control4=IDOK,button,1342373889
Class=CAboutDlg

[MNU:IDR_MAINFRAME]
Type=1
Class=CMainFrame
Command3=ID_FILE_SAVE
Command4=ID_FILE_SAVE_AS
Command5=ID_FILE_MRU_FILE1
Command6=ID_APP_EXIT
Command10=ID_EDIT_PASTE
Command11=ID_VIEW_TOOLBAR
Command12=ID_VIEW_STATUS_BAR
Command13=ID_APP_ABOUT
CommandCount=13
Command1=ID_FILE_NEW
Command2=ID_FILE_OPEN
Command7=ID_EDIT_UNDO
Command8=ID_EDIT_CUT
Command9=ID_EDIT_COPY

[ACL:IDR_MAINFRAME]
Type=1
Class=CMainFrame
Command1=ID_FILE_NEW
Command2=ID_FILE_OPEN
Command3=ID_FILE_SAVE
Command5=ID_EDIT_CUT
Command6=ID_EDIT_COPY
Command7=ID_EDIT_PASTE
Command8=ID_EDIT_UNDO
Command9=ID_EDIT_CUT
Command10=ID_EDIT_COPY
Command11=ID_EDIT_PASTE
Command12=ID_NEXT_PANE
CommandCount=13
Command4=ID_EDIT_UNDO
Command13=ID_PREV_PANE


[TB:IDR_MAINFRAME (Italian (Italy))]
Type=1
Class=?
Command1=ID_EDIT_CUT
Command2=ID_EDIT_COPY
Command3=ID_APP_ABOUT
CommandCount=3

[MNU:IDR_MAINFRAME (Italian (Italy))]
Type=1
Class=CMainFrame
Command1=ID_FILE_CANCELLAMEM
Command2=ID_FILE_LANGUAGE
Command3=ID_FILE_PRINT
Command4=ID_FILE_PRINT_PREVIEW
Command5=ID_APP_EXIT
Command6=ID_EDIT_CUT
Command7=ID_EDIT_COPY
Command8=ID_VIEW_TOOLBAR
Command9=ID_VIEW_STATUS_BAR
Command10=ID_VISUALIZZA_MESSAGGI
Command11=ID_APP_ABOUT
CommandCount=11

[ACL:IDR_MAINFRAME (Italian (Italy))]
Type=1
Class=?
Command1=ID_FILE_NEW
Command2=ID_FILE_OPEN
Command3=ID_FILE_SAVE
Command4=ID_EDIT_UNDO
Command5=ID_EDIT_CUT
Command6=ID_EDIT_COPY
Command7=ID_EDIT_PASTE
Command8=ID_EDIT_UNDO
Command9=ID_EDIT_CUT
Command10=ID_EDIT_COPY
Command11=ID_EDIT_PASTE
Command12=ID_NEXT_PANE
Command13=ID_PREV_PANE
CommandCount=13

[DLG:IDD_ABOUTBOX (Italian (Italy))]
Type=1
Class=CAboutDlg
ControlCount=4
Control1=IDC_STATIC,static,1342177283
Control2=IDC_VERSIONE,static,1342308480
Control3=IDC_STATIC,static,1342308352
Control4=IDOK,button,1342373889

[DLG:IDD_SMSERVICE_FORM]
Type=1
Class=CSMService
ControlCount=13
Control1=IDC_LIST_BLACKMSG,SysListView32,1350631425
Control2=IDC_MSG_AGGIORNA,button,1342242816
Control3=IDC_TESTSX,button,1342242816
Control4=IDC_TESTDX,button,1342242816
Control5=IDC_LASERDX,static,1342308352
Control6=IDC_MSGSX,static,1342308352
Control7=IDC_MSGDX,static,1342308352
Control8=IDC_STATIC,static,1342308352
Control9=IDC_STATIC,static,1342308352
Control10=IDC_MSG_BBOX,static,1342308353
Control11=IDC_LASERSX,static,1342308352
Control12=IDC_STATIC,button,1342177287
Control13=IDC_LEGEND,static,1342177294

[CLS:CSMService]
Type=0
HeaderFile=SMService.h
ImplementationFile=SMService.cpp
BaseClass=CFormView
Filter=D
VirtualFilter=VWC
LastObject=CSMService

[CLS:CLaserState]
Type=0
HeaderFile=LaserState.h
ImplementationFile=LaserState.cpp
BaseClass=CStatic
Filter=W
VirtualFilter=WC
LastObject=CLaserState

[TB:IDR_MAINFRAME1 (Italian (Italy))]
Type=1
Class=?
Command1=ID_FILE_NEW
Command2=ID_FILE_OPEN
Command3=ID_FILE_SAVE
Command4=ID_EDIT_CUT
Command5=ID_EDIT_COPY
Command6=ID_EDIT_PASTE
Command7=ID_FILE_PRINT
Command8=ID_APP_ABOUT
CommandCount=8

[DLG:IDD_MESSAGGI]
Type=1
Class=CDmessaggi
ControlCount=6
Control1=IDOK,button,1342242817
Control2=IDCANCEL,button,1342242816
Control3=IDC_STATIC,static,1342308352
Control4=IDC_STATIC,static,1342308352
Control5=IDC_FROMCODE,edit,1350631552
Control6=IDC_TOCODE,edit,1350631552

[CLS:CDmessaggi]
Type=0
HeaderFile=Dmessaggi.h
ImplementationFile=Dmessaggi.cpp
BaseClass=CDialog
Filter=D
VirtualFilter=dWC

