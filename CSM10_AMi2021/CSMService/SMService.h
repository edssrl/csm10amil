#if !defined(AFX_SMSERVICE_H__ABED2574_C21D_11D5_AAF8_00C026A019B7__INCLUDED_)
#define AFX_SMSERVICE_H__ABED2574_C21D_11D5_AAF8_00C026A019B7__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// SMService.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CSMService form view

#ifndef __AFXEXT_H__
#include <afxext.h>
#endif

#include "..\..\..\..\..\..\library\UnicodeListCtrl\ListCtrl.h"
#include "LaserState.h"

#include "..\PrintLibSrc\CPage.h"
#include "..\PrintLibSrc\Dib.h"

#define ID_TIMER_TESTOFFLINE 0xf5
#define ID_TIMER_BLACKBOX 0xf6
 
 
class CSMService : public CFormView
{
protected:
	CSMService();           // protected constructor used by dynamic creation
	DECLARE_DYNCREATE(CSMService)

// Form Data
public:
	//{{AFX_DATA(CSMService)
	enum { IDD = IDD_SMSERVICE_FORM};
	CStatic	m_ctrlLegend;
	CButton	m_testSx;
	CButton	m_testDx;
	CButton	m_bBoxUpdate;
	CLaserState	m_laserDx;
	CLaserState	m_laserSx;
	gxListCtrl	m_msgList;
	CString	m_msgTestDx;
	CString	m_msgTestSx;
	CString	m_msgBBox;
	//}}AFX_DATA

// Attributes
public:

// Operations
public:
	BOOL c_isPreview;
	int c_toCode;
	int c_fromCode;
	void enableButton(BOOL enable);
	UINT c_timerId;
	int c_sideTest;

int c_leftMarg;		// margine sinistro report in mm
int c_rightMarg;	// margine destro report in mm
int c_topMarg;		// margine alto report in mm
int c_bottomMarg;	// margine basso report in mm
// Rect Print
CRect prLimit;

	void onTestOffline(int size,int* data,int threshold=-1);
	void onValTestOffline(int size,int* data);

	BOOL addBBMsg(BlackRec bRec,BOOL last );
	CRect calcPrintInternalRect(CDC* pdc,CRect rcBounds,CRect* border=NULL);
	void reportTest(CDC* pDC,CRect rcBounds);


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSMService)
	public:
	virtual void OnInitialUpdate();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual void OnEndPrinting(CDC* pDC, CPrintInfo* pInfo);
	virtual void OnEndPrintPreview(CDC* pDC, CPrintInfo* pInfo, POINT point, CPreviewView* pView);
	virtual void OnBeginPrinting(CDC* pDC, CPrintInfo* pInfo);
	virtual BOOL OnPreparePrinting(CPrintInfo* pInfo);
	virtual void OnPrint(CDC* pDC, CPrintInfo* pInfo);
	//}}AFX_VIRTUAL

// Implementation
protected:
	virtual ~CSMService();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

	// Generated message map functions
	//{{AFX_MSG(CSMService)
	afx_msg void OnMsgAggiorna();
	afx_msg void OnEditCut();
	afx_msg void OnEditCopy();
	afx_msg void OnUpdateEditCopy(CCmdUI* pCmdUI);
	afx_msg void OnUpdateEditCut(CCmdUI* pCmdUI);
	afx_msg void OnTestdx();
	afx_msg void OnTestsx();
	afx_msg void OnTimer(UINT nIDEvent);
	afx_msg void OnVisualizzaMessaggi();
	afx_msg void OnFilePrint();
	afx_msg void OnFilePrintPreview();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
public:
	// Set / unset visible for black box system
	int EnableBBox(void);
	afx_msg void OnUpdateVisualizzaMessaggi(CCmdUI *pCmdUI);
	afx_msg void OnUpdateFileCancellamem(CCmdUI *pCmdUI);
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SMSERVICE_H__ABED2574_C21D_11D5_AAF8_00C026A019B7__INCLUDED_)
