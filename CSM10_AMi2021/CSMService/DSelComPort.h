#pragma once


// CDSelComPort dialog

class CDSelComPort : public CDialog
{
//	DECLARE_DYNAMIC(CDSelComPort)

public:
	CDSelComPort(CWnd* pParent = NULL);   // standard constructor
//	virtual ~CDSelComPort();
	CString	m_SelComPort;

// Dialog Data
	enum { IDD = IDD_DSELCOMPORT };

	int loadFromProfile(void);
	int saveToProfile(void);
	
	int getSpeed(void);

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	CString m_selSpeed;
	DECLARE_MESSAGE_MAP()
};
