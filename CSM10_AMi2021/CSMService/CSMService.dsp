# Microsoft Developer Studio Project File - Name="CSMService" - Package Owner=<4>
# Microsoft Developer Studio Generated Build File, Format Version 6.00
# ** DO NOT EDIT **

# TARGTYPE "Win32 (x86) Application" 0x0101

CFG=CSMService - Win32 Debug
!MESSAGE This is not a valid makefile. To build this project using NMAKE,
!MESSAGE use the Export Makefile command and run
!MESSAGE 
!MESSAGE NMAKE /f "CSMService.mak".
!MESSAGE 
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "CSMService.mak" CFG="CSMService - Win32 Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "CSMService - Win32 Release" (based on "Win32 (x86) Application")
!MESSAGE "CSMService - Win32 Debug" (based on "Win32 (x86) Application")
!MESSAGE 

# Begin Project
# PROP AllowPerConfigDependencies 0
# PROP Scc_ProjName ""
# PROP Scc_LocalPath ""
CPP=cl.exe
MTL=midl.exe
RSC=rc.exe

!IF  "$(CFG)" == "CSMService - Win32 Release"

# PROP BASE Use_MFC 6
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Release"
# PROP BASE Intermediate_Dir "Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 6
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "Release"
# PROP Intermediate_Dir "Release"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MD /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_AFXDLL" /Yu"stdafx.h" /FD /c
# ADD CPP /nologo /Zp2 /MD /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_AFXDLL" /D "_MBCS" /Yu"stdafx.h" /FD /c
# ADD BASE MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x409 /d "NDEBUG" /d "_AFXDLL"
# ADD RSC /l 0x409 /d "NDEBUG" /d "_AFXDLL"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 /nologo /subsystem:windows /machine:I386
# ADD LINK32 ListCtrl.lib /nologo /subsystem:windows /machine:I386 /libpath:"c:/user/library/ListCtrl/release"

!ELSEIF  "$(CFG)" == "CSMService - Win32 Debug"

# PROP BASE Use_MFC 6
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "Debug"
# PROP BASE Intermediate_Dir "Debug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 6
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "Debug"
# PROP Intermediate_Dir "Debug"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MDd /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_AFXDLL" /Yu"stdafx.h" /FD /GZ /c
# ADD CPP /nologo /Zp2 /MDd /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_AFXDLL" /D "_MBCS" /Yu"stdafx.h" /FD /GZ /c
# ADD BASE MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x409 /d "_DEBUG" /d "_AFXDLL"
# ADD RSC /l 0x409 /d "_DEBUG" /d "_AFXDLL"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 /nologo /subsystem:windows /debug /machine:I386 /pdbtype:sept
# ADD LINK32 ListCtrl.lib /nologo /subsystem:windows /debug /machine:I386 /pdbtype:sept /libpath:"c:/user/library/ListCtrl/debug"

!ENDIF 

# Begin Target

# Name "CSMService - Win32 Release"
# Name "CSMService - Win32 Debug"
# Begin Group "Source Files"

# PROP Default_Filter "cpp;c;cxx;rc;def;r;odl;idl;hpj;bat"
# Begin Source File

SOURCE=.\BlackBox.cpp
# End Source File
# Begin Source File

SOURCE=..\CSM20\PrintLibSrc\CPage.cpp
# End Source File
# Begin Source File

SOURCE=..\CSM20\PrintLibSrc\CPrinter.cpp
# End Source File
# Begin Source File

SOURCE=.\CProfile.cpp
# End Source File
# Begin Source File

SOURCE=.\CSMService.cpp
# End Source File
# Begin Source File

SOURCE=.\CSMService.rc
# End Source File
# Begin Source File

SOURCE=.\CSMServiceDoc.cpp
# End Source File
# Begin Source File

SOURCE=..\CSM20\PrintLibSrc\Dib.cpp
# End Source File
# Begin Source File

SOURCE=.\Dmessaggi.cpp
# End Source File
# Begin Source File

SOURCE=.\LaserState.cpp
# End Source File
# Begin Source File

SOURCE=.\LogOff.cpp
# End Source File
# Begin Source File

SOURCE=.\MainFrm.cpp
# End Source File
# Begin Source File

SOURCE=.\RpcMClient.cpp
# End Source File
# Begin Source File

SOURCE=.\SMService.cpp
# End Source File
# Begin Source File

SOURCE=.\StdAfx.cpp
# ADD CPP /Yc"stdafx.h"
# End Source File
# End Group
# Begin Group "Header Files"

# PROP Default_Filter "h;hpp;hxx;hm;inl"
# Begin Source File

SOURCE=.\BlackBox.h
# End Source File
# Begin Source File

SOURCE=..\CSM20\PrintLibSrc\CPage.h
# End Source File
# Begin Source File

SOURCE=..\CSM20\PrintLibSrc\CPrinter.h
# End Source File
# Begin Source File

SOURCE=.\CProfile.h
# End Source File
# Begin Source File

SOURCE=.\CSMService.h
# End Source File
# Begin Source File

SOURCE=.\CSMServiceDoc.h
# End Source File
# Begin Source File

SOURCE=..\CSM20\PrintLibSrc\Dib.h
# End Source File
# Begin Source File

SOURCE=.\Dmessaggi.h
# End Source File
# Begin Source File

SOURCE=.\LaserState.h
# End Source File
# Begin Source File

SOURCE=..\ListCtrl\ListCtrl.h
# End Source File
# Begin Source File

SOURCE=.\LogOff.h
# End Source File
# Begin Source File

SOURCE=.\MainFrm.h
# End Source File
# Begin Source File

SOURCE=.\Resource.h
# End Source File
# Begin Source File

SOURCE=.\RpcMClient.h
# End Source File
# Begin Source File

SOURCE=.\SMService.h
# End Source File
# Begin Source File

SOURCE=.\StdAfx.h
# End Source File
# Begin Source File

SOURCE=.\System.h
# End Source File
# Begin Source File

SOURCE=.\Versione.h
# End Source File
# End Group
# Begin Group "Resource Files"

# PROP Default_Filter "ico;cur;bmp;dlg;rc2;rct;bin;rgs;gif;jpg;jpeg;jpe"
# Begin Source File

SOURCE=.\res\bitmap1.bmp
# End Source File
# Begin Source File

SOURCE=.\res\bitmap_l.bmp
# End Source File
# Begin Source File

SOURCE=.\res\bmp00001.bmp
# End Source File
# Begin Source File

SOURCE=.\res\CSMService.ico
# End Source File
# Begin Source File

SOURCE=.\res\CSMService.rc2
# End Source File
# Begin Source File

SOURCE=.\res\CSMServiceDoc.ico
# End Source File
# Begin Source File

SOURCE=.\res\legendEng.bmp
# End Source File
# Begin Source File

SOURCE=.\res\legendIta.bmp
# End Source File
# Begin Source File

SOURCE=.\res\mainfram.bmp
# End Source File
# Begin Source File

SOURCE=.\sdi.ico
# End Source File
# Begin Source File

SOURCE=.\res\Toolbar.bmp
# End Source File
# End Group
# Begin Source File

SOURCE=.\ReadMe.txt
# End Source File
# End Target
# End Project
