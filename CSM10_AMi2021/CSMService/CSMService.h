// CSMService.h : main header file for the CSMSERVICE application
//

#if !defined(AFX_CSMSERVICE_H__ABED2564_C21D_11D5_AAF8_00C026A019B7__INCLUDED_)
#define AFX_CSMSERVICE_H__ABED2564_C21D_11D5_AAF8_00C026A019B7__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"       // main symbols

/////////////////////////////////////////////////////////////////////////////
// CCSMServiceApp:
// See CSMService.cpp for the implementation of this class
//

class CCSMServiceApp : public CWinApp
{
HMODULE		hModRes;	// Resource handle modificato 
HMODULE		hDefRes;	// Resource handle originario	

public:
	CCSMServiceApp();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CCSMServiceApp)
	public:
	virtual BOOL InitInstance();
	virtual int ExitInstance();
	//}}AFX_VIRTUAL

// Implementation
	//{{AFX_MSG(CCSMServiceApp)
	afx_msg void OnAppAbout();
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CSMSERVICE_H__ABED2564_C21D_11D5_AAF8_00C026A019B7__INCLUDED_)
