//==============================================================
// File        : Logoff.h
// Generated   : 21 Jun 2001 18:28
//        by   : Fulvio Corradi
// Company     : 
//--------------------------------------------------------------

//#UBLK-BEG-HEADERH
//==============================================================
// Description : 
//--------------------------------------------------------------
// Author      : Fulvio Corradi
// Created     : 23 May 2001
//--------------------------------------------------------------
// Change history : 
//   23 May 2001 (Fulvio Corradi) Initial version generated
//
//==============================================================




/****************************************************************************
    FUNCTION: LogOff (BOOL shutDown,BOOL reBoot)
	
	  Terminazione della sessione di lavoro:
		Logoff: esce e presenta la finestra Login
		Shutdown: esce e spegne il computer
		Reboot: esce, spegne e riattiva il computer


****************************************************************************/
void LogOff (BOOL shutDown = FALSE,BOOL reBoot=FALSE);
