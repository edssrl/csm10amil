// CSMServiceDoc.cpp : implementation of the CCSMServiceDoc class
//

#include "stdafx.h"
#include "CSMService.h"
#include "MainFrm.h"
#include "CSMServiceDoc.h"
#include "..\Profile.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CCSMServiceDoc

IMPLEMENT_DYNCREATE(CCSMServiceDoc, CDocument)

BEGIN_MESSAGE_MAP(CCSMServiceDoc, CDocument)
	//{{AFX_MSG_MAP(CCSMServiceDoc)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CCSMServiceDoc construction/destruction

CCSMServiceDoc::CCSMServiceDoc()
{
	// TODO: add one-time construction code here

}

CCSMServiceDoc::~CCSMServiceDoc()
{
}

BOOL CCSMServiceDoc::OnNewDocument()
{
if (!CDocument::OnNewDocument())
	return FALSE;

// TODO: add reinitialization code here
// (SDI documents will reuse this document)

CProfile profile;
CString s;
s = profile.getProfileString("init","BlackMsgFile","Blackmes");

c_blackBox.loadMsg(s);

return TRUE;
}



/////////////////////////////////////////////////////////////////////////////
// CCSMServiceDoc serialization

void CCSMServiceDoc::Serialize(CArchive& ar)
{
	if (ar.IsStoring())
	{
		// TODO: add storing code here
	}
	else
	{
		// TODO: add loading code here
	}
}

/////////////////////////////////////////////////////////////////////////////
// CCSMServiceDoc diagnostics

#ifdef _DEBUG
void CCSMServiceDoc::AssertValid() const
{
	CDocument::AssertValid();
}

void CCSMServiceDoc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CCSMServiceDoc commands
