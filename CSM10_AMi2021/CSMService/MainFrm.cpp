// MainFrm.cpp : implementation of the CMainFrame class
//

#include "stdafx.h"
#include "CSMService.h"

#include "CSMServicedoc.h"
#include "..\Profile.h"
#include "MainFrm.h"
#include "SMService.h"
#include "DSelComPort.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CMainFrame

IMPLEMENT_DYNCREATE(CMainFrame, CFrameWnd)

BEGIN_MESSAGE_MAP(CMainFrame, CFrameWnd)
	//{{AFX_MSG_MAP(CMainFrame)
	ON_WM_CREATE()
	ON_COMMAND(ID_FILE_CANCELLAMEM, OnFileCancellamem)
	ON_COMMAND(ID_FILE_LANGUAGE, OnFileLanguage)
	ON_COMMAND(ID_SERCHARAVAIL, OnRxSerChar)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

static UINT indicators[] =
{
	ID_SEPARATOR,           // status line indicator
//	ID_INDICATOR_CAPS,
//	ID_INDICATOR_NUM,
//	ID_INDICATOR_SCRL,
};


/*-----------------------------------------------------------------

			SWAPLONG () 		routine swap long format
	
-----------------------------------------------------------------*/

union bytelong
	{
	unsigned char byte [4];
	unsigned long word;
	};

void swaplong (unsigned long *vett,int number)
{
union bytelong local;
int i;

unsigned long *lfrom;
unsigned char *bto;

lfrom = vett;
bto = (unsigned char *) vett;

for (i=0;i<number;i++)
	{
	local.word = *lfrom;     /* carico valore in union */
	
	bto [0] = local.byte[3];	/* swap */
	bto [1] = local.byte[2];
	bto [2] = local.byte[1];
	bto [3] = local.byte[0];
	bto += sizeof (long);	/* next word */
	lfrom ++;
	}
}	





/////////////////////////////////////////////////////////////////////////////
// CMainFrame construction/destruction

CMainFrame::CMainFrame()
{
	// TODO: add member initialization code here
c_alarmThreshold = -1;	
}

CMainFrame::~CMainFrame()
{
}

int CMainFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CFrameWnd::OnCreate(lpCreateStruct) == -1)
		return -1;
	
	if (!m_wndToolBar.CreateEx(this, TBSTYLE_FLAT, WS_CHILD | WS_VISIBLE | CBRS_TOP
		| CBRS_GRIPPER | CBRS_TOOLTIPS | CBRS_FLYBY | CBRS_SIZE_DYNAMIC) ||
		!m_wndToolBar.LoadToolBar(IDR_MAINFRAME))
	{
		TRACE0("Failed to create toolbar\n");
		return -1;      // fail to create
	}

	if (!m_wndStatusBar.Create(this) ||
		!m_wndStatusBar.SetIndicators(indicators,
		  sizeof(indicators)/sizeof(UINT)))
	{
		TRACE0("Failed to create status bar\n");
		return -1;      // fail to create
	}

	// TODO: Delete these three lines if you don't want the toolbar to
	//  be dockable
	m_wndToolBar.EnableDocking(CBRS_ALIGN_ANY);
	EnableDocking(CBRS_ALIGN_ANY);
	DockControlBar(&m_wndToolBar);


// apertura porta seriale
CDSelComPort dialog;
dialog.loadFromProfile(); 

if (openPort(dialog.m_SelComPort,dialog.getSpeed(),0))
	{
	if (getSafeHwnd() != NULL)
		setEventDest(getSafeHwnd(),ID_SERCHARAVAIL);
	}
else
	AfxMessageBox(_T("ERROR: can't open COM port")); 

CProfile profile;
c_alarmThreshold = profile.getProfileInt("init","AlarmThreshold",5);

return 0;
}

BOOL CMainFrame::PreCreateWindow(CREATESTRUCT& cs)
{
	if( !CFrameWnd::PreCreateWindow(cs) )
		return FALSE;
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs

	return TRUE;
}

/////////////////////////////////////////////////////////////////////////////
// CMainFrame diagnostics

#ifdef _DEBUG
void CMainFrame::AssertValid() const
{
	CFrameWnd::AssertValid();
}

void CMainFrame::Dump(CDumpContext& dc) const
{
	CFrameWnd::Dump(dc);
}

#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CMainFrame message handlers



BOOL CMainFrame::serverRequest (int cmd,int size,CString dataStr)
{
// Override this virtual function in CMainFrame
static int numRecord = 0;

int data[100];
int dataIndex = 0;
CString dataStrTmp;
dataStrTmp = dataStr;

// per ogni valore in dataStr estraggo valore da mettere in data
while (dataStrTmp.GetLength() > 0)
	{
	int commaPos = dataStrTmp.Find(_T(","));
	CString v;
	if (commaPos > 0)
		v = dataStrTmp.Left(commaPos);
	else
		v = dataStrTmp;
	data[dataIndex ++ ] = atoi((LPCTSTR)v);
	// tolgo parte analizzata
	dataStrTmp = dataStrTmp.Right(dataStrTmp.GetLength()-(v.GetLength()+1));
	}

size = dataIndex;

CSMService* pView;

pView = (CSMService*) GetActiveView();

switch(cmd)
	{
	case 0:
		break;
/* Non Implementato
	case VAL_BLACKBOX:
		if (size == sizeof(long))
			{
			unsigned long* pl;
			pl = (unsigned long*)pter;
			swaplong(pl,1);
			numRecord = *pl;
			}
		break;
	case VAL_RAWBLACKBOX:
		{
		BlackRec* pbRec;
		pbRec = (BlackRec*)pter;		
		for (int i=0;i<size/sizeof(BlackRec);i++)
			{
			// se utilizzato campo data (long) bisogna swappare
			swaplong((unsigned long*)&(pbRec->date),1);
			numRecord--;
			// last ?
			pView->addBBMsg(*pbRec,!numRecord);
			pbRec ++;
			}
		}
		break;
---------------------------------*/
// il vecchio sistema riceveva info bit a bit 
// ora riceviamo valori da visualizzare 
// e su cui fare test di soglia impostata nel .ini
	case VAL_RISOFFLINETEST:
		{
		pView->onTestOffline(size,data);
		}
		break;
	case VAL_OFFLINETEST:
		{
		pView->onTestOffline(size,data,c_alarmThreshold);
		}
		break;
	default:
		break;
	}


return FALSE;
}


void CMainFrame::OnRxSerChar(void )
{
Csm10Comm::OnRxSerChar();
}

void CMainFrame::OnFileCancellamem() 
{
// TODO: Add your command handler code here
/* Non implementato 
CString s;
s.LoadString(CSMSERVICE_CLEAR_MSG);
// if (AfxMessageBox("Cancellare tutti i messaggi dalla memoria della testa?",
if (AfxMessageBox(s,
			MB_ICONQUESTION | MB_YESNO) == IDNO)
			return;

// start test Dx
// Invio msg 
Command cmd;
cmd.cmd = SET_CLEARBLACKBOX;
cmd.size = 0;
cmd.data = NULL;

if (!SendGeneralCommand(&cmd,(RPCID) c_hcSysId))
	{
	// AfxMessageBox("Errore nella cancellazione della memoria");
	CString s;
	s.LoadString(CSMSERVICE_FAIL_CLEARMEM);
	AfxMessageBox(s);
	}
else
	{
	// AfxMessageBox("Cancellazione eseguita");
	CString s;
	s.LoadString(CSMSERVICE_OK_CLEARMEM);
	AfxMessageBox(s);
	}

-----------------------------------------------------*/	
}

void CMainFrame::OnFileLanguage() 
{
CProfile profile;
		
CString s;
//s = "Utilizzare Lingua Italiana?";
s.LoadString(SMSERVICE_ITALIAN_LANG);

if (AfxMessageBox((LPCTSTR)s ,MB_YESNO | MB_ICONQUESTION) == IDYES)
	{
	profile.writeProfileString("init","LangDll","NONE");
	CString s;
	//s = "Per cambiare la lingua chiudere e riavviare il programma";
	s.LoadString(SMSERVICE_RESTART_LANG);

	AfxMessageBox((LPCTSTR)s,MB_ICONEXCLAMATION);
	return;
	}
	

// TODO: Add your command handler code here
CString szFilter ("dll Files (*.dll)|*.dll|All Files (*.*)|*.*||");

CFileDialog dialog (TRUE,(LPCTSTR) "*.dll",
	(LPCTSTR) "*.dll",0,(LPCTSTR) szFilter);

SetCurrentDirectory(".");

if (dialog.DoModal() == IDOK)
	{
	CString dllName;
	dllName = dialog.GetPathName();
	profile.writeProfileString("init","LangDll",dllName);
	
	CString s;
	//s = "Per cambiare la lingua chiudere e riavviare il programma";
	s.LoadString(SMSERVICE_RESTART_LANG);

	AfxMessageBox((LPCTSTR)s,MB_ICONEXCLAMATION);
	}


}
