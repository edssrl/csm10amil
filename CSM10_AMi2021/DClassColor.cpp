// DClassColor.cpp : implementation file
//

#include "stdafx.h"

#include "GraphDoc.h"
#include "GraphView.h"
#include "dyntempl.h"

#include "CSM10_AR.h"
#include "DClassColor.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CDClassColor dialog


CDClassColor::CDClassColor(CWnd* pParent /*=NULL*/)
	: CDialog(CDClassColor::IDD, pParent)
{
	//{{AFX_DATA_INIT(CDClassColor)
	m_colorA = _T("");
	m_colorB = _T("");
	m_colorBig = _T("");
	m_colorC = _T("");
	m_colorD = _T("");
	//}}AFX_DATA_INIT
}


void CDClassColor::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CDClassColor)
	DDX_Text(pDX, IDC_COLOR_A, m_colorA);
	DDX_Text(pDX, IDC_COLOR_B, m_colorB);
	DDX_Text(pDX, IDC_COLOR_BIG, m_colorBig);
	DDX_Text(pDX, IDC_COLOR_C, m_colorC);
	DDX_Text(pDX, IDC_COLOR_D, m_colorD);
	//}}AFX_DATA_MAP
	DDX_Control(pDX, IDC_BUTTON_COLORA, m_ctrlButtonA);
	DDX_Control(pDX, IDC_BUTTON_COLORB, m_ctrlButtonB);
	DDX_Control(pDX, IDC_BUTTON_COLORBIG, m_ctrlButtonBig);
	DDX_Control(pDX, IDC_BUTTON_COLORC, m_ctrlButtonC);
	DDX_Control(pDX, IDC_BUTTON_COLORD, m_ctrlButtonD);
	DDX_Control(pDX, IDC_COLOR_A, m_ctrlColorA);
	DDX_Control(pDX, IDC_COLOR_B, m_ctrlColorB);
	DDX_Control(pDX, IDC_COLOR_BIG, m_ctrlColorBig);
	DDX_Control(pDX, IDC_COLOR_C, m_ctrlColorC);
	DDX_Control(pDX, IDC_COLOR_D, m_ctrlColorD);
}


BEGIN_MESSAGE_MAP(CDClassColor, CDialog)
	//{{AFX_MSG_MAP(CDClassColor)
	ON_BN_CLICKED(IDC_BUTTON_COLORA, OnButtonColora)
	ON_BN_CLICKED(IDC_BUTTON_COLORB, OnButtonColorb)
	ON_BN_CLICKED(IDC_BUTTON_COLORBIG, OnButtonColorbig)
	ON_BN_CLICKED(IDC_BUTTON_COLORC, OnButtonColorc)
	ON_BN_CLICKED(IDC_BUTTON_COLORD, OnButtonColord)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CDClassColor message handlers

BOOL	CDClassColor::loadFromProfile(void)
{
CProfile profile;

m_colorA = profile.getProfileString (_T("Color"),_T("ClassColorA"),_T("0x00ff00"));
m_colorB = profile.getProfileString (_T("Color"),_T("ClassColorB"),_T("0x0000ff"));
m_colorC = profile.getProfileString (_T("Color"),_T("ClassColorC"),_T("0xff0000"));
m_colorD = profile.getProfileString (_T("Color"),_T("ClassColorD"),_T("0x800000"));
m_colorBig = profile.getProfileString (_T("Color"),_T("ClassColorBig"),_T("0x000080"));

return TRUE;
}


BOOL	CDClassColor::saveToProfile(void)
{
CProfile profile;

profile.writeProfileString (_T("Color"),_T("ClassColorA"),m_colorA);
profile.writeProfileString (_T("Color"),_T("ClassColorB"),m_colorB);
profile.writeProfileString (_T("Color"),_T("ClassColorC"),m_colorC);
profile.writeProfileString (_T("Color"),_T("ClassColorD"),m_colorD);
profile.writeProfileString (_T("Color"),_T("ClassColorBig"),m_colorBig);

return TRUE;
}


void CDClassColor::OnButtonColora() 
{
// TODO: Add your control notification handler code here
UpdateData(TRUE);

CColorDialog dlg;
dlg.m_cc.Flags |= CC_FULLOPEN;
dlg.m_cc.Flags |= CC_RGBINIT;
dlg.m_cc.rgbResult = Hex2Rgb(m_colorA);
//dlg.m_cc.rgbResult = RGB(0,155,0);
if (dlg.DoModal()==IDOK)
	m_colorA = Rgb2Hex(dlg.m_cc.rgbResult); 

UpdateData(FALSE);

}

void CDClassColor::OnButtonColorb() 
{
	// TODO: Add your control notification handler code here
UpdateData(TRUE);

CColorDialog dlg;
dlg.m_cc.Flags |= CC_FULLOPEN;
dlg.m_cc.Flags |= CC_RGBINIT;
dlg.m_cc.rgbResult = Hex2Rgb(m_colorB);
//dlg.m_cc.rgbResult = RGB(0,155,0);
if (dlg.DoModal()==IDOK)
	m_colorB = Rgb2Hex(dlg.m_cc.rgbResult); 

UpdateData(FALSE);
	
}

void CDClassColor::OnButtonColorbig() 
{
	// TODO: Add your control notification handler code here
UpdateData(TRUE);

CColorDialog dlg;
dlg.m_cc.Flags |= CC_FULLOPEN;
dlg.m_cc.Flags |= CC_RGBINIT;
dlg.m_cc.rgbResult = Hex2Rgb(m_colorBig);
//dlg.m_cc.rgbResult = RGB(0,155,0);
if (dlg.DoModal()==IDOK)
	m_colorBig = Rgb2Hex(dlg.m_cc.rgbResult); 

UpdateData(FALSE);
	
}

void CDClassColor::OnButtonColorc() 
{
	// TODO: Add your control notification handler code here
UpdateData(TRUE);

CColorDialog dlg;
dlg.m_cc.Flags |= CC_FULLOPEN;
dlg.m_cc.Flags |= CC_RGBINIT;
dlg.m_cc.rgbResult = Hex2Rgb(m_colorC);
//dlg.m_cc.rgbResult = RGB(0,155,0);
if (dlg.DoModal()==IDOK)
	m_colorC = Rgb2Hex(dlg.m_cc.rgbResult); 

UpdateData(FALSE);
	
}

void CDClassColor::OnButtonColord() 
{
	// TODO: Add your control notification handler code here
UpdateData(TRUE);

CColorDialog dlg;
dlg.m_cc.Flags |= CC_FULLOPEN;
dlg.m_cc.Flags |= CC_RGBINIT;
dlg.m_cc.rgbResult = Hex2Rgb(m_colorD);
//dlg.m_cc.rgbResult = RGB(0,155,0);
if (dlg.DoModal()==IDOK)
	m_colorD = Rgb2Hex(dlg.m_cc.rgbResult); 

UpdateData(FALSE);
	
}

CString CDClassColor::Rgb2Hex(COLORREF color)
{
CString rgbS;
rgbS.Format(_T("0x%02x%02x%02x"),
			GetRValue(color),GetGValue(color),GetBValue(color));
return rgbS;
}

COLORREF CDClassColor::Hex2Rgb(CString color)
{
int r,g,b;
// skip 0x
color = color.Right(color.GetLength()-2);

// get red
r = Hex2Int(color.Left(2));

// get green
color = color.Right(color.GetLength()-2);
g = Hex2Int(color.Left(2));
	
// get blue
color = color.Right(color.GetLength()-2);
b = Hex2Int(color.Left(2));

return (RGB(r,g,b));
}

int CDClassColor::Hex2Int(CString strHex)
{
int v=0;
int expo = 1;
while(strHex.GetLength() > 0)
	{
	TCHAR ch;
	ch = strHex[strHex.GetLength()-1];
	ch = toupper(ch);
	// check if letter
	if (ch >= 'A')
		{
		ch -= 'A';
		ch += 10;
		}
	else
		ch -= '0';
	if ((ch < 0)||(ch > 15))
		return -1;
	v += (ch * expo);
	expo *= 16;
	strHex = strHex.Left(strHex.GetLength()-1);
	}
return v;
}


BOOL CDClassColor::OnInitDialog()
{
CDialog::OnInitDialog();
// TODO:  Add extra initialization here

enable();


return TRUE;  // return TRUE unless you set the focus to a control
// EXCEPTION: OCX Property Pages should return FALSE
}

int CDClassColor::enable(void)
{

switch(c_numClasses)
	{
	case 1:
		this->m_ctrlButtonB.EnableWindow(FALSE);
		this->m_ctrlColorB.EnableWindow(FALSE);
	case 2:
		this->m_ctrlButtonC.EnableWindow(FALSE);
		this->m_ctrlColorC.EnableWindow(FALSE);
	case 3:
		this->m_ctrlButtonD.EnableWindow(FALSE);
		this->m_ctrlColorD.EnableWindow(FALSE);
	}


return 0;
}
