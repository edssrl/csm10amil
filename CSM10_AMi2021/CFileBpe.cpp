//-------------------------------------------------------------------
//
//			  CFileBpe    CFile con gestione compressore BPE
//
//
//-------------------------------------------------------------------


#include "stdafx.h"
#include "CFileBpe.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


// Class Member CFileBpe

/* Return index of character pair in hash table */
/* deleted nodes have count of 1 for hashing */

short CFileBpe::lookup (unsigned char a, unsigned char b)
{
short index;
int lCount;

/* Compute hash key from both characters */
index = (a ^ (b << 5)) & (HASHSIZE-1);
lCount = 0;
/* Search for pair of first empty slot */
while ((left[index] != a || right[index] != b) &&
		count[index] != 0)
	{
	index = (index + 1 ) & (HASHSIZE-1);
	lCount ++;
	if (lCount > HASHSIZE)
		return (-1);

	}

/* Store pair in table */
left  [index] = a;
right [index] = b;
return(index);
}

/* Init compress data struct for every block */
int CFileBpe::initCompress(void)
{
short c;

/* Reset hash table and pair table */
for (c = 0; c < HASHSIZE; c++)
	{
	count [c] = 0;
	}
for (c = 0;c < 256; c++)
	{
	leftCode[c] = (unsigned char)  c;
	rightCode[c] = (unsigned char) 0;
	}
size = 0;
used = 0;
return (BPE32NOERROR);
}
											
/* fill block from input into buffer */
int CFileBpe::writeCompress(const void* cBuf, UINT dim)
{
short index;
unsigned char c;
UINT byteCount = 0;
int retCode;

char *buf;

buf = (char *) cBuf;

retCode = BPE32NOERROR;

/* Read data until full or few unused chars */
while (byteCount < dim)
	{
	if ((size >= BLOCKSIZE) || (used >= MAXCHARS))
		{
		retCode = flushCompress();
		if (retCode != BPE32NOERROR)
			{
			// Cleanup
			initCompress();
			return (retCode);
			}
		}

	c = buf[byteCount++];
	if (size > 0)
		{
		index = lookup(buffer[size-1],c);
		if (index < 0)
			return (BPE32HASHOVERFLOW);
		if (count[index] < 255)
			++count[index];
		}
	buffer[size++] = c;
	/* Use right code to flag data chars found */
	if (!rightCode[c])
		{
		rightCode[c] = 1;
		used ++;
		}
	}

return (retCode);
}

/* Write each pair table and data block to output */
int CFileBpe::flushWriteCompress ()
{
short i, len, c;
unsigned char cBuff;

/* File compress marker */
CFile::Write("BPE01",5);

c = 0;

/* for each character 0-255 */
while (c < 256)
	{
	/* If not pair code, count run of literals */
	if (c == leftCode[c])
		{
		len = 1; c++;

		while (len < 127 && c < 256 && c == leftCode[c])
			{
			len ++; c ++;
			}
		cBuff = (unsigned char) (len+127);
		CFile::Write((void *) &cBuff,sizeof(cBuff));
		len = 0;
		if (c == 256) break;
		}
	else
		{
		len = 0; c++;
		while (len<127 && c < 256 && c != leftCode[c] ||
				len < 125 && c < 254 && c+1 != leftCode [c+1])
			{
			len ++; c++;
			}
		cBuff = (unsigned char)len;
		CFile::Write((void *)&cBuff,sizeof(cBuff));
		c -= len +1;
		}
	/* Write range of pairs to output */
	for (i = 0; i<= len; i++)
		{
		CFile::Write((void *)&leftCode[c],1);
		if (c != leftCode[c])
			{
			CFile::Write((void *)&rightCode[c],1);
			}
		c++;
		}
	}
/* write size bytes and compressed data block */
cBuff = (unsigned char)((size>>8)&0xff);
CFile::Write((void *)&cBuff,1);

cBuff = (unsigned char)(size&0xff);
CFile::Write((void *)&cBuff,1);

CFile::Write((void *)buffer,size);
 
return(BPE32NOERROR);
}

/* Compress to output file */
int CFileBpe::flushCompress (void )
{
unsigned char leftch, rightch;
short code,oldsize;

short index, r, w, best;
short retCode;

/* Compress data block */
code = 256;

/* Compress this block */
for (;;)
	{
	/* Get next unused char for pair code */
	for (code--; code >= 0; code--)
		if ((code == leftCode[code]) && !rightCode[code])
			break;

	/* Must quit if no unused chars left */
	if (code < 0)	break;
	/* Find most frequent pairs of chars */
	for ( best=2, index= 0; index<HASHSIZE; index ++)
		if (count[index] > best)
			{
			best = count[index];
			leftch = left[index];
			rightch = right[index];
			}
	/* Done if no more compression possible */
	if (best < THRESHOLD) break;
		/* Replace pairs in data, adjust pair counts */
	if (size > BLOCKSIZE) 
		return (BPE32INTERNALERROR);
	oldsize = size - 1;
	for (w = 0, r = 0; r < oldsize; r ++)
		{
		if ((buffer[r] == leftch) &&
			(buffer [r+1] == rightch))
			{
			if (r > 0)
				{
				index = lookup(buffer[w-1],leftch);
				if (index < 0)
					return (BPE32HASHOVERFLOW);
				if (count[index] > 1) --count[index];
				index = lookup(buffer[w-1],(unsigned char)code);
				if (index < 0)
					return (BPE32HASHOVERFLOW);
				if (count[index] < 255) ++count[index];
				}
			if (r < oldsize -1)
				{
				index = lookup(rightch,buffer[r+2]);
				if (index < 0)
					return (BPE32HASHOVERFLOW);
				if (count[index] > 1) --count[index];
				index = lookup((unsigned char)code,buffer[r+2]);
				if (index < 0)
					return (BPE32HASHOVERFLOW);
				if (count[index] < 255) ++count[index];
				}

			buffer [w++] = (unsigned char) code;
			r ++; size --;
			}
		else
			buffer [w++] = buffer[r];
		}
	buffer[w] = buffer [r];
	/* Add to pair substitution table */
	leftCode[code] = leftch;
	rightCode[code] = rightch;
	/* Delete pair from hash table */
	index = lookup(leftch,rightch);
	if (index < 0)
		return (BPE32HASHOVERFLOW);
	count[index] = 1;
	}
retCode = flushWriteCompress();
initCompress();
return(retCode);
}


/* Read  */
int CFileBpe::readCompress(void* buf, UINT uDim)
{

if (readIndex == -1)
	{
	int retCode = unpack();
	switch(retCode)
		{
		case BPE32NOERROR : break;
		case BPE32EFILETYPE :
			// Change file open mode
			mode = BPE_NORMAL;
			// Rewind file
			CFile::SeekToBegin();
			return(CFile::Read(buf,uDim));				
			break;
		default :	
			return (0);
		}
	}

char *cBuf = (char *) buf;
int dim = (int ) uDim;
int nByte = 0;
while (nByte < dim)
	{
	if (readIndex < readBuffer.GetSize())
		{
		cBuf [nByte] = readBuffer [readIndex];
		nByte ++;
		readIndex++;
		}
	else
		{
		if (unpack() != BPE32NOERROR)
			break;
		}
	}
return (nByte);
}

int CFileBpe::unpack(void)
{
/* Unpack One Block */

unsigned char left[256], right[256], stack[30];
short int c, count, i, size;
char marker [8];


// Read Block Marker 

int nb = CFile::Read ((void *)marker,5);
if (nb < 5)
	return (BPE32EOF);

marker [5] = '\0';

if (strcmp (marker,"BPE01"))
	{
	return (BPE32EFILETYPE);
	}

CFile::Read ((void *)&count,1);
count &= 0xff;

/* Set left to itself as literal flag */
for (i = 0;i<256;i++)
	left[i] = (unsigned char) i;

/* Read Pair Table */
for (c=0;;)
	{
	/* Skip Range of literal bytes */
	if (count > 127)
		{
		c += count - 127;
		count = 0;
		}
	if (c == 256) break;

	/* Read pairs, skip right, if literal */
	for (i = 0; i <= count; i++, c++)
		{
		nb = CFile::Read ((void *)&left[c],1);
		if (nb < 1)
			return (BPE32INTERNALERROR);

		if (c != left [c])
			{
			nb = CFile::Read ((void *)&right[c],1);
			if (nb < 1)
				return (BPE32INTERNALERROR);
			}
		}
	if (c == 256) break;

	nb = CFile::Read ((void *)&count,1);
	if (nb < 1)
		return (BPE32INTERNALERROR);

	count &= 0xff;
	}

/* Calculate packed data block size */
short size1,size2;
nb = CFile::Read ((void *)&size1,1);
if (nb < 1)
	return (BPE32INTERNALERROR);
size1 &= 0xff;

nb = CFile::Read ((void *)&size2,1);
if (nb < 1)
	return (BPE32INTERNALERROR);
size2 &= 0xff;

size = 256 * size1 + size2;


// Clear buffer di lettura
readBuffer.RemoveAll();
readIndex = 0;

int fillIndex=0;

/* Unpack data Block */
for (i=0;;)
	{
	/* Pop byte from stack or read byte */
	if (i)
		c = stack[--i];
	else
		{
		if (!size--) break;
		nb = CFile::Read ((void *)&c,1);
		if (nb < 1)
			return (BPE32INTERNALERROR);
		c &= 0xff;
		}
	/* Output byte or push pair on stack */
	if (c == left[c])
		{
		// Fill Buffer
		char ch = (char ) c;
		readBuffer.Add(ch);
		fillIndex ++;
		}
	else
		{
		stack[i++] = right[c];
		stack[i++] = left[c];
		}
	}

return (BPE32NOERROR);
}



