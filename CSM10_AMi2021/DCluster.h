#if !defined(AFX_DCLUSTER_H__15AE9323_16B4_4520_AACD_5242D366B8B9__INCLUDED_)
#define AFX_DCLUSTER_H__15AE9323_16B4_4520_AACD_5242D366B8B9__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// DCluster.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CDCluster dialog

class CDCluster : public CDialog
{
// Construction

int c_numClasses;

public:
	CDCluster(CWnd* pParent = NULL);   // standard constructor


void saveToProfile(void);
void loadFromProfile(void);

// Dialog Data
	//{{AFX_DATA(CDCluster)
	enum { IDD = IDD_CLUSTER };
	int		m_distanceA;
	int		m_distanceB;
	int		m_distanceC;
	int		m_distanceD;
	int		m_pinholeA;
	int		m_pinholeB;
	int		m_pinholeC;
	int		m_pinholeD;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CDCluster)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CDCluster)
	afx_msg void OnKillfocusDistanceA();
	afx_msg void OnKillfocusDistanceB();
	afx_msg void OnKillfocusDistanceC();
	afx_msg void OnKillfocusDistanceD();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
public:
	CEdit m_ctrlDistA;
	CEdit m_ctrlDistB;
	CEdit m_ctrlDistC;
	CEdit m_ctrlDistD;
	CEdit m_ctrlPinA;
	CEdit m_ctrlPinB;
	CEdit m_ctrlPinC;
	CEdit m_ctrlPinD;
	void setNumClasses(int numClasses){c_numClasses = numClasses;};
	virtual BOOL OnInitDialog();
	int enable(void);
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DCLUSTER_H__15AE9323_16B4_4520_AACD_5242D366B8B9__INCLUDED_)
