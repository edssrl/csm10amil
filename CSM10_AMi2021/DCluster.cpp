// DCluster.cpp : implementation file
//

#include "stdafx.h"

#include "GraphDoc.h"
#include "GraphView.h"
#include "dyntempl.h"

#include "CSM10_AR.h"
#include "DCluster.h"

#include "profile.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CDCluster dialog


CDCluster::CDCluster(CWnd* pParent /*=NULL*/)
	: CDialog(CDCluster::IDD, pParent)
{
	//{{AFX_DATA_INIT(CDCluster)
	m_distanceA = 0;
	m_distanceB = 0;
	m_distanceC = 0;
	m_distanceD = 0;
	m_pinholeA = 0;
	m_pinholeB = 0;
	m_pinholeC = 0;
	m_pinholeD = 0;
	c_numClasses = 4;
	//}}AFX_DATA_INIT
}


void CDCluster::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CDCluster)
	DDX_Text(pDX, IDC_DISTANCE_A, m_distanceA);
	DDV_MinMaxInt(pDX, m_distanceA, 0, 10000);
	DDX_Text(pDX, IDC_DISTANCE_B, m_distanceB);
	DDV_MinMaxInt(pDX, m_distanceB, 0, 10000);
	DDX_Text(pDX, IDC_DISTANCE_C, m_distanceC);
	DDV_MinMaxInt(pDX, m_distanceC, 0, 10000);
	DDX_Text(pDX, IDC_DISTANCE_D, m_distanceD);
	DDV_MinMaxInt(pDX, m_distanceD, 0, 10000);
	DDX_Text(pDX, IDC_PINHOLE_A, m_pinholeA);
	DDV_MinMaxInt(pDX, m_pinholeA, 0, 1000);
	DDX_Text(pDX, IDC_PINHOLE_B, m_pinholeB);
	DDV_MinMaxInt(pDX, m_pinholeB, 0, 1000);
	DDX_Text(pDX, IDC_PINHOLE_C, m_pinholeC);
	DDV_MinMaxInt(pDX, m_pinholeC, 0, 1000);
	DDX_Text(pDX, IDC_PINHOLE_D, m_pinholeD);
	DDV_MinMaxInt(pDX, m_pinholeD, 0, 1000);
	//}}AFX_DATA_MAP
	DDX_Control(pDX, IDC_DISTANCE_A, m_ctrlDistA);
	DDX_Control(pDX, IDC_DISTANCE_B, m_ctrlDistB);
	DDX_Control(pDX, IDC_DISTANCE_C, m_ctrlDistC);
	DDX_Control(pDX, IDC_DISTANCE_D, m_ctrlDistD);
	DDX_Control(pDX, IDC_PINHOLE_A, m_ctrlPinA);
	DDX_Control(pDX, IDC_PINHOLE_B, m_ctrlPinB);
	DDX_Control(pDX, IDC_PINHOLE_C, m_ctrlPinC);
	DDX_Control(pDX, IDC_PINHOLE_D, m_ctrlPinD);
}


BEGIN_MESSAGE_MAP(CDCluster, CDialog)
	//{{AFX_MSG_MAP(CDCluster)
	ON_EN_KILLFOCUS(IDC_DISTANCE_A, OnKillfocusDistanceA)
	ON_EN_KILLFOCUS(IDC_DISTANCE_B, OnKillfocusDistanceB)
	ON_EN_KILLFOCUS(IDC_DISTANCE_C, OnKillfocusDistanceC)
	ON_EN_KILLFOCUS(IDC_DISTANCE_D, OnKillfocusDistanceD)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CDCluster message handlers


void CDCluster::saveToProfile(void)
{
CProfile profile;
CString s;

// distance A
s.Format(_T("%d"),m_distanceA);
profile.writeProfileString (_T("Cluster"),_T("DistanceA"),s);

// distance B
s.Format(_T("%d"),m_distanceB);
profile.writeProfileString (_T("Cluster"),_T("DistanceB"),s);

// distance C
s.Format(_T("%d"),m_distanceC);
profile.writeProfileString (_T("Cluster"),_T("DistanceC"),s);

// distance D
s.Format(_T("%d"),m_distanceD);
profile.writeProfileString (_T("Cluster"),_T("DistanceD"),s);

// pinhole A
s.Format(_T("%d"),m_pinholeA);
profile.writeProfileString (_T("Cluster"),_T("PinholeA"),s);

// pinhole B
s.Format(_T("%d"),m_pinholeB);
profile.writeProfileString (_T("Cluster"),_T("PinholeB"),s);

// pinhole C
s.Format(_T("%d"),m_pinholeC);
profile.writeProfileString (_T("Cluster"),_T("PinholeC"),s);

// pinhole D
s.Format(_T("%d"),m_pinholeD);
profile.writeProfileString (_T("Cluster"),_T("PinholeD"),s);


}

void CDCluster::loadFromProfile(void)
{
CProfile profile;

// distance A
m_distanceA = profile.getProfileInt (_T("Cluster"),_T("DistanceA"),0);
// distance B
m_distanceB = profile.getProfileInt (_T("Cluster"),_T("DistanceB"),0);
// distance C
m_distanceC = profile.getProfileInt (_T("Cluster"),_T("DistanceC"),0);
// distance D
m_distanceD = profile.getProfileInt (_T("Cluster"),_T("DistanceD"),0);

// pinhole A
m_pinholeA = profile.getProfileInt (_T("Cluster"),_T("PinholeA"),0);
// pinhole B
m_pinholeB = profile.getProfileInt (_T("Cluster"),_T("PinholeB"),0);
// pinhole C
m_pinholeC = profile.getProfileInt (_T("Cluster"),_T("PinholeC"),0);
// pinhole D
m_pinholeD = profile.getProfileInt (_T("Cluster"),_T("PinholeD"),0);

}

void CDCluster::OnKillfocusDistanceA() 
{
UpdateData(TRUE);

m_distanceA /= 4;
m_distanceA *= 4;

UpdateData(FALSE);
	
}

void CDCluster::OnKillfocusDistanceB() 
{
	// TODO: Add your control notification handler code here
UpdateData(TRUE);

m_distanceB /= 4;
m_distanceB *= 4;

UpdateData(FALSE);
	
	
}

void CDCluster::OnKillfocusDistanceC() 
{
	// TODO: Add your control notification handler code here
UpdateData(TRUE);

m_distanceC /= 4;
m_distanceC *= 4;

UpdateData(FALSE);
	
	
}

void CDCluster::OnKillfocusDistanceD() 
{
	// TODO: Add your control notification handler code here
UpdateData(TRUE);

m_distanceD /= 4;
m_distanceD *= 4;

UpdateData(FALSE);
	
}




BOOL CDCluster::OnInitDialog()
{
CDialog::OnInitDialog();
// TODO:  Add extra initialization here

enable();

return TRUE;  // return TRUE unless you set the focus to a control
// EXCEPTION: OCX Property Pages should return FALSE
}


int CDCluster::enable(void)
{

switch(c_numClasses)
	{
	case 1:
		this->m_ctrlDistB.EnableWindow(FALSE);
		this->m_ctrlPinB.EnableWindow(FALSE);
	case 2:
		this->m_ctrlDistC.EnableWindow(FALSE);
		this->m_ctrlPinC.EnableWindow(FALSE);
	case 3:
		this->m_ctrlDistD.EnableWindow(FALSE);
		this->m_ctrlPinD.EnableWindow(FALSE);
	}


return 0;
}
