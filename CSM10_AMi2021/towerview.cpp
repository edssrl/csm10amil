// TowerView.cpp : implementation file
//

#include "stdafx.h"
#include "MainFrm.h"
#include "resource.h"
#include "Profile.h"

#include <AfxTempl.h>

#include "GraphDoc.h"
#include "GraphView.h"
#include "dyntempl.h"
#include "CSM10_AR.h"

#include "TowerView.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif









/////////////////////////////////////////////////////////////////////////////
// CTowerView




IMPLEMENT_DYNCREATE(CTowerView, CView)

CTowerView::CTowerView()
{
vType = TOWERVIEW;

// Init layout Fix Attributes

CProfile profile;
CString s;

layout.fCaption.size = profile.getProfileInt(_T("TowerLayout"),_T("CaptionSize"),120);
layout.fLabel.size =  profile.getProfileInt(_T("TowerLayout"),_T("LabelSize"),100);
layout.fNormal.size =  profile.getProfileInt(_T("TowerLayout"),_T("NormalSize"),110);


s = profile.getProfileString(_T("TowerLayout"),_T("NewLine"),_T("3,6"));
while (s.GetLength() >0)
	{
	CString subS;
	subS = s.SpanExcluding(_T(",;"));
	s = s.Right(s.GetLength()-subS.GetLength()-1);
	int v;
	_stscanf((LPCTSTR)subS,_T("%d"),&v);
	if ((v < MAX_NUMLABEL)&&(v>=0))
		layout.c_nlAfterLabel [v] = 1;
	}

s = profile.getProfileString(_T("TowerLayout"),_T("LabelOrder"),_T("0,1,2,3,4,5,6,7,8,9"));
int k = 0;
while (s.GetLength() >0)
	{
	CString subS;
	subS = s.SpanExcluding(_T(",;"));
	s = s.Right(s.GetLength()-subS.GetLength()-1);
	int v;
	_stscanf((LPCTSTR)subS,_T("%d"),&v);
	if ((v < MAX_NUMLABEL)&&(v>=0))
		layout.c_vOrderLabel [k++] = v;
	if (k>=MAX_NUMLABEL)
		break;
	}

s = profile.getProfileString(_T("TowerLayout"),_T("PixelSepLabel"),
							 _T("10,10,10,10,10,10,10,10,10,10"));
k = 0;
while (s.GetLength() >0)
	{
	CString subS;
	subS = s.SpanExcluding(_T(",;"));
	s = s.Right(s.GetLength()-subS.GetLength()-1);
	int v;
	_stscanf((LPCTSTR)subS,_T("%d"),&v);
	layout.c_pxSepLabel [k++] = v;
	if (k>9)
		break;
	}

// layout.setLabel ("label 1");
layout.setViewType(TOWERVIEW);
layout.setMode (LINEAR);
s = profile.getProfileString(_T("TowerLayout"),_T("SCaption"),
							 _T("CROSS WEB DISTRIBUTION"));
layout.setCaption(s);
for (int i=0;i<MAX_NUMLABEL;i++)
	{
	CString sIndex;
	sIndex.Format (_T("SLabel%d"),i);
	s = profile.getProfileString(_T("TowerLayout"),sIndex,sIndex);
	if (s != sIndex)
		layout.c_sLabel[i] = s;
	}

for (int i=0;(i<4);i++)
	{
	CString s;
	s = TCHAR('A'+i);
	layout.c_cLabel[i] = s;
	layout.c_cLabel[i].setTColor(CSM20GetClassColor(i));
	layout.c_cLabel[i].setTipoIcon(IPALLINO);
	}

// CalcDrawRect
layout.c_viewTopPerc = profile.getProfileInt(_T("TowerLayout"),_T("ViewTop%"),10);
layout.c_viewBottomPerc = profile.getProfileInt(_T("TowerLayout"),_T("ViewBottom%"),25);
layout.c_viewLeftPerc = profile.getProfileInt(_T("TowerLayout"),_T("ViewLeft%"),8);
layout.c_viewRightPerc = profile.getProfileInt(_T("TowerLayout"),_T("ViewRight%"),5);

	
}

CTowerView::~CTowerView()
{
}


BEGIN_MESSAGE_MAP(CTowerView, CView)
	//{{AFX_MSG_MAP(CTowerView)
	ON_WM_ERASEBKGND()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CTowerView drawing


// area disegno interna
CRect CTowerView::getDrawRect(void)
{
CRect rcBounds,rect;
GetClientRect (rcBounds);
layout.CalcDrawRect(rcBounds,rect);
return (rect);
}



void CTowerView::OnDraw(CDC* pDC)
{

DOC_CLASS* pDoc = (DOC_CLASS* )GetDocument();

// TODO: add draw code here
// Call Base Class Draw

CRect rectB;
CRect rcBounds;

GetClientRect (rcBounds);
// Set base Rect
layout.setDrawRect(rcBounds);
// Call Base Class Draw
layout.setMaxY(pDoc->GetTrendDScalaY());
layout.setMinY(0.);
//-----------------------------------------
// Init vLabel 
// layout.setVFori	(pDoc->getFori());
layout.c_vLabel[0] = pDoc->GetVPosition();
layout.c_vLabel [1] = pDoc->GetTrendDVPositionDal();
layout.c_vLabel [2] = pDoc->GetVCliente();
layout.c_vLabel [3] = 0;		// maxValX 
layout.c_vLabel [4] = pDoc->getRotolo();
layout.c_vLabel [5] = pDoc->lega;
CTime d(pDoc->startTime);
layout.c_vLabel [6] = d.Format( _T("%A, %B %d, %Y") );
// visualizzazione threshold
layout.c_vLabel [7] = pDoc->formatStringThreshold();

// contatori a,b c,d,
CString s;
for(int i=0;i<pDoc->c_difCoil.getNumClassi();i++)
	{
	if (i>0)
		s += ",";
	CString s1;
	s1.Format(_T(" %c=%2.0lf"),'A'+i,pDoc->c_difCoil.getTotDifetti('A'+i,0,(int)pDoc->GetVPosition()));
	s += s1;
	}
layout.c_vLabel [8] = s;
//------------------------
// densita`
for(int i=0;i<pDoc->c_difCoil.getNumClassi();i++)
	{
	CString s1;
	// densita`
	double densita=pDoc->c_difCoil.getDensityLevel('A'+i,pDoc->c_densityCalcLength);
	s1.Format(_T(" %c=%3.0lf"),'A'+i,densita);
	layout.c_vLabel [9+i] = s1;
	if (densita > pDoc->c_difCoil.getAllarme ('A'+i))
		{
		layout.c_vLabel [9+i].setTColor(RGB(255,0,0));
		}
	else
		{
		layout.c_vLabel [9+i].setTColor(RGB(0,0,0));
		}
	}
//------------------------
// max 4 classi 9+4=13
// lunghezza di base per calcolo densita`
s.Format(_T("%4.0lf"),pDoc->c_densityCalcLength);
layout.c_vLabel [13] = s;

int numClassi = pDoc->c_difCoil.getNumClassi();

// cluster  label 14,15,16,17
for (int i=0;i<numClassi;i++)
	{	
	s.Format(_T("%d"),pDoc->c_clusterCounter[i]);
	layout.c_vLabel [14+i] = s;
	}

layout.setNumSchede(pDoc->c_difCoil.getNumSchede());
layout.setNumClassi(numClassi);

layout.draw(pDC);

// Trovo Finestra disegno (grigia)
layout.CalcDrawRect (rcBounds,rectB);

// Disegno Posizione diaframma
drawPosDiaframma(pDC,rectB);

// Parametrizzata scala orizzontale su layout
double Xsize = 1. * layout.c_sizeBanda / layout.c_bandePerCol;

CRect clientRect;
clientRect = rectB;
clientRect.right =   clientRect.left;

CFont font,*pOldFont;
font.CreatePointFont (70,_T("Times New Roman"),pDC);
pOldFont = pDC->SelectObject(&font); 


for (int i=0;i<pDoc->c_difCoil.getNumSchede();i++)
	{
	double *dVal = new double [pDoc->c_difCoil.getNumClassi()];
	for (int j=0;j<pDoc->c_difCoil.getNumClassi();j++)
		{
		// Sostituita densita` con totali per classi
		// dVal[j] = pDoc->c_difCoil.getValDensita(i,'A'+j,pDoc->GetTrendDScalaX());
		// sostituita funzione veloce che usa totali	20-01-99 
		//dVal[j] = pDoc->c_difCoil.getTotDifetti(i,'A'+j,0,
		//	(int) pDoc->c_difCoil.getMeter());
		dVal[j] = pDoc->c_difCoil.getAllDifetti(i,'A'+j);
		}
	// inserito offset inizio grafico
	clientRect.left  = rectB.left + layout.c_sizeFBanda + (int ) (Xsize * i);
	clientRect.right = rectB.left + layout.c_sizeFBanda + (int ) (Xsize * (i+1));

	towerView[i].setScale(clientRect,pDoc->GetTrendDScalaY(),dVal);
	towerView[i].setPos(i+1);
	 
		
	towerView[i].draw(pDC);
	delete [] dVal;
	}

// Restore old font 
pDC->SelectObject(pOldFont); 
}



/////////////////////////////////////////////////////////////////////////////
// CTowerView diagnostics

#ifdef _DEBUG
void CTowerView::AssertValid() const
{
	CView::AssertValid();
}

void CTowerView::Dump(CDumpContext& dc) const
{
	CView::Dump(dc);
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CTowerView message handlers

BOOL CTowerView::OnEraseBkgnd(CDC* pDC) 
{

// TODO: Add your message handler code here and/or call default
CRect r;
GetClientRect(&r);
layout.OnEraseBkgnd(pDC,r);

return TRUE;	
}

BOOL CTowerView::Create(LPCTSTR lpszClassName, LPCTSTR lpszWindowName, DWORD dwStyle, const RECT& rect, CWnd* pParentWnd, UINT nID, CCreateContext* pContext) 
{

// TODO: Add your specialized code here and/or call the base class
if (!CView::Create(lpszClassName, lpszWindowName, 
	dwStyle, rect, pParentWnd, nID, pContext))
	return (FALSE);

DOC_CLASS* pDoc = (DOC_CLASS* )GetDocument();

towerView.SetSize (pDoc->c_difCoil.getNumSchede()); 

layout.Create(this);

// init colors pen and brush
for (int i=0;i<pDoc->c_difCoil.getNumSchede();i++)
	{
	towerView[i].init (pDoc->c_difCoil.getNumClassi());
	towerView[i].setMode (TOWER);
	for (int j=0;j<pDoc->c_difCoil.getNumClassi();j++)
		{
		towerView[i].setPenTB (j,CSM20GetClassColor(j));	  // Top and Bottom color
		towerView[i].setPenLR (j,layout.getRgbBackColor());	  // Red
		towerView[i].setBrush (j,CSM20GetClassColor(j));
		}
	}
return TRUE;
}




BOOL CTowerView::OnNotify(WPARAM wParam, LPARAM lParam, LRESULT* pResult) 
{
	// TODO: Add your specialized code here and/or call the base class

	CString s;
	s.Format(_T("Got Notify Message from %d"),(int)wParam);
	AfxMessageBox (s);
	return CView::OnNotify(wParam, lParam, pResult);
}

void CTowerView::OnUpdate(CView* pSender, LPARAM lHint, CObject* pHint) 
{
	// TODO: Add your specialized code here and/or call the base class
	
if ((lHint==0) && (pHint == NULL))
	CView::OnUpdate(pSender,lHint,pHint);	
else
	draw();

}


void CTowerView::drawPosDiaframma(CDC *pDC, CRect rectB)
{
DOC_CLASS* pDoc = (DOC_CLASS* )GetDocument();
CPen pen1,pen2,*oldPen;


if (!pDoc->c_doUseDiaframmi)
	return;

	// white
	pen1.CreatePen(PS_SOLID,1,RGB(255,255,255));
	oldPen = pDC->SelectObject(&pen1);
	double posLeft = pDoc->c_posDiaframmaSx;

	// 15-09-2004 corretto centrato +layout.c_sizeFBanda 
	int scPosLeft = rectB.left +layout.c_sizeFBanda + (1.  * posLeft * (double)layout.c_sizeBanda / (layout.c_bandePerCol*pDoc->c_sizeBande));

	pDC->MoveTo(scPosLeft,rectB.top);
	pDC->LineTo(scPosLeft,rectB.bottom);

	// white
	pen2.CreatePen(PS_SOLID,1,RGB(255,255,255));
	pDC->SelectObject(&pen2);
	double posRight = pDoc->c_posDiaframmaDx;

	// 15-09-2004 corretto centrato +layout.c_sizeFBanda 
	int scPosRight = rectB.left +layout.c_sizeFBanda + (1.  * posRight * (double)layout.c_sizeBanda / (layout.c_bandePerCol*pDoc->c_sizeBande));


	pDC->MoveTo(scPosRight,rectB.top);
	pDC->LineTo(scPosRight,rectB.bottom);
	
	pDC->SelectObject(oldPen);
	pen1.DeleteObject();
	pen2.DeleteObject();
	// Fine disegno pos. Diaframma
	//---------------------------------------------------------

}

