// CSM10_AREng.h : main header file for the CSM10_AREng DLL
//

#if !defined(AFX_CSM10_AREng_H__A0FB7A36_725A_11D2_A770_00C026A019B7__INCLUDED_)
#define AFX_CSM10_AREng_H__A0FB7A36_725A_11D2_A770_00C026A019B7__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"		// main symbols

/////////////////////////////////////////////////////////////////////////////
// CCSM10_AREngApp
// See CSM10_AREng.cpp for the implementation of this class
//

class CCSM10_AREngApp : public CWinApp
{
public:
	CCSM10_AREngApp();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CCSM10_AREngApp)
	//}}AFX_VIRTUAL

	//{{AFX_MSG(CCSM10_AREngApp)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CSM10_AREng_H__A0FB7A36_725A_11D2_A770_00C026A019B7__INCLUDED_)
