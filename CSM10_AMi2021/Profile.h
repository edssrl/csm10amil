//-----------------------------------------------------------------
//
//				 Profile.h
//
//	  Gestione profileString ed int
//-----------------------------------------------------------------

#ifndef _CPROFILE_H_
#define _CPROFILE_H_

class CProfile
{
CString strValue;
CWinApp* pApp;
public:
CProfile (CWinApp* pA) {pApp = pA;};
CProfile (void ) {pApp = AfxGetApp();};

BOOL getProfileBool (LPCTSTR s1, LPCTSTR s2, BOOL def);
CString &getProfileString (LPCTSTR s1, LPCTSTR s2, LPCTSTR def);
int getProfileInt (LPCTSTR s1, LPCTSTR s2, int def);
BOOL deleteProfile (LPCTSTR s1, LPCTSTR s2);
BOOL writeProfileString (LPCTSTR s1, LPCTSTR s2, LPCTSTR s3);

};

#endif