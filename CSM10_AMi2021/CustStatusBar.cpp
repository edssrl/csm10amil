// CustStatusBar.cpp: implementation of the CCustStatusBar class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
// #include "CSM10_AR.h"
#include "CustStatusBar.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CCustStatusBar::CCustStatusBar()
{

}

CCustStatusBar::~CCustStatusBar()
{

}


void CCustStatusBar::Init(void )
{
int  a = 0;

}

void CCustStatusBar::DrawItem(LPDRAWITEMSTRUCT lpDrawItemStruct)
	{
	   switch(lpDrawItemStruct->itemID)
       {
       case 2:
			{	
			// Attach to a CDC object
			CDC dc;
			dc.Attach(lpDrawItemStruct->hDC);

			// Get the pane rectangle and calculate text coordinates
			CRect rect(&lpDrawItemStruct->rcItem);
			CBrush aBrush(RGB(255,0,0)) ;
			CBrush* pOldBrush = (CBrush*)dc.SelectObject(&aBrush) ;
			dc.Rectangle(&rect) ;
			dc.SelectObject(pOldBrush) ;

       //     CBitmap* pBitmap = /* pointer to current CBitmap */;
	  //		CDC srcDC; // select current bitmap into a compatible CDC
      //    srcDC.CreateCompatibleDC(NULL);
      //    CBitmap* pOldBitmap = srcDC.SelectObject(pBitmap);
      //    dc.BitBlt(rect.left, rect.top, rect.Width(), rect.Height(),
      //               &srcDC, 0, 0, SRCCOPY); // BitBlt to pane rect
      //    srcDC.SelectObject(pOldBitmap);

			// Detach from the CDC object, otherwise the hDC will be
			// destroyed when the CDC object goes out of scope
			CString s((LPCTSTR)lpDrawItemStruct->itemData);
			dc.SetBkMode (TRANSPARENT);
			dc.TextOut(rect.left,rect.top,s,s.GetLength());
			dc.Detach();
           return;
       }

       CStatusBar::DrawItem(lpDrawItemStruct);
   }
}
	
	

void CCustStatusBar::SetBitmap(int i)
{	
	// Set new index.
	m_iCurrentDIB = i ;

	// Invalidate pane on statusbar to generate a paint message.
	CRect rect ;
	GetItemRect(1, &rect) ; 	
	InvalidateRect(rect, FALSE) ;
}
