// GraphView.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CBaseGraphView view

// Funzione di calcolo colori per Classi
// Funzione di calcolo colori per Classi
COLORREF CSM20GetClassColor (int cl);
void CSM20ClassColorInvalidate (void);

#define DOC_CLASS CLineDoc


#include "FontInfo.h"
#include "Label.h" 
#include "printLibSrc/CPage.h" 
#include "PdfPrintView.h"

//Nome Server Window
#ifndef CSM_REPORT_CLASSNAME
#define CSM_REPORT_CLASSNAME _T("CSM10_AR2021_REPORT")
#endif

#ifndef CSM_CLASSNAME
#define CSM_CLASSNAME _T("CSM10_AR2021")
#endif

struct LinePrinter
{
UINT flags;   // Flag Di stampa 
CString str;  // Valore da stampare
};

class PagePrinter
{
FontInfo fNormal;
FontInfo fCaption;
CArray <LinePrinter,LinePrinter&> page;
int nPage;
public:
PagePrinter(void){fCaption.name = "Arial";
	fCaption.size = 100;
	fNormal.name = "Arial";
	fNormal.size = 100;
	nPage = 0;
	};

BOOL add(CString &s,UINT f)
	{
	LinePrinter lp;
	lp.flags = f;
	lp.str = s;
	page.Add(lp);
	return TRUE;
	};
void clear (void) {page.RemoveAll();nPage=0;};
int print(int index,int numLine,CRect &rectB,CDC* pdc);
void setNumPage(int n){nPage = n;};
int  getNumPage(void){return(nPage);};
FontInfo *getFontNormal(void) {return(&fNormal);};
};

// modi di visualizzazione layout 

typedef  unsigned int Mode;

#define LINEAR		0x00000000
#define LOGARITHM	0x00000001
#define NOLABELV	0x00000002
#define NOLABELH	0x00000004
#define MSCALEV		0x00000008		// multiscala verticale

enum LabelId {S_CAPTION,
		S_POSITION,V_POSITION,
		S_POSITIONDAL,V_POSITIONDAL,
		S_CLIENTE,V_CLIENTE,
		S_DIMX,V_DIMX,
		S_DENSITA,V_DENSITA,
		S_FORI,V_FORI,
		S_ALARM,V_ALARM,
		S_RICETTA,V_RICETTA,
		S_ROTOLO,V_ROTOLO};


#define MAX_NUMCLASSI 4
#define MAX_NUMLABEL 20
	
class Layout
{
Mode  mode;
	public:

CLabel sCaption;
FontInfo fCaption;
FontInfo fLabel;
FontInfo fNormal;

CLabel c_sLabel [MAX_NUMLABEL];
CLabel c_vLabel [MAX_NUMLABEL];
int c_nlAfterLabel [MAX_NUMLABEL];
int c_vOrderLabel [MAX_NUMLABEL];		// ordine di disegno delle label
int c_pxSepLabel [MAX_NUMLABEL];

CLabel c_cLabel [MAX_NUMCLASSI];		// label classi
//---------------
//	percentuali di 
// vista dedicate alle varie regioni dello
// schermo intorno alla zona centrale
// usate solo da CalcDrawRect
int	c_viewTopPerc;
int	c_viewBottomPerc;
int	c_viewLeftPerc;
int	c_viewRightPerc;


	private:

CLabel sPosition;
CLabel sPositionDal;
CLabel sCliente;
CLabel sDimX;
CLabel sProduct;
CLabel sFori;
CLabel sAlarm;
CLabel sRicetta;
CLabel sRotolo;

CLabel vPosition;
CLabel vPositionDal;
CLabel vCliente;
CLabel vDimX;
CLabel vProduct;
CLabel vFori;
CLabel vAlarm;
CLabel vRicetta;
CLabel vRotolo;


double maxValY;
double minValY;

CRect rcBounds;
// esclude parte centrale dal BckGr
BOOL excludeUpdateBackGr;
// esclude parte laterale label verticali dal BckGr
BOOL excludeUpdateBkgLV;

ViewType viewType;
// Background intorno
COLORREF rgbBackColor;
// background interno
COLORREF rgbDrawBackColor;
	
public:
int c_numSchede;
int c_numClassi;	

// dati globali usati da TargetBoard
// larghezza prima banda
int c_sizeFBanda;
// larghezza bande
int c_sizeBanda;
// bande per colonna
int c_bandePerCol;

public:
Layout(void);

BOOL Create(CWnd* pParentWnd);

void setExcludeBkgr(BOOL v){excludeUpdateBackGr=v;};
void setExcludeBkgLV(BOOL v){excludeUpdateBkgLV=v;};

COLORREF getRgbBackColor(void) {return(rgbBackColor);};
void setRgbBackColor(COLORREF rgb) {rgbBackColor = rgb;};
COLORREF getRgbDrawBackColor(void) {return(rgbDrawBackColor);};
void setRgbDrawBackColor(COLORREF rgb) {rgbDrawBackColor = rgb;};

void setMaxY(double v){maxValY = v;};
void setMinY(double v){minValY = v;};
double getMaxY(void){return maxValY;};
double getMinY(void){return minValY;};
void setNumSchede(int n){c_numSchede = n;};
void setNumClassi(int n){c_numClassi = n;};

BOOL OnEraseBkgnd(CDC* pDC,CRect &rect); 
BOOL OnEraseDrawBkgnd(CDC* pDC);

void setViewType(ViewType type){viewType = type;};
void setFontCaption (int size,LPCTSTR name = _T("TimesNewRoman"))
	{fCaption.name = name;fCaption.size = size*10;};
void setFontLabel (int size,LPCTSTR name = _T("TimesNewRoman"))
	{fLabel.name = name;fLabel.size = size*10;};
void setFontNormal (int size,LPCTSTR name = _T("TimesNewRoman"))
	{fNormal.name = name;fNormal.size = size*10;};

void setDrawRect(CRect &rect){rcBounds=rect;};
void CalcDrawRect (const CRect& rcBounds,CRect& rcDraw);
void Draw3DRect (CDC* pdc,const CRect& rc);
void draw (CDC* pdc);
void setCaption  (LPCTSTR s){sCaption=s;};
void setPosition (LPCTSTR s){sPosition=s;};
void setVPosition(LPCTSTR s){vPosition=s;};
void setVPosition(int v){CString s;s.Format(_T("%6d"),v);
					vPosition = s;};
int  getVPosition(void){int v;CString s(vPosition.getString());
						_stscanf((LPCTSTR)s,_T("%d"),&v);
							return v;};

void setPositionDal (LPCTSTR s){sPositionDal=s;};
void setVPositionDal(LPCTSTR s){vPositionDal=s;};
void setVPositionDal(int v){CString s;s.Format(_T("%6d"),v);
						vPositionDal = s;};
int  getVPositionDal(void){int v;CString s(vPositionDal.getString());
					_stscanf((LPCTSTR)s,_T("%d"),&v);
							return v;};

void setAlarm(LPCTSTR s){sAlarm=s;};
void setVAlarm(LPCTSTR s){vAlarm=s;};
void setVAlarm(int v){CString s;s.Format(_T("%6d"),v);
		vAlarm = s;};

void setCliente(LPCTSTR s){sCliente=s;};
void setVCliente(LPCTSTR s){vCliente=s;};
void setRicetta(LPCTSTR s){sRicetta=s;};
void setVRicetta(LPCTSTR s){vRicetta=s;};
void setRotolo(LPCTSTR s){sRotolo=s;};
void setVRotolo(LPCTSTR s){vRotolo=s;};

// void setLabel(LPCTSTR s){c_label.setString(s);};

void setDimX(LPCTSTR s){sDimX=s;};
void setVDimX(LPCTSTR s){vDimX=s;};
void setVDimX(int v){CString s;s.Format(_T("%6d"),v);
				vDimX = s;};
int  getVDimX (void){int v;CString s(vDimX.getString());
					_stscanf((LPCTSTR)s,_T("%d"),&v);
							return v;};

void setMode(Mode m){mode = m;};


void setProduct(LPCTSTR s){sProduct=s;};
void setVProduct(LPCTSTR s){vProduct=s;};
 
void setFori(LPCTSTR s){sFori=s;};
void setVFori(LPCTSTR s){vFori=s;};

};




/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
// CLineView view

// Aggiornato 27-10-97
struct LineInfo
{
double max;
double min;
double limit;
double *val;
int count;			// numero valori presenti vettore
int numStep;		// numero passi totali
int actual;
LineInfo (void) {val = NULL; min = 0.; limit = 0.; max=0.;count = 0;};
~LineInfo (void) {if (val != NULL) {delete [] val; val = NULL;}};
// Safe allocation
void newVal (int size)
	{if (val != NULL) {delete [] val; val = NULL;}
	val = new double [size];
	};
};


class SubLine
{
Mode mode;
int limit;
BOOL c_drawLimit;
LPPOINT gVertex; // Lista Punti Polyline Disegno
int count;
int actual;
int numStep;
CPoint oldPoint;
COLORREF colorPen;
COLORREF colorPenLarge;
CRect clip;
public:
SubLine(void) {gVertex = NULL;mode = LINEAR;
				setPen(RGB(0,0,0));setPenLarge(RGB(0,0,0));
				c_drawLimit = FALSE;};
~SubLine(void) {if (gVertex != NULL) delete [] gVertex;};

void setPen (COLORREF  rgb)
	{colorPen = rgb;};
void setPenLarge (COLORREF  rgb)
	{colorPenLarge = rgb;};
void setDrawLimit(BOOL drawLimit){c_drawLimit = drawLimit;};

void setMode(Mode m){mode = m;};
// calcola dimensione rettangolo relativo a valore 
// da visualizzare,valore della scala, dimensione in della
// finestra di visualizzazione. 
void setScale(CRect &r,LineInfo &lInfo);
void draw(CDC* pDC);
BOOL IsEmpty (void) {if (gVertex == NULL) return TRUE;
					return FALSE;};
};



/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
// CDensView view


class SubRect
{
CRect bar;
LOGBRUSH lBrush;								  
LOGPEN  lPenTB;		// Pen top bottom
LOGPEN  lPenLR;		// Pen left right

char id;
int pos;
public:
void setId (char c){id = c;};
void setPos (int v){pos = v;};
void setPen (COLORREF  rgb,UINT lopnWidth=0,UINT lopnStyle=PS_SOLID)
{
setPenTB (rgb,lopnWidth,lopnStyle);
setPenLR (rgb,lopnWidth,lopnStyle);
};
void setPenTB (COLORREF  rgb,UINT lopnWidth=0,UINT lopnStyle=PS_SOLID)
{
lPenTB.lopnStyle = lopnStyle;

lPenTB.lopnWidth.x = lopnWidth;
lPenTB.lopnWidth.y = 0;	// Not Used

lPenTB.lopnColor = rgb;
};
void setPenLR (COLORREF  rgb,UINT lopnWidth=0,UINT lopnStyle=PS_SOLID)
{
lPenLR.lopnStyle = lopnStyle;

lPenLR.lopnWidth.x = lopnWidth;
lPenLR.lopnWidth.y = 0;	// Not Used

lPenLR.lopnColor = rgb;
};

void setBrush (COLORREF lbColor,UINT lbStyle = BS_SOLID ,LONG lbHatch = HS_BDIAGONAL) 
{
lBrush.lbStyle = lbStyle;
lBrush.lbColor = lbColor;
lBrush.lbHatch = lbHatch;
};

// calcola dimensione rettangolo relativo a valore 
// da visualizzare, valore della scala, dimensione in della
// finestra di visualizzazione.
// torna top  
int setScale(CRect &r,int offset,double max,double val);
void draw(CDC* pDC);
};


enum ViewDensMode {FLAT,TOWER};

class ViewDensUnit
{

ViewDensMode vMode;
CArray <SubRect,SubRect &> subR;


// SubRect subA;
// SubRect subB;
// SubRect subC;
// SubRect subD;
public:
ViewDensUnit(int n){init(n);};
ViewDensUnit(void){init(2);};
void init(int n){subR.SetSize(n);vMode = FLAT;
	for (int i=0;i<subR.GetSize();i++)
		subR[i].setId(('A'+i));};
void setMode (ViewDensMode m){vMode = m;};

void setPos(int v){for(int i=0;i<subR.GetSize();i++) subR[i].setPos(v);};
void setScale(CRect &r,double max,double val[]); 
//	double valB,double valC,double valD);
void draw(CDC* pDC)
	{for(int i=0;i<subR.GetSize();i++)subR[i].draw(pDC);};
void setPen (int id,COLORREF  rgb){ASSERT(id < subR.GetSize());subR[id].setPen(rgb);};
void setPenTB (int id,COLORREF  rgb){ASSERT(id < subR.GetSize());subR[id].setPenTB(rgb);};
void setPenLR (int id,COLORREF  rgb){ASSERT(id < subR.GetSize());subR[id].setPenLR(rgb);};

void setBrush (int id,COLORREF lbColor,UINT lbStyle = BS_SOLID ,LONG lbHatch = 0)
	{ASSERT(id<subR.GetSize());subR[id].setBrush(lbColor,lbStyle,lbHatch);}; 
};



/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
// CInitView view

class CInitView : public CPdfPrintView
{
protected:
	CInitView();           // protected constructor used by dynamic creation
	DECLARE_DYNCREATE(CInitView)
ViewType vType;

// Attributes
public:

int c_leftMarg;		// margine sinistro report in mm
int c_rightMarg;	// margine destro report in mm
int c_topMarg;		// margine alto report in mm
int c_bottomMarg;	// margine basso report in mm

// Tipi di grafici in stampa
// Densita` trasversale
CArray <ViewDensUnit, ViewDensUnit &>densView;
Layout  layoutD;

// Trend longitudinale
LineInfo lineInfo	[4];
SubLine trendLine	[4];
Layout  layoutL;

// Allarme Densita`
LineInfo lineInfoAD [4];
SubLine trendAD		[4];
Layout  layoutAD;

// Indice Qualita`
LineInfo lineInfoQ  [4];
SubLine trendQ		[4];
Layout  layoutQ;

// Compressioni d'asse
Layout c_layoutCompressABBig ;
CTargetBoard	c_targetBoardABBig;


// Rect Print
CRect prLimit;

// CDAttesaStampa waitPrint;

// Stampa formato testo
PagePrinter pagePr;
PagePrinter pagePrEnd;	 // Testo fine Report

BOOL onlyTextReport;	 // Stampa report solo testo
BOOL directPrint;		// Stampa diretta senza dialog 
int c_numTextPage;		// numero di pagine testo 
int c_numTextPage2;		// numero di pagine testo cluster
int c_graphMap [4];		// mappatura config pagine grafiche in stampa
						// partendo da c_graphMap [0] metto indici di pagina da stampare


int LinePerPage;
int ColumnPerPage;		 // Dettaglio difetti numero di colonne 1-4

// Report option
BOOL	m_DATI_CLIENTE;
BOOL	m_DATA_LAVORAZIONE;
BOOL	m_LUNGHEZZA_BOBINA;
BOOL	m_DESCR_PERIODICI;
BOOL	m_DESCR_RANDOM;
BOOL	m_DESCR_COMPRESSION;
//
BOOL	m_DESCR_DOWNWEB;
BOOL	m_DESCR_CROSSWEB;
BOOL	m_DESCR_THRESHOLD;
//
BOOL	m_SOGLIE_ALLARME;
BOOL	m_ALLARMISISTEMA;
double	m_DALMETRO;
double	m_ALMETRO;
double	m_INTERVALLO1;
double	m_INTERVALLO2;
double	m_INTERVALLO3;

// Operations
public:
	void draw(void){CClientDC dc(this);OnDraw(&dc);};
	ViewType getViewType(void) {return(vType);};
	CRect getDrawRect(void);
	CRect getMeterRect(void); //empty rect

int  prepareTextPage(CDC* pdc); 
int  prepareTextPageEx(CDC* pdc,CRect r); 
// pagina cluster
int  prepareTextPageEx2(CDC* pdc,CRect r); 
void pageGraphicPrint(CDC* pdc,int curPage,CRect &rcBounds);
double printTextPageEx(CPage* ps,CRect rcBounds,int pageIndex); 
double printTextPageEx2(CPage* ps,CRect rcBounds,int pageIndex); 
void reportRotolo2(CDC* pDC,CRect rcBounds);

void drawPosDiaframma(CDC *pDC, CRect rectB,Layout* layout);

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CInitView)
	public:
	virtual BOOL Create(LPCTSTR lpszClassName, LPCTSTR lpszWindowName, DWORD dwStyle, const RECT& rect, CWnd* pParentWnd, UINT nID, CCreateContext* pContext = NULL);
	virtual void OnPrepareDC(CDC* pDC, CPrintInfo* pInfo = NULL);
	virtual void OnInitialUpdate();
	protected:
	virtual void OnDraw(CDC* pDC);      // overridden to draw this view
	virtual void OnPrint(CDC* pDC, CPrintInfo* pInfo);
	virtual void OnEndPrinting(CDC* pDC, CPrintInfo* pInfo);
	virtual void OnBeginPrinting(CDC* pDC, CPrintInfo* pInfo);
	virtual BOOL OnPreparePrinting(CPrintInfo* pInfo);
	//}}AFX_VIRTUAL

// Implementation
protected:
	virtual ~CInitView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

	// Generated message map functions
public:
	CRect calcPrintInternalRect(CDC* pdc,CRect rcBounds,CRect* border=NULL);
	//{{AFX_MSG(CInitView)
	afx_msg void OnFilePrint();
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	afx_msg void OnFilePrintPreview();
	afx_msg void OnUpdateFilePrint(CCmdUI* pCmdUI);
	afx_msg void OnUpdateFilePrintPreview(CCmdUI* pCmdUI);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////
