//-----------------------------------------------------------------
//
//				 Profile.cpp 
//
//	  Gestione profileString ed int
//-----------------------------------------------------------------

#include "stdafx.h"
#include "Profile.h"



// Bool Value

BOOL CProfile::getProfileBool (LPCTSTR s1, LPCTSTR s2, BOOL def)
{

strValue = pApp->GetProfileString(s1, s2, 
				_T("NODATA"));

if (strValue == _T("NODATA"))
	{
	if (def)
		strValue = _T("TRUE");
	else
		strValue = _T("FALSE");
 
	pApp->WriteProfileString(s1, s2, 
				strValue);
 	}

if ((strValue == _T("TRUE"))
	||
	(strValue == _T("ON")))
	return TRUE;
else
	return FALSE;

}

// CString *Value
CString &CProfile::getProfileString (LPCTSTR s1, LPCTSTR s2, LPCTSTR def)
{

strValue = pApp->GetProfileString(s1, s2, 
				_T("NODATA"));

if (strValue == _T("NODATA"))
	{
 
	pApp->WriteProfileString(s1, s2, 
				def);
 	
	strValue = def;
	}

return strValue;
}


// CString *Value
int CProfile::getProfileInt (LPCTSTR s1, LPCTSTR s2, int def)
{
int iVal;

iVal = pApp->GetProfileInt(s1, s2, 
				32767);

if (iVal == 32767)
	{
 
	pApp->WriteProfileInt(s1, s2, 
				def);
 	
	iVal = def;
	}

return iVal;
}

// delete Value
BOOL CProfile::deleteProfile (LPCTSTR s1, LPCTSTR s2)
{
 
return(pApp->WriteProfileString(s1, s2,NULL));
 	
}


// Write Value
BOOL CProfile::writeProfileString (LPCTSTR s1, LPCTSTR s2,LPCTSTR s3)
{
 
return(pApp->WriteProfileString(s1, s2,s3));
 	
}
