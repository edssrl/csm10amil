//------------------------------------------------------
//
//
//					RTextFile
//
// Legge ed interpreta File Testo formattati per linee
//
//------------------------------------------------------

#include "stdafx.h"
#include <afxtempl.h>


#include "RTextFile.h"



//------------------------------------------------------
//
//
//					open CFile 
//
// 
//
//------------------------------------------------------

BOOL TextFile::open (CString &name, CFile::OpenFlags mode)
{
CFileException error;
BOOL retCode;

if (opened)
	return(FALSE);
nome = name;

retCode = file.Open( (LPCTSTR)nome,mode,&error);

if (retCode == FALSE)
	return(retCode);

opened = TRUE;

// Determino Numero campi
CString line;
readLine (line);


return TRUE;
}


//------------------------------------------------------
//
//
//					close CFile 
//
// 
//
//------------------------------------------------------

BOOL TextFile::close (void )
{

if (!opened)
	return (FALSE);

file.Close( );

opened = FALSE;  
return TRUE;
}

//------------------------------------------------------
//
//
//					read line 
//
// Legge intera linea file
//
//------------------------------------------------------

void TextFile::readLine (CString &line)
{
ASSERT (opened == TRUE);


// Clear Line
line.Empty();

char ch = 0;
int nBytes = 0;
// store line start
c_linePosition = file.GetPosition();

while (ch != _T('\n'))
	{
	nBytes = file.Read(&ch,1);
	if (nBytes == 0)
		break;
	line += ch;
	}

// calcolo numero di campi X qs riga
calcNumFields((LPCTSTR) line);

}
//------------------------------------------------------
//
//
//					readField 
//
// Legge Campo numero, in linea
//
//------------------------------------------------------


BOOL TextFile::fieldNumFromLine (CString &line,int num,CString &field)
{
if (num > numField)
	return FALSE;

CString localLine;
localLine = line;

c_fieldOffsetInLine = 0;
for (int i=0;i<num;i++)
	{
	field = localLine.SpanExcluding( (LPCTSTR)separator);
	c_fieldOffsetInLine += field.GetLength()+1;
	localLine = localLine.Right(localLine.GetLength() - (field.GetLength()+1));
	}
 return(TRUE);
 }



//------------------------------------------------------
//
//
//					readField 
//
// Legge Campo numero, in riga identificata da key
// 3 Valori di ritorno CString, int, double
//------------------------------------------------------


BOOL TextFile::readField (CString &key,int num,CString &value,LPCTSTR keyExt)
{
ASSERT (opened == TRUE);
CString line;
CString fieldValue;
CString keyExtStr;
// rewind file	CFile

file.SeekToBegin();
readLine (line);

while (line.GetLength()>0)
	{
	if (fieldNumFromLine(line,1,fieldValue))
		{
		if ((c_extKeyPosition > 0)&&
			(keyExt != NULL))
			{
			if(fieldNumFromLine(line,c_extKeyPosition,keyExtStr))
				{
				if ((fieldValue == key)&&
					(keyExtStr == keyExt))
					{ // Found
					return (fieldNumFromLine(line,num,value));
					}
				}
			}
		else
			{
			if (fieldValue == key)
				{ // Found
				return (fieldNumFromLine(line,num,value));
				}
			}
		}
	readLine (line);
	}
return FALSE;
}

// Versione int
BOOL TextFile::readField (CString &key,int num,int &value,LPCTSTR keyExt)
{
CString fieldValue;
int val;

if (readField (key,num,fieldValue,keyExt))
	{// Found
	_stscanf ((LPCTSTR)fieldValue,_T("%d"),&val);
	value = val;
	return(TRUE);
	}
return FALSE;
}

// Versione double
BOOL TextFile::readField (CString &key,int num,double &value,LPCTSTR keyExt)
{
CString fieldValue;
double val;

if (readField (key,num,fieldValue,keyExt))
	{// Found
	_stscanf ((LPCTSTR)fieldValue,_T("%lf"),&val);
	value = val;
	return(TRUE);
	}
return FALSE;
}

void TextFile::setExtKeyPosition(int pos)
{
if (pos <= numField)
	c_extKeyPosition = pos;
}

int TextFile::calcNumFields(LPCTSTR sLine)
{
CString field;
CString line;

line = sLine;

numField = 0;
while (line.GetLength() > 0)
	{
	field = line.SpanExcluding( (LPCTSTR)separator);
	line = line.Right(line.GetLength() - (field.GetLength()+1));
	while ((line.Left(1)==_T("\n"))||
		(line.Left(1)==_T("\r")))
		line = line.Right(line.GetLength() - 1);
	numField ++;
	}
return (numField);

}


//---------------------------------------------------------
//
//
//		Cerca ultima (per data-ora) chiave per tipologia di record (Buono o Scarto )
// torna la chiave da usare per ricerche successive e il tipo
//
// Formato
// 04-11-2002 17:10:00,B,Y,XX3456Y,1400.00,Al678,22.09
// 04-11-2002 17:10:00,S,N,ZZ3456Y,1400.00,Al678,22.09

BOOL TextFile::getLastKey(CString fType,CString &key,CString &subKey)
{
CString line;
CString fieldDate,fieldType;
CTime lastTime;
CString lastKey,lastSubKey;

lastTime= CTime(LONG_MAX);
lastKey = "";
lastSubKey = "";

file.SeekToBegin();
readLine (line);

while (line.GetLength()>0)
	{
	if ((fieldNumFromLine(line,1,fieldDate))&&
		(fieldNumFromLine(line,2,fieldType)))
		{
		int gg,mm,aa;
		int h,m,s;
		_stscanf((LPCTSTR)fieldDate,_T("%d-%d-%d %d:%d:%d"),&gg,&mm,&aa,&h,&m,&s);
		CTime t(aa,mm,gg,h,m,s);
		if ((fType == fieldType)&&
			(t < lastTime))
			{
			lastTime = t;
			lastKey = fieldDate;
			lastSubKey = fieldType;
			}
		}
	readLine (line);
	}


key = lastKey;
subKey = lastSubKey;

if (lastTime == CTime(LONG_MAX))
	return(FALSE);
else
	return(TRUE);
}


// modifica il valore di un campo. ( vincolo stessa lunghezza campo esistente )
bool TextFile::writeField (CString key,int num,CString value,LPCTSTR keyExt)
	{
	// load position
	CString val;
	if (!readField (key,num,val,keyExt))
		return false;
	if (val.GetLength() != value.GetLength())
		return false;

	file.Seek(calcLastFieldOffsetInFile(),0);
	file.Write(value.GetString(),value.GetLength());
	return false;
	}
