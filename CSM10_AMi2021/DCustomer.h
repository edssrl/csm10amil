#if !defined(AFX_DCUSTOMER_H__02A312B2_D462_11D1_A6B2_00C026A019B7__INCLUDED_)
#define AFX_DCUSTOMER_H__02A312B2_D462_11D1_A6B2_00C026A019B7__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// DCustomer.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CDCustomer dialog

class CDCustomer : public CDialog
{
// Construction
public:
	CDCustomer(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CDCustomer)
	enum { IDD = IDD_CUSTOMER };
	CString	m_name;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CDCustomer)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CDCustomer)
	virtual void OnOK();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DCUSTOMER_H__02A312B2_D462_11D1_A6B2_00C026A019B7__INCLUDED_)
