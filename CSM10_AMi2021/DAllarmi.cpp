// DAllarmi.cpp : implementation file
//

#include "stdafx.h"
#include "Graphdoc.h"
#include "dyntempl.h"
#include "CSM10_AR.h"

#define IDD_ALLARMI 141

#include "Profile.h"
#include "DAllarmi.h"
#include "MainFrm.h"

#include "DBSet.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CDAllarmi dialog


CDAllarmi::CDAllarmi(CWnd* pParent /*=NULL*/)
	: CDialog(CDAllarmi::IDD, pParent)
{
	//{{AFX_DATA_INIT(CDAllarmi)
	//}}AFX_DATA_INIT
c_secAlarmSoundToEnd = 0;
c_soundID = 0;
c_alarmSoundActive = FALSE;
c_alarmType =  _T("SCONOSCIUTO");

c_fCaption.name = _T("TimesNewRoman");
c_fCaption.size = 110;

c_fNormal.name = _T("TimesNewRoman");
c_fNormal.size = 90;

 // In the CMyWindow constructor...
 c_hAccel = ::LoadAccelerators 
              (AfxGetInstanceHandle (),
               MAKEINTRESOURCE (IDR_MAINFRAME));

// allarme inattivo
c_statusDensClassA = FALSE;
c_statusDensClassB = FALSE;
c_statusDensClassC = FALSE;
c_statusDensClassD = FALSE;

}



void CDAllarmi::setAlarmSound(int sec)
{

if (sec < 0)
	sec = 0;

if (!c_alarmSoundActive)
	{
	c_alarmSoundActive = TRUE;
	c_soundID	= rand();
	SetTimer(c_soundID,1000,NULL);
	}

// Istante di attesa
c_secAlarmSoundToEnd = sec;
}



void CDAllarmi::DoDataExchange(CDataExchange* pDX)
{

	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CDAllarmi)
	DDX_Control(pDX, IDC_MESSAGE1, m_Message1);
	//}}AFX_DATA_MAP

for (int i=m_Message1.GetLineCount()-1;i<c_messages.GetSize();i++)
	{
	m_Message1.SetSel(-1, -1);              // end of edit text
	m_Message1.ReplaceSel(c_messages[i]);
	}            // append string..

m_Message1.SetSel(-1, -1);              // end of edit text
SetWindowText(c_alarmType);
}

void CDAllarmi::randomMoveWindow(void )
{// Random Move

// Move Window
CRect rect,rectParent;
// Parent Client Rect
GetParent()->GetClientRect(&rectParent);
// this WRect
GetWindowRect (&rect);

int deltax= rand() % ((rectParent.right - rectParent.left)-(rect.right - rect.left)); 
int deltay = rand() % ((rectParent.top - rectParent.bottom)-(rect.top - rect.bottom)); 

MoveWindow (deltax,deltay,abs(rect.right - rect.left),
	  abs(rect.top - rect.bottom),TRUE);

// Repaint all parent area
#ifdef ID_VIEW_REPAINT
	GetParent()->PostMessage(WM_COMMAND,ID_VIEW_REPAINT);
#endif
}

void CDAllarmi::fixMoveWindow(void )
{// Legge posizione finestra da INI file

CProfile profile;

int xPos = profile.getProfileInt(c_alarmType,_T("X_Pos"),10);
int yPos = profile.getProfileInt(c_alarmType,_T("Y_Pos"),10);


if (GetSafeHwnd() != NULL)
	// Move Window Ignore size parameter
	SetWindowPos(&wndTop,xPos,yPos,0,0,SWP_NOSIZE); 

}


BEGIN_MESSAGE_MAP(CDAllarmi, CDialog)
	//{{AFX_MSG_MAP(CDAllarmi)
	ON_WM_TIMER()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CDAllarmi message handlers


void CDAllarmi::OnTimer(UINT nIDEvent) 
{
// TODO: Add your message handler code here and/or call default

// Almeno una volta
if (--c_secAlarmSoundToEnd > 0)
	{// Do Sound	
	MessageBeep(0xffffffff); // Beep
	// 1998
	// Eliminata riposizionamento finestra
	// randomMoveWindow();
	}
}


BOOL CDAllarmi::DestroyWindow() 
{
	// TODO: Add your specialized code here and/or call the base class

if (c_alarmSoundActive)
	{// Stop Sound
	KillTimer(c_soundID);
    c_secAlarmSoundToEnd = 0;
	c_alarmSoundActive = FALSE;
	}

if (::IsWindow(m_hWnd))
	{
	// Save position configuration
	CRect rect;
	GetWindowRect(&rect);
	CString s;
	CProfile profile;
	s.Format (_T("%d"),rect.TopLeft().x);
	profile.writeProfileString(c_alarmType,_T("X_Pos"),s);
	s.Format (_T("%d"),rect.TopLeft().y);
	profile.writeProfileString(c_alarmType,_T("Y_Pos"),s);

	// Repaint all parent area
#ifdef ID_VIEW_REPAINT
	GetParent()->PostMessage(WM_COMMAND,ID_VIEW_REPAINT);
#endif
	}
return (CDialog::DestroyWindow());
}



//----------------------------
// Serialization

CDAllarmi& CDAllarmi::operator=(CDAllarmi& source)
{


c_alarmType = source.c_alarmType;

c_secAlarmSoundToEnd = source.c_secAlarmSoundToEnd;
c_soundID = source.c_soundID;
c_MemoData = source.c_MemoData;

// Clear ARRAY
c_messages.RemoveAll();

for (int i=0;i<source.c_messages.GetSize();i++)
	{
	c_messages.Add(source.c_messages[i]);
 	}

return (*this);
}
BOOL CDAllarmi::save(CFileBpe *cf)
{
// Molto Brutale				

// Save alarmType
int size = sizeof(TCHAR) * (c_alarmType.GetLength() + 1); // insert EOS
TCHAR c = '\0';
cf->Write((void *)&size,sizeof(int));
cf->Write((void *)((LPCTSTR)c_alarmType),size -sizeof(TCHAR));
cf->Write((void *)&c,sizeof (c));



cf->Write((void *)&c_secAlarmSoundToEnd,sizeof(c_secAlarmSoundToEnd));
cf->Write((void *)&c_soundID,sizeof(c_soundID));
cf->Write((void *)&c_MemoData,sizeof(c_MemoData));

// cf->Write((void *)&alarmSoundActive,sizeof(alarmSoundActive));

// save Size
size = c_messages.GetSize();
cf->Write((void *)&size,sizeof(int));

for (int i=0;i<c_messages.GetSize();i++)
	{
	// save Size
	size = sizeof(TCHAR) *(c_messages[i].GetLength() + 1);
	cf->Write((void *)&size,sizeof(int));
	cf->Write((void *)((LPCTSTR)c_messages[i]),size - sizeof(TCHAR));
	cf->Write((void *)&c,sizeof (c));

	}

return (TRUE);
}



BOOL CDAllarmi::load(CFileBpe *cf,bool notUnicode,int ver)
{
// Molto Brutale				

// Load ID
int size,i;

cf->Read((void *)&size,sizeof(int));
if (notUnicode)
	{
	char *pc;
	pc = new char [size];
	if (pc == NULL)
		{
		CString s;
		s.Format (_T("Load File Alloc Failed on size %d"),size);
		AfxGetMainWnd()->MessageBox(s,_T("Errore"));
		return FALSE;
		}

	cf->Read((void *)pc,size);
	c_alarmType = CString(pc);
	delete [] pc;
	}
else
	{
	TCHAR *pc;
	pc = new TCHAR [size];
	if (pc == NULL)
		{
		CString s;
		s.Format (_T("Load File Alloc Failed on size %d"),size);
		AfxGetMainWnd()->MessageBox(s,_T("Errore"));
		return FALSE;
		}

	cf->Read((void *)pc,size);
	c_alarmType = pc;
	delete [] pc;
	}
cf->Read((void *)&c_secAlarmSoundToEnd,sizeof(c_secAlarmSoundToEnd));
cf->Read((void *)&c_soundID,sizeof(c_soundID));
cf->Read((void *)&c_MemoData,sizeof(c_MemoData));

// cf->Read((void *)&alarmSoundActive,sizeof(alarmSoundActive));

// load CArray Size
cf->Read((void *)&size,sizeof(size));
// Clear ARRAY
c_messages.RemoveAll();

if (notUnicode)
	{
	int strSize;
	char *pci;
	char *pc;
	for (i=0;i<size;i++)
		{
		// load string Size
		CStringA cs;
		cf->Read((void *)&strSize,sizeof(int));
		pci = new char [strSize];
		pc = pci;
		if (pc == NULL)
			{
			CString s;
			s.Format (_T("Load File Alloc Failed on size %d"),size);
			AfxGetMainWnd()->MessageBox(s,_T("Errore"));
			return FALSE;
			}

		cf->Read((void *)pc,strSize);
		cs = pci;
		delete [] pci;
		c_messages.Add(CString(cs));
		}
	}
else
	{
	int strSize;
	TCHAR *pci;
	TCHAR *pc;
	for (i=0;i<size;i++)
		{
		// load string Size
		CString cs;
		cf->Read((void *)&strSize,sizeof(int));
		pci = new TCHAR [strSize];
		pc = pci;
		if (pc == NULL)
			{
			CString s;
			s.Format (_T("Load File Alloc Failed on size %d"),size);
			AfxGetMainWnd()->MessageBox(s,_T("Errore"));
			return FALSE;
			}

		cf->Read((void *)pc,strSize);
		cs = pci;
		delete [] pci;
		c_messages.Add(cs);
 		}
	}
return (TRUE);
}


int CDAllarmi::print(int index,int numLine,CRect &rectB,CDC* pdc)
{
TEXTMETRIC tm;
CPoint p;

int lineCaption = 0;

if (index < 0)
	return 0;

CFont fontNormal;
CFont fontCaption;

fontNormal.CreatePointFont (c_fNormal.size,c_fNormal.name,pdc);
fontCaption.CreatePointFont (c_fCaption.size,c_fCaption.name,pdc);

pdc->SelectObject(&fontCaption); 
pdc->GetTextMetrics(&tm);

pdc->SetBkMode (TRANSPARENT);

CString	str;
p.y = rectB.top + tm.tmHeight + tm.tmExternalLeading;
p.x = (rectB.right - rectB.left)/2;


CString mlString;
// str = "Dettaglio " + c_alarmType;
mlString.LoadString(CSM_DALLARMI_DETTAGLIO);
str = mlString + c_alarmType;
pdc->SetTextAlign(TA_CENTER | TA_TOP);

// Solo prima pagina
if (index == 0)
	{
	lineCaption ++;
	pdc->ExtTextOut (p.x,p.y,0,
		NULL,str,str.GetLength(),NULL);
	}

p.x =  rectB.left + tm.tmAveCharWidth;
p.y += tm.tmHeight;
pdc->SetTextAlign(TA_LEFT | TA_TOP);

pdc->SelectObject(&fontNormal); 
pdc->GetTextMetrics(&tm);
int i;
for (i=index;i<c_messages.GetSize();i++)
	{
	str = c_messages[i];
	p.y += tm.tmHeight + tm.tmExternalLeading;

	pdc->ExtTextOut (p.x,p.y,0,
		NULL,str,str.GetLength()-1,NULL);
	
	
	if (p.y > (rectB.bottom - 4*tm.tmHeight))
		break;
	}

// Clip rectB for other printing
rectB.top = p.y;

return(i);
}

int CDAllarmi::getSizeY(CDC* pdc)
{
TEXTMETRIC tm;

CFont fontNormal;
CFont fontCaption;

pdc->SaveDC();

int sizeY = 0;

// ::PrintDlg();

fontNormal.CreatePointFont (c_fNormal.size,c_fNormal.name,pdc);
fontCaption.CreatePointFont (c_fCaption.size,c_fCaption.name,pdc);

pdc->SelectObject(&fontCaption); 
pdc->GetOutputTextMetrics(&tm);

CString	str;
sizeY += tm.tmHeight + tm.tmExternalLeading;

pdc->SelectObject(&fontNormal); 
pdc->GetOutputTextMetrics(&tm);

for (int i=0;i<c_messages.GetSize();i++)
	{
	sizeY += tm.tmHeight + tm.tmExternalLeading; 

	}

pdc->RestoreDC(-1);

return(sizeY);
}

int CDAllarmi::getSizeChar(CDC* pdc)
{
TEXTMETRIC tm;

CFont fontNormal;

pdc->SaveDC();

int sizeY = 0;

fontNormal.CreatePointFont (c_fNormal.size,c_fNormal.name,pdc);

pdc->SelectObject(&fontNormal); 
pdc->GetTextMetrics(&tm);

sizeY += tm.tmHeight + tm.tmExternalLeading; 

pdc->RestoreDC(-1);

return(sizeY);
}



void CDAllarmi::insert(CString &str,CWnd *pWnd,int secAlarmSound)
{
// DestroyWindow();
appendMsg(str);
visualizza (pWnd,secAlarmSound);
}


// Visualizza allarme
void CDAllarmi::visualizza(CWnd *pWnd,int secAlarmSound)
{

// Display Alarm Window
// Run Modeless
if (!(::IsWindow(m_hWnd)))
	Create(IDD_ALLARMI,pWnd);

setAlarmSound(secAlarmSound);
// 1998
// eliminata randomMovewindow
// randomMoveWindow();

UpdateData(FALSE);
// Non funziona sotto 3.51
m_Message1.LineScroll(m_Message1.GetLineCount()-1);
fixMoveWindow();

ShowWindow(SW_SHOW);
// AfxGetMainWnd()->SetFocus();

}



// Decode accelator
// PreTranslateMessage override
BOOL CDAllarmi::PreTranslateMessage (MSG* pMsg)
{
return ((c_hAccel != NULL) &&
	  ::TranslateAccelerator (m_hWnd, c_hAccel,
	      pMsg));
}


