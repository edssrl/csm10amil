#pragma once

#include "stdafx.h"

#include "E:/ADLINK/PCIS-DASK/INCLUDE/dask.h"

#include "cthread.h"

class	CLineDoc;
class	CMainFrame;


class CThEncoder :
	public CThread
{

// GPIO Card Config
I16 c_card, c_err;
U16 c_card_num;
U16 c_func_sel;
U16 c_gptc_Ctr;
U16 c_gptc_Mode;
U16 c_gptc_SrcCtrl;
U16 c_gptc_PolCtrl;
U32 c_gptc_Reg1Val;
U32 c_gptc_Reg2Val;
U16 c_gptc_Status;
U32 c_gptc_CtrVal;

int	c_pulseMetro;

virtual int run(void* pData);

int		safeValue(void);

public:
	CThEncoder(void);
	~CThEncoder(void);

	CLineDoc* c_pDoc;
	CWnd*	c_pWndClient;	// CMasterView riceve msg updat

BOOL	config(void);

void	setPWnd(CWnd *pWnd){
	c_pWndClient = pWnd;};
void	setPDoc(CLineDoc* pDoc){
	c_pDoc = pDoc;};
	
void	setPulseMetro(int pulse){
	c_pulseMetro = pulse;};
};

