// CSM10_ARView.h : interface of the CSM10_ARView class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_DEMOHOLEVIEW_H__92CB472D_CAFD_11D1_A6AC_00C026A019B7__INCLUDED_)
#define AFX_DEMOHOLEVIEW_H__92CB472D_CAFD_11D1_A6AC_00C026A019B7__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

class CSM10_ARView : public CView
{
protected: // create from serialization only
	CSM10_ARView();
	DECLARE_DYNCREATE(CSM10_ARView)

// Attributes
public:
	CSM10_ARDoc* GetDocument();

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSM10_ARView)
	public:
	virtual void OnDraw(CDC* pDC);  // overridden to draw this view
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	protected:
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CSM10_ARView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	//{{AFX_MSG(CSM10_ARView)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

#ifndef _DEBUG  // debug version in CSM10_ARView.cpp
inline CSM10_ARDoc* CSM10_ARView::GetDocument()
   { return (CSM10_ARDoc*)m_pDocument; }
#endif

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DEMOHOLEVIEW_H__92CB472D_CAFD_11D1_A6AC_00C026A019B7__INCLUDED_)
