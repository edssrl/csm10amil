// DCustomer.cpp : implementation file
//

#include "stdafx.h"
#include "GraphDoc.h"
#include "GraphView.h"
#include "dyntempl.h"
#include "CSM10_AR.h"
#include "DCustomer.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CDCustomer dialog


CDCustomer::CDCustomer(CWnd* pParent /*=NULL*/)
	: CDialog(CDCustomer::IDD, pParent)
{
	//{{AFX_DATA_INIT(CDCustomer)
	m_name = _T("");
	//}}AFX_DATA_INIT
CProfile profile;
m_name = profile.getProfileString (_T("OPTIONS"),_T("Customer"),_T("Customer"));

}


void CDCustomer::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CDCustomer)
	DDX_Text(pDX, IDC_EDIT_NAME, m_name);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CDCustomer, CDialog)
	//{{AFX_MSG_MAP(CDCustomer)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CDCustomer message handlers

void CDCustomer::OnOK() 
{
	CDialog::OnOK();
	// TODO: Add extra validation here
CProfile profile;
profile.writeProfileString (_T("OPTIONS"),_T("Customer"),m_name);
	
}
