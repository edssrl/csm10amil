// DbInfoPlant.cpp : implementation file
//

#include "stdafx.h"
#include "GraphDoc.h"
#include "GraphView.h"
#include "dyntempl.h"

#include "CSM10_AR.h"
#include "DbInfoPlant.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CDbInfoPlant

IMPLEMENT_DYNAMIC(CDbInfoPlant, CDaoRecordset)

CDbInfoPlant::CDbInfoPlant(CDaoDatabase* pdb)
	: CDaoRecordset(pdb)
{
	//{{AFX_FIELD_INIT(CDbInfoPlant)
	m_NOME_ASSE_TMP = _T("");
	m_NOME_ASSE_DEF = _T("");
	m_STATO = FALSE;
	m_LARGHEZZA = 0.0;
	m_LEGA = _T("");
	m_SPESSORE = 0.0;
	m_CLIENTE = _T("");
	m_NOME_FILE_TMP = _T("");
	m_nFields = 8;
	//}}AFX_FIELD_INIT
	m_nDefaultType = dbOpenDynaset;
}


CString CDbInfoPlant::GetDefaultDBName()
{
	return _T("C:\\User\\Eds\\hole\\CSM10_AR\\InfoPlant.mdb");
}

CString CDbInfoPlant::GetDefaultSQL()
{
	return _T("[INFO_PLANT]");
}

void CDbInfoPlant::DoFieldExchange(CDaoFieldExchange* pFX)
{
	//{{AFX_FIELD_MAP(CDbInfoPlant)
	pFX->SetFieldType(CDaoFieldExchange::outputColumn);
	DFX_Text(pFX, _T("[NOME_ASSE_TMP]"), m_NOME_ASSE_TMP);
	DFX_Text(pFX, _T("[NOME_ASSE_DEF]"), m_NOME_ASSE_DEF);
	DFX_Bool(pFX, _T("[STATO]"), m_STATO);
	DFX_Double(pFX, _T("[LARGHEZZA]"), m_LARGHEZZA);
	DFX_Text(pFX, _T("[LEGA]"), m_LEGA);
	DFX_Double(pFX, _T("[SPESSORE]"), m_SPESSORE);
	DFX_Text(pFX, _T("[CLIENTE]"), m_CLIENTE);
	DFX_Text(pFX, _T("[NOME_FILE_TMP]"), m_NOME_FILE_TMP);
	//}}AFX_FIELD_MAP
}

/////////////////////////////////////////////////////////////////////////////
// CDbInfoPlant diagnostics

#ifdef _DEBUG
void CDbInfoPlant::AssertValid() const
{
	CDaoRecordset::AssertValid();
}

void CDbInfoPlant::Dump(CDumpContext& dc) const
{
	CDaoRecordset::Dump(dc);
}
#endif //_DEBUG
