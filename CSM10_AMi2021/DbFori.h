#if !defined(AFX_DBFORI_H__8BFB5791_692C_44BD_B87C_D179A6E3B7FA__INCLUDED_)
#define AFX_DBFORI_H__8BFB5791_692C_44BD_B87C_D179A6E3B7FA__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// DbFori.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CDbFori DAO recordset

class CDbFori : public CDaoRecordset
{
public:
	CDbFori(CDaoDatabase* pDatabase = NULL);
	DECLARE_DYNAMIC(CDbFori)

// Field/Param Data
	//{{AFX_FIELD(CDbFori, CDaoRecordset)
	double	m_BANDA;
	CString	m_CLASSE;
	CString	m_NOME_ASSE;
	double	m_NUM_FORI;
	double	m_POSIZIONE;
	//}}AFX_FIELD

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CDbFori)
	public:
	virtual CString GetDefaultDBName();		// Default database name
	virtual CString GetDefaultSQL();		// Default SQL for Recordset
	virtual void DoFieldExchange(CDaoFieldExchange* pFX);  // RFX support
	//}}AFX_VIRTUAL

// Implementation
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif
	// win10
BOOL isDbOpen(){return ((m_pDatabase != NULL) && m_pDatabase->IsOpen());};
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DBFORI_H__8BFB5791_692C_44BD_B87C_D179A6E3B7FA__INCLUDED_)
