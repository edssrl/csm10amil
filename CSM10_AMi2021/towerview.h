#if !defined(AFX_TOWERVIEW_H__92114CD1_CF80_11D1_A6AE_00C026A019B7__INCLUDED_)
#define AFX_TOWERVIEW_H__92114CD1_CF80_11D1_A6AE_00C026A019B7__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// TowerView.h : header file
//



/////////////////////////////////////////////////////////////////////////////
// CTowerView view

class CTowerView : public CView
{
protected:
	CTowerView();           // protected constructor used by dynamic creation
	DECLARE_DYNCREATE(CTowerView)
	ViewType vType; 

// Attributes
public:
CArray <ViewDensUnit,ViewDensUnit &> towerView;
Layout  layout;

double maxValY;

// Operations
public:
	ViewType getViewType(void) {return(vType);};
	CRect getDrawRect(void);
	CRect getMeterRect(void);
	void drawPosDiaframma(CDC *pDC, CRect rectB);

	void draw(void){CClientDC dc(this);OnDraw(&dc);};
// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CTowerView)
	public:
	virtual BOOL Create(LPCTSTR lpszClassName, LPCTSTR lpszWindowName, DWORD dwStyle, const RECT& rect, CWnd* pParentWnd, UINT nID, CCreateContext* pContext = NULL);
	protected:
	virtual void OnDraw(CDC* pDC);      // overridden to draw this view
	virtual BOOL OnNotify(WPARAM wParam, LPARAM lParam, LRESULT* pResult);
	virtual void OnUpdate(CView* pSender, LPARAM lHint, CObject* pHint);
	//}}AFX_VIRTUAL

// Implementation
protected:
	virtual ~CTowerView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

	// Generated message map functions
protected:
	//{{AFX_MSG(CTowerView)
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_TOWERVIEW_H__92114CD1_CF80_11D1_A6AE_00C026A019B7__INCLUDED_)
